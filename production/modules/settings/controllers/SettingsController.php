<?php

class SettingsController extends UController
{   
    public function init(){
        $this->breadcrumbs['Личный кабинет'] = array('links' => '');
        $this->breadcrumbs['Настройки'] = array('links' => '');
        $this->title = 'Настройки личного кабинета';
    }
    public function actions()
    {
        return array(
            'settings'=>array(
              'class'=>
                 'application.modules.settings.models.SettingsAction',
           ),
        );
    }
}