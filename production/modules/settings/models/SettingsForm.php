<?php

class SettingsForm extends CFormModel
{
    public  $getnum, $range,  $address_dep, $number_registry, $range_registry, $np, $address_dep2, $dropdown_ad2;
    public function rules()
    {
            return array(
                array('getnum, range, address_dep, number_registry, range_registry, np', 'safe'),
            );
    }
    public function attributeLabels()
    {
        return array(
            'getnum'            => 'Получать номера отправлений',
            'range'             => 'Диапазон',
            'address_dep'       => 'Адрес места сдачи/приема отправлений',
            'address_dep2'      => 'Адрес места сдачи/приема отправлений',
            'dropdown_ad2'      => '',
            'number_registry'   => 'Получать номера реестров',
            'range_registry'    => 'Диапазон',
            'np'    => 'Сумма наложенного платежа',
        );
    }
}