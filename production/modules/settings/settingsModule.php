<?php

class settingsModule extends CWebModule{
    public function init()
    {   
        $this->setImport(array(
            'settings.models.*',
            'settings.components.*',
            'application.modules.settings.models.SettingsAction',
        ));
    }
}