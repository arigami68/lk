<?php

class RequestAction extends CAction{
    public static $FLAG_RATING 	        = 'rating';
	public static $FLAG_SMS 	        = 'sms';
	public static $FLAG_WAREHOUSE       = 'warehouse';
	public static $FLAG_WAREHOUSEGROUP  = 'warehousegroup';
	public static $FLAG_DELIVERY        = 'delivery';

    public function run($flag){

        switch($flag){
            case self::$FLAG_RATING:
                $request = new RatingRequest();
            break;
			case self::$FLAG_SMS:
				$request = new SMSRequest();
			break;
	        case self::$FLAG_WAREHOUSE:
		        $request = new  WarehouseRequest();
		    break;
	        case self::$FLAG_WAREHOUSEGROUP:
		        $request = new  WarehouseGroupRequest();
		    break;
	        case self::$FLAG_DELIVERY:
		        $request = new  WarehouseDeliveryRequest();
		    break;
        }
        return print (isset($request)) ? $request->init() : false;
    }
}