<?php

class WarehouseDeliveryRequest extends RequestAbstract{
	public static $OPERATION_READ = 'list';

	public function init(){
		$json = $this->getJson();
		$json = $this->decodeJson($json);


		switch($json->operation){
			case self::$OPERATION_READ:
				$article = htmlspecialchars($json->article);

				$result = $this->read(array('article' => $article));
				break;
		}
		return json_encode($result);
	}
	public function read($array){

		$article = $array['article'];
		$array = array();

		if($db = DBnomenclature::model()->article($article)->user()->find()){
			$article = $db->article;
			$name    = $db->name;
			$unit    = $db->unit->name;
			$rate    = $db->rate;

			$array[$article]['article'] = $article;
			$array[$article]['name']    = $name;
			$array[$article]['unit']    = $unit;
			$array[$article]['rate']    = $rate;
		}

		return $array;
	}
}