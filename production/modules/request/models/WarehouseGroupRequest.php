<?php

class WarehouseGroupRequest extends RequestAbstract{
	public static $OPERATION_READ = 'read';

	public function init(){
		$json = $this->getJson();
		$json = $this->decodeJson($json);
		$result = array();


		switch($json->operation){
			case self::$OPERATION_READ:

				$id = htmlspecialchars($json->id);

				$result = $this->read(array('id' => $id));
				break;
		}
		return json_encode($result);
	}
	public function read($array){

		$id = $array['id'];
		$array = array();
		if($db = DBnomenclaturegroupname::model()->id($id)->user()->find()){

			$group = DBstoragelinknomenclaturenomenclaturegroupname::model()->nomenclaturegroup($db->id)->findAll();
			foreach($group as $k=>$v){
				$nomenclature = $v->nomenclature;
				$article = $nomenclature->article;
				$array[$article]['article'] = $nomenclature->article;
			}
		}

		return $array;
	}
}