<?php
/**
 * Class SMSRequest
 */

class SMSRequest extends RequestAbstract{
	public static $FLAG_SAVE = 2;
	public static $FLAG_LOAD = 3;

	public function init(){
		$result = null;
		$json = $this->getJson();

		$array = array(
			'id'		=> (int)$json->id,
			'od'        => (int)$json->od,
			'status'    => (int)$json->status,
			'number'   	=> (int)$json->sms_num,
			'check_time'=> $json->check_time,
			'text'  	=> htmlspecialchars($json->text_sms),
			'flag'		=> (int)$json->flag,
			'from'		=> (int)$json->from,
			'to'		=> (int)$json->to,
		);
		if($array['flag'] == self::$FLAG_SAVE){
			$result = $this->add($array);
		}elseif($array['flag'] == self::$FLAG_LOAD){
			$result = $this->load($array);
		}

		return $result;
	}
	public function load($array){
		$record = $this->getRecord($array);
		$flag = $record->isNewRecord ? 0 : 1;
		$array = array();

		if($flag){
			$db = DBSmsStatus::getConvertTime($record);
			$array = array(
				'text' 		=> $record->text,
				'time' 	=> array(
					'fromHH' => $db['fromHH'],
					'fromMM' => $db['fromMM'],
					'toHH' 	 => $db['toHH'],
					'toMM' 	 => $db['toMM'],
				),
			);
		}
		$array = $this->encode($array);

		return $array;
	}
	public function encode($array){
		$array = json_encode($array);
		return $array;
	}

	public function add($array){
		$record = $this->getRecord($array);
		if(!$array['od']) return false;
		$record->clientID 	= Yii::app()->user->auth->id;
		$record->od 		= $array['od'];
		$record->status 	= $array['status'];
		$record->isNewRecord ? $record->dateCreate = date('Y-m-d H:i:s') : null;
		$record->dateUpdate = date('Y-m-d H:i:s');
		$record->text 		= $array['text'];
		$record->number		= $array['number'];
		$record->timeUTC	= 'MSK';
		$record->flag1s		= 0;
		$record->work		= 1;
		$record->timeFrom   = $array['from'];
		$record->timeTo	    = $array['to'];

		return $record->save();
	}
	/**
	 * @param $id
	 * @return DBSmsStatus
	 */
	public function getRecord($array){

		$sms = ($db = DBSmsStatus::model()->od($array['od'])->status($array['status'])->number($array['number'])->find()) ? $db : new DBSmsStatus;
		return $sms;
	}
}
