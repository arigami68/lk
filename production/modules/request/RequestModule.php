<?php

class RequestModule extends CWebModule{
    public function init()
    {   
        $this->setImport(array(
            'request.models.*',
        ));
    }
}