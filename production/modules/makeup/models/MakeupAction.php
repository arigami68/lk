<?php

class MakeupAction extends CAction{
    
    public function run(){
        $db = new ORDS;
         if(Yii::app()->request->getParam('excel')){
            return $this->writeXls($db->QuSe($db::$SEARCH_RETURNS));
        }
        
        return $this->getController()->render('application.components.views.makeup.makeup', array('model' => $db));
    }
   public function readXls(){
        
        spl_autoload_unregister(array('YiiBase', 'autoload'));
        $phpExcelPath = Yii::getPathOfAlias('extensions.php-excel.Classes');
        include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');
        spl_autoload_register(array('YiiBase', 'autoload'));
 
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator("user")
            ->setLastModifiedBy("ИдеаЛоджик")
            ->setTitle("ИдеаЛоджик")
            ->setSubject("Отчет об отправлениях")
            ->setDescription("Отчет")
            ->setKeywords("repost")
            ->setCategory("report");
        return $objPHPExcel;
    }
    public function writeXls(CArrayDataProvider $model){
        $model->pagination->pageSize = $model->totalItemCount;
        
        $objPHPExcel = $this->readXls();
        
                foreach($model->rawData as $k=>$v){
                    $idk = $k;
                    
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $idk, $idk);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $idk, $v['megapolis']);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $idk, $v['dateCreate']);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $idk, $v['np']);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $idk, $v['address']);
                }
        $objPHPExcel = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");;
        header("Content-Disposition: attachment;filename=ideaLogic ".date('Y-m-d H:i').".xls"); 
        header("Content-Transfer-Encoding: binary ");
        return $objPHPExcel->save('php://output');
    }
}