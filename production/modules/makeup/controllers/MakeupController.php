<?php

class MakeupController extends UController
{
    public function init(){
        $this->breadcrumbs['Склад'] = array('links' => '');
        $this->title = 'Возвраты';
    }
    public function actions(){
        $this->breadcrumbs['Возвраты'] = array('links' => '');
        return array(
            'makeup'=>array(
              'class'=>
                 'application.modules.makeup.models.MakeupAction',
           ),
        );
    }
}