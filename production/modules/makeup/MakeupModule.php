<?php

# @info - Отчет:Отправления
class makeupModule extends CWebModule{
    public function init()
    {
        $this->setImport(array(
            'makeup.models.*',
            'makeup.components.*',
        ));
    }
}