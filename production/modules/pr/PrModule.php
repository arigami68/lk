<?php

class PrModule extends CWebModule{
    public function init()
    {
        $this->setImport(array(
            'pr.models.*',
            'pr.components.*',
            'extensions.captchaExtended.*',
        ));
    }
}