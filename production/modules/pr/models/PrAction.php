<?php
# @info: PrAction - Восстановление аккаунта
class PrAction extends CAction{
    public function run(){
        $model = new RecoveryPass();
        if (Yii::app()->request->isPostRequest){
            $model->attributes= $reg = $_POST['RecoveryPass'];

            if($model->validate()){


               if(!$client = CLIT::model()->contract($model->contract)->mail($model->email)->find()){
                   Yii::app()->user->setFlash('error', '<B>Ошибка. Вы указали не верные данные!</B>');
                   return $this->getController()->render('application.components.views.pr.pr', array('model' => new RecoveryPass()));
               }
               if(!$role = RecoveryPass::model()->contract($model->contract)->find()){
                    Yii::app()->user->setFlash('error', '<B>Пожалуйста свяжитесь с одним из наших менеджеров!</B>');
                    return $this->getController()->render('application.components.views.pr.pr', array('model' => new RecoveryPass()));
               }
               # Если клиент указал верно, то отсылаем секретный ключ для смены пароля
               $recp = $model->RecP($role);
               Emailserver::SendModRole('recove', array('email' => $reg['email'], 'contract' => $reg['contract'], 'key' => $recp));
               return $this->getController()->render('application.components.views.pr.prText', array('model' => array('text' => Yii::t("total", "cообщение о смене пароля"))));
            }
        }
        return $this->getController()->render('application.components.views.pr.pr', array('model' => $model));
    }
}