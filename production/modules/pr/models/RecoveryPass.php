<?php

class RecoveryPass extends CActiveRecord
{
    public static $tableName = 'clients_role';
    public $email;
    public $contract;
    public $verifyCode;
    
    public static function model($className= __CLASS__) {return parent::model($className);}
    public function tableName() {return self::$tableName;}

    public function rules(){
        return array(
            array('contract, email', 'required', 'message'=> Yii::t("total", "обязательное поле")),
            array('email', 'email', 'message'=>Yii::t("total", "не кооректный E-mail")),
            array('verifyCode', 'CaptchaExtendedValidator', 'allowEmpty'=>!CCaptcha::checkRequirements(), 'message'=> Yii::t("total", "не верно введен код")),
        );
    }
    public function attributeLabels(){
        return array(
            'contract'      => Yii::t("total", "номер договора"),
            'email'         => Yii::t("total", "e-mail"),
            'verifyCode'    => Yii::t("total", "проверочный код"),
        );
    }
    # Обновление ключа, который требуется для подтвердждение email'а
    # @input : object RecoveryPass
    # @output: boolean
    public function RecP(RecoveryPass $client){
        $client->secret = $secret = md5(microtime());
        
        return $client->update() ? $secret : FALSE;
    }
    public function contract($id)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition'=> 'contract= "'.$id.'"',
        ));
        return $this;
    }
}