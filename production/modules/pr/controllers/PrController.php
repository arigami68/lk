<?php
class PrController extends UController
{
    public function actions()
    {
        return array(
            # Восстановление пароля
            'pr'=>array(
              'class'=>
                 'application.modules.pr.models.PrAction',
           ),
            'captcha'=>array(
                'class'=>'extensions.captchaExtended.CaptchaExtendedAction',
                'mode'=> CaptchaExtendedAction::MODE_DEFAULT,
                'testLimit'=> 1,
            ),
        );
    }
}