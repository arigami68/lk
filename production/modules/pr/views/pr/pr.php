<H3>Восстановление пароля</H3>
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'horizontalForm',
    'type'=>'horizontal',
)); ?>
<div style='width: 36%;  margin-left: 180px'>
    <?php $form->widget('bootstrap.widgets.TbAlert', array(
        'closeText'=> false, // close link text - if set to false, no close link is displayed
        'alerts'=>array( // configurations per alert type
            'htmlOptions'=>array('class'=>'span1'),
            'error'=>array('block'=>true, 'fade'=>true, ), // success, info, warning, error or danger
        ),
    )); ?>
</div>
<?php echo $form->textFieldRow($model,'contract',array('rows'=>2, 'class'=>'span7')); ?>
<?php echo $form->textFieldRow($model,'email',array('rows'=>2, 'class'=>'span7')); ?>
<div style='margin-left: 180px; margin-bottom: 10px'>
    <?php $this->widget('CCaptcha');?>
</div>
<?php echo $form->textFieldRow($model,'verifyCode',array('rows'=>2, 'class'=>'span1')); ?>

<div style='width: 8%; float: left; margin-left: 180px'>
<?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type'       => 'primary',
        'label'      => 'Создать',
        'size'       => 'Normal'
    )
); ?>
</div>

<?php $this->endWidget(); ?>

