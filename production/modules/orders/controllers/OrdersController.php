<?php
class OrdersController extends UController
{   
    public function init(){
        $this->breadcrumbs['Отправления'] = array('links' => '');
        $this->breadcrumbs['Ваши заказы'] = array('links' => '');
        $this->title = 'Ваши реестры отправлений';
    }
    public function actions()
    {
        return array(
            'index'  => array('class'=>'application.modules.orders.models.OrdersAction'),
            'addDep' => array('class'=>'application.modules.orders.models.AddDepAction'),
            'print'  => array('class'=>'application.modules.orders.models.PrintAction'),
        );
    }
}