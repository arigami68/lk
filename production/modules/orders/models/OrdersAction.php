<?php

class OrdersAction extends CAction{
    public static $OPERATION_PRINT = 'print';
    public function run(){
        if(true){
            $ord = new stdClass;
            $ord->request = Yii::app()->request;
            $ord->data = new stdClass;
        }

        if($operation = Yii::app()->request->getParam('operation')){

            if($operation == OrdersAction::$OPERATION_PRINT){
                if($megapolis = Yii::app()->request->getParam('order')){
                    if($ORDS = ORDS::model()->user()->megapolis($megapolis)->findAll()){
                        return $this->writeXls($ORDS);
                    }
                }elseif(!$registry = Yii::app()->request->getParam('registry')){

                }else{
                    if($ORDS = ORDS::model()->user()->registry($registry)->findAll()){
                        return $this->writeXls($ORDS);
                    }
                }
            }
        }
        $ord->db['name'] = Yii::app()->user->auth->test ? Yii::import('application.components.db.ORDT') : Yii::import('application.components.db.ORDS');
        $ord->db['ORDS'] = new $ord->db['name'];

        return $this->getController()->render('application.components.views.orders.orders', array('model' => $ord->db['ORDS']));
    }
    public function writeXls($data){
        $objPHPExcel =  MegapolisExcel::get()->write();
        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
        );
        $op_take = array_search($data[0]->opTake, FuncMegapolis::Handbook()->op_take);
        #$date_otpr = MegapolisTime::get()->timeToConvert(MegapolisTime::$TIME_YEAR, MegapolisTime::$TIME_DEFAULT_FULL, new DateTime(date('Y-m-d H:i', $data[0]->dateSend))); #Дата сдачи/приема отправлений:

        $date_otpr = $data[0]->dateSurrender;
        $name_otpr = Yii::app()->user->auth->name;
        $add_otpr  = $data[0]->addressdep; #Адрес места сдачи/приема отправлений:
        $cap_otpr  = $data[0]->capacity ? $data[0]->capacity.' ' : 'Не указан';
        $take_otpr = $op_take;
        $contract = Yii::app()->user->auth->contract;
        $registry = $data[0]->registry;
        $header = array(
            '№ п/п'                                                 => array('title' => array(0 => '14'),  'coord' => array('x' => array('A14' => 'A16'), 'y' => array('A14' => 'A14'))),
            'Номер заказа'                                          => array('title' => array(1 => '14'),  'coord' => array('x' => array('B14' => 'B16'), 'y' => array('B14' => 'B14'))),
            'Код(получателя)'                                       => array('title' => array(2 => '14'),  'coord' => array('x' => array('C14' => 'C16'), 'y' => array('C14' => 'C14'))),
            'ФИО'                                                   => array('title' => array(3 => '14'),  'coord' => array('x' => array('D14' => 'D16'), 'y' => array('D14' => 'D14'))),
            'Получатель'                                            => array('title' => array(4 => '14'),  'coord' => array('x' => array('E14' => 'E14'), 'y' => array('E14' => 'O14'))),
            'страна'                                                => array('title' => array(4 => '15'),  'coord' => array('x' => array('E15' => 'E16'), 'y' => array('E15' => 'E15'))),
            'индекс'                                                => array('title' => array(5 => '15'),  'coord' => array('x' => array('F15' => 'F16'), 'y' => array('F15' => 'F15'))),
            'регион'                                                => array('title' => array(6 => '15'),  'coord' => array('x' => array('G15' => 'G16'), 'y' => array('G15' => 'G15'))),
            'район'                                                 => array('title' => array(7 => '15'),  'coord' => array('x' => array('H15' => 'H16'), 'y' => array('H15' => 'H15'))),
            'пункт назначения'                                      => array('title' => array(8 => '15'),  'coord' => array('x' => array('I15' => 'I16'), 'y' => array('I15' => 'I15'))),
            'Адрес'                                                 => array('title' => array(9 => '15'),  'coord' => array('x' => array('J15' => 'J15'), 'y' => array('J15' => 'N15'))),
            'Улица'                                                 => array('title' => array(9 => '16'),  'coord' => array('x' => array('J16' => 'J16'), 'y' => array('J16' => 'J16'))),
            'дом'                                                   => array('title' => array(10 =>'16'),  'coord' => array('x' => array('K16' => 'K16'), 'y' => array('K16' => 'K16'))),
            'корпус'                                                => array('title' => array(11 =>'16'),  'coord' => array('x' => array('L16' => 'L16'), 'y' => array('L16' => 'L16'))),
            'этаж'                                                  => array('title' => array(12 =>'16'),  'coord' => array('x' => array('M16' => 'M16'), 'y' => array('M16' => 'M16'))),
            'офис/квартира'                                         => array('title' => array(13 =>'16'),  'coord' => array('x' => array('N16' => 'N16'), 'y' => array('N16' => 'N16'))),
            'Телефон'                                               => array('title' => array(14 =>'15'),  'coord' => array('x' => array('O15' => 'O16'), 'y' => array('O16' => 'O16'))),
            'ХАРАКТЕРИСТИКИ ОТПРАВЛЕНИЯ'                            => array('title' => array(15 =>'14'),  'coord' => array('x' => array('P14' => 'AE14'), 'y' => array('P14' => 'P14'))),
            'Номер отправления'                                     => array('title' => array(15 =>'15'),  'coord' => array('x' => array('P15' => 'P16'), 'y' => array('P16' => 'P16'))),
            'Вид вложения'                                          => array('title' => array(16 =>'15'),  'coord' => array('x' => array('Q15' => 'Q16'), 'y' => array('Q16' => 'Q16'))),
            'Наименование ТМЦ  (для отправлений)'                   => array('title' => array(17 =>'15'),  'coord' => array('x' => array('R15' => 'R16'), 'y' => array('R16' => 'R16'))),
            'Стоимость ТМЦ, руб'                                    => array('title' => array(18 =>'15'),  'coord' => array('x' => array('S15' => 'S16'), 'y' => array('S16' => 'S16'))),
            'Вес отправления, определенный Отправителем, кг'        => array('title' => array(19 =>'15'),  'coord' => array('x' => array('T15' => 'T16'), 'y' => array('T16' => 'T16'))),
            'Наименование оператора доставки'                       => array('title' => array(20 =>'15'),  'coord' => array('x' => array('U15' => 'U16'), 'y' => array('T16' => 'T16'))),
            'Вид отправления'                                       => array('title' => array(21 =>'15'),  'coord' => array('x' => array('V15' => 'V16'), 'y' => array('V16' => 'V16'))),
            'Категория отправления'                                 => array('title' => array(22 =>'15'),  'coord' => array('x' => array('W15' => 'W16'), 'y' => array('V16' => 'V16'))),
            'Наличие описи вложения'                                => array('title' => array(23 =>'15'),  'coord' => array('x' => array('X15' => 'X16'), 'y' => array('V16' => 'V16'))),
            'Уведомление(наличие\вид)'                              => array('title' => array(24 =>'15'),  'coord' => array('x' => array('Y15' => 'Y16'), 'y' => array('Y16' => 'Y16'))),
            'Признак хрупкости'                                     => array('title' => array(25 =>'15'),  'coord' => array('x' => array('Z15' => 'Z16'), 'y' => array('Z16' => 'Z16'))),
            'Необходимость дополнительной упаковки'                 => array('title' => array(26 =>'15'),  'coord' => array('x' => array('AA15' => 'AA16'), 'y' => array('Z16' => 'Z16'))),
            'Вид дополнительной упаковки (короб, пакет и тп)'       => array('title' => array(27 =>'15'),  'coord' => array('x' => array('AB15' => 'AB16'), 'y' => array('Z16' => 'Z16'))),
            'Сумма объявленной ценности, руб'                       => array('title' => array(28 =>'15'),  'coord' => array('x' => array('AC15' => 'AC16'), 'y' => array('Z16' => 'Z16'))),
            'Сумма страховой ценности, руб'                         => array('title' => array(29 =>'15'),  'coord' => array('x' => array('AD15' => 'AD16'), 'y' => array('Z16' => 'Z16'))),
            'Сумма наложенного платежа, руб.'                       => array('title' => array(30 =>'15'),  'coord' => array('x' => array('AE15' => 'AE16'), 'y' => array('Z16' => 'Z16'))),
            'ПРИМЕЧАНИЕ'                                            => array('title' => array(31 =>'14'),  'coord' => array('x' => array('AF14' => 'AF16'), 'y' => array('Z16' => 'Z16'))),

            'Наименование отправителя:'              => array('title' => array(0 =>'8'),  'coord' => array('x' => array('A8' => 'A8'), 'y' => array('Z16' => 'Z16'))),
            'Дата сдачи/приема отправлений:'         => array('title' => array(0 =>'9'),  'coord' => array('x' => array('A9' => 'A9'), 'y' => array('Z16' => 'Z16'))),
            'Вид приёма:'                            => array('title' => array(0 =>'10'),  'coord' => array('x' => array('A10' => 'A10'), 'y' => array('Z16' => 'Z16'))),
            'Адрес места сдачи/приема отправлений:'  => array('title' => array(0 =>'11'),  'coord' => array('x' => array('A11' => 'A11'), 'y' => array('Z16' => 'Z16'))),
            'Общий объем отправлений:'               => array('title' => array(0 =>'12'),  'coord' => array('x' => array('A12' => 'A12'), 'y' => array('Z16' => 'Z16'))),
            'Номер договора'                         => array('title' => array(6 =>'8'),  'coord' => array('x' => array('G8' => 'G8'), 'y' => array('Z16' => 'Z16'))),
            '№ '                                     => array('title' => array(7 =>'8'),  'coord' => array('x' => array('G8' => 'G8'), 'y' => array('Z16' => 'Z16'))),
            #'Время сдачи/приема отправлений:'        => array('title' => array(6 =>'9'),  'coord' => array('x' => array('G9' => 'G9'), 'y' => array('Z16' => 'Z16'))),
            'РЕЕСТР'                                 => array('title' => array(9 =>'7'),  'coord' => array('x' => array('J6' => 'J6'), 'y' => array('Z16' => 'Z16'))),

            '№'                                      => array('title' => array(10 =>'7'),  'coord' => array('x' => array('K7' => 'K7'), 'y' => array('Z16' => 'Z16'))),

            $registry      => array('title' => array(11 =>7),  'coord' => array('x' => array('K6' => 'K6'), 'y' => array('Z16' => 'Z16'))),
            $name_otpr     => array('title' => array(1 => 8),  'coord' => array('x' => array('C10' => 'C10'), 'y' => array('Z16' => 'Z16'))),
            $date_otpr     => array('title' => array(1 => 9),  'coord' => array('x' => array('C11' => 'C11'), 'y' => array('Z16' => 'Z16'))),
            $op_take       => array('title' => array(1 => 10),  'coord' => array('x' => array('C12' => 'C12'), 'y' => array('Z16' => 'Z16'))),
            $add_otpr      => array('title' => array(1 => 11),  'coord' => array('x' => array('B12' => 'B12'), 'y' => array('Z12' => 'Z12'))),
            $cap_otpr      => array('title' => array(1 => 12),  'coord' => array('x' => array('C22' => 'C22'), 'y' => array('Z1' => 'Z1'))),
            $contract      => array('title' => array(8 => 8),  'coord' => array('x' => array('I8' => 'I8'), 'y' => array('Z16' => 'Z16'))),

            '1'                      => array('title' => array(0 => '17'),  'coord' => array('x' => array('A14' => 'A16'), 'y' => array('A14' => 'A14'))),
            '2'                      => array('title' => array(1 => '17'),  'coord' => array('x' => array('A14' => 'A16'), 'y' => array('A14' => 'A14'))),
            '3'                      => array('title' => array(2 => '17'),  'coord' => array('x' => array('A14' => 'A16'), 'y' => array('A14' => 'A14'))),
            '4'                      => array('title' => array(3 => '17'),  'coord' => array('x' => array('A14' => 'A16'), 'y' => array('A14' => 'A14'))),
            '5'                      => array('title' => array(4 => '17'),  'coord' => array('x' => array('A14' => 'A16'), 'y' => array('A14' => 'A14'))),
            '6'                      => array('title' => array(5 => '17'),  'coord' => array('x' => array('A14' => 'A16'), 'y' => array('A14' => 'A14'))),
            '7'                      => array('title' => array(6 => '17'),  'coord' => array('x' => array('A14' => 'A16'), 'y' => array('A14' => 'A14'))),
            '8'                      => array('title' => array(7 => '17'),  'coord' => array('x' => array('A14' => 'A16'), 'y' => array('A14' => 'A14'))),
            '9'                      => array('title' => array(8 => '17'),  'coord' => array('x' => array('A14' => 'A16'), 'y' => array('A14' => 'A14'))),
            '10'                     => array('title' => array(9 => '17'),  'coord' => array('x' => array('A14' => 'A16'), 'y' => array('A14' => 'A14'))),
            '11'                     => array('title' => array(10 => '17'),  'coord' => array('x' => array('A14' => 'A16'), 'y' => array('A14' => 'A14'))),
            '12'                     => array('title' => array(11 => '17'),  'coord' => array('x' => array('A14' => 'A16'), 'y' => array('A14' => 'A14'))),
            '13'                     => array('title' => array(12 => '17'),  'coord' => array('x' => array('A14' => 'A16'), 'y' => array('A14' => 'A14'))),
            '14'                     => array('title' => array(13 => '17'),  'coord' => array('x' => array('A14' => 'A16'), 'y' => array('A14' => 'A14'))),
            '15'                     => array('title' => array(14 => '17'),  'coord' => array('x' => array('A14' => 'A16'), 'y' => array('A14' => 'A14'))),
            '16'                     => array('title' => array(15 => '17'),  'coord' => array('x' => array('A14' => 'A16'), 'y' => array('A14' => 'A14'))),
            '17'                     => array('title' => array(16 => '17'),  'coord' => array('x' => array('A14' => 'A16'), 'y' => array('A14' => 'A14'))),
            '18'                     => array('title' => array(17 => '17'),  'coord' => array('x' => array('A14' => 'A16'), 'y' => array('A14' => 'A14'))),
            '19'                     => array('title' => array(18 => '17'),  'coord' => array('x' => array('A14' => 'A16'), 'y' => array('A14' => 'A14'))),
            '20'                     => array('title' => array(19 => '17'),  'coord' => array('x' => array('A14' => 'A16'), 'y' => array('A14' => 'A14'))),
            '21'                     => array('title' => array(20 => '17'),  'coord' => array('x' => array('A14' => 'A16'), 'y' => array('A14' => 'A14'))),
            '22'                     => array('title' => array(21 => '17'),  'coord' => array('x' => array('A14' => 'A16'), 'y' => array('A14' => 'A14'))),
            '23'                     => array('title' => array(22 => '17'),  'coord' => array('x' => array('A14' => 'A16'), 'y' => array('A14' => 'A14'))),
            '24'                     => array('title' => array(23 => '17'),  'coord' => array('x' => array('A14' => 'A16'), 'y' => array('A14' => 'A14'))),
            '25'                     => array('title' => array(24 => '17'),  'coord' => array('x' => array('A14' => 'A16'), 'y' => array('A14' => 'A14'))),
            '26'                     => array('title' => array(25 => '17'),  'coord' => array('x' => array('A14' => 'A16'), 'y' => array('A14' => 'A14'))),
            '27'                     => array('title' => array(26 => '17'),  'coord' => array('x' => array('A14' => 'A16'), 'y' => array('A14' => 'A14'))),
            '28'                     => array('title' => array(27 => '17'),  'coord' => array('x' => array('A14' => 'A16'), 'y' => array('A14' => 'A14'))),
            '29'                     => array('title' => array(28 => '17'),  'coord' => array('x' => array('A14' => 'A16'), 'y' => array('A14' => 'A14'))),
            '30'                     => array('title' => array(29 => '17'),  'coord' => array('x' => array('A14' => 'A16'), 'y' => array('A14' => 'A14'))),
            '31'                     => array('title' => array(30 => '17'),  'coord' => array('x' => array('A14' => 'A16'), 'y' => array('A14' => 'A14'))),
            '32'                     => array('title' => array(31 => '17'),  'coord' => array('x' => array('A14' => 'A16'), 'y' => array('A14' => 'A14'))),
        );
        foreach($header as $k=>$v){
            $key3 = key($v['title']);
            $value3 = $v['title'][$key3];
            $coordx1 = key($v['coord']['x']);
            $coordy1 = $v['coord']['x'][$coordx1];
            $coordx2 = key($v['coord']['y']);
            $coordy2 = $v['coord']['y'][$coordx2];
            
            $objPHPExcel->getActiveSheet()->mergeCells($coordx1.':'.$coordy1);
            $objPHPExcel->getActiveSheet()->mergeCells($coordx2.':'.$coordy2);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($key3, $value3, $k);
        }
        $idk=17;
        $a=0;


        foreach($data as $k=>$v){
            if(get_class($v) == 'ORDS'){
                        $idk+=1;
                        $a+=1;
                        $handbook = FuncMegapolis::Handbook();
                        $op_inventory = array_flip($handbook->op_inventory);
                        $op_category = array_flip($handbook->op_category);
                        $op_type = array_flip($handbook->op_type);
                        $id_agent = array_flip($handbook->agent_id);

                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $idk, $a);                    # № п/п
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $idk, $v->megapolis);          # Номер заказа
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $idk, $v->agent);                    # Код(получателя)
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $idk, $v->fio);               # ФИО
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $idk, 'Россия');              # страна
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $idk, $v->zip);               # индекс
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $idk, $v->region);            # регион
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $idk, $v->area);              # район
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $idk, $v->city);              # Пункт назначения
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $idk, $v->street);           # улица
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $idk, $v->house);            # дом 
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, $idk, isset($v->building) ? $v->building : '');          # корпус
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, $idk, $v->floor);            # этаж
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $idk, $v->office);           # офис/квартира
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, $idk, $v->phone);            # Телефон
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(15, $idk, $v->orderN);        # Номер отправления
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(16, $idk, isset($v->attachments) ? $v->attachments : '');        # Вид вложения
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(17, $idk, '');        # Наименование ТМЦ  (для отправлений)
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(18, $idk, '');        # Стоимость ТМЦ, руб
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(19, $idk, isset($v->weightEnd) ? $v->weightEnd : '');        # Вес отправления, определенный Отправителем, кг
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(20, $idk, $op_type[$v->opType]);        # Наименование оператора доставки
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(21, $idk, $id_agent[$v->idAgent]);        # Вид отправления
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(22, $idk, $op_category[$v->opCategory]);        # Категория отправления
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(23, $idk, $op_inventory[$v->opInventory]); # Наличие описи вложения
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(24, $idk, $v->opNotification); # Уведомление(наличие\вид)
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(25, $idk, $v->opFragile); # Признак хрупкости
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(26, $idk, $v->opPacking ? 1 : 0); # Необходимость дополнительной упаковки
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(27, $idk, $v->opPacking); # Вид дополнительной упаковки (короб, пакет и тп)
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(28, $idk, $v->costPublic); # Сумма объявленной ценности, руб
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(29, $idk, $v->costInsurance); # Сумма страховой ценности, руб
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(30, $idk, $v->costDelivery); # Сумма наложенного платежа, руб.
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(31, $idk, '');
            }
        }

        $header = array(

            'Реестр имеет приложение на'                => array('title' => array(1 => $idk+1),  'coord' => array('x' => array('B14' => 'B16'), 'y' => array('B14' => 'B14'))),
            'и содержит'                                => array('title' => array(1 => $idk+2),  'coord' => array('x' => array('B14' => 'B16'), 'y' => array('B14' => 'B14'))),
            '  _______________________________'         => array('title' => array(2 => $idk+1),  'coord' => array('x' => array('C26' => 'C26'), 'y' => array('F14' => 'F14'))),
            '  _______________________________ '        => array('title' => array(2 => $idk+2),  'coord' => array('x' => array('C26' => 'C26'), 'y' => array('F14' => 'F14'))),
            'листах'                                    => array('title' => array(3 => $idk+1),  'coord' => array('x' => array('C26' => 'C26'), 'y' => array('F14' => 'F14'))),
            'порядковых номеров записей'                => array('title' => array(3 => $idk+2),  'coord' => array('x' => array('C26' => 'C26'), 'y' => array('F14' => 'F14'))),
            'прописью'                                  => array('title' => array(2 => $idk+3),  'coord' => array('x' => array('C26' => 'C26'), 'y' => array('F14' => 'F14'))),

            'Отпуск отправлений произвел'               => array('title' => array(1 => $idk+5),  'coord' => array('x' => array('B14' => 'B16'), 'y' => array('B14' => 'B14'))),
            '  _______________________________  '       => array('title' => array(2 => $idk+5),  'coord' => array('x' => array('B14' => 'B16'), 'y' => array('B14' => 'B14'))),
            ' должность пидпись расшифровка подписи'     => array('title' => array(2 => $idk+6),  'coord' => array('x' => array('B14' => 'B16'), 'y' => array('B14' => 'B14'))),

            'Отправления принял'                        => array('title' => array(4 => $idk+5),  'coord' => array('x' => array('B14' => 'B16'), 'y' => array('B14' => 'B14'))),
            '  _______________________________   '       => array('title' => array(5 => $idk+5),  'coord' => array('x' => array('B14' => 'B16'), 'y' => array('B14' => 'B14'))),
            ' должность пидпись расшифровка подписи '     => array('title' => array(5 => $idk+6),  'coord' => array('x' => array('B14' => 'B16'), 'y' => array('B14' => 'B14'))),

            'Отправления получил'                       => array('title' => array(4 => $idk+8),  'coord' => array('x' => array('B14' => 'B16'), 'y' => array('B14' => 'B14'))),
            '  _______________________________    '       => array('title' => array(5 => $idk+8),  'coord' => array('x' => array('B14' => 'B16'), 'y' => array('B14' => 'B14'))),
            ' должность пидпись расшифровка подписи  '     => array('title' => array(5 => $idk+9),  'coord' => array('x' => array('B14' => 'B16'), 'y' => array('B14' => 'B14'))),
        );
        foreach($header as $k=>$v){
            $key3 = key($v['title']);
            $value3 = $v['title'][$key3];
            $coordx1 = key($v['coord']['x']);
            $coordy1 = $v['coord']['x'][$coordx1];
            $coordx2 = key($v['coord']['y']);
            $coordy2 = $v['coord']['y'][$coordx2];

           # $objPHPExcel->getActiveSheet()->mergeCells($coordx1.':'.$coordy1);
           # $objPHPExcel->getActiveSheet()->mergeCells($coordx2.':'.$coordy2);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($key3, $value3, $k);
        }

        $objPHPExcel->getDefaultStyle()->getFont()
            ->setName('Times New Roman')
            ->setSize(10);
        $objPHPExcel->getActiveSheet()->getStyle("A14:AF".$idk)->applyFromArray($styleArray);
        return MegapolisExcel::get()->output($objPHPExcel);
    }
}