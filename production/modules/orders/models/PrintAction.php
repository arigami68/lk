<?php

class PrintAction extends CAction{

    public function run(){

        if($register = Yii::app()->request->getQuery('registry')){
            $ORDS = ORDS::model()->user()->registry($register)->findAll();
        }
        if($order = Yii::app()->request->getQuery('order')){
            $criteria = new CDBcriteria;
            $criteria->compare('id', $order);
            
            $ORDS = ORDS::model()->user()->findAll($criteria);
            
        }
        if(!isset($ORDS)) return print 'По Вашему запросу ничего не найдено';
        if(!$ORDS) return print 'По Вашему запросу ничего не найдено';

        $user = Yii::app()->user->auth->id;
        $text='';

        foreach($ORDS as $k=>$v){
            $ord[] = '/production/file/'.DSS.$user.DSS.'barcode'.DSS.$v->megapolis.'.png';

            #$ord[] = Yii::app()->assetManager->publish((Yii::getPathOfAlias('file').DSS.$user.DSS.'barcode'.DSS.$v->megapolis.'.png'));
            $xls[] = Yii::getPathOfAlias('file').DSS.$user.DSS.'barcode'.DSS.$v->megapolis.'.png';

            $text.= '&';
            $text.= 'barcode['.$k.']'.'='.$v->megapolis;
        }
        if($register = Yii::app()->request->getQuery('xls')){
            return $this->writeXls($xls);
        }

        file_get_contents('http://api.hydra.megapolis.loc/barcode/barcode/barcode?client='.$user.$text);
        return $this->getController()->renderPartial('application.components.views.orders.print', array('model' => $ord));
    }
    public function readXls(){
        spl_autoload_unregister(array('YiiBase', 'autoload'));
        $phpExcelPath = Yii::getPathOfAlias('extensions.php-excel.Classes');
        include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');
        spl_autoload_register(array('YiiBase', 'autoload'));

        $objPHPExcel = new PHPExcel_Worksheet_Drawing();


        $objPHPExcel = new PHPExcel();


        $objPHPExcel->getProperties()->setCreator("user")
            ->setLastModifiedBy("ИдеаЛоджик")
            ->setTitle("ИдеаЛоджик")
            ->setSubject("Отчет об отправлениях")
            ->setDescription("Отчет")
            ->setKeywords("repost")
            ->setCategory("report");
        return $objPHPExcel;
    }
    public function writeXls($data){
        $objPHPExcel = $this->readXls();




        foreach($data as $k=>$link){

            $iDrowing = new PHPExcel_Worksheet_Drawing();
            $iDrowing->setPath($link);
            $iDrowing->setCoordinates('A'.($k ? $k*6+1 : 1));
            $iDrowing->setResizeProportional(false);
            $iDrowing->setWorksheet($objPHPExcel->getActiveSheet());
            #$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 1, $link);
        }

        $objPHPExcel = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');



        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");;
        header("Content-Disposition: attachment;filename=ideaLogic ".date('Y-m-d H:i').".xls");
        header("Content-Transfer-Encoding: binary ");
        return $objPHPExcel->save('php://output');
    }
}