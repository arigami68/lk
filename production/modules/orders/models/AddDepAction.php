<?php

class AddDepAction extends CAction{

    public function run($idr){
        $ord->db['name'] = Yii::import('application.components.db.ORDS');
        $model = new $ord->db['name'];

        ob_start();
        $this->getcontroller()->widget('bootstrap.widgets.TbGridView', array(
        'type'=>'striped bordered condensed',
        'dataProvider'=> $model->QuSe($model::$SEARCH_DEPARTURE, NULL, $idr),
        'template'=>"{items}\n{pager}",
        'ajaxVar' => '',
        'columns'=>array(
            array(
                'name'=>'id',
                'header' => Yii::t("total", '№ заявки'),
                'htmlOptions'=> array('style'=>'width: 30px;'),
            ),
            array(
                'name'=> 'agent',
                'header'=> Yii::t("total", 'идентификатор почтового отправления'),
            ),
            array(
                'name'  => 'megapolis',
                'header'=> Yii::t("total", 'номер груза'),
                'type'  => 'raw',
                'value' => function($data){
                        print '<a href=/order?id='.$data['registry'].'&idea='.$data['megapolis'].'&operation=read style="color: #0088cc;">'.$data['megapolis'].'</a>';
                    },
            ),
            array(
                'name'=> 'dateCreate',
                'header'=> Yii::t("total", 'дата приема отправления от отправителя'),
            ),

            array(
                'name'=> 'registry',
                'header'=> Yii::t("total", 'реестр'),
            ),
            array(
                'name'=> 'oc',
                'header'=> Yii::t("total", 'ОЦ, руб.'),
            ),
            array(
                'name'=> 'np',
                'header'=> Yii::t("total", 'НП, руб '),
            ),
            array(
                'name'=> 'address',
                'header'=> Yii::t("total", 'адрес доставки'),
                'type'  => 'raw',
                'value' => function($data){
                        $r = !empty($data['region']) ? $data['region'].', ' : '';
                        $a = !empty($data['area'])   ? $data['area'].', '   : '';
                        $c = $data['city'];
                        $address = $r.$a.$c;

                        print $address;
                }
            ),
            array(
                'name'  => 'fio',
                'header'=> Yii::t("total", 'ФИО получателя'),
            ),
            array(
                'name'  => 'steps',
                'header'=> Yii::t("total", 'статус доставки'),
            ),
            array(
                'name'=>'operation',
                'header'=> Yii::t("total", 'операции'),
                'value' => 'HelpViewWidget::ORDS_status_operation($data)',
                'type'  => 'raw',
                
                'value' => function($data){
                    $words = array(
                      'delete'          => Yii::t("total", 'удалить отправление'),
                      'print_order'     => Yii::t("total", 'печать отправления'),
                      'print_barcode'   => Yii::t("total", 'печать штрихкода по отправлению'),
                      'float_barcode'   => Yii::t("total", 'штрих код'),
					);
                        $array['d'] =  array(
                            'htmloption' =>  array('title' => $words['delete'], 'class' => 's2_delete icon-trash'),
                            'url' => '/order?id='.$data['idorders'].'&operation=delete',
                        );
                        $array['order'] =  array(
                            'htmloption' =>  array('title' => $words['print_order'], 'class' => 'icon-print'),
                            'url' => '/orders?operation=print&order='.$data['megapolis'],
                        );
                        $array['print'] =  array(
                            'htmloption' =>  array('title' => $words['print_barcode'], 'class' => 'printO icon-barcode', 'onclick'=>'
                           window.open("/orders/orders/print?order='.$data['idorders'].'",
                           '.$words['float_barcode'].',
                           "width=420,height=230,resizable=yes,scrollbars=yes,status=yes"
                        )'),
                        );
                        return
                            !empty($data['dateSend']) ?
                               CHtml::link('', '' ,$array['print']['htmloption']).'&nbsp&nbsp&nbsp'.
                               CHtml::link('',     $array['order']['url'] ,$array['order']['htmloption'])
                             : CHtml::link('', $array['d']['url'] ,$array['d']['htmloption']).'&nbsp&nbsp&nbsp';
                }
            ),
        ),
        'enablePagination' => true,
        'pager' => array(
                'class'             => 'CLinkPager',
                'cssFile'           => false,
                'header'            => false,
                'firstPageLabel'    => Yii::t("total", 'начало'),
                'prevPageLabel'     => Yii::t("total", 'назад'),
                'nextPageLabel'     => Yii::t("total", 'вперед'),
                'lastPageLabel'     => Yii::t("total", 'конец'),
         ),
        'htmlOptions'=>array('style'=>'width: 1400px '),
     ));

     $text = ob_get_contents();
     ob_end_clean();
     print $text;
    }
}