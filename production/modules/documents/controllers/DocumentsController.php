<?php

class documentsController extends UController
{
    public function init(){
        $this->breadcrumbs['Взаиморасчеты'] = array('links' => '');
        $this->title = Yii::t("total", 'закрывающие документы');
    }
    public function actions()
    {
        $this->breadcrumbs['Закрывающие документы'] = array('links' => '');
        return array(
            'documents'=>array(
              'class'=>
                 'application.modules.documents.models.DocumentsAction',
           ),
        );
    }
}