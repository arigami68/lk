<?php

class DocumentsAction extends CAction{
    public $dir                 = 'documents';
    public static $DATA_FILE    = "file";
    public static $DATA_DIR     = "dir";
    public function run(){

        if(Yii::app()->request->isPostRequest){
            $dir = $this->dir;
            $user = Yii::app()->user->auth->id;
            $json = json_decode(Yii::app()->request->getParam('json'));
            $text = ($result =  $this->scan($json, $dir, $user)) ? $result : '';
            return print $text;
        }
        return $this->getController()->render('application.components.views.documents.documents');
    }
    public function scan(stdClass $json, $dir, $user){
        $json = array(
            'name' => $this->filterPath($json->name),
            'path' => '',
        );
        $path['all']   = Yii::getPathOfAlias('file').DS.$user.DS.$dir.DS;
        $path['site']  =   DS.'production'.DS.'file'.DS.$user.DS.$dir.DS.$json['name'].DS;

        $dirs = scandir($path['all'].$json['name']);

        $files = array();

        foreach ($dirs as $module){
            if ($module[0] == ".") continue;
            $files[] = $module;
        }
        $text = '';
        foreach ($files as &$name){
            $m =  rawurlencode($name);

            $class  = (strripos($name, '.') === FALSE) ? self::$DATA_DIR : self::$DATA_FILE;
            $href   = $class == self::$DATA_FILE ? $path['site'].$m : '#';
            $data   = '<a href='.$href.'>'.$name.'</a>';

            $text.= '<div  class="'.$class.'">'.$data.'</div>';
        }

        return $text;
    }
    public function filterPath($string){
        $string = htmlspecialchars($string);
        $string = str_replace(".", "", $string);
        $string = str_replace("..", "", $string);
        return $string;
    }
}