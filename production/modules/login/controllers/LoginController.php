<?php

class loginController extends UController
{
    public function actions()
    {
        return array(
            'login'=>array(
              'class'=>
                 'application.modules.login.models.LoginAction',
           ),
            'logout'=>array(
              'class'=>
                 'application.modules.login.models.LogoutAction',
           ),
        );
    }
}