<H3>Войти в личный кабинет</H3>
<div style="padding-bottom: 10px;">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'label'=>'Регистрация',
        'type'=>'success',
        'size'=>'low',
        'url' => 'reg',
    ));?>
</div>
<div style="padding-bottom: 10px;">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'label'=>'Восстановление пароля',
        'type'=>'success', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'size'=>'low', // null, 'large', 'small' or 'mini'
        'url' => 'pr',
    ));?>
</div>
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'horizontalForm',
    'type'=>'horizontal',
)); ?>
<div style='width: 36%;  margin-left: 180px'>
    <?php $form->widget('bootstrap.widgets.TbAlert', array(
        'closeText'=> false, // close link text - if set to false, no close link is displayed
        'alerts'=>array( // configurations per alert type
            'htmlOptions'=>array('class'=>'span1'),
            'error'=>array('block'=>true, 'fade'=>true, ), // success, info, warning, error or danger
        ),
    )); ?>
</div>
<?php echo $form->textFieldRow($model,'user',array('rows'=>2, 'class'=>'span7', 'value' => '3143142')); ?>
<?php echo $form->passwordFieldRow($model,'pass',array('rows'=>2, 'class'=>'span7', 'value' => '123')); ?>

<div style="display: inline;">
    <div style='width: 8%; float: left; margin-left: 180px'>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type'       => 'primary',
            'label'      => 'Войти',
            'size'       => 'Normal'
        )
    ); ?>
    </div>
</div>
<?php $this->endWidget(); ?>