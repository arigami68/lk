<?php

class LoginAction extends CAction{
    
    public function run(){
       $model = new EnterForm();
       $db    = new NEWS();
       
        if(!Yii::app()->user->getIsGuest()){ # Если пользователь авторизован, но опять зашел на эту страницу
            return $this->getController()->render('application.components.views.login.ident', array('model' => $db));
        }
        if(Yii::app()->request->isPostRequest){
            $model->attributes= $enter = $_POST['EnterForm'];
            
            if($model->validate()){
                $identity=new UserIdentity($enter['user'], $enter['pass']);

                if($identity->authenticate()){
                    # Проверка на то, что у пользователя не стоит секретного ключа. Если стоит, сбивать на единицу.
                    Yii::app()->user->login($identity);
                    return $this->getController()->render('application.components.views.login.ident', array('model' => $db)); 
                }else{
                    Yii::app()->user->setFlash('error', $identity->errorCode);
                }
            }
        }
        
        return $this->getController()->render('application.components.views.login.index', array('model' => $model));
    }
}