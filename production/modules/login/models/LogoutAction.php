<?php
class LogoutAction extends CAction{
    public function run(){
       Yii::app()->user->logout();
       return $this->getController()->redirect(Yii::app()->homeUrl);
    }
}