<?php

class TrackingForm extends CFormModel
{
	public $text;
    public $file;
        
	public function rules()
	{
            return array(
                array('file', 'file', 'message'=> 'Загрузите пожалуйста файл'),
            );
	}
	public function attributeLabels()
	{
		return array(
			'fileField' => Yii::t("total", 'выбрать файл'),
			'file'      => Yii::t("total", 'загрузка файла'),
			'text'      => Yii::t("total", 'номер отправления'),
		);
    }
}