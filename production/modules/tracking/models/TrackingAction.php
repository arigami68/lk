<?php

class TrackingAction extends CAction{
  public static $CHECK_TEXT = 'Отслеживание отправлений';
  public function run(){
        $state['tracking']['load'] = 0; # Флаг
        
        $track = new stdClass();
        $track->_formname = 'TrackingForm';
        $track->form = new $track->_formname;
        $track->request = Yii::app()->request;
        
        $track->model = new $track->_formname;
        if($excel = Yii::app()->request->getParam('excel')){
            return $this->writeXls(json_decode($excel));
        }
        $track->model->file=CUploadedFile::getInstance($track->model,'fileField');
        if(isset(Yii::app()->session['agent'])){
           Yii::app()->clientScript->registerScript("Vars","
                    var agent = [".Yii::app()->session['agent']."];
            ", CClientScript::POS_HEAD);
           unset(Yii::app()->session['agent']);
        }

        if(!empty($track->model->file)){
            $xls = MegapolisExcel::get()->read($track->model->file);
            $track->array = '';
            if($xls[1]['A'] == self::$CHECK_TEXT){
                foreach($xls as $k=>$sheet){
                    if(!$sheet['A']) continue;
                    if($sheet['A'] == self::$CHECK_TEXT) continue;
                    $track->array = $track->array."'".$sheet['A']."',";
                }
                Yii::app()->session['agent'] = $track->array;
                $this->getController()->refresh();
            }else{
                Yii::app()->user->setFlash('error', 'Не соответсвует шаблону');
            }

        }
       
       $this->getController()->render('views.tracking.index', array( 'model' => $track->model));
    }
    public function writeXls(array $model){
        $objPHPExcel = MegapolisExcel::get()->write();
        $res = 0;
        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
        );
        if(true){
            $idkm=1;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($res, $idkm, 'Отправление');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$res, $idkm, 'Оператор доставки');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$res, $idkm, 'Адрес доставки');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$res, $idkm, 'Индекс');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$res, $idkm, 'Объявленная ценность');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$res, $idkm, 'Наложенный платеж');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$res, $idkm, 'Место операции');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$res, $idkm, 'Дата последней операции');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$res, $idkm, 'Статус');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$res, $idkm, 'Вручено');
        }
        foreach($model as $k=>$v){
            $res = 0;
            $o = $v[1];
            $idk = $k + 2;
            if(empty($o)) continue;
            
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($res, $idk, $o);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$res, $idk, $v[2]);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$res, $idk, $v[3]);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$res, $idk, $v[4]);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$res, $idk, $v[5]);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$res, $idk, $v[6]);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$res, $idk, $v[7]);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$res, $idk, $v[8]);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$res, $idk, $v[9]);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$res, $idk, $v[10]);
        }
        $objPHPExcel->getActiveSheet()->getStyle("A1:J".$idk)->applyFromArray($styleArray);

        return MegapolisExcel::get()->output($objPHPExcel);
    }
}
