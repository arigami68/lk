<?php
#set_time_limit(1);
class AutoAction extends CAction{
  public function run(){
      $manual = new stdClass();
      $manual->request = Yii::app()->request;
      $agent = $manual->request->getParam('agent');

	  $last = TRAC::model()->last()->code($agent)->find();

      if($last){
	    $array = $last->attributes;
		$array['agent'] = $agent;
		$text = $this->last($array);

      }else{
	      $text = "<td>".$agent."</td><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td>";

      }

      die($text);
    }
	public function last($array){
		$agent      = $array['agent'];

		$orders = ORDS::model()->agent($agent)->find();

		$address    = $array['address'];
		$zip        = $array['zip'];
		$np         = $orders->costDelivery;
		$oc         = $orders->costPublic;
		$operation  = $array['operation'];
		$delivery   = 'вручение';
		$date       = $array['date'];
		$status     = $array['status'];

		if($operation == $delivery){
			$text   = "<td>".$agent."</td><td>".$address."</td><td>".$zip."</td><td>".$oc."</td><td>".$np."</td><td>".$address."</td><td>".date('d.m.Y H:i', $date)."</td><td>".$status."</td><td>".date('d.m.Y H:i', $date)."</td>";
		}else{
			$text   = "<td>".$agent."</td><td>".$address."</td><td>".$zip."</td><td>".$oc."</td><td>".$np."</td><td>".$address."</td><td>".date('d.m.Y H:i', strtotime($date))."</td><td>".$status."</td><td></td>";
		}
		return $text;
	}

    public static function autoDataProvider(){
        $pagination = new CPagination();
        $pagination->pageVar = 'page';
        $pagination->pageSize = 10;
        $sort = new CSort();
        $sort->sortVar = 'sort';
        return new CArrayDataProvider( array(), array(
            'pagination'=> $pagination,
            'sort' => $sort,
        ));
    }

}