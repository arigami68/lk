<?php

class TrackingController extends UController
{
    public function init(){
        $this->breadcrumbs['Отправления'] = array('links' => '');
        $this->breadcrumbs['Отслеживание отправлений'] = array('links' => '');
        $this->title = Yii::t("total", 'отслеживание отправлений');
    }
    public function actions()
    {

        return array(
            'tracking'=>array(
              'class'=>
                 'application.modules.tracking.models.TrackingAction',
           ),
            'manual'=>array(
              'class'=>
                 'application.modules.tracking.models.ManualAction',
           ),
            'auto'=>array(
              'class'=>
                 'application.modules.tracking.models.AutoAction',
           ),    
        );
    }
}