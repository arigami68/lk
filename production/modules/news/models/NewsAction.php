<?php
class NewsAction extends CAction{
    
    public function run(){
       if(!$id = Yii::app()->request->getQuery('id')) $id = 1;
       $news = NEWS::model()->findByPk($id);
       return $this->getController()->render('application.components.views.news.news', array('model' => $news));
    }
}