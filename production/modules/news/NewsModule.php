<?php

class newsModule extends CWebModule{
    public function init()
    {

        $this->setImport(array(
            'news.models.*',
            'news.components.*',
        ));
    }
}