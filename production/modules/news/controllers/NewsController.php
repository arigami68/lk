<?php

class newsController extends UController
{
    public function init(){
        $this->breadcrumbs['Новости'] = array('links' => '');
    }
    public function actions()
    {
        return array(
            'news'=>array(
              'class'=>
                 'application.modules.news.models.NewsAction',
           ),
        );
    }
}