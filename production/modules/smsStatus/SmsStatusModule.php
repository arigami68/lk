<?php

class SmsStatusModule extends CWebModule{
    public function init()
    {
		Yii::setPathOfAlias('app', DATA);
        $this->setImport(array(
            'smsStatus.models.*',
			'app.components.*',
			'app.db.*',
        ));
    }
}