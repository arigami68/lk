<?php
class SmsStatusAction extends CAction{
	public static $fStatic 		= 'stat';
	public static $fSettings 	= 'settings';
	public static $fReport 		= 'report';

    public function run(){
		$sms = new SmsStatus;
		$sms->link = Yii::app()->request->getParam('f');

		$array = array(
			'od'  	 => (int)Yii::app()->request->getQuery('od'),
			'status' => (int)Yii::app()->request->getQuery('status'),
			'num'  	 => (int)Yii::app()->request->getQuery('sms_num'),
			'from'   => (int)Yii::app()->request->getQuery('from'),
			'to'     => (int)Yii::app()->request->getQuery('to'),
			'work'	 => Yii::app()->request->getQuery('work'),
			'delete' => Yii::app()->request->getQuery('delete'),
		);
		$sms->setDefault($array);

		switch($sms->link){
			case self::$fStatic:
				$this->fStatic($sms);

				break;
			case self::$fSettings:

				$this->fSettings($sms);
				break;
			case self::$fReport:
				$this->fReport($sms);
				break;
			default:
				$this->fSettings($sms);
		}

		return $this->getController()->render('application.components.views.smsStatus.smsStatus', array('array' => $sms->getArray()));
    }
	public function fStatic(SmsStatus $sms){
		$sms->db = new DBMSmsStatus;
		$sms->link = self::$fStatic;
		$default = $sms->getDefault();


		if($default['od'] && $default['status'] && $default['sms_num']){
			if($default['work'] !== null){
				$sms->switchSms($default['work']);
			}
			if($default['delete'] !== null){
				$sms->deleteSms();
			}

			$this->getController()->redirect( Yii::app()->createUrl($this->getId(),array('f'=> $sms->link)) );
		}
	}
	public function fReport(SmsStatus $sms){
		$sms->db = new DBSmsStatusReport;
		$sms->link = self::$fReport;
	}
	public function fSettings(SmsStatus $sms){
		$sms->db = new DBSmsStatus;

		$sms->link = self::$fSettings;
		$sms->getUnique(new DBtrack2comment, 'od');
		$sms->getUnique(new DBtrack2comment, 'status');
	}
}

class SmsStatus{
	public static $SMS_TEXT = 'Введите текст Вашей СМС';
	public $link;	// Загруженная страница
	public $db;	// База данных
	public $status;
	public $od;
	private $default = array(
		'od' 		=> 0,
		'status' 	=> 0,
		'sms_num' 	=> 1,
		'text'		=> '',
		'timeFrom'	=> 0,
		'timeTo'	=> 0,
		'HH'		=> array(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23),
		'MM'		=> array(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59),
		'delete'	=> null,
		'work'		=> null,
	);
	public function getDefault(){return $this->default;}
	public function setDefault($array){
		$this->default['od'] 		= $array['od'];
		$this->default['status'] 	= $array['status'];
		$this->default['sms_num'] 	= $array['num'];
		$this->default['timeFrom'] 	= $array['from'];
		$this->default['timeTo'] 	= $array['to'];
		$this->default['delete'] 	= $array['delete'] === null ? null : (int)$array['delete'];
		$this->default['work'] 		= $array['work']   === null ? null : (int)$array['work'];


		$db = DBSmsStatus::model()->clientID(Yii::app()->user->id)->od($this->default['od'])->status($this->default['status'])->number($this->default['sms_num'])->find();
		$this->default['text'] 		= $db ? $db->text : self::$SMS_TEXT;
		$this->default['db'] 	= $db;

		return true;
	}
	public function deleteSms(){
		$def = $this->default;
		if($db = DBSmsStatus::model()->clientID(Yii::app()->user->id)->od($def['od'])->status($def['status'])->number($def['sms_num'])->find()){
			$db->delete();
		}
	}
	public function switchSms($work){
		$def = $this->default;
		$flag = 0;

		if($db = DBSmsStatus::model()->clientID(Yii::app()->user->id)->od($def['od'])->status($def['status'])->number($def['sms_num'])->find()){
			$db->work = $work;
			$db->dateUpdate = date('Y-m-d H:i:s');
			$flag = $db->save();
		}
		return $flag;
	}
	public function getArray(){
		$array = array(
			'db' 		=> $this->db 	  ? $this->db 		: null,
			'link' 		=> $this->link 	  ? $this->link 	: null,
			'od' 		=> $this->od 	  ? $this->od 		: null,
			'status'	=> $this->status  ? $this->status 	: null,
			'default'	=> $this->default,
		);
		return $array;
	}
	public function getUnique($db, $value){
		$criteria = new CDbCriteria();

		$criteria->group = $value;
		$db = $db->model()->findAll($criteria);

		$array = array();
		foreach($db as $k=>$v){
			$name = 'value'.$value;

			$id = $v->$name->id;
			$val = $v->$name->$value;
			$array["$id"] = $val;
		}

		if($array){$array =  array('') + $array;}

		$this->$value = $array;
	}
}