<?php

class DBMSmsStatus extends DBSmsStatus{
	public function view(){

		$criteria 	= new CDbCriteria();
		$pagination = new CPagination();
		$sort	 	= new CSort();

		$criteria->scopes = array('clientID');
		$pagination->pageVar = 'page';
		$pagination->pageSize = 10;
		$sort->sortVar = 'sort';

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
			'pagination'=> $pagination,
			'sort' => $sort,
		));
	}
}