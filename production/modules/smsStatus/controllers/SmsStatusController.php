<?php

class SmsStatusController extends UController
{
	public function init(){
		$this->breadcrumbs['Личный кабинет'] = array('links' => '');
		$this->breadcrumbs['Настройки'] = array('links' => '');
		$this->title = 'SMS рассылки';
	}
	public function actions()
	{

		return array(
			'sms'=>array(
				'class'=>
					'application.modules.smsStatus.models.smsStatusAction',
			),
		);
	}
}