<?php

class NpController extends UController
{
    public function init(){
        $this->breadcrumbs['Взаиморасчеты'] = array('links' => '');
        $this->title = Yii::t("total", "наложенный платеж");
    }
    public function actions()
    {
        $this->breadcrumbs['Наложенный платеж'] = array('links' => '');
        return array(
            'np'=>array(
              'class'=>
                 'application.modules.np.models.NpAction',
           ),
            
        );
    }
}