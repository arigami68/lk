<?php

class NpForm extends CFormModel
{
    public $list, $date_s, $date_f;
    public $file;

	public function rules()
	{
        return array(
            array('file', 'file', 'message'=>'Загрузите пожалуйста файл'),
            array('date_s, date_f, list ', 'safe'),
		);
	}
    public function attributeLabels()
    {
        return array(
            'list'      => Yii::t("total", "Поиск по ШПИ/ № груза"),
            'date_s'    => Yii::t("total", "с"),
            'date_f'    => Yii::t("total", "по"),
            'fileField' => Yii::t("total", "загрузка"),
            'file'      => '',
        );
    }
}