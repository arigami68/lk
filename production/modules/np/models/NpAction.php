<?php

class NpAction extends CAction{
    public static $CHECK_TEXT = 'Проверка наложенного платежа';
    public function run(){
        $model = new CLNP;
        $form = new NpForm();
        $criteria = new CDbCriteria();
        $sort = new CSort();

        if($array = Yii::app()->request->getParam(get_class($form))){

            $s           = $array['date_s'];
            $f           = $array['date_f'];
            $agent       = $array['list'];

            if($file=CUploadedFile::getInstance($form,'file')){

                if($form->validate()){
                    $sheet = MegapolisExcel::get()->read($file);
                    $array = array();
                    if($sheet[1]['A'] == self::$CHECK_TEXT){
                        foreach($sheet as $value){
                            $column = $value['A'];
                            if(empty($column)) continue;
                            array_push($array, $column);
                        }
                        $sort->attributes = array('id' => array('id'));
                        $model->scenario = 'file';
                        $criteria->addInCondition('agent', $array);
                    }else{
                        Yii::app()->user->setFlash('error', 'Не соответсвует шаблону');
                    }
                }else{
                    # Ошибка валидации
                }
            }else{

                $setting = Yii::app()->request->getParam(get_class($form));

                $form->attributes = $setting;
                $redirect = array('np');

                if($agent){
                    $redirect['agent'] = $agent;
                }else{
                    if($s) $redirect['s']         = $s;
                    if($f) $redirect['f']         = $f;
                }

                return $this->getController()->redirect($redirect);
            }
        }

        return $this->getController()->render('application.components.views.np.np', array('model' => $model, 'form' => $form, 'criteria' => $criteria, 'sort' => $sort));
    }
}