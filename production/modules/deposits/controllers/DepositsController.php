<?php

class DepositsController extends UController
{
    public function init(){
        $this->breadcrumbs['Взаиморасчеты'] = array('links' => '');
        $this->title = 'Депозиты';
    }
    public function actions()
    {
        $this->breadcrumbs['Депозиты'] = array('links' => '');
        return array(
            'deposits'=>array(
              'class'=>
                 'application.modules.deposits.models.DepositsAction',
            ),
        );
    }
}