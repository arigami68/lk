<?php

class DepositsAction extends CAction{
    public function run(){
        $model = new DEPO;
        $form = new DepositsForm();

        if(Yii::app()->request->isPostRequest){
            $form->attributes= $_POST['DepositsForm'];

            if(!$DETA = DETA::model()->user(Yii::app()->user->auth->id)->find()){
                # Заполните реквизиты
            }else{
                if($form->validate()){
                    $num = new DETA_NUM;
                    $num->user = Yii::app()->user->auth->id;
                    $num->insert();
                    if(true){
                        $params = new STDClass;
                        $params->number = $num->id;
                        $params->date1 = date('Y-m-d H:i');
                        $params->date2 = date('j').' '.FuncMegapolis::getMonthName(date('n')).' '.date('Y');
                        $params->sum = $form->sum;
                        $params->contract = Yii::app()->user->auth->contract;
                        $params->sum_propis = FuncMegapolis::num2str($params->sum);
                        $params->details= $DETA->fullname.', ИНН '.$DETA->inn.', КПП '.$DETA->kpp.', '.$DETA->addresslegal;
                        $this->getController()->beginWidget('application.components.widget.PDFWidget',array(
                            'params'=> $params,
                        ));
                        $this->getController()->endWidget();
                    }
                }
            }
        }
        return $this->getController()->render('application.components.views.deposits.deposits', array('model' => $model, 'form' => $form));
    }
}