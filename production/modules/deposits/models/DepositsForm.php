<?php

class DepositsForm extends CFormModel
{
    public $sum;

    public function rules()
    {
        return array(
            array('sum', 'numerical', 'message'=> 'Введите число'),
            array('sum', 'required', 'message'=> 'Введите число'),
        );
    }
    public function attributeLabels()
    {
        return array(
            'sum' => 'Сумма',
        );
    }
}