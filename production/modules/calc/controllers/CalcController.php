<?php

class CalcController extends UController
{
    public function init(){

        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Credentials: true");

        $this->breadcrumbs['Отправления']          = array('links' => '');
        $this->breadcrumbs['Калькулятор доставки'] = array('links' => '');
        $this->title                               = 'Калькулятор';
    }
    public function actions()
    {
        return array(
            'calc'        =>array( 'class'=> 'application.modules.calc.models.CalcAction'),
            'find'        =>array( 'class'=> 'application.modules.calc.models.FindCalcAction'),
            'ideaFind'    =>array( 'class'=> 'application.modules.calc.models.ideaFindAction'), # Замена функции find/ 26.11.14
            'payment'     =>array( 'class'=> 'application.modules.calc.models.PaymentAction'),
            'ideaPayment' =>array( 'class'=> 'application.modules.calc.models.ideaPaymentAction'), # Замена функции payment/ 26.11.14 / Рабочая функция.
        );
    }
}