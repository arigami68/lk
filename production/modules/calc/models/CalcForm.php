<?php

class CalcForm extends CFormModel{ 
    public $weight;
    public $typeahead2;
    public $oc;
    public $np;
    
    public function attributeLabels()
    {
        return array(
            'weight'     => '',
            'typeahead2' => '',
            'oc'         => '',
            'np'         => '',
        );
    }
}