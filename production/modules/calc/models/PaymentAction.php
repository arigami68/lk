<?php

class PaymentAction extends CAction{
    public static $FORMAT_MULTISHIP = "multiship";
    public static $FORMAT_HTML      = "html";
    public static $FORMAT_IDEA      = "idea";
    public static $METHOD_GET       = "get";


    public function run($format = "html"){
        if(true){
            $pay = new stdClass();
            $pay->array = new stdClass();
            $pay->request = Yii::app()->request;

            $settings = new PaymentSettings();
            if(Yii::app()->request->getQuery('method') == self::$METHOD_GET){
                $settings->from     = $pay->request->getQuery('from');
                $settings->to       = $pay->request->getQuery('to');
                $settings->oc       = $pay->request->getQuery('oc');
                $settings->np       = $pay->request->getQuery('np');
                $settings->weight   = $pay->request->getQuery('weight');
                $settings->npR      = $pay->request->getQuery('npR');

                if(true){ # trash

                    $pay->array->weight = $pay->request->getQuery('w');
                    $pay->array->from = $pay->request->getQuery('from');
                    $pay->array->to = $pay->request->getQuery('to');
                    $pay->array->oc = $pay->request->getQuery('oc');
                    $pay->array->np = $pay->request->getQuery('np');
                    $pay->array->npR = $pay->request->getQuery('npR');
                    $pay->heap = new stdClass();
                }
            }else{
                $settings->from     = $pay->request->getParam('from');
                $settings->to       = $pay->request->getParam('to');
                $settings->oc       = $pay->request->getParam('oc');
                $settings->np       = $pay->request->getParam('np');
                $settings->weight   = $pay->request->getParam('weight');
                $settings->npR      = $pay->request->getParam('npR');

                if(true){ # trash
                    $pay->array->weight = $pay->request->getParam('w');
                    $pay->array->from = $pay->request->getParam('from');
                    $pay->array->to = $pay->request->getParam('to');
                    $pay->array->oc = $pay->request->getParam('oc');
                    $pay->array->np = $pay->request->getParam('np');
                    $pay->array->npR = $pay->request->getParam('npR');
                    $pay->heap = new stdClass();
                }
            }
        }

        if(!$pay->array->to){ $pay->array->to = $pay->request->getQuery('IK');}
        if(!$pay->array->to) return false;

        if(true){
            $criteria = new CDbCriteria;
            $criteria->compare('idkladr', $pay->array->to);
        }
        $pay->db = CATO::model()->findAll($criteria);
        if(!$pay->db) return NULL;

        if($format == self::$FORMAT_MULTISHIP){
            unset($pay->heap->text);
            foreach($pay->db as $k=>$v){
                $pay->heap->text.= $v->OD;
                $pay->heap->text.=';';
                $pay->heap->text.= $v->TP;
                $pay->heap->text.=';';
                $pay->heap->text.= $v->tarif;
                $pay->heap->text.=';';
                $pay->heap->text.= $v->term;
                $pay->heap->text.=';';
                $pay->heap->text.= $v->weight;
                $pay->heap->text.="\t";
            }
            $pay->heap->text = substr($pay->heap->text, 0, -2);
            die($pay->heap->text);
        }elseif($format == self::$FORMAT_HTML){

            $CLPR = CLPR::model()->findAll();
            $pay->payment = array();

            foreach($pay->db as $k=>$v){
                $payment = new PaymentComponent;
                $payment->od    = $v->CLOD->od;
                $payment->weight = $pay->array->weight ? $pay->array->weight : 0.0;
                $payment->tp    = 'Стандарт';
                $payment->term  = '';
                $payment->tz  = $v->tz;

                switch($v->od){
                    case CLOD::$OD_EMS:

                        foreach($CLPR as $k2=>$v2){
                            $min = $v2->min_w + 0;
                            $max = $v2->max_w + 0;
                            $w = $payment->weight;

                            if($min < $w AND $max >= $w){
                                
                                if($v2->zone_id == $payment->tz){
                                    $payment->tarif = $v->center ? $v2->price1 : $v2->price2;
                                    $payment->tarif+= ($pay->array->oc / 100) * 1;
                                    $payment->tarif+= ($pay->array->np / 100) * $pay->array->npR;
                                }
                            }
                        }
                        break;
                    case CLOD::$OD_DPD:
                        if(false){
                            if(!$v->ok) break;
                            if($payment->weight <= 32){
                                foreach($v->CLPR_DPD as $k3=>$v3){
                                        if($payment->weight > $v3->weight1){
                                            $obj=$v3;
                                            continue;
                                        }
                                    break;
                                }
                                if(!isset($obj)) break;

                                $price = $v->center ? $obj->price1 : $obj->price2;
                                $payment->od    = 'DPD';
                                $payment->tarif = $price;
                                $payment->tarif+= ($pay->array->oc / 100) * 4;
                                $payment->tarif+= ($pay->array->np / 100) * $pay->array->npR;
                            }
                        }
                        break;
                    case CLOD::$OD_PR: # БАНДЕРОЛЬ
                        if($payment->weight <= 2.5){
                            $payment->tarif = 0;
                            $pay->heap->pr = array(
                                '1' => array(126.26, 18.88), # Без ОЦ
                                '2' => array(169.92, 18.88), # С ОЦ
                            );
                            $payment->od    = $v->CLOD->od.' (бандероль 1-го класса)';

                            if(empty($pay->array->oc)){ # Без ОЦ
                                $default = $pay->heap->pr[1];
                                $payment->tarif+= $default[0] + ($payment->weight < 0.1 ? 0 : (ceil($payment->weight/0.1) * $default[1]));
                                $payment->tarif+= ($pay->array->oc / 100) * 4;
                            }else{ # с ОЦ
                                $default = $pay->heap->pr[2];
                                $payment->tarif+= $default[0] + ($payment->weight < 0.1 ? 0 : (ceil($payment->weight/0.1) * $default[1]));
                                $payment->tarif+= ($pay->array->oc / 100) * 4;
                            }
                            $payment->tarif+= ($pay->array->np / 100) * $pay->array->npR;
                            $pay->payment[] = clone $payment;
                        }
                    case CLOD::$OD_PR: # ПОСЫЛКА
                        if($payment->weight <= 50){
                            $pay->heap->pr = array(
                                '1' => array(149.90, 13.40),
                                '2' => array(152.00, 15.60),
                                '3' => array(158.10, 22.30),
                                '4' => array(192.60, 31.80),
                                '5' => array(215.00, 36.40),
                            );
                            $payment->od    = $v->CLOD->od.' (посылка)';
                            $default = $pay->heap->pr[$payment->tz];

                            $payment->tarif = $default[0] + $payment->weight/0.5 * $default[1];

                            $payment->tarif+= ($pay->array->oc / 100) * 4;
                            $payment->tarif+= ($pay->array->np / 100) * $pay->array->npR;
                        }
                        break;
                    case CLOD::$OD_IDEA: # ИдеаЛоджик

                        if($payment->weight <= 32){

                            $idea = $v->CLPR_IDEA;

                            $pay->heap->pr = array(
                                '1' => array($idea->price, $idea->next),
                            );

                            $payment->od    = 'ИдеаЛоджик';

                            $payment->tarif = $pay->heap->pr[1][0];
                            $payment->tarif+=  $pay->heap->pr[1][1] * ceil($payment->weight - 1); # Отнимаем 1кг так как за первый кг мы уже взяли
                            $payment->tarif+= ($pay->array->oc / 100) * 1;
                            $payment->tarif+= ($pay->array->np / 100) * $pay->array->npR;
                        }
                        break;
                }
                if(!$payment->tarif) continue;

                $pay->payment[] = clone $payment;

            }
        }elseif(self::$FORMAT_IDEA){
            
            $CLPR = CLPR::model()->findAll();
            $pay->payment = array();
            foreach($pay->db as $k=>$v){
                $payment = new PaymentComponent;
                $payment->od    = $v->CLOD->od;
                $payment->weight = $pay->array->weight ? $pay->array->weight : 0.0;
                $payment->tp    = 'Стандарт';
                $payment->term  = '';
                $payment->tz  = $v->tz;
                switch($v->od){
                    case CLOD::$OD_EMS:
                        foreach($CLPR as $k2=>$v2){
                            $min = $v2->min_w + 0;
                            $max = $v2->max_w + 0;
                            $w = $payment->weight;

                            if($min < $w AND $max >= $w){

                                if($v2->zone_id == $payment->tz){

                                    $payment->tarif = $v->center ? $v2->price1 : $v2->price2;
                                    $payment->tarif+= ($pay->array->oc / 100) * 1;
                                    $payment->tarif+= ($pay->array->np / 100) * $pay->array->npR;
                                }

                            }

                        }
                        break;
                    case CLOD::$OD_DPD:
                        if(false){
                            if(!$v->ok) break;
                            if($payment->weight <= 32){
                                foreach($v->CLPR_DPD as $k3=>$v3){
                                    if($payment->weight > $v3->weight1){
                                        $obj=$v3;
                                        continue;
                                    }
                                    break;
                                }
                                if(!isset($obj)) break;

                                $price = $v->center ? $obj->price1 : $obj->price2;
                                $payment->od    = 'DPD';
                                $payment->tarif = $price;
                                $payment->tarif+= ($pay->array->oc / 100) * 4;
                                $payment->tarif+= ($pay->array->np / 100) * $pay->array->npR;
                            }
                        }
                        break;
                    case CLOD::$OD_PR: # БАНДЕРОЛЬ
                        if($payment->weight <= 2.5){
                            $payment->tarif = 0;
                            $pay->heap->pr = array(
                                '1' => array(126.26, 18.88), # Без ОЦ
                                '2' => array(169.92, 18.88), # С ОЦ
                            );
                            $payment->od    = $v->CLOD->od.' (бандероли 1-го класса)';

                            if(empty($pay->array->oc)){ # Без ОЦ
                                $default = $pay->heap->pr[1];
                                $payment->tarif+= $default[0] + ($payment->weight < 0.1 ? 0 : (ceil($payment->weight/0.1) * $default[1]));
                                $payment->tarif+= ($pay->array->oc / 100) * 4;
                            }else{ # с ОЦ
                                $default = $pay->heap->pr[2];
                                $payment->tarif+= $default[0] + ($payment->weight < 0.1 ? 0 : (ceil($payment->weight/0.1) * $default[1]));
                                $payment->tarif+= ($pay->array->oc / 100) * 4;
                            }
                            $payment->tarif+= ($pay->array->np / 100) * $pay->array->npR;
                            $pay->payment[] = clone $payment;
                        }
                    case CLOD::$OD_PR: # ПОСЫЛКА
                        if($payment->weight <= 50){
                            $pay->heap->pr = array(
                                '1' => array(149.90, 13.40),
                                '2' => array(152.00, 15.60),
                                '3' => array(158.10, 22.30),
                                '4' => array(192.60, 31.80),
                                '5' => array(215.00, 36.40),
                            );
                            $payment->od    = $v->CLOD->od.' (посылка)';
                            $default = $pay->heap->pr[$payment->tz];
                            $payment->tarif = $default[0] + $payment->weight/0.5 * $default[1];
                            $payment->tarif+= ($pay->array->oc / 100) * 4;
                            $payment->tarif+= ($pay->array->np / 100) * $pay->array->npR;
                        }
                        break;
                    case CLOD::$OD_IDEA: # ИдеаЛоджик
                        if($payment->weight <= 32){
                            $idea = $v->CLPR_IDEA;
                            $pay->heap->pr = array(
                                '1' => array($idea->price, $idea->next),
                            );

                            $payment->od    = 'ИдеаЛоджик';

                            $payment->tarif = $pay->heap->pr[1][0];
                            $payment->tarif+=  $pay->heap->pr[1][1] * ceil($payment->weight - 1); # Отнимаем 1кг так как за первый кг мы уже взяли
                            $payment->tarif+= ($pay->array->oc / 100) * 1;
                            $payment->tarif+= ($pay->array->np / 100) * $pay->array->npR;
                        }
                        break;
                }
                if(!$payment->tarif) continue;

                $pay->payment[] = clone $payment;
            }
            return print json_encode($pay->payment);
        }

        if(!isset($pay->payment)) return false;

        $this->viewHeader();

        foreach($pay->payment as $k=>$v){
            $this->view($v);
        }
        $this->viewFooter();
    }

    public function processing(CATO $data){
        switch($data->od){
            case CLOD::$OD_EMS:
                print 1;
                break;
            case CLOD::$OD_PR:
                print 1;
                break;
            case CLOD::$OD_IDEA:
                print 1;
                break;
        }
    }
    public function view(PaymentComponent $payment){

                print '<tr>';
                   print '<td  class="sold">';
                        print '<div>';
                        print CHtml::radioButton('check');
                        print '</div>';
                    print '</td>';
                    print '<td  class="sold">';
                        print '<div>';
                        print PaymentComponent::$count++;
                        print '</div>';
                    print '</td>';
                    print '<td  class="sold">';
                        print '<div>';
                        print $payment->od;
                        print '</div>';
                    print '</td>';
                    print '<td  class="sold">';
                        print '<div>';
                        print $payment->tp;
                        print '</div>';
                    print '</td>';
                    print '<td  class="sold">';
                        print '<div>';
                        print $payment->tarif;
                        print '</div>';
                    print '</td>';
                    print '<td  class="sold">';
                        print '<div>';
                        print $payment->term;
                        print '</div>';
                    print '</td>';
                print '</tr>';
    }
    public function viewFooter(){
        print '</table>';
        print '<div style="float: right;margin-right: 83px">';
        $this->getController()->widget('bootstrap.widgets.TbButton', array(
                'buttonType' => 'button',
                'type'       => '',
                'label'      => 'Добавить в общий расчет',
                'size'       => 'Normal',
                'htmlOptions'=>array('id'=> 'get_total', 'class' => 'btn1'),
        ));
        print '</div>';
        print '<br></br>';
        print '<br></br>';
    }
    public function viewHeader(){
        print '<table id="Vladivostok">';
        print '<tr>';
        print '<td  class="sold">';
            print '<div>';
            print 'X';
            print '</div>';
        print '</td>';
        print '<td  class="sold">';
            print '<div>';
            print '№';
            print '</div>';
        print '</td>';
        print '<td  class="sold">';
            print '<div>';
            print 'ОД';
            print '</div>';
        print '</td>';
        print '<td  class="sold">';
            print '<div>';
            print 'ТП';
            print '</div>';
        print '</td>';
        print '<td  class="sold">';
            print '<div>';
            print 'Тариф';
            print '</div>';
        print '</td>';
        print '<td  class="sold">';
            print '<div>';
            print 'Срок';
            print '</div>';
        print '</td>';
        print '</tr>';
    }

    /* Для добавления новых операторов доставки в ТАБЛИЦУ
    public function ttttt(){
               $t = TEMP::model()->findAll();
        foreach($t as $k=>$v){

            $code   = (string)$v->t1;  # код
            $center = $v->t2;  # центр или нет

            if(true){ # EMS
                $emsTz = $v->t3;  # номер тарифной зоны ems
                $emsOk = $v->t4;  # возможность доставки через ems в этот населенный пункт

                if(true){
                    $CATO = new CATO;
                    $CATO->idkladr = $code;
                    $CATO->center = $center;
                    $CATO->od = 1;
                    $CATO->tz = $emsTz;
                    $CATO->ok = $emsOk;
                    $CATO->insert();
                    unset($CATO);
                }
            }

            if(true){ # Russian Post
                $rpTz = $v->t5;  #  номер тарифной зоны Почты
                $rpOk = $v->t6;  #  возможность доставки через Почту в этот населенный пункт
                if(true){
                    $CATO = new CATO;
                    $CATO->idkladr = $code;
                    $CATO->center = $center;
                    $CATO->od = 2;
                    $CATO->tz = $rpTz;
                    $CATO->ok = $rpOk;
                    $CATO->insert();
                    unset($CATO);
                }
            }

            if(true){ # DPD
                $dpdTz = $v->t7;  #Dpdcon_tar – номер тарифной зоны dpd consumers
                $dpdOk = $v->t9;  #Dpdcon_ok – простая возможность доставки в этот населенный пункт по dpd consumer (1 – да, «пусто» - нет).

                $dpdCity = $v->t8;  #Dpdcon_gor – признак город или область (1 – город, «пусто» - область). Тарифы для города и области считаются по-разному.
                $dpdNp = $v->t10; #Dpdcon_np - возможность доставки с наложенным платежом в этот населенный пункт по dpd consumer (1 – да, «пусто» - нет).
                if(true){
                    $CATO = new CATO;
                    $CATO->idkladr = $code;
                    $CATO->center = $center;
                    $CATO->od = 3;
                    $CATO->tz = $dpdTz;
                    $dpdOk ? $CATO->ok = 1 : $CATO->ok = 0;
                    $dpdCity ? $CATO->city = 1 : $CATO->city = 0;
                    $dpdNp ? $CATO->np = 1 : $CATO->np = 0;
                    $CATO->insert();
                    unset($CATO);
                }
            }
        }
    }
    */
}
class PaymentSettings extends CModel{
    public $weight; # вес
    public $from;   # откуда
    public $to;     # куда
    public $oc;     # Объявленная ценность
    public $np;     # Наложенный платеж
    public $npR;     # ???
    public $method = 'post';     # ???

    public function attributeNames(){}
}