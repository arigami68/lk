<?php

class CalcAction extends CAction{
    
    public function run(){
        $calc = new stdClass;
        $calc->model['name'] = Yii::import('application.modules.calc.models.CalcForm');
        $calc->model['form'] = new $calc->model['name'];

        if(Yii::app()->request->getParam('excel')){
            return $this->writeXls(json_decode($_POST['excel']));
        }
        return $this->getController()->render('application.components.views.calc.calc', array('model' => $calc->model['form']));
    }

    public function writeXls(array $model){
        $objPHPExcel = MegapolisExcel::get()->write();
        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
        );
        if(true){
            $idkm=1;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $idkm, '№ п/п');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $idkm, 'Город отправления');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $idkm, 'Город доставки');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $idkm, 'ОД');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $idkm, 'Услуга');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $idkm, 'Вес, кг');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $idkm, 'Тариф в руб, с НДС');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $idkm, 'Срок доставки, рабочие дни');
        }
        $idk = 0;
        foreach($model as $k=>$v){
            if($v === NULL) continue;
            $cityIN = $v[1];
            $cityTO = $v[2];
            $tarif  = $v[3];
            $od     = $v[4];
            $weight = $v[5];
            $price  = $v[6];
            $days   = $v[7];
            $idk = $k + 1;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $idk, $idk- $idkm);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $idk, $cityIN);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $idk, $cityTO);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $idk, $tarif);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $idk, $od);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $idk, $weight);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $idk, $price);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $idk, $days);
        }
        $objPHPExcel->getActiveSheet()->getStyle("A1:H".$idk)->applyFromArray($styleArray);
        return MegapolisExcel::get()->output($objPHPExcel);
    }
}