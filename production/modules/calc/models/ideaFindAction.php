<?php
# name='Потапов Геннадий'
# description='Модуль. Поиск и вывод городов';
class ideaFindAction extends CAction{

    public static $test = 0;
    public function run(){

        if(true){
            $model = new ideaFindSettings;

            if(Yii::app()->request->getQuery('method') == ideaFindSettings::$METHOD_GET){
                $model->item    = Yii::app()->request->getQuery('item')   ? Yii::app()->request->getQuery('item')   : '';
                $model->format  = Yii::app()->request->getQuery('format') ? Yii::app()->request->getQuery('format') : 'csv';
                $model->limit   = Yii::app()->request->getQuery('limit')  ? Yii::app()->request->getQuery('limit')  : 5;
            }else{
                $model->item    = Yii::app()->request->getParam('item')   ? Yii::app()->request->getParam('item')   : '';
                $model->format  = Yii::app()->request->getParam('format') ? Yii::app()->request->getParam('format') : 'csv';
                $model->limit   = Yii::app()->request->getParam('limit')  ? Yii::app()->request->getParam('limit')  : 5;
            }
            $model->explodeItem();
        }
        if(true){ # pre Исплючения
            if($model->format == ideaFindSettings::$FORMAT_INSALE){
                if(isset($model->item[0])){
                    if(isset($model->item[1])){
                        if($model->item[0] == 'москва' AND $model->item[1] == 'москва'){
                            unset($model->item[1]);
                        }elseif($model->item[0] == 'санкт-петербург' AND $model->item[1] == 'санкт-петербург'){
                            unset($model->item[1]);
                        }elseif($model->item[0] == 'севастополь' AND $model->item[1] == 'севастополь'){
                            unset($model->item[1]);
                        }elseif($model->item[0] == 'байконур' AND $model->item[1] == 'байконур'){
                            unset($model->item[1]);
                            $model->item[0] = 'БАЙКОНУР Г';
                        }
                    }
                }

            }
        }
        $model->search = ($t = Yii::app()->request->getParam('search')) ? $t : FindModel::$SEARCH_FULL;
        $data = $model->search(new MegapolisCalc);

        //if($search == FindModel::$SEARCH_KLADR) die($input->zip);
        
        $output  = $model->format($data, $model->format);

        if(true){ # after Исплючения
            if($model->format == ideaFindSettings::$FORMAT_INSALE){
                Yii::log(serialize($model), 'info');

                if($output !== 'error' AND $output !== NULL){ # МНожество записей
                    $itog = CATO::model()->kladr($output->idKladr)->od(CLOD::$OD_PR)->find();
                    $tz = $itog->tz;
                    $error = 0;
                    $weight = Yii::app()->request->getQuery('weight');
                    $np = Yii::app()->request->getQuery('prices');
                    $oc = Yii::app()->request->getQuery('prices');

                    if($weight <= 2.5){
                        $tarif = $this->PRBanderol($weight, $tz, $np, $oc);
                    }elseif($weight <= 50){
                        $tarif = $this->PR($weight, $tz, $np, $oc);
                    }else{
                        $tarif = 0;
                        $error = 'Вес не найден';
                    }

                    return print json_encode(array('price' => $tarif, 'error' => $error));
                }else{
                    $price = 0;
                    $error = 'Значение не найдено';

                    return print json_encode(array('price' => $price, 'error' => $error));
                }
            }

        }
        if(self::$test){
            print_r(json_decode($output));
            exit;
        }
        die($output);
    }
    public function PRBanderol($weight, $tz, $np, $oc){
        $array = array(
            '1' => array(126.26, 18.88), # Без ОЦ
            '2' => array(169.92, 18.88), # С ОЦ
        );

        $tz     = $tz;
        $weight = $weight;
        $oc     = $oc;
        $np     = $np;
        $npR    = 5;

        if(empty($oc)){ # Без ОЦ

            $default = $array[1];

            $tarif+= $default[0] + ($weight < 0.1 ? 0 : ((ceil($weight/0.1)-1) * $default[1]));
            $tarif+= ($oc / 100) * 4;
        }else{ # с ОЦ
            $default = $array[2];
            $tarif+= $default[0] + ($weight < 0.1 ? 0 : ((ceil($weight/0.1)-1) * $default[1]));
            $tarif+= ($oc / 100) * 4;
        }
        #$tarif+= ($np / 100) * $npR;

        return $tarif;
    }
    public function PR($weight, $tz, $np, $oc){

        $tarif = 0;
        $tz     = $tz;
        $weight = $weight;
        $oc     = $oc;
        $np     = $np;
        $npR    = 5;

        $array  = array(
            '1' => array(149.90, 13.40),
            '2' => array(152.00, 15.60),
            '3' => array(158.10, 22.30),
            '4' => array(192.60, 31.80),
            '5' => array(215.00, 36.40),
        );
        $default = $array[$tz];

        $tarif = $default[0] + (ceil($weight/0.5)-1) * $default[1];

        $tarif+= ($oc / 100) * 4;
        #$tarif+= ($np / 100) * $npR;

        return $tarif;
    }
}
class ideaFindSettings{
    public $search;

    public $format                     = 'csv';     # Формат текста
    public $item                       = '';        # Данные которые пришли от USER
    public $limit                      = 5 ;         # Сколько записей выводить
    public $limitTable                 = 100;
    public $method                     = 'post';
    public static $FORMAT_CSV          = 'csv';
    public static $FORMAT_JSON         = 'json';
    public static $FORMAT_INSALE       = 'insale';
    public static $METHOD_GET          = 'get';
    public static $SEPARATION_COMMA    = ',';
    public static $SEARCH_FULL         = 'full';
    public static $SEARCH_REGION       = 'region';
    public static $SEARCH_AREA         = 'area';
    public static $SEARCH_CITY         = 'city';
    public static $SEARCH_KLADR        = 'kladr';
    public static $SEARCH_REGION_CITY  = 'region_city';
    public static $SEARCH_ROC          = 'roc'; # region OR city

    public function setLimit($limit){ return $limit>11 ? $limit = 10 : $limit;}
    public function explodeItem(){

        if(is_array($this->item)){
            $this->item;
        }else{
            $this->item = explode(self::$SEPARATION_COMMA, $this->item);
            foreach($this->item as &$value){
                $value = trim($value);
            }
        }

        return $this->item;
    }
    public function search(MegapolisCalc $find){

        switch($this->search){
            case self::$SEARCH_REGION:      $result = $this->region(); break;
            case self::$SEARCH_REGION_CITY: $result = $this->region(); break;
            case self::$SEARCH_AREA:        $result = $this->region(); break;
            case self::$SEARCH_CITY:        $result = $this->region(); break;
            case self::$SEARCH_KLADR:       $result = $this->region(); break;
            case self::$SEARCH_FULL:        $result = $find->next($this); break;
            case self::$SEARCH_ROC:         $result = $find->roc($this); break;
            default: $result = NULL;
        }
        return $result;
    }

    public function format($input, $format){

        if($this->format == self::$FORMAT_CSV){
            $output = '';
            foreach($input as $k=>$v){
                $output.= $this->toProcessCsv($v, $format);
            }
        }elseif($this->format == self::$FORMAT_JSON){


            $output = array();
            foreach($input as $k=>$v){
                if(empty($v)) conitnue;

                if($json = $this->toProcessJson($v, $format)){
                    array_push($output, $json);
                }
            }
            $output = json_encode($output);

        }elseif($this->format == self::$FORMAT_INSALE){
            if(count($input) == 1){
                $output = $input[0];

            }else{

                if(false){ # Выводить ошибку или первую запись
                    $output = 'error';
                    $output = json_encode($output);
                }else{
                    $output = $input[0];
                }
            }
        }

        return $output;
    }

    public function toProcessCsv(MEGE $find, $format){

        $result = '';
        if($format == self::$SEARCH_FULL){
            if(true){
                $city = explode(' ', $find->city);
                array_pop($city);
                $city = implode(' ', $city);
            }
            if(true){
                $result.= $find->idKladr.';';
                $result.= $city;
                $result.= empty($find->locality) ? '' : self::$SEPARATION_COMMA;
                $result.= $find->locality ? $find->locality.self::$SEPARATION_COMMA : self::$SEPARATION_COMMA;
                $result.= $find->area ? $find->area.self::$SEPARATION_COMMA : '';
                $result.= $find->region;
                $result.= "\t\n";
            }
        }elseif( $format== self::$SEARCH_REGION){
            $result.= $find->idKladr.';';
            $result.= $find->region;
            $result.= "\t\n";
        }elseif( $format== self::$SEARCH_AREA){
            $result.= $find->idKladr.';';
            $result.= $find->area;
            $result.= "\t\n";
        }elseif( $format== self::$SEARCH_CITY){

            $result.= $find->idKladr.';';
            if(!empty($find->locality)){
                $result.= $find->locality.', ';
            }
            if(!empty($find->city)){
                $result.= ' '.$find->city;
            }
            $result.= "\t\n";
        }
        return $result = mb_convert_case(substr($result, 0, -1), MB_CASE_LOWER, "UTF-8");
    }
    public function toProcessJson(MEGE $find, $format){
        $result = '';
        if($format == self::$SEARCH_FULL){
            if(true){
                $city = explode(' ', $find->city);
                array_pop($city);
                $city = implode(' ', $city);
            }
            if($zip = $this->kladr($find->idKladr)){ # @OPTIM оставить только на исплючениях
                $result.= $zip->zip.',';
            }
            if(true){
                if(!empty($city) AND !empty($find->locality)){
                    $result.= $find->idKladr.',';

                    #$result.= empty($find->locality) ? '' : self::$SEPARATION_COMMA.' ';
                    $result.= $find->locality ? $find->locality.self::$SEPARATION_COMMA : self::$SEPARATION_COMMA;

                    $result.= $city.self::$SEPARATION_COMMA;
                    $result.= $find->area ? $find->area.self::$SEPARATION_COMMA : '';
                    $result.= $find->region;
                }else{
                    $result.= $find->idKladr.',';
                    $result.= empty($city) ? '' : $city.self::$SEPARATION_COMMA;
                    $result.= empty($find->locality) ? '' : $find->locality.self::$SEPARATION_COMMA;
                    $result.= $find->area ? $find->area.self::$SEPARATION_COMMA : '';
                    $result.= $find->region;
                }
            }
        }elseif( $format== self::$SEARCH_REGION){
            if($zip = $this->kladr($find->idKladr)){ # @OPTIM оставить только на исплючениях
                $result.= $zip->zip.';';
            }
            $result.= $find->region;
        }elseif( $format== self::$SEARCH_AREA){
            $result = $find->area;
        }elseif($format== self::$SEARCH_CITY){
            if($zip = $this->kladr($find->idKladr)){
                $result.= $zip->zip.';';
            }
            if(!empty($find->locality)){
                $result.= $find->locality;
            }
            if(!empty($find->city)){
                $result.= $find->city ? ''.$find->city : ',';

            }
        }elseif($format == self::$SEARCH_REGION_CITY){
            if(true){
                $city = explode(' ', $find->city);
                array_pop($city);
                $city = implode(' ', $city);
            }
            if(!empty($city) OR !empty($find->locality)){

                $text= $find->locality ? $find->locality : $city;
                $text = mb_strtolower($text, 'UTF-8');
                $item = mb_strtolower($this->item[1], 'UTF-8');

                if(strstr($text, $item) !== FALSE){
                    if($zip = $this->kladr($find->idKladr)){
                        $result.= $zip->zip.';';
                    }
                    $text = mb_strtoupper($text, 'UTF-8');
                    $result.=$text;
                }
            }
        }elseif($format == self::$FORMAT_INSALE){

            $result['idKladr']= $find->idKladr;
            $result['region'].= $find->region;
            $result['area'].= $find->area;
            $result['city'].= $find->city;
            $result['locality'].= $find->locality;
        }


        return $result;
    }

}

class MegapolisCalc extends CModel{
    public function attributeNames(){}

    public function city(){
        $region     = $this->item[0];
        $area       = $this->item[1];
        $city       = $this->item[2];
        $criteria   = new CDbCriteria();

        $criteria->condition = 'region ="'.$region.'" AND area= "'.$area.'" AND ((city LIKE "'.$city.'%") OR (locality LIKE "'.$city.'%"))';
        $criteria->select = 'idKladr, city, locality';
        $criteria->limit = $this->limit;
        $criteria->group = 'city';
        $result = MEGE::model()->findAll($criteria);

        return $result;
    }
    public function kladr($kladr){
        $result = NULL;
        $criteria = new CDbCriteria();
        $criteria->condition = 'kladr='.$kladr;
        $criteria->select = 'zip';
        $result = CLKL::model()->find($criteria);

        return $result;
    }
    public function region(){
        $result = NULL;
        $region = $this->item[0];
        $criteria = new CDbCriteria();
        $criteria->addSearchCondition('region', $region);
        $criteria->select = 'idKladr, region';
        $criteria->limit = $this->limit;
        $criteria->group = 'region';
        $result = MEGE::model()->findAll($criteria);
        return $result;
    }
    public function region_city(){

        $region     = $this->item[0];
        $locality = $city       = $this->item[1];
        if($city){
            $criteria   = new CDbCriteria();
            $criteria->addSearchCondition('region', $region, false);
            $criteria->addSearchCondition('area', '', 'AND');
            $criteria->addSearchCondition('city', $city.'%', false);

            if(!$result = MEGE::model()->findAll($criteria)){
                $criteria   = new CDbCriteria();
                $criteria->addSearchCondition('region', $region, false);
                $criteria->addSearchCondition('area', '', 'AND');
                $criteria->addSearchCondition('locality', $locality.'%', false);

            }
        }
        $criteria->limit = $this->limit;
        $result = MEGE::model()->findAll($criteria);

        return $result;
    }
    public function area(){
        $region     = $this->item[0];
        $area       = $this->item[1];

        $criteria   = new CDbCriteria();

        $criteria->condition = 'region ="'.$region.'"';
        $criteria->addSearchCondition('area', $area);
        $criteria->select = 'idKladr, area';
        $criteria->limit = $this->limit;
        $criteria->group = 'area';

        $result = MEGE::model()->findAll($criteria);

        return $result;
    }
    public function roc(ideaFindSettings $model){
        $item = $model->item;
        $criteria = new CDbCriteria();


        if(count($item) == 1){
            $data1 = $item[0];
            $criteria->condition = ' city LIKE "%'.$data1.'%" OR locality LIKE "%'.$data1.'%" OR region  LIKE "%'.$data1.'%"';

            $result = MEGE::model()->findAll($criteria);
        }else{

            $data2 = $item[0];
            $data1 = $item[1];
            $criteria->condition = '
            (city LIKE "'.$data1.'%" OR locality LIKE "'.$data1.'%" OR area  LIKE "'.$data1.'%") AND (region  LIKE "'.$data2.'%" OR area  LIKE "'.$data2.'%")
            OR
            (city LIKE "'.$data2.'%" OR locality LIKE "'.$data2.'%" OR area  LIKE "'.$data2.'%") AND (region  LIKE "'.$data1.'%" OR area  LIKE "'.$data1.'%")
            ';

            $result = MEGE::model()->findAll($criteria);
        }

        return isset($result) ? $result : FALSE;

    }
    public function next(ideaFindSettings $model){
        $item = $model->item;

        if(count($item) == 1){
            $criteria = new CDbCriteria();
            $criteria->addSearchCondition('city', $item[0].'%', false);
            $criteria->limit = $model->limitTable;
            $result1 = MEGE::model()->findAll($criteria);

            $criteria = new CDbCriteria();
            $criteria->addSearchCondition('locality', $item[0].'%', false);
            $criteria->limit = $model->limitTable;
            if(!count($result = MEGE::model()->findAll($criteria))){}
            $result = array_merge($result1, $result);

            return isset($result) ? $result : FALSE;
        }

        $step[1] = isset($this->item[0]) ? trim($this->item[0]) : '';
        $step[2] = isset($this->item[1]) ? trim($this->item[1]) : '';
        $step[3] = isset($this->item[2]) ? trim($this->item[2]) : '';
        $result = array();

        if($step[1]){
            $criteria = new CDbCriteria();
            $criteria->addSearchCondition('city', $step[1].'%', false);
            $criteria->limit = $this->limit;
            $result1 = MEGE::model()->findAll($criteria);

            if(true){

                $criteria = new CDbCriteria();
                $criteria->addSearchCondition('locality', $step[1].'%', false);
                $criteria->limit = $this->limit;
                if(!count($result = MEGE::model()->findAll($criteria))){}
                $result = array_merge($result1, $result);
            }

        }
        if($step[2]){
            $criteria->addSearchCondition('area', $step[2].'%', false,'AND');
            $criteria->addSearchCondition('region', $step[2].'%', false,'OR');
            $criteria->addSearchCondition('city', $step[1].'%', false, 'OR');
            $criteria->addSearchCondition('locality', $step[1].'%', false,'AND');

            $result = MEGE::model()->findAll($criteria);
        }

        if(true){
            $criteria = new CDbCriteria();
            $criteria->addSearchCondition('region', $step[1].'%', false);
            $criteria->addCondition('data0 IN (188571, 188572, 188573, 188574)');

            if($MEGE = MEGE::model()->find($crit  eria)){
                array_unshift($result, $MEGE);
            }
        }
        return isset($result) ? $result : FALSE;
        /*
        $criteria = new CDbCriteria();
        $criteria->addCondition('data0 IN (188571, 188572, 188573, 188574)');

        if($step[1]){
            $criteria->addSearchCondition('city', $step[1].'%', false);
            $criteria->addSearchCondition('locality', $step[1].'%', false, 'OR');
            $criteria->limit = $this->limit;
        }
        $text2 = !empty($step[2]) ?  $step[2] : $step[1];
        $criteria->addSearchCondition('region', $text2.'%', false);
        if($step[2]){
            $criteria->addSearchCondition('area', $step[2].'%', false,'OR');
            //$criteria->addSearchCondition('region', $step[2].'%', false,'OR');
        }
        print_r($criteria);

        $result = MEGE::model()->findAll($criteria);
        */

    }
}