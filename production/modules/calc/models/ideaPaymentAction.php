<?php

class ideaPaymentAction extends CAction{
	public function run(){

		$text = '';
		try{
			$settings = new ideaPaymentSettings(Yii::app()->request);
			$payment = new ideaPaymentOD($settings);
			$findAll = CATO::model()->kladr($settings->to)->findAll();

			foreach($findAll as $value){
				if($value->od == 6 || $value->od == 7) continue;

				$idea = new ideaPaymentComponent($settings, $value);
				if($idea->tarif = $payment->pop($idea)){
					$result[] =  $idea->getAttributes();
				}

				if(true){ # исключение
					if($value->od == CLOD::$OD_PR){
						if($settings->country == 112){
							$value->od = CLOD::$OD_PRAviaBelorus;
							if($idea->tarif = $payment->pop($idea)){
								$result[] =  $idea->getAttributes();
							}

							$value->od = CLOD::$OD_PRNazemBelorus;
							if($idea->tarif = $payment->pop($idea)){
								$result[] =  $idea->getAttributes();
							}


						}elseif($settings->country == 398){

							$value->od = CLOD::$OD_PRAviaKazah;

							if($idea->tarif = $payment->pop($idea)){
								$result[] =  $idea->getAttributes();
							}

							$value->od = CLOD::$OD_PRNazemKazah;
							if($idea->tarif = $payment->pop($idea)){
								$result[] =  $idea->getAttributes();
							}
						}elseif($settings->country == 643){

							$value->od = CLOD::$OD_PRBanderol;
							if($idea->tarif = $payment->pop($idea)){
								$result[] =  $idea->getAttributes();
							}


							$value->od = CLOD::$OD_POSILKA;
							if($idea->tarif = $payment->pop($idea)){

								$result[] =  $idea->getAttributes();
							}

							$value->od = CLOD::$OD_COURIER;
							if($idea->tarif = $payment->pop($idea)){
								$result[] =  $idea->getAttributes();
							}

						}
					}

					if($value->od == CLOD::$OD_IDEA){
						$value->od = CLOD::$OD_IDEAPvz;
						if($idea->tarif = $payment->pop($idea)){
							$result[] =  $idea->getAttributes();
						}
					}
				}
			}

		}catch (MegapolisException $exc){
			return print $text;
		}


		if(!isset($result)){

		}else{

			$view = new ideaPaymentView($settings, $this);

			foreach($result as $value){

				$view->view($value);

			}

			$text = $view->format($settings);

		}

		return print $text;
	}
}
class ideaPaymentView extends CAction{
	public $settings;
	public $action;

	public static $FORMAT_MULTISHIP = "multiship";
	public static $FORMAT_HTML      = "html";
	public static $FORMAT_IDEA      = "idea";
	public static $FORMAT_JSON      = "json";
	public static $text;

	public function __construct(ideaPaymentSettings $settings, $action){

		$this->settings = $settings;
		$this->action = $action;
	}
	public function attributeNames(){}

	public function view($value){

		$settings = $this->settings;
		switch($settings->format){
			case self::$FORMAT_HTML:
				self::$text.= $this->HTML($value);
				break;
			case self::$FORMAT_IDEA:
				self::$text[]= $value;
				break;
		}
	}
	public function HTML($value){

		$text= '
        <tr><td  class="sold"><div>
        '.CHtml::radioButton('check').'
        </div></td><td  class="sold"><div>
        '.ideaPaymentComponent::$count++.'
        </div></td><td  class="sold"><div>
        '.$value['od'].'
        </div></td><td  class="sold"><div>
        '.$value['tp'].'
        </div></td><td  class="sold"><div>
        '.$value['tarif'].'
        </div></td><td  class="sold"><div>
        '.$value['term'].'
        </div></td></tr>';
		return $text;
	}
	public function format(ideaPaymentSettings $settings){

		switch($settings->format){
			case self::$FORMAT_HTML:

				$text = $this->viewHTML(self::$text);

				break;
			case self::$FORMAT_IDEA:
			case self::$FORMAT_JSON:
				$text = $this->viewJSON(self::$text);
				break;
		}
		
		return $text;
	}
	public function viewJSON($text){
		return json_encode($text);
	}
	public function viewHTML($data){

		$text = $this->viewHeaderHTML();
		$text.=$data;
		$text.= $this->viewFooterHTML();

		return $text;
	}
	public function viewHeaderHTML(){
		return $text= '<table id="Vladivostok"><tbody><tr><td  class="sold"><div>X</div></td><td  class="sold"><div>№</div></td><td  class="sold"><div>ОД</div></td><td  class="sold"><div>ТП</div></td><td  class="sold"><div>Тариф</div></td><td  class="sold"><div>Срок</div></td></tr></tbody>';

	}
	public function viewFooterHTML(){
		$text= '</table>';
		$text.='<div style="float: right;margin-right: 83px">';
		ob_start();
		$this->action->getController()->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'button',
			'type'       => '',
			'label'      => 'Добавить в общий расчет',
			'size'       => 'Normal',
			'htmlOptions'=>array('id'=> 'get_total', 'class' => 'btn1'),
		));
		$out = ob_get_contents();
		ob_end_clean();
		$text.= $out;
		$text.='</div>';
		$text.='<br></br>';
		$text.='<br></br>';

		return $text;
	}
}
# Общие настройки
class ideaPaymentSettings{
	public $w;      # вес
	public $from;   # откуда
	public $to;     # куда
	public $oc;     # Объявленная ценность
	public $np;     # Наложенный платеж
	public $npR = 5;    # Процент за Наложенный платеж
	public $ik;
	public $country = 643;
	public $format = 'html';
	public $array = array('from','to','oc','np','w','npR','ik','format', 'country');
	public static $METHOD_GET  = 'GET';
	public static $METHOD_POST = 'POST';

	public function __construct(CHttpRequest $request){
		$get = ($request->getQuery('method') == self::$METHOD_GET) ? 'getQuery' : 'getParam';
		$array = $this->array;
		foreach($array as $value){
			$request->$get($value) ? $this->$value = $request->$get($value) : '';
		}
	}
}

/**
 * Компонент для Операторов Доставки
 * Class ideaPaymentComponent
 */
class ideaPaymentComponent extends CModel{
	public function attributeNames(){
		return array(
			'od',
			'tp',
			'tarif',
			'term',
			'weight',
			'country'
		);
	}

	public static $count = 1;
	public $od;
	public $tp;
	public $tarif;
	public $term; # Срок хранения
	public $weight = 0;
	public $tz;
	public $db;
	public $country;
	public $difficult;

	public function __construct(ideaPaymentSettings $setting, CATO $data){

			$this->od        = $data->CLOD->od;
			$this->tp        = 'Стандарт';
			$this->term      = '';
			$this->tz        = $data->tz;
			$this->weight    = $setting->w;
			$this->country   = $setting->country;
			$this->db        = $data;

			$this->change();

	}


	/**
	 * Подмена данных
	 */
	public function change(){
		switch($this->country){
			case 112:
			case 398:
				$this->tz = $this->country;
			    $this->db->tz = $this->country;
			break;
		}
	}
}
class ideaPaymentOD{
	public static $GO_AVIA  = 2;
	public static $GO_NAZEM = 3;
	public $settings;
	/**
	 * Применяем фильтры до поиска
	 * @param $db
	 *
	 * @return bool
	 */
	public function beforeFilter($db){
		$flag = true;

		if($this->settings->country != 643){
			if($db->od < 6000 AND $db->od > 7000){
				$flag = false;
			}
		}
		return $flag;
	}

	public function __construct(ideaPaymentSettings $settings){
		$this->settings = $settings;
	}
	public function pop(ideaPaymentComponent $idea){
		$tarif = NULL;
		$db = $idea->db;

		if(!$this->beforeFilter($db)) return false;

		switch($db->od){
			case CLOD::$OD_EMS:

				$tarif = $this->EMS($db, $idea);
				$idea->term = $db->term*2;
				break;
			case CLOD::$OD_PR:

				if($idea->weight <= 50){
					if($idea->country !== 643) return false;
					$biling = $this->PR($db, $idea);
					$tarif = $biling->getTarif();
					$idea->od = 'Почта России (посылка)';

					$idea->difficult = $biling->difficult;

					$idea->term = $db->term ? $db->term : '';
				}
				break;
			case CLOD::$OD_PRBanderol:
				if($idea->weight <= 2.5){
					$biling = $this->PRBanderol($db, $idea);

					$tarif = $biling->getTarif();

					$idea->term = $db->magic ? $db->magic : '';
					$idea->od = 'Почта России (бандероль 1-го класса)';
				}
				break;
			case CLOD::$OD_PRAviaBelorus:
				if($idea->weight <= 32){

					$tarif = $this->PRInternal($idea, self::$GO_AVIA);

					$idea->od = 'Почта России (Посылка международная Авиа Беларусь)';
				}
				break;
			case CLOD::$OD_PRNazemBelorus:
				if($idea->weight <= 32){
					$tarif = $this->PRInternal($idea, self::$GO_NAZEM);
					$idea->od = 'Посылка международная наземная Беларусь';
				}
				break;
			case CLOD::$OD_PRNazemKazah:

				if($idea->weight <= 32){
					$tarif = $this->PRInternal($idea, self::$GO_NAZEM);
					$idea->od = 'Посылка международная наземная Казахстан';
				}
				break;
			case CLOD::$OD_PRAviaKazah:
				if($idea->weight <= 32){
					$tarif = $this->PRInternal($idea, self::$GO_AVIA);
					$idea->od = 'Посылка международная Авиа Казахстан';
				}
				break;
			case CLOD::$OD_DPD:
				break;
			case CLOD::$OD_IDEA:
				if($idea->weight <= 32){
					$idea->term = $db->term ? $db->term : '';
					$tarif = $this->IDEA($db, $idea);
					$idea->od = 'ИдеаЛоджик';
				}
				break;
			case CLOD::$OD_IDEAPvz:
				if($idea->weight <= 32 && $db->CLPR_IDEA->pvz){
					$idea->term = $db->term ? $db->term : '';
					$tarif = $this->IDEAPvz($db, $idea);
					$idea->od = 'ИдеаЛоджик ПВЗ';
				}
				break;
			case CLOD::$OD_COURIER:

				$idea->term = $db->term ? $db->term : '';
				$tarif = $this->COURIER($db, $idea);
				$idea->od = 'Почта России (КУРЬЕР-ОНЛАЙН)';
				break;
			case CLOD::$OD_POSILKA:
				$idea->term = $db->term ? $db->term : '';
				$tarif = $this->POSILKA($db, $idea);
				$idea->od = 'Почта России (ПОСЫЛКА-ОНЛАЙН)';
				break;
		}
		return $tarif;
	}
	public function POSILKA(CATO $db, ideaPaymentComponent $idea){

		if($cato = CATO::model()->od(7)->kladr($idea['db']->idkladr)->find()){
			$idea->tz = $cato->tz;
		}

		$db = DBcalc_prices_posilka::model()->findAll();
		foreach($db as $value){
			$min  = $value->weight_min + 0;
			$max  = $value->weight_max + 0;
			$weight    = $this->settings->w;

			if($min < $weight AND $max >= $weight){

				if($value->tz == $idea->tz){
					$oc = (float)$this->settings->oc;
					$np = (float)$this->settings->np;
					$tarif = $value->tarif;

					if($idea->country !== 643){
						$tarif+= ( $oc / 100) * 1;
					}else{
						$tarif+= ( $oc / 100) * 1;
						$tarif+= ($np / 100.00) * 2;
					}
				}
			}

		}
		return isset($tarif) ? $tarif : 0;
	}
	public function COURIER(CATO $db, ideaPaymentComponent $idea){
		if($cato = CATO::model()->od(6)->kladr($idea['db']->idkladr)->find()){
			$idea->tz = $cato->tz;
		}
		$db = DBcalc_prices_courier::model()->findAll();
		foreach($db as $value){

			$min  = $value->weight_min + 0;
			$max  = $value->weight_max + 0;
			$weight    = $this->settings->w;

			if($min < $weight AND $max >= $weight){

				if($value->tz == $idea->tz){


					$oc = (float)$this->settings->oc;
					$np = (float)$this->settings->np;
					$tarif = $value->tarif;

					if($idea->country !== 643){
						$tarif+= ( $oc / 100) * 1;
					}else{
						$tarif+= ( $oc / 100) * 1;
						$tarif+= ($np / 100.00) * 2;
					}
				}
			}

		}

		return isset($tarif) ? $tarif : 0;
	}
	public function MAXIPOST(CATO $db, ideaPaymentComponent $idea){

		$db = DBcalc_prices_maxipost::model()->findAll();
		foreach($db as $value){

			$min  = $value->weight_min + 0;
			$max  = $value->weight_max + 0;
			$weight    = $this->settings->w;

			if($min < $weight AND $max >= $weight){

				if($value->tz == $idea->tz){


					$oc = (float)$this->settings->oc;
					$np = (float)$this->settings->np;
					$tarif = $value->tarif;

					if($idea->country !== 643){
						$tarif+= ( $oc / 100) * 1;
					}else{
						$tarif+= ( $oc / 100) * 1;
						$tarif+= ($np / 100.00) * 2;
					}
				}
			}

		}
		return $tarif ? $tarif : 0;
	}
	public function EMS(CATO $db, ideaPaymentComponent $idea){
		$zip = array();
		$tarif =    0;

		foreach($db->CLKL as $value){
			$zip[] = $value->zip;
		}

		$criteria = new CDbCriteria();
		$criteria->addInCondition('zip', $zip);
		$remoteAddr = DBcalcremoteaddresses::model()->find($criteria);
		$trydn = 110;
		if($idea->country != 643){
			$db->tz = $idea->country;
		}


		foreach($db->CLPR as $value){

			$min  = $value->min_w + 0;
			$max  = $value->max_w + 0;
			$weight    = $this->settings->w;

			if($min < $weight AND $max >= $weight){
				if($value->zone_id == $idea->tz){

					$center = $db->center;
					$oc = (float)$this->settings->oc;
					$np = $this->settings->np;
					$npR = $this->settings->npR;

					if($idea->country !== 643){
						$tarif = $value->price1;
						$tarif+= ( $oc / 100) * 1;
					}else{

						$tarif = $center ? $value->price1 : $value->price2;

						$tarif+= ( $oc / 100) * 1;
						$tarif+= ($np / 100.00) * $npR;

						if($remoteAddr) $tarif+= $trydn * (ceil($weight/1)- 1);
					}
				}
			}

		}
		return $tarif ? $tarif : 0;
	}
	public function DPD(){ # ДПД
		if(false){
			if(!$v->ok) break;
			if($payment->weight <= 32){
				foreach($v->CLPR_DPD as $k3=>$v3){
					if($payment->weight > $v3->weight1){
						$obj=$v3;
						continue;
					}
					break;
				}
				if(!isset($obj)) break;

				$price = $v->center ? $obj->price1 : $obj->price2;
				$payment->od    = 'DPD';
				$payment->tarif = $price;
				$payment->tarif+= ($pay->array->oc / 100) * 1;
				$payment->tarif+= ($pay->array->np / 100) * $pay->array->npR;
			}
		}
	}
	public function IDEA(CATO $db, ideaPaymentComponent $idea){ # ИдеаЛоджик

		$tz     = $idea->tz;
		$weight = $idea->weight;
		$oc     = $this->settings->oc;
		$np     = $this->settings->np;
		$npR = $this->settings->npR;
		$CLPR = $db->CLPR_IDEA;
		$array = array(
			'1' => array($CLPR->price, $CLPR->next),
		);

		$tarif = $array[1][0];

		$tarif+= $array[1][1] * (ceil($weight/1)- 1); # Отнимаем 1кг так как за первый кг мы уже взяли

		$tarif+= ($oc / 100) * 1;
		$tarif+= ($np / 100) * $npR;

		return $tarif;
	}

	public function IDEAPvz(CATO $db, ideaPaymentComponent $idea){ # ИдеаЛоджик Pvz(Пункт выдачи заказов)
		$tz     = $idea->tz;
		$weight = $idea->weight;
		$oc     = $this->settings->oc;
		$np     = $this->settings->np;
		$npR = $this->settings->npR;
		$CLPR = $db->CLPR_IDEA;
		$array = array(
			'1' => array($CLPR->pvzprice, $CLPR->pvznext),
		);

		$tarif = $array[1][0];

		$tarif+= $array[1][1] * (ceil($weight/1)- 1); # Отнимаем 1кг так как за первый кг мы уже взяли

		$tarif+= ($oc / 100) * 1;
		$tarif+= ($np / 100) * $npR;

		return $tarif;
	}
	public function PRInternal(ideaPaymentComponent $idea, $flag){
		$db = $idea->db;
		$array = array(
			112 => array(
				'avia' => array(575, 193.2),
				'nazem'=> array(575, 109)
			),
			398 => array(
				'avia' => array(559.5, 181),
				'nazem'=> array(559.5, 91)
			),
			'total' => array('tam' => 75, 'exc' => 50)
		);
		$tarif = 0;

		foreach($db->CLPR as $value){
			$min  = $value->min_w + 0;
			$max  = $value->max_w + 0;
			$weight    = $this->settings->w;

			if($min < $weight AND $max >= $weight){
				$oc = (float)$this->settings->oc;

				$npR = $this->settings->npR;

					if($flag == 2){ // Авиа
						if($idea->country == 112){
							$tarif = $array[112]['avia'][0] + $array[112]['avia'][1]*(ceil($weight/1));
						}elseif($idea->country == 398){
							$tarif = $array[398]['avia'][0] + $array[398]['avia'][1]*(ceil($weight/1));
						}
					}elseif($flag=3){ // Наземная
						if($idea->country == 112){
							$tarif = $array[112]['nazem'][0] + $array[112]['nazem'][1]*(ceil($weight/1));
						}elseif($idea->country == 398){
							$tarif = $array[398]['nazem'][0] + $array[398]['nazem'][1]*(ceil($weight/1));
						}
					}
			}

		}

		if($tarif) $tarif+= + $array['total']['tam'] + $array['total']['exc'];
		return $tarif;
		/*
		 *
- «Посылка международная Авиа Беларусь»
575 руб. весовой сбор. + за каждый  полный или неполный кг 193,2 руб.
 - «Посылка международная Авиа Казахстан» 559,5 руб. весовой сбор + за каждый полный или неполный кг 181 руб.
- «Посылка международная наземная Беларусь» 575 руб.  весовой сбор + за каждый полный или неполный кг 109 руб.
 - «Посылка международная наземная Казахстан» 559,5 руб. весовой сбор + за каждый полный или неполный кг 91 руб.
Тариф за таможенное оформление – 75 руб. с НДС – для всех ТПО.
Тариф за экспедиционное сопровождение - 50 руб. с НДС – для ТПО  с объявленной стоимостью.

		 */
	}
	public function PRBanderol(CATO $db, ideaPaymentComponent $idea){

		$array = array(
			'1' => array(126.26, 30.68), # Без ОЦ
			'2' => array(169.92, 30.68), # С ОЦ
		);
		$tz     = $idea->tz;
		$weight = $idea->weight;
		$oc     = (int)$this->settings->oc;
		$np     = $this->settings->np;
		$npR    = $this->settings->npR;

		$obj = array(
			'weight'    => $idea->weight,
			'npR'       => $this->settings->npR,
			'oc'        => $this->settings->oc,
			'np'        => $this->settings->np,
			'formula'   => array(
				'1' => array(126.26, 30.68), # Без ОЦ
				'2' => array(169.92, 30.68), # С ОЦ
			),
			'tz'        => empty($oc) ? 1 : 2,
			'step'      => 0.1,
		);
		$billing = new BillingPR($obj);
		$dif = new DifficultPR($db);

		if($dif->getFind()){
			if($status = $dif->difficultPR($dif->getFind())){
				$dif->exclusion(end($status));
			}

		}else{
			$pr = new DBcalc_difficult_pr;
			$pr->tz             = $obj['tz'];
			$pr->deliveryType   = BillingPR::$deliveryType_Earth;
			$pr->coef           = 0;
			$pr->base_tarif     = 0;

			$dif->setRoute($pr);
		}

		$billing->difficult = ($dif->getStatus() == DifficultPR::$STATUS_ROUTE) ? 1 : 0;

		foreach($dif->getRoute() as $route){

			$setting = array(
				'deliveryType'  => $route->deliveryType,
				'base_tarif'    => $route->base_tarif,
				'coef'          => $route->coef,
				'tz'            => $obj['tz'],
			);

			$billing->setTarif($billing->calc($setting));

		}

		if($billing->getTarif() != 0){
			$billing->setTarif($billing->endTarif());

			$billing->setTarif($billing->oc());
			$billing->setTarif($billing->np());
		}
		return $billing;

		/*
			$tarif = 0;

			if(empty($oc)){ # Без ОЦ
				$default = $array[1];
				$tarif+= $default[0] + ($weight < 0.1 ? 0 : ((ceil($weight/0.1)-1) * $default[1]));
				$tarif+= ($oc / 100) * 4;
			}else{ # с ОЦ
				$default = $array[2];
				$tarif+= $default[0] + ($weight < 0.1 ? 0 : ((ceil($weight/0.1)-1) * $default[1]));
				$tarif+= ($oc / 100) * 4;
			}
			$tarif+= ($np / 100) * $npR;

			return $tarif;
		*/
	}
	public function PR(CATO $db, ideaPaymentComponent $idea){

		$obj = array(
			'weight'    => $idea->weight,
			'npR'       => $this->settings->npR,
			'oc'        => $this->settings->oc,
			'np'        => $this->settings->np,
			'formula'   => array(
				'1' => array(149.90, 13.40),
				'2' => array(152.00, 15.60),
				'3' => array(158.10, 22.30),
				'4' => array(192.60, 31.80),
				'5' => array(215.00, 36.40),
			),
			'step'      => 0.5,
		);

		$billing = new BillingPR($obj);
		$dif = new DifficultPR($db);


		if($dif->getFind()){
			if($status = $dif->difficultPR($dif->getFind())){
				$dif->exclusion(end($status));
			}


		}else{
			$pr = new DBcalc_difficult_pr;
			$pr->tz             = $idea->tz;
			$pr->deliveryType   = BillingPR::$deliveryType_Earth;
			$pr->coef           = 0;
			$pr->base_tarif     = 0;

			$dif->setRoute($pr);
		}


		$billing->difficult = ($dif->getStatus() == DifficultPR::$STATUS_ROUTE) ? 1 : 0;

		foreach($dif->getRoute() as $route){

			$setting = array(
				'deliveryType'  => $route->deliveryType,
				'base_tarif'    => $route->base_tarif,
				'coef'          => $route->coef,
				'tz'            => $route->tz,
			);
			
			$billing->setTarif($billing->calc($setting));

		}

		if($billing->getTarif() != 0){

			$billing->setTarif($billing->endTarif());

			$billing->setTarif($billing->oc());
			$billing->setTarif($billing->np());
		}
		return $billing;
	}

}
class BillingPR extends abstractBilling{
	public static $deliveryType_Earth       = 2;
	public static $deliveryType_Sky_kombo   = 3;

	public $difficult = 0; # Труднодоступность
	public $npR = 0;
	public $formula;
	public $step = 0.5;
	private $param;

	public function resetParam(){$this->param = array();}
	public function getParam(){return $this->param;}
	public function setParam($param){$this->param = $param;}

	public function __construct($obj){
		$this->weight   = $obj['weight'];
		$this->oc       = $obj['oc'];
		$this->np       = $obj['np'];
		$this->npR      = $obj['npR'];
		$this->formula  = $obj['formula'];
		$this->step     = $obj['step'];
	}
	public function calc(array $param){
		$this->setParam($param);
		$deliveryType = $this->param['deliveryType'];
		$tarif = 0;

		if($deliveryType == self::$deliveryType_Earth){
			$tarif = $this->C();
		}elseif($deliveryType == self::$deliveryType_Sky_kombo){
			$tarif = $this->DA();
		}

		$this->addProcessTarif($tarif);
		$this->resetParam();

		return $tarif;
	}
	/*
	 * @name :: addProcessTarif
	 * @comment :: Дополнительная проверки во время процесса расчета
	 */
	public function addProcessTarif(&$tarif){

		$deliveryType = $this->param['deliveryType'];

		if($this->difficult){

			if($this->weight >= 10 AND $this->weight <= 20){
				if($deliveryType == self::$deliveryType_Earth){
					$tarif+=$this->F1();
				}elseif($deliveryType == self::$deliveryType_Sky_kombo){
					$tarif+=$this->F2();
				}
			}
			if($this->weight > 20){
				$tarif = 0;
			}
		}
	}
	/*
	 * @name :: endTarif
	 * @comment :: Проверка по окончании расчетов
	 */
	public function endTarif(){
		$tarif = 0;

		if($this->difficult){

			if($this->weight >= 0 AND $this->weight <= 20){
				$tarif+=$this->G();
				$tarif+=$this->Q();
			}
		}
		return $tarif;
	}
	public function oc(){ return ($this->oc/100)*4;}
	public function np(){ return ($this->np / 100) * $this->npR;}


	public function G() { return 295;}
	public function Q() { return 15*($this->n()+1);}

	public function C() { return $this->A() + ($this->B() * $this->n());}
	public function DA(){ return $S = $this->D() * $this->step * ($this->n() + 1);}
	public function n() { return $S = ($this->weight / $this->step) - 1;}
	public function F2(){ return 88.5;}
	public function F1(){ return $this->C()*0.3;}
	public function A() { return $this->formula[$this->param['tz']][0];}
	public function B() { return $this->formula[$this->param['tz']][1];}

	public function D() {
		$param = $this->getParam();
		return  $param['base_tarif'] * $param['coef'];
	}
}
abstract class abstractBilling{
	public $weight;
	public $oc;
	public $np;
	/*
	 * @name :: tarif
	 * @comment :: Стоимость расчета
	 */
	private $tarif = 0;

	public function setTarif($tarif){$this->tarif+= $tarif;}
	public function getTarif(){return $this->tarif;}
	public function resetTarif(){return $this->tarif = 0;}
}

class DifficultPR{
	public static $STATUS_CLOSE = 1;
	public static $STATUS_ROUTE = 2;
	public static $STATUS_OPEN  = 3;

	public static $deliveryType_Earth  = 2;
	public static $deliveryType_CLOSE  = 1;

	private $status = 3;
	private $find;
	private $route = array();

	public $countF = 0;

	public $db;

	public function getFind()           { return $this->find;}
	public function getStatus()         { return $this->status;}
	public function setRoute($route)    { $this->route[] = $route;}
	public function getRoute()          { return $this->route;}
	public function setStatus($status)  {
		if($this->status != self::$STATUS_CLOSE AND $this->status != self::$STATUS_ROUTE){ $this->status = $status;}

		if($status == self::$STATUS_CLOSE){$this->status = self::$STATUS_CLOSE;}
	}

	public function __construct(CATO $db){
		if($clkl = $db->CLKL){
			foreach($clkl as $k=>$value){
				$zip[] = $value->zip;
			}
			$zip = $zip[0];
		}
		$findAll = DBcalc_difficult_pr::model()->zip_ops($zip)->findAll();
		foreach($findAll as $value){
			$this->find[] = $value;
		}
	}

	public function difficultPR($pr){

		foreach($pr as $value){
			if($this->difficultRoute($value)) continue;
		}

		$r = $this->getRoute();
		$end = end($r);

		if(!$end){
			$this->setStatus(self::$STATUS_CLOSE);
		}elseif($end->deliveryType == self::$deliveryType_CLOSE){
			$this->setStatus(self::$STATUS_CLOSE);
		}else{
			$this->setStatus(self::$STATUS_ROUTE);
		}

		return $this->getRoute();
	}
	public function exclusion(DBcalc_difficult_pr $pr){
		if($this->getStatus() == self::$STATUS_CLOSE) return false;

		if($pr->zip_pp == 101000) return false;
		if($kz = CLKL::model()->zip($pr->zip_pp)->find()){
			if($CATO = CATO::model()->kladr($kladr = $kz->kladr)->od(CLOD::$OD_PR)->find()){
				$tz = $CATO->tz;
			}
		}

		$obj = new DBcalc_difficult_pr;
		$obj->id            = '';
		$obj->OPS           = $pr->PP;
		$obj->zip_ops       = $pr->zip_pp;
		$obj->deliveryType  = BillingPR::$deliveryType_Earth;
		$obj->PP            = 'МОСКВА';
		$obj->base_tarif    = 1.00;
		$obj->coef          = 0;
		$obj->count         = 1;
		$obj->tz            = $tz ? $tz : 2;

		$this->setRoute($obj);

	}
	public function difficultRoute(DBcalc_difficult_pr $pr){
		if($this->rangeTime($pr)){ return self::$STATUS_OPEN;}
		if($pr->count == 0) return self::$STATUS_CLOSE;
		$this->setStatus(self::$STATUS_ROUTE);
		$this->setRoute($pr);

		if(empty($pr->zip_pp)) return true;
		// Дата актуализации
		foreach(DBcalc_difficult_pr::model()->zip_ops($pr->zip_ops)->findAll() as $value){
			$zipOPS = $value->zip_ops;
			if(isset($array[$zipOPS])){
				$time = strtotime($value->date_actual);
				if($time > $array[$zipOPS]->date_actual){
					$array[$zipOPS] = $value;
				}
			}else{
				$array[$zipOPS] = $value;
			}
		}


		if($dif = DBcalc_difficult_pr::model()->zip_ops($pr->zip_pp)->findAll()){
			$this->difficultPR($dif);
		}else{
			return true;
		}

	}

	private function rangeTime($pr){
		$date_start = new DateTime($pr->date_start);
		$date_start = $date_start->format('d.m').'.'.date('Y');
		$date_end   = new DateTime($pr->date_end);
		$date_end   = $date_end->format('d.m').'.'.date('Y');

		$start  = strtotime($date_start);
		$end    = strtotime($date_end);
		$curr   = time();

		if($start > $curr OR $curr > $end){
			return 1;
		}

		return 0;
	}
}