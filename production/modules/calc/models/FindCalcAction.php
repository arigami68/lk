<?php
# name='Потапов Геннадий'
# description='Модуль. Поиск и вывод городов';
class FindCalcAction extends CAction{
    public static $test = 0;
    public function run(){
        if(true){
            $name = Yii::import('application.components.component.FindModel');
            $model = new $name;
            $model->item = Yii::app()->request->getParam('item') ? Yii::app()->request->getParam('item') : '';
            $model->format = Yii::app()->request->getParam('format') ? Yii::app()->request->getParam('format') : 'csv';
            $model->limit  = Yii::app()->request->getParam('limit') ? Yii::app()->request->getParam('limit') : 5;
            $model->explodeItem();
            $output = '';
        }
        if (!$model->validate()){
            die($output);
        }

        $search = ($t = Yii::app()->request->getParam('search')) ? $t : FindModel::$SEARCH_FULL;

        $input = $model->search($search);
        if($search == FindModel::$SEARCH_KLADR) die($input->zip);

        $output  = $model->format($input, $search);
        if(self::$test){
            print_R(json_decode($output));
            exit;
        }
        die($output);
    }
}