<?php

class CalcModule extends CWebModule{
    public function init()
    {
        $this->setImport(array(
            'complement.models.*',
            'complement.components.*',
        ));
    }
}