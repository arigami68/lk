<?php
class WarehouseController extends UController
{

    public function init(){
        $this->breadcrumbs['Склад'] = array('links' => '');
        $this->breadcrumbs['Товары на складе'] = array('links' => '');
        $this->title = 'Товары на складе';
    }
    public function actions()
    {
        return array(
            'warehouse'=>array( 'class'=> 'application.modules.mywarehouse.models.WarehouseAction'),
        );
    }
}