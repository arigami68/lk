<?php

class WarehouseAction extends CAction{
    public function run(){

        return $this->getController()->render('application.components.views.warehouse.warehouse', array());
    }
}