<?php

# #info - не дописан
class mywarehouseModule extends CWebModule{
    public function init()
    {
	    $this->setModules(array(
		    'settings',
		    'nomenclature',
		    'delivery',
		    'options',
		    'reporting'
	    ));
        $this->setImport(array(
            'mywarehouse.models.*',
	        'mywarehouse.modules.*',
        ));
    }
}