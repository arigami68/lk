<?php

class ReportingController extends UController
{
	public function init(){
		$this->title = 'Заявки на поставку';
	}
	public function actions()
	{
		return array(
			'reporting'=>array(  'class'=> 'WReportingAction'),
		);
	}
}