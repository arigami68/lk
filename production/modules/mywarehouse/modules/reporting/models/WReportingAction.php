<?php

class WReportingAction extends CAction{
	public function run(){

		return $this->getController()->render('application.components.views.warehouse.reporting', array());
	}
}