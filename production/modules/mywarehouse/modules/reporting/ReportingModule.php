<?php

class reportingModule extends CWebModule{
    public function init()
    {
        $this->setImport(array(
            'mywarehouse.modules.reporting.models.*',
        ));
    }
}