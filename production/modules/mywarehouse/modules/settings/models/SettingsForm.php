<?php

class SettingsForm extends CFormModel{
	public  $prefix_delivery,
		$prefix_options,
		$prefix_goods,
		$packing
	;

	public function attributeLabels(){
		return array(
			'prefix_delivery'   => 'Префикс заявок на поставку',
			'prefix_options'    => 'Префикс заявок на комплектацию',
			'prefix_goods'      => 'Префикс номеров грузов (Комплектация)',
			'packing'           => 'Использовать упаковки для номенклатуры'
		);
	}
	public function loadForm($array){

		$this->attributes = $array;
	}
	public function rules()
	{
		return array(
			array('prefix_delivery, prefix_options, prefix_goods', 'safe'),
		);
	}
}