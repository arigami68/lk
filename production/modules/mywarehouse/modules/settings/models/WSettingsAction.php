<?php
class WSettingsAction extends CAction{

	public function run(){
		$form = new SettingsForm();
		$user = Yii::app()->user->id;
		$form->loadForm(($db = DBclientsprefix::model()->id(Yii::app()->user->id)->find()) ? $db->attributes : array());

		return $this->getController()->render('application.components.views.warehouse.settings', array('data' => array('model' => $form)));
	}

}