<?php

class DeliveryForm extends DBstoragedelivery{
	public
		$provider,      // Поставщик клиента
		$client,        // Клиент
		$creatinonDate, // Дата документа
		$editionDate,   // Дата редактирования
		$number,        // Номер документа
		$array,          // Номенклатуры
		$article
	;

	public function attributeLabels(){
		return array(
			'provider'      => 'Поставщик клиента',
			'client'        => 'Клиент',
			'creationDate'  => 'Дата создания',
			'editionDate'   => 'Дата редактирования',
			'number'        => 'Номер документа',
			'array'         => 'Данные',
		);
	}

}