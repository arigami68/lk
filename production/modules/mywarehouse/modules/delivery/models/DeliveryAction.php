<?php

class DeliveryAction extends CAction{
	public function run($operation = 'create'){
		$delivery           = new Delivery;
		$delivery->model    = new DeliveryForm;

		switch($operation){
			case 'create':
				$delivery->create();
			case 'read':
				if(Yii::app()->request->getQuery('art')){
					$delivery->read(array('article' => Yii::app()->request->getQuery('art')));
				}
				break;
			case 'update':
			case 'delete':
			default:
			$delivery->create();
		}

		return $this->getController()->render('application.components.views.warehouse.delivery.create', array('data' => array('model' => $delivery->model, 'data' => $delivery->data)));
	}
}

class DeliveryAbstract{
	public $view = 'views.warehouse.nomenclature.';
}
class Delivery extends DeliveryAbstract{
	public $model;
	public $data;

	public function create(){
		$this->view.='create';
		$nomenclature = new DBnomenclature;
		foreach($nomenclature->search()->getData() as $k=>$v){
			$this->data['article'][] =  $v->article;
		}


	}
	public function read($data){
		$this->view.='create';

	}
	public function update(){

	}
	public function delete(){

	}
}
