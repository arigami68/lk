<?php

class DeliveryController extends UController
{

	public function init(){
		$this->title = 'Заявки на поставку';
	}
	public function actions()
	{
		return array(
			'delivery'  => array('class'=> 'DeliveryAction'),
			'group'     => array('class'=>  'GroupAction'),
		);
	}
}