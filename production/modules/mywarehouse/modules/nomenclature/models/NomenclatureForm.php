<?php


class NomenclatureForm extends DBnomenclature{
	public
		$article,
		$code,
		$name,
		$print,
		$unit,
		$rate,
		$weight,
		$volume,
		$length,
		$width,
		$height,
		$comment,
		$group
	;

	/**
	 * Check group for client
	 */
	public function group(){
		exit;
	}
	public function attributeLabels(){
		return array(
			'article'   => 'Артикул',
			'code'      => 'Код',
			'name'      => 'Рабочее наименование',
			'print'     => 'Наименование для печати',
			'unit'      => 'Единица учета',
			'rate'      => 'Ставка НДС' ,
			'weight'    => 'Вес(брутто)',
			'volume'    => 'Объем',
			'length'    => 'Длина',
			'width'     => 'Ширина',
			'height'    => 'Высота',
			'comment'   => 'Комментарий',
			'group'     => 'Выбор группы'
		);
	}

}