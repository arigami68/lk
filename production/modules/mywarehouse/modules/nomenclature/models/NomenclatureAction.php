<?php

class NomenclatureAction extends CAction{
	public function run($operation = 'create'){

		$nomencl    = new Nomenclature;
		$nomencl->model = new NomenclatureForm();

		if($data = Yii::app()->request->getPost('NomenclatureForm')){
			$nomencl->model->attributes = $data;

			if($nomencl->model->validate()){
				if($find = DBnomenclature::model()->user()->article($nomencl->model['article'])->find()){
					$find->attributes = $nomencl->model->attributes;
					$nomencl->model = $find;

					$act = 'update';
				}else{
					$nomencl->model->clientID = Yii::app()->user->id;
					$nomencl->model->creationDate = date('Y-m-d H:i:s');
					$nomencl->model->editionDate  = date('Y-m-d H:i:s');
					$act = 'insert';
				}

				$nomencl->model->$act();

				$storage = new  DBstoragelinknomenclaturenomenclaturegroupname;
				$storage->nomenclatureID          = $nomencl->model->id;
				$storage->nomenclaturegroupnameID = $data['group'];
				$storage->insert();


				$this->getController()->redirect('mywN?operation=read&art='.$nomencl->model['article']);
			}
		}

		switch($operation){
			case 'create':
				$nomencl->create();
			case 'read':
				if(Yii::app()->request->getQuery('art')){
					$nomencl->read(array('article' => Yii::app()->request->getQuery('art')));
				}
				break;
			case 'update':
			case 'delete':
			default:
				$nomencl->create();
		}

		return $this->getController()->render($nomencl->view, array('data' => array('model' => $nomencl->model, 'data' => $nomencl->data)));
	}
}

class NomenclatureAbstract{
	public $view = 'views.warehouse.nomenclature.';
}
class Nomenclature extends NomenclatureAbstract{
	public $model;
	public $data;

	public function create(){
		$this->view.='create';
		$group = new DBnomenclaturegroupname;
		$search = $group->searchFilter($group->search(''));
		$this->data['group'] = $search;
	}
	public function read($data){
		$this->view.='read';
		$article = $data['article'];

		if($db = DBnomenclature::model()->user()->article($article)->find()){
			$this->model->attributes = $db->attributes;
		}
	}
	public function update(){

	}
	public function delete(){

	}
}
