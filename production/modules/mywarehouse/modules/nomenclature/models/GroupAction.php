<?php

class GroupAction extends CAction{
	public function run($operation = 'read'){
		$model      = new NomenclatureForm();
		$group      = new GroupForm();

		if($data = Yii::app()->request->getPost('GroupForm')){
			$this->getPost($data);
			$this->getController()->refresh();
		}
		$db = DBnomenclaturegroupname::model()->findAll();

		$link = array(0 => '');
		foreach($db as $k=>$v){
			$link[$v->id] = $v->name;
		}

		return $this->getController()->render('views.warehouse.nomenclature.group', array('data' => array('model' => $model, 'group' => $group, 'link' => $link)));
	}
	public function getPost($data){

		$link   = isset($data['link']) ? $data['link'] : null;
		$name   = $data['name'];
		$comment= $data['comment'];

		$group = new DBnomenclaturegroupname;
		$group->clientID= Yii::app()->user->auth->id;
		$group->name    = $name;
		$group->comment = $comment;
		$group->insert();

		$three = new DBnomenclaturegroup;
		$three->current = $group->id;

		if(!$link){
			$three->prev = 0;
		}else{

			$prev = ($db = DBnomenclaturegroupname::model()->id($link)->user()->find()) ? $db->id : 0;
			$three->prev = $prev;
		}

		return $three->insert();
	}
}