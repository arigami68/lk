<?php

class GroupForm extends DBnomenclaturegroupname{
	public
		$name,
		$link,
		$comment
	;

	public function attributeLabels(){
		return array(
			'name'   => 'Введите имя группы',
			'link'   => 'Вложение в группу (если пусто, значит группа находится в корневом разделе)',
			'comment'=> 'Описание'
		);
	}

}