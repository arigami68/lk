<?php

class nomenclatureModule extends CWebModule{
    public function init()
    {
        $this->setImport(array(
            'mywarehouse.modules.nomenclature.models.*',
        ));
    }
}