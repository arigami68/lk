<?php
class complementAction extends CAction{
    public function run($action = 'show', $load = 'manual'){
        if(true){
            $com = new stdClass();
            $com->flag = new stdClass();
            $com->form = new stdClass();
            $com->heap = new stdClass();
            $com->db = new PRTI();
            $com->form->file = new FileForm();
            $com->request = Yii::app()->request;
            $com->view = array(
                'show' => 'views.complement.complement',
                'operation' => 'views.complement.complement_operation',
            );
            $com->model->main = '';
            $com->model->add = '';
            $com->model->operation = $com->request->getParam('operation') ? $com->request->getParam('operation') : 'create';
        }

        if($action == 'status'){
            
            $form = new EditForm();
            $id=NULL;
            $post_id = $com->request->getQuery('id') ? $com->request->getQuery('id') : -1;

            if($PRTI = PRTI::model()->user()->application($post_id)->find()){
                $id = $PRTI->id_idapplication;
                
                $form->date_shipping = $PRTI->date_shipping ? date('Y-m-d', strtotime($PRTI->date_shipping)) : NULL;
                $form->id = $id;
                if($form->date_shipping){
                    $form->edit = $PRTI->status ? 1 : 0;
                }
            }else{
              Yii::app()->user->setFlash('file', 'Такой номер не зарегистрирован');
              return $this->getController()->redirect('/com?action=show');
            }
            
            if(Yii::app()->request->isPostRequest){
                $form->attributes = $_POST['EditForm'];
               
                if($form->validate()){
                        $PRTI->status = 1;
                        $PRTI->date_shipping = $form->date_shipping;
                        $PRTI->update();
                        return $this->getController()->redirect('/com?action=show');
                }else{

                }
            }
            
            return $this->getController()->render('application.components.views.complement.status', array('model' => $form));
        }elseif($action == 'show'){
            $com->model->main= $com->db;
            
        }elseif($action == 'operation'){
            
            if($load == 'auto'){
                $com->model->main= $com->form->file;
                if($com->request->getParam(get_class($com->form->file))){
                    Yii::app()->user->setFlash('file', 'Файл не загружен');
                    $com->form->file->file=CUploadedFile::getInstance($com->form->file,'file');
                    if($com->form->file->validate()){
                        $com->form->file->processing(&$xml);
                        # Добавить индикатор загрузки

                        $funcmegapolis = FuncMegapolis::pushXml($xml, MEGAPOLIS_API, 'equip&secret='.Yii::app()->user->auth->Secretkey);
                        if(strpos($funcmegapolis, 'Не правильно сформированная заявка'))
                        {
                            Yii::app()->user->setFlash('file', 'Не правильно сформированная заявка');
                        }
                        else
                        {
                            Yii::app()->user->setFlash('file', 'Файл успешно загружен');
                        }
                    }
                }
            }elseif(true){
                # Загрузка в ручную
                
                $com->model->main = new ComplementForm();
                $com->model->add = new ComplementAddForm();
                $com->model->component = new ComplementComponent();
                $com->model->error = 0;
                $com->model->db = new stdClass();
               
               
                
                # Если указан АРТИКУЛ значит идут данные с другой страницы
                if($com->request->getParam('article')){
                    
                    $com->heap->article = new stdClass();
                    $com->heap->article->article = $com->request->getParam('article');
                    $com->heap->article->count = $com->request->getParam('count');
                    $com->heap->article->wiVAT = $com->request->getParam('wiVAT');
                    $com->heap->article->woVAT = $com->request->getParam('woVAT');
                    $com->heap->article->array = $_POST;
                    
                    #$com->heap->article->array->bailor = Yii::app()->user->auth->name;
                    
                    unset($com->heap->article->array['yt1']);
                    $form = $com->model->main->getWarehouse($com->heap->article->array);
                    if(true){ # Подделываем правильный POST
                        $post = '';
                        $post = $_POST;
                        unset($_POST);
                        $_POST[get_class($com->model->main)] = $form;
                        
                    }
                }
                
                if($com->param = $com->request->getParam(get_class($com->model->main))){
                    $com->model->data = $com->model->component->conversion($com->param);
                    
                    if(!$com->model->data) $this->getController()->refresh();
                    if($com->model->operation == PRTI::$OPERATION_EDIT){
                        $id = $com->model->data->product[0]->id;
                        if($id != $com->request->getQuery('id')){
                            
                            Yii::app()->user->setFlash('error', 'Задание не совпадает');
                            $this->getController()->refresh();
                        }
                        
                    }

                    foreach($com->model->data->product as $k=>$v){
                        $com->model->main->attributes = $v->attributes;
                        if($com->model->main->validate()){
                            
                        }else{
                            $com->model->error =1;
                            $v->errors = $com->model->main->errors;
                            Yii::app()->user->setFlash('error', 'Одно из отправлений не заполнено!');
                        }

                        $com->model->main->product[] = clone $com->model->main;
                        if($com->model->operation == PRTI::$OPERATION_EDIT){
                         #   $com->model->data->product=array_reverse($com->model->data->product);
                        }
                    }
                    $com->model->main->component = $com->model->data->form;
                    
                    # Если нет ошибок, то загружаем в API::MEGAPOLIS
                    if(!$com->model->error){
                        if($com->model->operation == PRTI::$OPERATION_EDIT){
                            if(!PRTI::model()->user()->find($id)){
                                    Yii::app()->user->setFlash('success', 'Данный номер не был найден');
                                    return $this->getController()->redirect( Yii::app()->createUrl('com',array('action'=>'show')) );
                            }else{
                                    $get = file_get_contents(MEGAPOLIS_API_DATA.'api/api/PRTI?id='.$id.'&secret='.Yii::app()->user->auth->Secretkey);
                                    $get = json_decode($get);
                                    if(isset($get->access)){
                                        if($get->access == 'true'){
                                            $get = file_get_contents(MEGAPOLIS_API_DATA.'api/api/PRTI?id='.$id.'&secret='.Yii::app()->user->auth->Secretkey.'&operation=delete');
                                        }else{
                                            Yii::app()->user->setFlash('error', 'Номер недоступен');
                                            return $this->getController()->redirect( Yii::app()->createUrl('com',array('action'=>'show')) );
                                        }
                                    }
                            }
                        }
                        $id = $com->model->data->product[0]->id;
                        
                        $get = file_get_contents(MEGAPOLIS_API_DATA.'api/api/PRTI?id='.$id.'&secret='.Yii::app()->user->auth->Secretkey);
                        $get = json_decode($get);
                        if(isset($get->access)){
                            if($get->access == 'false'){
                                FuncMegapolis::pushXml($com->model->component->convertToXml($com->model->data->product), MEGAPOLIS_API, 'equip&secret='.Yii::app()->user->auth->Secretkey);
                                Yii::app()->user->setFlash('error', 'Задание успешно добавлено!');
                                return $this->getController()->redirect(array('/com?action=show'));
                            }else{
                                Yii::app()->user->setFlash('error', 'Номер недоступен');
                            }
                        }
                    }
                }
                
                 # Если указан id
                if($com->request->getParam('id')){

                    if(true){
                        $criteria = new CDBcriteria();
                        $criteria->compare('id_idapplication', $com->request->getParam('id'));
                    }
                    unset($com->heap->PRTI);
                    if($com->heap->PRTI = PRTI::model()->user()->find($criteria)){
                        if($com->model->operation == PRTI::$OPERATION_EDIT){
                          
                           if(empty($com->model->main->product)){
                                foreach($com->heap->PRTI->PRHS as $k=>$v){
                                    
                                    $com->model->main->attributes = $v->PRST->attributes;
                                    $com->model->main->id = $v->idapplication;
                                    $com->model->main->product[] = clone $com->model->main;
                                }
                               $com->model->main->product=array_reverse($com->model->main->product);
                           }
                        }elseif($com->model->operation == PRTI::$OPERATION_DELETE){
                             foreach($com->heap->PRTI->PRHS as $k=>$v){
                                 if($PRST = PRST::model()->find($v->idstorage))
                                    $PRST->delete();
                                 $v->delete();
                             }
                             $com->heap->PRTI->delete();
                             Yii::app()->user->setFlash('error', 'Задание удалено');
                             return $this->getController()->refresh();
                        }elseif(true){
                            foreach($com->heap->PRTI->PRHS as $k=>$v){
                                $com->model->main->attributes = $v->PRST->attributes;
                                $com->model->main->id = $v->idapplication;
                                $com->model->main->product[] = clone $com->model->main;
                                
                            }
                            $com->model->main->product=array_reverse($com->model->main->product);
                            $com->model->operation = 'show';
                        }
                       
                    }else{
                        return $this->getController()->redirect('/com?action=show');
                    }
                }
            }
        }
        
        $operation = isset($com->model->operation) ? $com->model->operation : NULL;
        $com->models = array('model' => $com->model->main, 'model_add' => $com->model->add, 'operation' => $operation);
        $view = isset($com->view[$action]) ? $com->view[$action] : $com->view['show'];
         
        return $this->getController()->render( $view, $com->models);
    }
}