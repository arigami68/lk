<?php

class OrdersAddAction extends CAction{
    public function run($AI = 1, $process = 'main'){
        $goods = new stdClass();
        $goods->process = new stdClass();
        $goods->process->main['name'] = 'main';
        $goods->request = Yii::app()->request;
        $goods->model = new stdClass();
        $goods->array['len'] = $goods->request->getParam('goods') ? $goods->request->getParam('goods')+1 : 1;
        $goods->array['yw3'] = $goods->request->getParam('yw3') ? $goods->request->getParam('yw3') : 3;
        $AI = $goods->array['len'] + 16;
        $goods->model->name = Yii::import('application.modules.complement.models.ComplementForm');
        $goods->model->active = new $goods->model->name;
        ob_start();
        $form= $this->getController()->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id'=>'location-form',
        'type'=>'horizontal',
        ));
        ob_end_clean();
        
        $goods->text = '';
        $goods->text.= '<span class="deleteOrders"><a name='.$goods->array['yw3'].'_tab_'.($goods->array['len']+15).' class=order'.$goods->array['len'].' href=#>Удалить</a></span>';

        $goods->name = 'act';
        $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($goods->model->active).'['.$goods->process->main['name'].']'.'['.$AI.']['.$goods->name.']');
        $goods->text.= $form->textFieldRow($goods->model->active,$goods->name, $htmloption);
        
        $goods->name = 'surname';
        $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($goods->model->active).'['.$goods->process->main['name'].']'.'['.$AI.']['.$goods->name.']');
        $goods->text.= $form->textFieldRow($goods->model->active,$goods->name, $htmloption);
        
        $goods->name = 'name';
        $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($goods->model->active).'['.$goods->process->main['name'].']'.'['.$AI.']['.$goods->name.']');
        $goods->text.= $form->textFieldRow($goods->model->active,$goods->name, $htmloption);
        
        $goods->name = 'lastname';
        $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($goods->model->active).'['.$goods->process->main['name'].']'.'['.$AI.']['.$goods->name.']');
        $goods->text.= $form->textFieldRow($goods->model->active,$goods->name, $htmloption);
        
        $goods->name = 'phone';
        $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($goods->model->active).'['.$goods->process->main['name'].']'.'['.$AI.']['.$goods->name.']');
        $goods->text.= $form->textFieldRow($goods->model->active,$goods->name, $htmloption);
        
        $goods->name = 'country';
        $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($goods->model->active).'['.$goods->process->main['name'].']'.'['.$AI.']['.$goods->name.']');
        $goods->text.= $form->textFieldRow($goods->model->active,$goods->name, $htmloption);
        
        $goods->name = 'region';
        $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($goods->model->active).'['.$goods->process->main['name'].']'.'['.$AI.']['.$goods->name.']');
        $goods->text.= $form->textFieldRow($goods->model->active,$goods->name, $htmloption);
        
        $goods->name = 'area';
        $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($goods->model->active).'['.$goods->process->main['name'].']'.'['.$AI.']['.$goods->name.']');
        $goods->text.= $form->textFieldRow($goods->model->active,$goods->name, $htmloption);
        
        $goods->name = 'delivery';
        $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($goods->model->active).'['.$goods->process->main['name'].']'.'['.$AI.']['.$goods->name.']');
        $goods->text.= $form->textFieldRow($goods->model->active,$goods->name, $htmloption);
        
        $goods->name = 'zip';
        $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($goods->model->active).'['.$goods->process->main['name'].']'.'['.$AI.']['.$goods->name.']');
        $goods->text.= $form->textFieldRow($goods->model->active,$goods->name, $htmloption);
        
        $goods->name = 'street';
        $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($goods->model->active).'['.$goods->process->main['name'].']'.'['.$AI.']['.$goods->name.']');
        $goods->text.= $form->textFieldRow($goods->model->active,$goods->name, $htmloption);
        
        $goods->name = 'house';
        $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($goods->model->active).'['.$goods->process->main['name'].']'.'['.$AI.']['.$goods->name.']');
        $goods->text.= $form->textFieldRow($goods->model->active,$goods->name, $htmloption);

        $goods->name = 'nod';
        $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($goods->model->active).'['.$goods->process->main['name'].']'.'['.$AI.']['.$goods->name.']');
        $goods->text.= $form->textFieldRow($goods->model->active,$goods->name, $htmloption);
        
        $goods->name = 'viewdep';
        $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($goods->model->active).'['.$goods->process->main['name'].']'.'['.$AI.']['.$goods->name.']');
        $goods->text.= $form->textFieldRow($goods->model->active,$goods->name, $htmloption);

        $goods->name = 'coder';
        $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($goods->model->active).'['.$goods->process->main['name'].']'.'['.$AI.']['.$goods->name.']');
        $goods->text.= $form->textFieldRow($goods->model->active,$goods->name, $htmloption);
        
        $goods->name = 'body';
        $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($goods->model->active).'['.$goods->process->main['name'].']'.'['.$AI.']['.$goods->name.']');
        $goods->text.= $form->textFieldRow($goods->model->active,$goods->name, $htmloption);
        
        $goods->name = 'floor';
        $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($goods->model->active).'['.$goods->process->main['name'].']'.'['.$AI.']['.$goods->name.']');
        $goods->text.= $form->textFieldRow($goods->model->active,$goods->name, $htmloption);
        
        $goods->name = 'office';
        $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($goods->model->active).'['.$goods->process->main['name'].']'.'['.$AI.']['.$goods->name.']');
        $goods->text.= $form->textFieldRow($goods->model->active,$goods->name, $htmloption);
        
        $goods->name = 'categorydep';
        $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($goods->model->active).'['.$goods->process->main['name'].']'.'['.$AI.']['.$goods->name.']');
        $goods->text.= $form->textFieldRow($goods->model->active,$goods->name, $htmloption);
        
        $goods->name = 'aoii';
        $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($goods->model->active).'['.$goods->process->main['name'].']'.'['.$AI.']['.$goods->name.']');
        $goods->text.= $form->textFieldRow($goods->model->active,$goods->name, $htmloption);

        $goods->name = 'flagnotif';
        $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($goods->model->active).'['.$goods->process->main['name'].']'.'['.$AI.']['.$goods->name.']');
        $goods->text.= $form->textFieldRow($goods->model->active,$goods->name, $htmloption);
        
        $goods->name = 'viewnotif';
        $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($goods->model->active).'['.$goods->process->main['name'].']'.'['.$AI.']['.$goods->name.']');
        $goods->text.= $form->textFieldRow($goods->model->active,$goods->name, $htmloption);
        
        $goods->name = 'fragility';
        $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($goods->model->active).'['.$goods->process->main['name'].']'.'['.$AI.']['.$goods->name.']');
        $goods->text.= $form->textFieldRow($goods->model->active,$goods->name, $htmloption);

        $goods->name = 'note';
        $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($goods->model->active).'['.$goods->process->main['name'].']'.'['.$AI.']['.$goods->name.']');
        $goods->text.= $form->textFieldRow($goods->model->active,$goods->name, $htmloption);
        
        $goods->name = 'codei';
        $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($goods->model->active).'['.$goods->process->main['name'].']'.'['.$AI.']['.$goods->name.']');
        $goods->text.= $form->textFieldRow($goods->model->active,$goods->name, $htmloption);
    
        $tabs[] = array('label'=>'+ Товар' , 'linkOptions' => array('id'=>'addGoods'));
        
        print $text= '<div id="'.$goods->array['yw3'].'_tab_'.($goods->array['len']+15).'" class="tab-pane">'.$goods->text;
        
        
            $this->getController()->widget('bootstrap.widgets.TbTabs2', array(
                'id' => 'tabs_orders_'.($goods->array['len']+16),
                'type'=>'tabs',
                'placement'=>'top',
                'tabs'=> $tabs,
            ));
        
         
        
        print '</div>';
        return;
    } 
}