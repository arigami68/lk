<?php

class Fps extends CActiveRecord  implements IECartPosition {
    public static $tableName = 'prod_storage';
    public static function model($className= __CLASS__){ return parent::model($className);}
    public function tableName(){ return self::$tableName;}
    public function getDbConnection(){ return Yii::app()->db;}
    
    public static function paymentgoods($array){
        foreach($array as $k=>$v){
            $keeper = $v['keeper'];
            $nomencl = $v['nomencl'];
            $artical = $v['artical'];
            $implprice = $v['implprice'];
            $amount = $v['amount'];
            $coder = $v['coder'];
            $id = $v['id'];

            isset($r[$coder]) ? FALSE :  $r[$coder] = new FpsComponent();
            
            $r[$coder]->setId($id);
            $r[$coder]->setKeeper($keeper);
            $r[$coder]->setArtical($artical);
            $r[$coder]->setNomencl($nomencl);
            $r[$coder]->setImplprice($implprice);
            $r[$coder]->setAmount($amount);
            $r[$coder]->setCost();
            
        }

        return $r;
        
    }
    
    public function getId(){
        return 'Fps'.$this->id;
    }

    public function getPrice(){
        return $this->price;
    }
}