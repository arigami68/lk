<?php

class FileForm extends CFormModel
{
    public $file;
    
    public function rules(){
            return array(
                array('file', 'file', 'types'=> 'xml, xls, xlsx', 'message'=>'Загрузите файл'),
            );
    }
    public function attributeLabels(){
        return array( 'file' => 'Загрузка файла');        
    }
    public function processing($xml){
        $file = new stdClass();
        $file->render = new UploadFileAPI();
        $value = array();
        
        $file->file = $this->file;
        $component = $file->render->render($file->file, 'complement');
        
        $xml =ConvertToXml::getFormatComplement($component);
    }
}