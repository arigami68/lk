<?php

class GoodsAddAction extends CAction{
    public function run($AI = 1, $process = 'main'){
        
        $goods = new stdClass();
        $goods->process = new stdClass();
        $goods->process->main['name'] = 'add';
        $goods->request = Yii::app()->request;
        $goods->model = new stdClass();
        $goods->array['len'] = $goods->request->getParam('goods') ? $goods->request->getParam('goods') : 1;
        
        $goods->array['orders'] = $goods->request->getParam('orders') ? $goods->request->getParam('orders') : 1;
        $goods->model->name = Yii::import('application.modules.complement.models.ComplementForm');
        $goods->model->active = new $goods->model->name;
        $AI = $goods->array['len']+1024;
        $len = explode('_', $goods->array['orders']);
        $goods->array['orders'] = $len[2];
        
        ob_start();
        $form= $this->getController()->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id'=>'location-form',
        'type'=>'horizontal',
        ));
        ob_end_clean();
        
        $goods->text = '';
        $goods->text.= '<span class="deleteGoods"><a class=goods'.$goods->array['len'].' href=#>Удалить</a></span>';
        
        $goods->name = 'tdv';
        $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($goods->model->active).'['.$goods->process->main['name'].']'.'['.$goods->array['orders'].']'.'['.$AI.']['.$goods->name.']');
        $goods->text.= $form->textFieldRow($goods->model->active,$goods->name, $htmloption);
        
        $goods->name = 'codamount';
        $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($goods->model->active).'['.$goods->process->main['name'].']'.'['.$goods->array['orders'].']'.'['.$AI.']['.$goods->name.']');
        $goods->text.= $form->textFieldRow($goods->model->active,$goods->name, $htmloption);
    
        $goods->name = 'accpack';
        $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($goods->model->active).'['.$goods->process->main['name'].']'.'['.$goods->array['orders'].']'.'['.$AI.']['.$goods->name.']');
        $goods->text.= $form->textFieldRow($goods->model->active,$goods->name, $htmloption);

        $goods->name = 'viewaccpack';
        $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($goods->model->active).'['.$goods->process->main['name'].']'.'['.$goods->array['orders'].']'.'['.$AI.']['.$goods->name.']');
        $goods->text.= $form->textFieldRow($goods->model->active,$goods->name, $htmloption);

        $goods->name = 'weightdep';
        $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($goods->model->active).'['.$goods->process->main['name'].']'.'['.$goods->array['orders'].']'.'['.$AI.']['.$goods->name.']');
        $goods->text.= $form->textFieldRow($goods->model->active,$goods->name, $htmloption);
    
        $goods->name = 'artical';
        $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($goods->model->active).'['.$goods->process->main['name'].']'.'['.$goods->array['orders'].']'.'['.$AI.']['.$goods->name.']');
        $goods->text.= $form->textFieldRow($goods->model->active,$goods->name, $htmloption);

        $goods->name = 'attachments';
        $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($goods->model->active).'['.$goods->process->main['name'].']'.'['.$goods->array['orders'].']'.'['.$AI.']['.$goods->name.']');
        $goods->text.= $form->textFieldRow($goods->model->active,$goods->name, $htmloption);

        $goods->name = 'nomencl';
        $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($goods->model->active).'['.$goods->process->main['name'].']'.'['.$goods->array['orders'].']'.'['.$AI.']['.$goods->name.']');
        $goods->text.= $form->textFieldRow($goods->model->active,$goods->name, $htmloption);

        $goods->name = 'unit';
        $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($goods->model->active).'['.$goods->process->main['name'].']'.'['.$goods->array['orders'].']'.'['.$AI.']['.$goods->name.']');
        $goods->text.= $form->textFieldRow($goods->model->active,$goods->name, $htmloption);

        $goods->name = 'amount';
        $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($goods->model->active).'['.$goods->process->main['name'].']'.'['.$goods->array['orders'].']'.'['.$AI.']['.$goods->name.']');
        $goods->text.= $form->textFieldRow($goods->model->active,$goods->name, $htmloption);

        $goods->name = 'implprice';
        $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($goods->model->active).'['.$goods->process->main['name'].']'.'['.$goods->array['orders'].']'.'['.$AI.']['.$goods->name.']');
        $goods->text.= $form->textFieldRow($goods->model->active,$goods->name, $htmloption);

        $goods->name = 'implsum';
        $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($goods->model->active).'['.$goods->process->main['name'].']'.'['.$goods->array['orders'].']'.'['.$AI.']['.$goods->name.']');
        $goods->text.= $form->textFieldRow($goods->model->active,$goods->name, $htmloption);
        
        $text= '<div id="yw'.$goods->array['orders'].'_tab_'.$goods->array['len'].'"  class="tab-pane">'.$goods->text.'</div>';
        
        die($text);
    } 
}