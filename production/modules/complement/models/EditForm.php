<?php

class EditForm extends CFormModel
{
    public $id, $edit, $date_shipping;
    
    public function rules()
    {
            return array(
                array('edit, date_shipping', 'required', 'message'=> 'Введите текст'),
            );
    }
    public function attributeLabels()
    {
        return array(
            'id' => 'Номер заявки',
            'edit' => 'Изменить статус',
            'date_shipping' => 'Дата поступления',
        );
    }
}