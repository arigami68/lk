<?php

class ComplementForm extends CFormModel
{   
    public $id, $keeper, $bailor, $act, $surname, $name, $lastname, $phone, $country, $region, $area, $delivery, $zip, $street, $house, $artical, $attachments, $nomencl, $unit, $amount, $implprice, $implsum, $nod, $viewdep, $tdv, $codamount;
    public $coder, $body, $floor, $office, $codei, $categorydep, $aoii, $flagnotif, $viewnotif, $fragility, $accpack, $viewaccpack, $weightdep, $note;
    public $component = array();
    public $product = array();
    
    public function rules(){
            return array(
                array('id, keeper, bailor, act, surname, name, lastname, phone, country, region, area, delivery, zip, street, house, artical, attachments, nomencl, unit, amount, implprice, implsum, nod, viewdep, tdv, codamount', 'required', 'message'=> 'Введите текст'),
                array('coder, body, floor, office, codei, categorydep, aoii, flagnotif, viewnotif, fragility, accpack, viewaccpack, weightdep, note' , 'safe'),
            );
    }
    public function attributeLabels(){
        return array(
            'id' => 'Номер заявки',
            'keeper' => 'Организация-хранитель',
            'bailor' => 'Контрагент-поклаждатель',
            'act' => 'Номер заказа',
            'coder' => 'Код(получателя)',
            'surname' => 'Фамилия',
            'name' => 'Имя',
            'lastname' => 'Отчество',
            'phone' => 'Телефон',
            'country' => 'Страна',
            'region' => 'Регион/область',
            'area' => 'Район',
            'delivery' => 'Пункт доставки',
            'zip' => 'Индекс',
            'street' => 'Улица',
            'house' => 'Дом, строение',
            'body' => 'Корпус',
            'floor' => 'Этаж',
            'office' => 'Офис/квартира',
            'codei' => 'Код(внутр)',
            'artical' => 'Артикул( код котрагента)',
            'attachments' => 'Вид вложения',
            'nomencl' => 'Номенклатура( наименование товара)',
            'unit' => 'Единица хранения',
            'amount' => 'Количество',
            'implprice' => 'Цена реализации, руб',
            'implsum' => 'Сумма реализации, руб',
            'nod' => 'Наименование оператора доставки',
            'viewdep' => 'Вид отправления',
            'categorydep' => 'Категория отправления',
            'aoii' => 'Наличие описи вложения',
            'flagnotif' => 'Наличие уведомления',
            'viewnotif' => 'Вид уведомления',
            'fragility' => 'Признак хрупкости',
            'accpack' => 'Необходимость дополнительной упаковки',
            'viewaccpack' => 'Вид дополнительной упаковки( короб, пакет и.т.п)',
            'weightdep' => 'Расчетный вес отправления, кг/заказ',
            'tdv' => 'Сумма объявленной/страховой ценности, руб./строка',
            'codamount' => 'Сумма наложенного платежа, руб./строка',
            'note' => 'Примечание',
        );
    }
    
    
 # Перед тем как данные приходят из формы
    public function getAttributes(array $names=NULL){
        if($names){return parent::getAttributes($names);}
        return array(parent::getAttributes($names));
    }
    public function getWarehouse(array $values=NULL){
        
        $values['article'];
        $values['count'];
        $values['wiVAT'];
        $values['woVAT'];
        $param = array();
        $param['id'] = ''; 
        $param['keeper'] = '';
        $param['bailor'] = Yii::app()->user->auth->name;
        $param_link = &$param['main'][1];
        $param_link['act'] = '';
        $param_link['surname'] = '';
        $param_link['name'] = '';
        $param_link['lastname'] = '';
        $param_link['phone'] = '';
        $param_link['country'] = '';
        $param_link['region'] = '';
        $param_link['area'] = '';
        $param_link['delivery'] = '';
        $param_link['zip'] = '';
        $param_link['street'] = '';
        $param_link['house'] = '';
        $param_link['nod'] = '';
        $param_link['viewdep'] = '';
        $param_link['coder'] = '';
        $param_link['body'] = '';
        $param_link['floor'] = '';
        $param_link['office'] = '';
        $param_link['categorydep'] = '';
        $param_link['aoii'] = '';
        $param_link['flagnotif'] = '';
        $param_link['viewnotif'] = '';
        $param_link['fragility'] = '';
        $param_link['note'] = '';
        $param_link['codei'] = '';
        $param_link_add = &$param['add'][1];
        $heap = array();
        
        foreach($values as $k =>$v){
            foreach($v as  $k2=>$v2){
                $heap[$k2][$k] = $v2;
            }
        }
        
        foreach($heap as $k=>$v){
                $goods = $v;
                
                $article = $v['article'];
                $application = $v['application'];
                if(true){
                    # Поиск информации о Товаре
                    if(!$ASDB = ASDB::model()->user()->find('artical=:artical', array('artical' => $article))) return false;
                    if(!$APH1 = APH1::model()->find('id_storage=:id_storage AND id_idapplication=:id_idapplication', array('id_storage' => $ASDB->id, 'id_idapplication' => $application))) return false;
                }
                
                $implsum = $APH1->woVAT * $goods['count'];
                $param_link_add[$k]['tdv']         = $APH1->woVAT;
                $param_link_add[$k]['codamount']   = $APH1->wiVAT;
                $param_link_add[$k]['accpack']     = '';
                $param_link_add[$k]['viewaccpack'] = '';
                $param_link_add[$k]['weightdep']   = '';
                $param_link_add[$k]['artical']     = $goods['article'];
                $param_link_add[$k]['attachments'] = '';
                $param_link_add[$k]['nomencl']     = $ASDB['nomencl'];
                $param_link_add[$k]['unit']        = $ASDB['unit'];
                $param_link_add[$k]['amount']      = $goods['count'];
                $param_link_add[$k]['implprice']   = $goods['woVAT'];
                $param_link_add[$k]['implsum']     = $implsum;
        }
        return $param;
    }
}
