<?php

class ComplementController extends UController
{
    public function init(){
        $this->breadcrumbs['Склад'] = array('links' => '');
        $this->title = 'Задания на комплектацию';
    }
    public function actions()
    {
        $this->breadcrumbs['Задания'] = array('links' => '');
        return array(
            'complement'=> array('class'=> 'application.modules.complement.models.complementAction'),
            'ordersAdd'=> array('class'=> 'application.modules.complement.models.OrdersAddAction'),
            'goodsAdd'=> array('class'=> 'application.modules.complement.models.GoodsAddAction'),
        );
    }
}