<?php

class ReadXlsx extends CBehavior{
    
    public $file;
    public $inputFileType;
    public $component;
    public static $END = 'Итого:';
    
    public function read(){
        spl_autoload_unregister(array('YiiBase', 'autoload'));
        $phpExcelPath = Yii::getPathOfAlias('extensions.php-excel.Classes');
        include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');
        spl_autoload_register(array('YiiBase', 'autoload'));
 
        $objReader = PHPExcel_IOFactory::createReader($this->inputFileType);
       
        $objReader->setLoadAllSheets();
        $objPHPExcel = PHPExcel_IOFactory::load($this->file);
        
        $components = $this->renderread($objPHPExcel);
        
        return $components;
    }
    public function renderread($objPHPExcel = 0){
        
        if(get_class($objPHPExcel) !== 'PHPExcel') return 0;
        
        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
        $array = array();
        $xml = array();
        $xml_count = array();
        
        for($a=10;$a;$a++){
            if(empty($sheetData[$a]['A'])) break;
            if($sheetData[$a]['A'] == self::$END) break;
                $xml_count[] = $a;
        }
        
        if(!$xml_count) return NULL;
        
        $xml = array();
        foreach($xml_count as $v=>$filled){
            $array = new stdClass();
            $array->id = $sheetData[3]['B'];
            
            $array->keeper = $sheetData[3]['G'];
            $array->bailor = $sheetData[5]['G'];
            $array->act    = $sheetData[$filled]['B'];
            $array->coder  = $sheetData[$filled]['C'];
            $array->surname = $sheetData[$filled]['D'];
            $array->name = $sheetData[$filled]['D'];
            $array->lastname = $sheetData[$filled]['D'];
            $array->phone = $sheetData[$filled]['E'];
            $array->country = $sheetData[$filled]['F'];
            $array->region = $sheetData[$filled]['G'];
            $array->area = $sheetData[$filled]['H'];
            $array->delivery = $sheetData[$filled]['I'];
            $array->zip = $sheetData[$filled]['J'];
            $array->street = $sheetData[$filled]['K'];
            $array->house = $sheetData[$filled]['L'];
            $array->body = $sheetData[$filled]['M'];
            $array->floor = $sheetData[$filled]['N'];
            $array->office = $sheetData[$filled]['O'];
            $array->codei  = $sheetData[$filled]['P'];
            $array->artical  = $sheetData[$filled]['Q'];
            $array->attachments = $sheetData[$filled]['R'];
            $array->nomencl = $sheetData[$filled]['S'];
            $array->unit = $sheetData[$filled]['T'];
            $array->amount = $sheetData[$filled]['U'];
            $array->implprice = $sheetData[$filled]['V'];
            $array->implsum = $sheetData[$filled]['W'];
            $array->nod = $sheetData[$filled]['X'];
            $array->viewdep = $sheetData[$filled]['Y'];
            $array->categorydep = $sheetData[$filled]['Z'];
            $array->aoii = $sheetData[$filled]['AA'];
            $array->flagnotif = $sheetData[$filled]['AB'];
            $array->viewnotif = $sheetData[$filled]['AC'];
            $array->fragility = $sheetData[$filled]['AD'];
            $array->accpack = $sheetData[$filled]['AE'];
            $array->viewaccpack = $sheetData[$filled]['AF'];
            $array->weightdep = $sheetData[$filled]['AG'];
            $array->tdv = $sheetData[$filled]['AH'];
            $array->codamount = $sheetData[$filled]['AI'];
            $array->note = $sheetData[$filled]['AJ'];
            
            $xml[] = $array;
        }
        
        $component = new $this->component;
        foreach($xml as $k=>$array){
            $component->product[$k] = $this->getComponent($array);
            
            $component->id = $component->product[$k]->id;
        }
        
        return $component;
    }
    public function getComponent($array){
        $component = new $this->component;
        
        $component->id =            $array->id;
        $component->keeper =        $array->keeper;
        $component->bailor =        $array->bailor;
        $component->act    =        $array->act;
        $component->coder  =        $array->coder;
        $component->surname =       $array->surname;
        $component->name =          $array->name;
        $component->lastname =      $array->lastname;
        $component->phone =         $array->phone;
        $component->country =       $array->country;
        $component->region =        $array->region;
        $component->area =          empty($array->area) ? '-' : $array->area;
        $component->delivery =      $array->delivery;
        $component->zip =           $array->zip;
        $component->street =        $array->street;
        $component->house =         $array->house;
        $component->body =          $array->body;
        $component->floor =         $array->floor;
        $component->office =        $array->office;
        $component->codei  =        $array->codei;
        $component->artical  =      $array->artical;
        $component->attachments=    empty($array->attachments) ? '-' : $array->attachments;
        $component->nomencl =       $array->nomencl;
        $component->unit =          $array->unit;
        $component->amount =        $array->amount;
        $component->implprice =     $array->implprice;
        $component->implsum =       $array->implsum;
        $component->nod =           $array->nod;
        $component->viewdep =       $array->viewdep;
        $component->categorydep=    $array->categorydep;
        $component->aoii =          $array->aoii;
        $component->flagnotif =     $array->flagnotif;
        $component->viewnotif =     $array->viewnotif;
        $component->fragility =     $array->fragility;
        $component->accpack =       $array->accpack;
        $component->viewaccpack=    $array->viewaccpack;
        $component->weightdep =     $array->weightdep;
        $component->tdv =           $array->tdv;
        $component->codamount =     $array->codamount;
        $component->note =          $array->note;

        return $component;
    }

}
