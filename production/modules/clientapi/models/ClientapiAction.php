<?php

class ClientapiAction extends CAction{
    public function run(){
        
        $this->getController()->render('application.components.views.api.api');
    }
    public function transferToAPI(){
        $client = Yii::app()->user->auth;
        $data = array(
                 array(
                    'code' => '4',
                    'weight' => 2,
                    'oc' => 1,
                    'np' => 1,
                    'address' => 'Большая Семеновская',
                    'zip' => '140320',
                    'status' => 1,
                    'operation' => 2,
                    'date' => date('Y-m-d H:i'),
                    'placeOp' => 'мегаполис СЦ',
                 )
        );
        $array['data']               = $data;
        $array['client']['contract'] = $client->contract;
        $array['client']['secret']   = $client->Secretkey; 
        
        $url = 'api.hydra.megapolis.loc/load/load/MultiTrack';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, array(
            'data' => json_encode($array['data']),
            'client[contract]' => $array['client']['contract'],
            'client[secret]'   => $array['client']['secret'],
        ));
        $result = curl_exec ($ch);
        print_R($result);
        exit;
        curl_close ($ch);
        return $result;
    }
}