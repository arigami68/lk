<?php

class clientapiModule extends CWebModule{
    public function init()
    {
        $this->setImport(array(
            'clientapi.models.*',
            'clientapi.components.*',
        ));
    }
    
}