<?php

class ContractController extends UController
{
    public function init(){
        $this->breadcrumbs['Компания'] = array('links' => '');
        $this->title = 'Договора и дополнительные соглашения';
    }
    public function actions()
    {
        $this->breadcrumbs['Договора'] = array('links' => '');
        return array(
            'contract'=>array(
              'class'=>
                 'application.modules.contract.models.ContractAction',
            ),
        );
    }
}