<?php

class ContractAction extends CAction{
    public function run(){
        $user = Yii::app()->user->auth->id;
        $dir = 'contracts';
        $modules_dirs = scandir(Yii::getPathOfAlias('file').DS.$user.DS.$dir);
        $files= array();
        if($modules_dirs){
            foreach ($modules_dirs as $module){
                if ($module[0] == ".") continue;
                $files[] = mb_convert_encoding($module, 'UTF-8', 'WINDOWS-1251');
            }
            foreach ($files as &$file){
                $m =  rawurlencode($file);
                $file = '<a href=production/file/'.Yii::app()->user->auth->id.'/'.$dir.'/'.$m.'>'.$file.'</a>';
            }
        }
        return $this->getController()->render('application.components.views.contract.contract', array('files' => $files));
    }
}