<?php

class GetcurController extends UController{
    public function init(){
        $this->breadcrumbs['Отправления'] = array('links' => '');
        $this->breadcrumbs['Заявка на вызов курьера'] = array('links' => '');
        $this->title = Yii::t("total", 'заявка на вызов курьера');
    }
    public function actions(){

        return array(
            'getcur'=>array(
              'class'=>
                 'application.modules.getcur.models.Getcur2Action',
           ),
            'app'=>array(
              'class'=>
                 'application.modules.getcur.models.AppAction',
           ),
        );
    }

}