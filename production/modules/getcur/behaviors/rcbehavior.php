<?php

class rcbehavior extends CActiveRecordBehavior{
    public function beforeSave($event) {
        parent::beforeSave($event);
        $this->owner->client = Yii::app()->user->auth->id;
    }
    public function search(){
        $criteria = new CDbCriteria;
        $criteria->compare('statusRequest', RCDB::$RC_STATUS_REQUEST_OPEN);
        $criteria->compare('statusRequest', RCDB::$RC_STATUS_REQUEST_MODIFIED, false, 'OR');
        $criteria->compare('statusRequest', RCDB::$RC_STATUS_REQUEST_LOCK, false, 'OR');
        $criteria->scopes = array('user');
        $pagination = new MegapolisPagination();
        $pagination->pageVar = 'page';
        $pagination->pageSize = 10;

        $sort = new CSort();
        $sort->defaultOrder='id DESC';

        return new CActiveDataProvider(get_class($this->owner), array(
            'criteria'=>$criteria,
            'pagination'=> $pagination,
            'sort' => $sort,
        ));
    }
    public function criteriaGetcur($id, $client_id){
        $criteria = new CDbCriteria();
        $criteria->select = "*";
        $criteria->condition = 'id='.$id.' AND client='.$client_id;

        return $criteria;
    }
    public static function view_getcur_te($data){
        $data->statusRequest = RCDB::$RC_STATUS_REQUEST_MODIFIED;
        $oppmode = RCDB::oppMode($data);
        $img = array(0 => '/megapolis-stop.gif', 1 => '/megapolis_state_tick.png');
        $src =  $oppmode ? 's_read icon-off' :'icon-lock';
        $array = array(
            'read' => array(
                'htmloption' =>  array('title' => $oppmode ? 'У Вас есть возможность редактирования заявки' : 'У Вас нет возможности редактирования заявки', 'class' => $src),
            ),
        );
        return (
                CHtml::link('', '', $array['read']['htmloption'])
        );
    }
    public static function unloadInAppForm(RCDB $object, AppForm $form){
        $form->attributes = $object->attributes;
        $form->dateApp = date('Y-m-d', strtotime($object->dateApp));
        $form->timeStart = (int) date('H', strtotime($object->timeStart));
        $form->timeEnd = (int) date('H', strtotime($object->timeEnd));
        $form->date = date('d-m-Y', strtotime($object->timeStart));
        return $form;
    }
}