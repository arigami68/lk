<?php

class AppForm extends CFormModel
{
    public $occupancy, $date, $dateApp, $timeStart, $timeEnd, $addressOfLoad, $contactName, $phone, $shouldBeSkipped, $note;
    public function attributeLabels()
    {
        return array(
            'date'              => Yii::t("total", 'дата'),
            'dateApp'           => Yii::t("total", 'дата заявки'),
            'timeStart'         => Yii::t("total", 'С'),
            'timeEnd'           => Yii::t("total", 'По'),
            'occupancy'         => Yii::t("total", 'объем'),
            'addressOfLoad'     => Yii::t("total", 'адрес пункта погрузки'),
            'contactName'       => Yii::t("total", 'контактное лицо'),
            'phone'             => Yii::t("total", 'телефон'),
            'shouldBeSkipped'   => Yii::t("total", ''),
            'note'              => Yii::t("total", 'примечание'),
        );
    }
    public function rules()
    {
            return array(
                array('occupancy, date, timeStart, timeEnd, addressOfLoad, contactName, phone', 'required', 'message'=> 'Введите текст'),
                array('shouldBeSkipped, note, dateApp', 'safe'),
                array('date', 'date', 'format'=>'dd-MM-yyyy', 'message'=> 'Введите дату в формате год-месяц-день c ведущим нулем'),
                #array('date', 'oppMode', 'message'=> 'Не верная дата погрузки. Попробуйте сместить дату вперед!'),
                array('occupancy, timeStart, timeEnd', 'numerical'),
            );
    }
    public function afterValidate(){
        parent::afterValidate();
        if(!$this->errors){
            $this->timeStart = date('d-m-Y H:i:s', strtotime($this->date) + $this->timeStart * 3600);
            $this->timeEnd   = date('d-m-Y H:i:s', strtotime($this->date) + $this->timeEnd * 3600);
            $this->dateApp   = date('d-m-Y H:i:s');
        }
    }
}