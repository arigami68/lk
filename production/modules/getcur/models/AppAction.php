<?php
        
class AppAction extends CAction{
    public function run($edit = 0){
        if(true){
            $app = new stdClass();
            $app->request = Yii::app()->request;
            $app->form_name = Yii::import('application.modules.getcur.models.AppForm');
            $app->form = new $app->form_name;
            $app->db_name = Yii::import('application.components.db.RCDB');
            $app->db = new $app->db_name;
            $app->db->attachBehavior('rc', array('class' => 'application.modules.getcur.behaviors.rcbehavior'));
            $app->db->form = &$app->form;
        }

        if($app->request->isPostRequest){
            $app->form->attributes = $app->request->getParam($app->form_name);
            if($app->form->validate()){
                $app->db->attributes = $app->form->attributes;
                if($app->param = $app->request->getQuery($app->db_name)){
                    $app->criteria = $app->db->criteriaGetcur($app->param['id'], Yii::app()->user->auth->id);

                    if($app->db = $app->db->find($app->criteria)){
                        $app->db->attributes = $app->form->attributes;
                        if($app->param['status'] == 2){ $app->db->statusRequest = RCDB::$RC_STATUS_REQUEST_MODIFIED;
                        }else{ $app->db->statusRequest = RCDB::$RC_STATUS_REQUEST_OPEN;
                        }
                    }else{
                        return $this->getController()->render('application.components.views.getcur.getcur2', array('model' => $app->db));
                    }
                }
                if($app->db->save()){
                    Yii::app()->user->setFlash('success', Yii::t("total", 'заявка сохранена'));
                }else{
                    $app->form->addError('date', 'Не верная дата погрузки. Попробуйте сместить дату вперед! ');
                    Yii::app()->user->setFlash('success', Yii::t("total", 'не верная дата погрузки'));
                    return $this->getController()->render('application.components.views.getcur.app2', array('model' => $app->form));
                }

                return $this->getController()->redirect('getcur');
            }else{
                Yii::app()->user->setFlash('error', Yii::t("total", 'не все поля были заполнены'));
                return $this->getController()->render('application.components.views.getcur.app2', array('model' => $app->form));
            }
        }
        
        if($app->param = $app->request->getParam($app->db_name)){
            $app->form_name = Yii::import('application.modules.getcur.models.AppForm');
            $app->form = new $app->form_name;
            
            $app->criteria = $app->db->criteriaGetcur($app->param['id'], Yii::app()->user->auth->id);
            
            if($app->find = $app->db->find($app->criteria)){
                        return $this->getController()->render('application.components.views.getcur.app2', array('model' =>  Rcbehavior::unloadInAppForm($app->find, $app->form)));
            }
        }

        return $this->getController()->render('application.components.views.getcur.app2', array('model' => $app->form));
    }
}