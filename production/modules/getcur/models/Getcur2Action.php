<?php
class Getcur2Action extends CAction{
    
    public function run(){

        $getcur = new stdClass();
        $getcur->request = Yii::app()->request;
        $getcur->db_name = Yii::import('application.components.db.RCDB');
        $getcur->db = new $getcur->db_name;
        $getcur->db->attachBehavior('rc', array('class' => 'application.modules.getcur.behaviors.rcbehavior'));
        $app = new StdClass();
        # Поставить фильтр на данные
            if($getcur->param = $getcur->request->getQuery($getcur->db_name)){

                $getcur->criteria = $getcur->db->criteriaGetcur($getcur->param['id'], Yii::app()->user->auth->id);

                if($getcur->find = $getcur->db->find($getcur->criteria)){
                    if($getcur->param['status'] == RCDB::$RC_STATUS_REQUEST_OPEN){

                        $getcur->find->statusRequest = RCDB::$RC_STATUS_REQUEST_OPEN;

                        $app->form_name = Yii::import('application.modules.getcur.models.AppForm');

                        $app->form = new $app->form_name;
                        $app->form->attributes = $getcur->find->attributes;
                        return $this->getController()->render('application.components.views.getcur.app_disable2', array('model' => $app->form));
                        
                    }elseif($getcur->param['status'] == RCDB::$RC_STATUS_REQUEST_DELETE){

                        $getcur->find->statusRequest = RCDB::$RC_STATUS_REQUEST_DELETE;
                        $getcur->find->update();
                        
                        return $this->getController()->redirect('getcur');
                    }elseif($getcur->param['status'] == RCDB::$RC_STATUS_REQUEST_MODIFIED){
                        $this->statusRequest = RCDB::$RC_STATUS_REQUEST_MODIFIED;
                        $app->form_name = Yii::import('application.modules.getcur.models.AppForm');
                        $app->form = new $app->form_name;
                        $app->form->attributes = $getcur->find->attributes;
                        
                        return $this->getController()->render('application.components.views.getcur.app_disable2', array('model' => $app->form));
                    }elseif($getcur->param['status'] == RCDB::$RC_STATUS_REQUEST_LOCK){
                        $getcur->find->statusRequest = RCDB::$RC_STATUS_REQUEST_LOCK;
                        if(!$getcur->find->update()){
                            Yii::app()->user->setFlash('true', 'Заявка №'.$getcur->find->id.' не может быть закрыта');
                        }else{
                            Yii::app()->user->setFlash('true', 'Заявка №'.$getcur->find->id.' закрыта');
                        }

                        return $this->getController()->redirect('getcur');

                    }
                }else{
                    # Выбросить что запись не найдена у этого клиента
                }
            }
          return $this->getController()->render('application.components.views.getcur.getcur2', array('model' => $getcur->db));
    }
}