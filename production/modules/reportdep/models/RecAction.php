<?php

ini_set("memory_limit","512M");
ini_set('max_execution_time', 100);
class RecAction extends CAction{
    public function run(){
        Yii::app()->controller->layout = 'application.themes.classic.layouts.partial';
        $list = Yii::app()->request->getQuery('list');
        $report = '';
        $db = new ORDS;

        if(isset($_POST['excel'])){
            if($list =Yii::app()->request->getQuery('list')){
                if($list == 1){ return $this->writeXls($db->QuSe($db::$SEARCH_AGENT));}
                if($list == 2){ return $this->writeXls2($db->QuSe($db::$SEARCH_STATUS));}
                if($list == 3){ return $this->writeXls3($db->QuSe($db::$SEARCH_RETURNS));}
            }
        }

        if($list == 1){
            $report = $this->getController()->render('application.components.views.rd.report_1', array('model' => $db));
        }elseif($list == 2){
            $report = $this->getController()->render('application.components.views.rd.report_2', array('model' => $db));
        }elseif($list == 3){
            $report = $this->getController()->render('application.components.views.rd.report_3', array('model' => $db));
        }else{
            exit;
        }
        return $report;
    }
    public function writeXls(CActiveDataProvider $model){

        $model->pagination->pageSize = $model->totalItemCount;
        $objPHPExcel = MegapolisExcel::get()->write();
        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
        );

        $start = microtime(true);
        if(true){
            $idkm=1;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $idkm, '№ п/п');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $idkm, 'Номер ОД');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $idkm, 'Клиентский номер');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $idkm, 'Дата приема');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $idkm, 'Наименование отправителя');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $idkm, 'ОЦ, руб');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $idkm, 'НП, руб');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $idkm, 'Наименование получателя');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $idkm, 'Адрес доставки');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $idkm, 'Телефон');
        }
        #print (!function_exists('memory_get_usage')) ? '' : round(memory_get_usage()/1024/1024, 2) . 'MB'.'<br>';
        if($model->modelClass == 'ORDS'){
            foreach($model->getData() as $k=>$v){
                $address = MegapolisFunction::get()->implodeAddress($v->region, $v->area, $v->city);

                $idk = $k + 1 + $idkm;
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $idk, $idk- $idkm);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $idk, $v->megapolis);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $idk, $v->agent);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $idk, date('d-m-Y H:i', $v->dateCreate));
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $idk, Yii::app()->user->auth->name);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $idk, $v->costPublic);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $idk, $v->costDelivery);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $idk, $v->fio);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $idk, $address);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $idk, $v->phone);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $idk, $v->price);
            }
        }
        #print (!function_exists('memory_get_usage')) ? '' : round(memory_get_usage()/1024/1024, 2) . 'MB'.'<br>';
        #$time = microtime(true) - $start;
        #printf('Скрипт выполнялся %.4F сек.', $time);
        $objPHPExcel->getActiveSheet()->getStyle("A1:J".$idk)->applyFromArray($styleArray);
        return MegapolisExcel::get()->output($objPHPExcel);
    }
    public function writeXls2(CActiveDataProvider $model){

        $model->pagination->pageSize = $model->totalItemCount;
        $objPHPExcel = MegapolisExcel::get()->write();
        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
        );
        if(true){
            $idkm=1;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $idkm, '№ п/п');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $idkm, 'Идентификатор почтового отправления');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $idkm, 'Идентификационный номер, присвоенный отправлению Клиентом');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $idkm, 'Дата приема отправления от отправителя');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $idkm, 'Наименование отправителя');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $idkm, 'Объявленная ценность, руб.');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $idkm, 'Сумма наложенного платежа, руб.');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $idkm, 'Наименование получателя');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $idkm, 'Адрес доставки');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $idkm, 'телефон');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $idkm, 'Последний статус');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, $idkm, 'Дата последнего статуса');
        }
        foreach($model->getData() as $k=>$v){
	        $trace = TRAC::model()->code($data->agent)->last()->find();
            $address = MegapolisFunction::get()->implodeAddress($v->region, $v->area, $v->city);
            $time = MegapolisTime::get()->timeToConvert(MegapolisTime::$TIME_YEAR, MegapolisTime::$TIME_DEFAULT_FULL, new DateTime(date('Y-m-d H:i', $v['dateCreate'])));
            $idk = $k+$idkm+1;

            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $idk, $idk-1);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $idk, $v['megapolis']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $idk, $v['agent']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $idk, $time);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $idk, Yii::app()->user->auth->name);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $idk, $v['costPublic']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $idk, $v['costDelivery']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $idk, $v['fio']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $idk, $address);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $idk, $v['phone']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $idk, $trace ? $trace->status : '');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, $idk, $trace ? date('m-d-Y', strtotime($trace->dateD)) : '');
        }
        $objPHPExcel->getActiveSheet()->getStyle("A1:L".$idk)->applyFromArray($styleArray);
        return MegapolisExcel::get()->output($objPHPExcel);
    }
    public function writeXls3(CArrayDataProvider $model){
        $model->pagination->pageSize = $model->totalItemCount;
        $objPHPExcel = MegapolisExcel::get()->write();
        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
        );
        if(true){
            $idkm=1;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $idkm, '№ п/п');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $idkm, 'Идентификатор почтового отправления');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $idkm, 'Идентификационный номер, присвоенный отправлению Клиентом');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $idkm, 'Дата приема отправления от отправителя');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $idkm, 'Наименование отправителя');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $idkm, 'Объявленная ценность, руб.');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $idkm, 'Сумма наложенного платежа, руб.');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $idkm, 'Наименование получателя');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $idkm, 'Адрес доставки');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $idkm, 'телефон');
        }
        foreach($model->rawData as $k=>$v){
            $idk = $k+1;
            $date = MegapolisTime::get()->timeToConvert(MegapolisTime::$TIME_YEAR, MegapolisTime::$TIME_DEFAULT_FULL, new DateTime($v['dateCreate']));
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $idk, $idk-1);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $idk, $v['megapolis']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $idk, $v['agent']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $idk, $date);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $idk, Yii::app()->user->auth->name);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $idk, $v['oc']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $idk, $v['np']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $idk, $v['fio']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $idk, $v['address']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $idk, $v['phone']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $idk, $v['status']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, $idk, $v['status_date']);
        }
        $objPHPExcel->getActiveSheet()->getStyle("A1:J".$idk)->applyFromArray($styleArray);
        return MegapolisExcel::get()->output($objPHPExcel);
    }

}
