<?php
class ReportAction extends CAction{
    public function run(){
        $db = new ORDS;
        $form = new ReportForm;
        $form->list = Array(
            Yii::t("total", 'по присвоению номерам отправлений'),
            Yii::t("total", 'статусы отправлений'),
            Yii::t("total", 'возвраты'),
        );
        $form->date_s = date('d-m-Y');
        $form->date_f = date('d-m-Y');

        $form->date_s = Yii::app()->request->getQuery('s');
        $form->date_f = Yii::app()->request->getQuery('f');

        if(isset($_POST['excel'])){
            if($list =Yii::app()->request->getQuery('list')){
                if($list == 1){ return $this->writeXls(ORDS::search_agent_s1());}
                if($list == 2){ return $this->writeXls2($model->QuSe($model::$SEARCH_STATUS));}
                if($list == 3){ return $this->writeXls2($db->search_s1());}
            }
        }

        if($form_r = Yii::app()->request->getParam('ReportForm')){
            if(empty($form_r['date_s'])){
                Yii::app()->user->setFlash('success', 'Заполните пожалуйста дату!');
                $form_r = NULL;
            }
            if(empty($form_r['date_f'])){
                Yii::app()->user->setFlash('success', 'Заполните пожалуйста дату!');
                $form_r = NULL;
            }
        }elseif($form_r =Yii::app()->request->getQuery('list')){

            return $this->getController()->render('application.components.views.rd.report', array('model' => $form, 'value' => isset($form_r['list']) ? $form_r['list'] : ''));
            $form_r = NULL;
        }else{
            $form_r = NULL;
        }


        if($form_r){
            $value = $form_r['list'] + 1;
            $date_s = $form_r['date_s'];
            $date_f = $form_r['date_f'];

            return $this->getController()->redirect('rd?list='.$value.'&s='.$date_s.'&f='.$date_f);
        }

        return $this->getController()->render('application.components.views.rd.report', array('model' => $form));
    }
    public function readXls(){
        
        spl_autoload_unregister(array('YiiBase', 'autoload'));
        $phpExcelPath = Yii::getPathOfAlias('extensions.php-excel.Classes');
        include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');
        spl_autoload_register(array('YiiBase', 'autoload'));
 
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator("user")
            ->setLastModifiedBy("ИдеаЛоджик")
            ->setTitle("ИдеаЛоджик")
            ->setSubject("Отчет об отправлениях")
            ->setDescription("Отчет")
            ->setKeywords("repost")
            ->setCategory("report");
        return $objPHPExcel;
    }
    public function writeXls(CActiveDataProvider $model){
        $model->pagination->pageSize = $model->totalItemCount;
        $objPHPExcel = $this->readXls();
        if(true){
            $idkm=1;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $idkm, '№ п/п');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $idkm, 'Номер ОД');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $idkm, 'Клиентский номер');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $idkm, 'Дата приема');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $idkm, 'Наименование отправителя');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $idkm, 'ОЦ, руб');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $idkm, 'НП, руб');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $idkm, 'Наименование получателя');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $idkm, 'Адрес доставки');
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $idkm, 'Телефон');
        }
        if($model->modelClass == 'ORDS'){
                foreach($model->getData() as $k=>$v){
                    $idk = $k + 1 + $idkm;
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $idk, $idk- $idkm);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $idk, $v->megapolis);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $idk, $v->agent);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $idk, date('d-m-Y H:i', $v->dateCreate));
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $idk, Yii::app()->user->auth->name);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $idk, $v->costPublic);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $idk, $v->costDelivery);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $idk, $v->fio);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $idk, $v->address);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $idk, $v->phone);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $idk, $v->price);
                }
        }
        $objPHPExcel->getActiveSheet()->getStyle("A1:J".$idk)->applyFromArray($styleArray);
        $objPHPExcel = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");;
        header("Content-Disposition: attachment;filename=ideaLogic ".date('Y-m-d H:i').".xls"); 
        header("Content-Transfer-Encoding: binary ");
        return $objPHPExcel->save('php://output');
    }
   public function readXls2(){

        spl_autoload_unregister(array('YiiBase', 'autoload'));
        $phpExcelPath = Yii::getPathOfAlias('extensions.php-excel.Classes');
        include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');
        spl_autoload_register(array('YiiBase', 'autoload'));

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator("user")
            ->setLastModifiedBy("ИдеаЛоджик")
            ->setTitle("ИдеаЛоджик")
            ->setSubject("Отчет об отправлениях")
            ->setDescription("Отчет")
            ->setKeywords("repost")
            ->setCategory("report");
        return $objPHPExcel;
    }
    public function writeXls2(CArrayDataProvider $model){
        $model->pagination->pageSize = $model->totalItemCount;
        $objPHPExcel = $this->readXls();
                foreach($model->rawData as $k=>$v){
                    $idk = $k;
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $idk, $idk);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $idk, $v['megapolis']);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $idk, $v['agent']);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $idk, $v['dateCreate']);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $idk, Yii::app()->user->auth->name);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $idk, $v['oc']);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $idk, $v['np']);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $idk, $v['fio']);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $idk, $v['address']);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $idk, $v['phone']);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $idk, $v['status']);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, $idk, $v['status_date']);
                }
        $objPHPExcel = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");;
        header("Content-Disposition: attachment;filename=ideaLogic ".date('Y-m-d H:i').".xls");
        header("Content-Transfer-Encoding: binary ");
        return $objPHPExcel->save('php://output');
    }
    public function readXls3(){
        spl_autoload_unregister(array('YiiBase', 'autoload'));
        $phpExcelPath = Yii::getPathOfAlias('extensions.php-excel.Classes');
        include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');
        spl_autoload_register(array('YiiBase', 'autoload'));

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator("user")
            ->setLastModifiedBy("ИдеаЛоджик")
            ->setTitle("ИдеаЛоджик")
            ->setSubject("Отчет об отправлениях")
            ->setDescription("Отчет")
            ->setKeywords("repost")
            ->setCategory("report");
        return $objPHPExcel;
    }
    public function writeXls3(CArrayDataProvider $model){
        $model->pagination->pageSize = $model->totalItemCount;
        $objPHPExcel = $this->readXls();
        foreach($model->rawData as $k=>$v){
            $idk = $k;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $idk, $idk);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $idk, $v['megapolis']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $idk, $v['agent']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $idk, $v['dateCreate']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $idk, Yii::app()->user->auth->name);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $idk, $v['oc']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $idk, $v['np']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $idk, $v['fio']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $idk, $v['address']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $idk, $v['phone']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $idk, $v['status']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, $idk, $v['status_date']);
        }
        $objPHPExcel = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");;
        header("Content-Disposition: attachment;filename=ideaLogic ".date('Y-m-d H:i').".xls");
        header("Content-Transfer-Encoding: binary ");
        return $objPHPExcel->save('php://output');
    }
}
