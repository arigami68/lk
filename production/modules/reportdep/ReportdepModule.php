<?php

# @info - Отчет:Отправления
class reportdepModule extends CWebModule{
    public function init()
    {
        $this->setImport(array(
            'reportdep.models.*',
            'reportdep.components.*',
        ));
    }
}