<?php

class ReportController extends UController
{

    public function init(){
        $this->breadcrumbs['Отправления'] = array('links' => '');
        $this->breadcrumbs['Отчет'] = array('links' => '');
        $this->title = '&nbsp';
    }
    public function actions()
    {

        return array(
            'report'     => array( 'class'=> 'application.modules.reportdep.models.ReportAction'),
            'rec' => array( 'class'=> 'application.modules.reportdep.models.RecAction'),
        );
    }
}