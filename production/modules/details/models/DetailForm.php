<?php

class DetailForm extends CFormModel
{
    public $fullname, $shortname, $orgform, $addresslegal, $addressfact, $inn, $kpp, $okpo, $calculatedinvoice, $correspondentinvoice, $bik, $fiohead, $fioaccountant, $addressdocuments, $tel;
    public $okogy, $okato, $okbed, $okfc, $ogph, $namebank, $positionhead, $fax, $mail_np, $mail_notice, $www;
    
    public function rules()
    {
            return array(
                array('fullname, shortname, orgform, addresslegal, addressfact, inn, kpp, okpo, calculatedinvoice, correspondentinvoice, bik, fiohead, fioaccountant, addressdocuments, tel', 'required', 'message'=> 'Введите текст'),
                array('okogy, okato, okbed, okfc, ogph, namebank, positionhead, fax, mail_np, mail_notice, www', 'safe')
            );
    }
    public function attributeLabels()
    {
        return array(
            'fullname'              => Yii::t("total", "полное наименование организации"),
            'shortname'             => Yii::t("total", "краткое наименование организации"),
            'orgform'               => Yii::t("total", "организационно-правовая форма"),
            'addresslegal'          => Yii::t("total", "адрес юридический"),
            'addressfact'           => Yii::t("total", "адрес фактический"),
            'inn'                   => Yii::t("total", "ИНН"),
            'kpp'                   => Yii::t("total", "КПП (только для ООО)"),
            'okpo'                  => Yii::t("total", "ОКПО"),
            'calculatedinvoice'     => Yii::t("total", "Расчетный счет"),
            'correspondentinvoice'  => Yii::t("total", "Корреспондентский счет"),
            'bik'                   => Yii::t("total", "БИК"),
            'okato'                 => Yii::t("total", "ОКАТО"),
            'fiohead'               => Yii::t("total", "Ф.И.О. руководителя организации"),
            'fioaccountant'         => Yii::t("total", "Ф.И.О. главного бухгалтера"),
            'addressdocuments'      => Yii::t("total", "Адрес для доставки документов (счетов, с/ф, актов)"),
            'tel'                   => Yii::t("total", "Телефон"),
            'okogy'                 => Yii::t("total", "ОКОГУ"),
            'okat'                  => Yii::t("total", "ОКАТО"),
            'okbed'                 => Yii::t("total", "ОКВЭД"),
            'okfc'                  => Yii::t("total", "ОКФС/ОКОПФ"),
            'ogph'                  => Yii::t("total", "ОГРН/ОГРНИП"),
            'namebank'              => Yii::t("total", "Наименование банка, в т.ч. место (город) нахождения"),
            'positionhead'          => Yii::t("total", "Должность руководителя организации"),
            'fax'                   => Yii::t("total", "Факс"),
            'mail_np'               => Yii::t("total", "E-mail реестров для наложенных платежей"),
            'mail_notice'           => Yii::t("total", "E-mail для уведомлений"),
            'www'                   => Yii::t("total", "www"),
        );
    }
}