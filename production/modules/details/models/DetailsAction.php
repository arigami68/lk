<?php

class DetailsAction extends CAction{
    public function run(){
        $model = new DetailForm();
        $user = Yii::app()->user->auth->id;
        $dir = 'contracts';
        ($DETA = DETA::model()->user(Yii::app()->user->auth->id)->find()) ? $model->attributes = $DETA->attributes : $DETA = new DETA();
        if(Yii::app()->request->isPostRequest){
            $model->attributes= $_POST['DetailForm'];
            if($model->validate()){
                $DETA->attributes = $model->attributes;
                $DETA->user = Yii::app()->user->auth->id;
                $DETA->dateUpdate = date("Y-m-d H:i:s");
                $DETA->save();
                Yii::app()->user->setFlash('error', Yii::t("total", 'реквизиты успешно сохранены'));
                $this->getController()->refresh();
            }else{
                Yii::app()->user->setFlash('error', Yii::t("total", 'не все поля были заполнены'));
            }
        }

        $modules_dirs = scandir(Yii::getPathOfAlias('file').DS.$user.DS.$dir);
        $files= array();
        if($modules_dirs){
            foreach ($modules_dirs as $module){
                if ($module[0] == ".") continue;
                $files[] = $module;
            }
            $result = '<ul>';
            foreach ($files as $file){
                $m =  rawurlencode($file);

                $result.= '<li><a href=production/file/'.Yii::app()->user->auth->id.'/'.$dir.'/'.$m.'>'.$file.'</a>';
            }
            $result.= '</ul>';
        }

        $this->getController()->render('application.components.views.details.details', array('model' => $model, 'string' => $result));
    }
}