<?php

class detailsController extends UController
{
    public function init(){
        $this->breadcrumbs['Компания'] = array('links' => '');
        $this->title = Yii::t("total", "настройки личного кабинета");
    }
    public function actions()
    {
        $this->breadcrumbs['Реквизиты'] = array('links' => '');
        return array(
            'details' => array(
              'class'=>
                 'application.modules.details.models.DetailsAction',
            ),
       );
    }
}