<?php

class FileForm extends CFormModel
{
    public $file;
    
    public function rules()
    {
            return array(
                array('file', 'file', 'types'=> 'xml, xls, xlsx', 'message'=>'Загрузите файл'),
            );
    }
    public function attributeLabels()
    {
        return array(
            'file' => 'Загрузка файла'
        );
    }
}