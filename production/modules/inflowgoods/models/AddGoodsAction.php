<?php

class AddGoodsAction extends CAction{
    public function run(){
        $goods = new stdClass();
        $goods->request = Yii::app()->request;
        $goods->array['len'] = $goods->request->getParam('goods') ? $goods->request->getParam('goods') : 1;

        $model_name = Yii::import('application.modules.inflowgoods.models.IwForm');
        $model = new $model_name;
        ob_start();
        $form= $this->getController()->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id'=>'location-form',
        'type'=>'horizontal',
        ));
        ob_end_clean();
        $text_add1 = '';
        $text_add1.= '<div class="delete"><a class=tovar'.$goods->array['len'].' href=#>Удалить</a></div>';
        $text_add1.= $form->textFieldRow($model,'internal',array('rows'=>2, 'class'=>'span7', 'name' => 'IwForm[internal]['.$goods->array['len'].']'));
        $text_add1.= $form->textFieldRow($model,'artical',array('rows'=>2, 'class'=>'span7', 'name' => 'IwForm[artical]['.$goods->array['len'].']'));
        $text_add1.= $form->textFieldRow($model,'nomencl',array('rows'=>2, 'class'=>'span7', 'name' => 'IwForm[nomencl]['.$goods->array['len'].']'));
        $text_add1.= $form->textFieldRow($model,'unit',array('rows'=>2, 'class'=>'span7', 'name' => 'IwForm[unit]['.$goods->array['len'].']'));
        $text_add1.= $form->textFieldRow($model,'amount',array('rows'=>2, 'class'=>'span7', 'name' => 'IwForm[amount]['.$goods->array['len'].']'));
        $text_add1.= $form->textFieldRow($model,'woVAT',array('rows'=>2, 'class'=>'span7', 'name' => 'IwForm[woVAT]['.$goods->array['len'].']'));
        $text_add1.= $form->textFieldRow($model,'wiVAT',array('rows'=>2, 'class'=>'span7', 'name' => 'IwForm[wiVAT]['.$goods->array['len'].']'));
        $text_add1.= $form->textFieldRow($model,'rateVAT',array('rows'=>2, 'class'=>'span7', 'name' => 'IwForm[rateVAT]['.$goods->array['len'].']'));
        $text_add1.= $form->textFieldRow($model,'cost',array('rows'=>2, 'class'=>'span7', 'name' => 'IwForm[cost]['.$goods->array['len'].']'));
        $text_add1.= $form->textFieldRow($model,'note',array('rows'=>2, 'class'=>'span7', 'name' => 'IwForm[note]['.$goods->array['len'].']'));
        $text= '<div id="yw3_tab_'.$goods->array['len'].'" class="tab-pane">'.$text_add1.'</div>';
        
        die($text);
        
    }
}