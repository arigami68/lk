<?php

class InflowgoodsAction extends CAction{
    public function run(){
        $iw = new stdClass();
        # Читать ID
        $iw->request = Yii::app()->request;
        $iw->formname = ($iw->request->getQuery('auto') !== NULL) ? Yii::import('application.modules.inflowgoods.models.FileForm') : Yii::import('application.modules.inflowgoods.models.IwForm');
        $iw->form = new $iw->formname;
        
        if($iw->request->getParam('article')){
            $param['article'] = $iw->request->getParam('article');
            $param['count'] = $iw->request->getParam('count');
            $param['wiVAT'] = $iw->request->getParam('wiVAT');
            $param['woVAT'] = $iw->request->getParam('woVAT');
            # Удалить одинаковые articul
            $array = $this->transfer($param);
            foreach($array as $k=>$v){
                $dep = $v['db'];
                
                $addon['amount'] = $v['amount'];
                $addon['wiVAT'] = $v['wiVAT'];
                $addon['woVAT'] = $v['woVAT'];
                
                $rend[] = $this->rendingComponentForm($dep, $addon);
            }
            
                $form_component = array();
                $dep = array();
                foreach($rend as $k=>$inflow){
                    
                    $dep[$k] = $inflow->db->attributes;
                    $dep[$k]['amount'] = $inflow->amount;
                    $dep[$k]['wiVAT'] = $inflow->wiVAT;
                    $dep[$k]['woVAT'] = $inflow->woVAT;
                    $dep[$k]['cost'] = $inflow->cost;
                    
                    $form_component['id']           = $inflow->id;
                    $form_component['contractor']   = $inflow->contractor;
                    $form_component['provider']     = $inflow->provider;
                    $form_component['internal'][]   = $inflow->internal;
                    $form_component['artical'][]    = $inflow->artical;
                    $form_component['nomencl'][]    = $inflow->nomencl;
                    $form_component['unit'][]       = $inflow->unit;
                    $form_component['amount'][]     = $inflow->amount;
                    $form_component['woVAT'][]      = $inflow->woVAT;
                    $form_component['wiVAT'][]      = $inflow->wiVAT;
                    $form_component['rateVAT'][]    = $inflow->rateVAT;
                    $form_component['cost'][]       = $inflow->amount * $inflow->wiVAT;
                    $form_component['note'][]       = $inflow->note;
                }
                
                $iw->form->attributes = $form_component;
                
                return $this->getController()->render('views.inflowgoods.inflowgoods', array('model' => $iw->form, 'dep' => $dep));
        }
        if($iw->request->getParam($iw->formname)){
            # Загрузка файла
            if($iw->request->getParam('FileForm')){
                $iw->form->file=CUploadedFile::getInstance($iw->form,'file');
                 if($iw->form->validate()){
                     
                     $iw->form->attachBehavior('extra', array(
                        'class' => 'application.modules.inflowgoods.behaviors.UploadedFileOrder',
                        'user'=> Yii::app()->user->auth,
                     ));
                     $iw->form->setFile($iw->form->file);
                     $iw->read_name =  Yii::import('application.modules.inflowgoods.behaviors.ReadXlsx');
                     $iw->read = new $iw->read_name;
                     $iw->read->file = $iw->form->file->tempName;
                     $iw->read->component = new App_storageComponent();
                     $iw->read->inputFileType = 'Excel2007';
                     $files = $iw->read->read();
                     
                     if(!$iw->inquiry = $iw->form->getFormatMegapolis($files)){
                        Yii::app()->user->setFlash('success', 'Ошибка при загрузки файла');
                        $this->getController()->refresh();
                    }                     
                    #header("Content-type: text/xml");
                     
                     $iw->answer->parseXML = $files->parseXML( FuncMegapolis::pushXml($iw->inquiry, MEGAPOLIS_API, 'product&secret='.Yii::app()->user->auth->secretkey) );
                     
                    if($iw->answer->parseXML == 129){
                        Yii::app()->user->setFlash('success', 'Добавлено в список');
                        $this->getController()->refresh();
                    }
                    if($iw->answer->parseXML == 130)
                        Yii::app()->user->setFlash('error', 'Ошибка в состовлении заявки');
                    if($iw->answer->parseXML == 128) 
                        Yii::app()->user->setFlash('info', 'Этот номер заявки уже используется');
                    
                     $this->getController()->refresh();
                 }
                 
            # Если обычная форма
            }elseif($iw->request->getParam('IwForm')){
                
                $iw->param = $iw->request->getParam('IwForm');
                
                $func = function($array){
                        if(!isset($array['internal'])) return 0;
                        foreach($array['internal'] as $k=>$v){
                                $return[$k]['id'] = $array['id'];
                                $return[$k]['contractor'] = $array['contractor'];
                                $return[$k]['provider'] = $array['provider'];
                            foreach($array as $k2=>$v2){
                                if($k2 == 'id' OR $k2 == 'contractor' OR $k2 == 'provider') continue;
                                if(isset($array[$k2][$k])){
                                    $return[$k][$k2] = $array[$k2][$k];
                                }
                            }
                        }
                        return isset($return) ? $return : 0;
                };
                
                if(!$dep = $func($iw->param)){
                    Yii::app()->user->setFlash('error', 'Ошибка в состовлении заявки');
                    $this->getController()->refresh();
                }
                $repeat = array();
                foreach($dep as $k=>$v){
                    $iw->form->attributes =  $v;
                    
                    if(isset($repeat[$v['artical']])){
                        $error[] = 1;
                    }else{
                        $repeat[$v['artical']] = 1;
                    }
                    
                    if($iw->form->validate()){
                        !isset($error) ? $error = NULL : '';
                    }else{
                       $error[$k] = $iw->form->getErrors();  
                    }   
                }
                
                if($error){
                    $iw->form->attributes = $iw->param;
                    Yii::app()->user->setFlash('error', 'Ошибка в состовлении заявки');
                    return $this->getController()->render('views.inflowgoods.inflowgoods', array('model' => $iw->form, 'dep' => $dep, 'errors' => $error));
                }
                
                $iw->component->app = new App_storageComponent();
                $iw->component->app->load($dep);
                $iw->xml->inquiry = $iw->component->app->convertToXML();
                
                if($apti = APTI::model()->find('id_idapplication=:id_idapplication', array('id_idapplication' => $iw->param['id']))){
                    if($apti->status != APTI::$APP_CREATE) return $this->getController()->redirect('getiw');
                }
                if(AHDB::undertow($iw->param['id'])){
                    $iw->xml->answer->parseXML = $iw->component->app->parseXML(FuncMegapolis::pushXml($iw->xml->inquiry, MEGAPOLIS_API, 'product&secret='.Yii::app()->user->auth->secretkey));
                }
                if($iw->xml->answer->parseXML == 129){
                    Yii::app()->user->setFlash('success', 'Добавлено в список');
                    $this->getController()->refresh();
                }
                if($iw->xml->answer->parseXML == 130)
                    Yii::app()->user->setFlash('error', 'Ошибка в состовлении заявки');
                if($iw->xml->answer->parseXML == 128){
                    $iw->form->attributes = $iw->param;
                    Yii::app()->user->setFlash('info', 'Этот номер заявки уже используется');
                    return $this->getController()->render('views.inflowgoods.inflowgoods', array('model' => $iw->form, 'dep' => $dep, 'errors' => $error ? $error : 0));
                }
            }
        }
        
        # Просмотр заявки
        if($iw->request->getParam('ASDB')){
            $iw->ASDB = $iw->request->getParam('ASDB');
            if(!isset($iw->ASDB['id'])) $this->getController()->redirect('getiw');
            
            if( $id = (int)$iw->ASDB['id']){
                $iw->db = new stdClass();
                $iw->db->AHDB = new AHDB();
                $criteria = new CDbCriteria();
                $criteria->compare('idapplication', $id);
                
                $AHDB = AHDB::model()->findAll($criteria);
                $array = array();
                foreach($AHDB as $k=> $v){
                    $array[$v->idapplication][] = array(
                        'storage' => ASDB::model()->findByPk($v->idstorage),
                        'number' => $v
                    );
                }
                
                $form_component = array();
                $dep = array();
               
                foreach($array as $k2=>$v2){
                    foreach($v2 as $k=>$v){
                        $value = $v['storage'];
                        $amount = $AHDB[$k]->number;
                        $woVAT  = $AHDB[$k]->woVAT;
                        $wiVAT  = $AHDB[$k]->wiVAT;
                        
                        $value->amount = $amount;
                        $dep[] = $value->attributes;
                        
                        $form_component['id'] = $k2;
                        $form_component['contractor'] = $value->contractor;
                        $form_component['provider'] = $value->provider;
                        $form_component['internal'][] = $value->internal;
                        $form_component['artical'][] = $value->artical;
                        $form_component['nomencl'][] = $value->nomencl;
                        $form_component['unit'][] = $value->unit;
                        $form_component['amount'][] = $amount;
                        $form_component['woVAT'][] = $woVAT;
                        $form_component['wiVAT'][] = $wiVAT;
                        $form_component['rateVAT'][] = $value->rateVAT;
                        $form_component['cost'][] = $amount * $wiVAT;
                        $form_component['note'][] = $value->note;
                    }
                }
                $iw->form->attributes = $form_component;
            }
            return $this->getController()->render('views.inflowgoods.inflowgoods', array('model' => $iw->form, 'dep' => $dep));
        }
        
        return $this->getController()->render('views.inflowgoods.inflowgoods', array('model' => $iw->form));
    }
    
    
    
    
    public function transfer($param){
        
        $article = $param['article'];
        $amount = $param['count'];
        $wiVAT = $param['wiVAT'];
        $woVAT = $param['woVAT'];
        $array = array();
        $article = array_unique($article);
        foreach($article as $k=>$art){
            $ASDB = ASDB::model()->user()->find('artical=:artical', array('artical' => $art));
            $array[$k]['db'] = $ASDB;
            $array[$k]['amount'] = $amount[$k];
            $array[$k]['wiVAT'] = $wiVAT[$k];
            $array[$k]['woVAT'] = $woVAT[$k];
        }
        
        return $array;
    }
    public function rendingComponentForm(ASDB $ASDB, $array){
        $inflow = new InflowComponent;
        
        $amount = $array['amount'];
        $wiVAT = $array['wiVAT'];
        $woVAT = $array['woVAT'];
        $rateVAT = $ASDB->rateVAT;
        $cost = $wiVAT * $amount;
        
        $inflow->id = '';
        $inflow->contractor = '';
        $inflow->provider = '';
        
        $inflow->store = $ASDB->store;
        $inflow->internal = $ASDB->internal;
        $inflow->artical = $ASDB->artical;
        $inflow->nomencl = $ASDB->nomencl;
        $inflow->unit = $ASDB->unit;
        $inflow->note = $ASDB->note;
        $inflow->db = $ASDB;
        
        $inflow->amount = $amount;
        $inflow->woVAT = $woVAT;
        $inflow->wiVAT = $wiVAT;
        $inflow->rateVAT = $rateVAT;
        $inflow->cost = $cost;
        
        return $inflow;
    }
}