<?php

class MappingAction extends CAction{
    public function run(){
        $mapping = new stdClass();
        $mapping->request = Yii::app()->request;
        $mapping->db = new ASDB();

        return $this->getController()->render('views.inflowgoods.inflowgoods_mapping', array('model' => $mapping->db));
    }
}