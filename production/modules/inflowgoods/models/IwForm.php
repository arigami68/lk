<?php

class IwForm extends CFormModel
{
    public $id, $contractor, $provider, $internal, $artical, $nomencl, $unit, $amount, $woVAT, $wiVAT, $rateVAT, $cost;
    public $note;
    
    public function rules()
    {
            return array(
                array('id, contractor, provider, internal, artical, nomencl, unit, amount, woVAT, wiVAT, rateVAT, cost', 'required', 'message'=> 'Введите текст'),
                array('amount, woVAT, wiVAT, rateVAT, cost, id', 'numerical', 'message'=> 'Введите число'),
                array('note' , 'safe'),
            );
    }
    public function attributeLabels()
    {
        return array(
            'id' => 'Номер заявки',
            'contractor' => 'Контрагент-поклажедатель',
            'provider' => 'Поставщик контрагента',
            'internal' => 'Код товара',
            'artical' => 'Артикул',
            'nomencl' => 'Номенклатура',
            'unit' => 'Единица хранения',
            'amount' => 'Планируемое количество',
            'woVAT' => 'Цена без НДС, руб',
            'wiVAT' => 'Цена с НДС, руб',
            'rateVAT' => 'Ставка НДС, %',
            'cost' => 'Стоимость, руб',
            'note' => 'Примечание',
        );
    }
}