<?php

class EditGoodsAction extends CAction{
    public function run(){
        $edit = new stdClass();
        $edit->request = Yii::app()->request;
        $edit->form = new EditForm();
        $edit->db = new APTI();
        
        if($get = $edit->request->getQuery(get_class($edit->db))){
            $idapplication = (int)$get['id'];
            
            $criteria = new CDbCriteria();
            $criteria->compare('id_clients', Yii::app()->user->auth->id);
            $criteria->compare('id_idapplication', $idapplication);
            if($APTI = APTI::model()->find($criteria)){
                $edit->form->id = $APTI->id_idapplication;
                if($APTI->status){
                    $edit->form->date_shipping = date('Y-m-d', strtotime($APTI->date_shipping));
                    $edit->form->edit = $APTI->status;
                }
                
            }else{
                # Не найдено такого номера заявки
                exit;
            }
            
            if($post = $edit->request->getParam(get_class($edit->form))){
                $edit->form->attributes = $post;
                if($edit->form->validate()){
                    if($APTI->status == APTI::$APP_CREATE){
                        $APTI->date_shipping = date('Y-m-d H:i:s', strtotime($edit->form->date_shipping));
                        $APTI->status = $edit->form->edit;
                        $APTI->update();
                        Yii::app()->user->setFlash('success', 'Обновление статуса заявки');
                        $this->getController()->refresh();
                    }else{
                        # Статус заявки уже меняли
                        Yii::app()->user->setFlash('info', 'Статус заявки уже меняли');
                        $this->getController()->refresh();
                    }
                }
            }
        }else{
            
            # Перенаправление на страницу назад
            exit;
        }
        
        return $this->getController()->render('views.inflowgoods.inflowgoods_edit', array('model' => $edit->form));
    }
}