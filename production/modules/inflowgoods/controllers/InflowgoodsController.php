<?php

class InflowgoodsController extends UController
{
    public function init(){
        $this->breadcrumbs['Склад'] = array('links' => '');
        $this->title = 'Заявки';
    }
    public function actions()
    {
        switch (Yii::app()->request->pathInfo) {
            case 'getiw':
                $this->breadcrumbs['Просмотр заявок'] = array('links' => '');
                break;
            case 'iw':
                $this->breadcrumbs['Заявка на поставку товара'] = array('links' => '');
                break;
        }
        
        return array(
            'inflowgoods'=>array(
              'class'=>
                 'application.modules.inflowgoods.models.InflowgoodsAction',
            ),
            'addgoods' => array(
              'class'=>
                 'application.modules.inflowgoods.models.AddGoodsAction',
            ),
            'editgoods' => array(
              'class'=>
                 'application.modules.inflowgoods.models.EditGoodsAction',
            ),
            'mapping' => array(
              'class'=>
                 'application.modules.inflowgoods.models.MappingAction',
            ),
            'deletegoods' => array(
              'class'=>
                 'application.modules.inflowgoods.models.DeleteGoodsAction',
            )
        );
    }
}