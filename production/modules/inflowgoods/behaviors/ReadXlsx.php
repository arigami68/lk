<?php

class ReadXlsx extends CBehavior{
    
    public $file;
    public $inputFileType;
    public $component;
    
    public function read(){
        spl_autoload_unregister(array('YiiBase', 'autoload'));
        $phpExcelPath = Yii::getPathOfAlias('extensions.php-excel.Classes');
        include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');
        spl_autoload_register(array('YiiBase', 'autoload'));
        $objReader = PHPExcel_IOFactory::createReader($this->inputFileType);
        $objReader->setLoadAllSheets();
        $objPHPExcel = PHPExcel_IOFactory::load($this->file);
        $components = $this->renderread($objPHPExcel);
        
        return $components;
    }
    public function renderread($objPHPExcel = 0){
        
        if(get_class($objPHPExcel) !== 'PHPExcel') return 0;
        
        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
        $array = array();
        $xml = array();
        $xml_count = array();
        
        if(!isset($sheetData[8]['A'])) return 0;
        for($a=10;$a;$a++){
            if(($sheetData[$a]['A']) == 'Итого:') break;
                $xml_count[] = $a;
        }
        if(!$xml_count) return 0;
        
        foreach($xml_count as $v=>$filled){
            unset($array);
            $array['id']         = !empty($sheetData['3']['B']) ? $sheetData['3']['B'] : '';
            $array['contractor'] = $sheetData['5']['E'];
            $array['provider']   = $sheetData['6']['E'];
            $array['internal']   = $sheetData[$filled]['B'];
            $array['artical']    = $sheetData[$filled]['C'];
            $array['nomencl']    = $sheetData[$filled]['E'];
            $array['unit']       = $sheetData[$filled]['F'];
            $array['amount']     = $sheetData[$filled]['G'];
            $array['woVAT']      = $sheetData[$filled]['H'];
            $array['wiVAT']      = $sheetData[$filled]['I'];
            $array['rateVAT']    = $sheetData[$filled]['J'];
            $array['cost']       = $sheetData[$filled]['K'];
            $array['note']       = $sheetData[$filled]['L'];
            
            $xml[] = $array;
        }
        
        $component = new $this->component;
        
        if(!($array['id'])){ # Исправить, тут полная жесть
            $criteria = new CDBcriteria;
            $criteria->select = 'max(id_idapplication) as id_idapplication';
            $model = APTI::model()->user(Yii::app()->user->auth->id)->find($criteria);

            if(!$model){
                $flag_id = 1;
            }else{
                
                $flag_id = $model->id_idapplication+1;
            }
        }else{
            $flag_id = $array['id'];
        }
       
        $component->id = $flag_id;
        foreach($xml as $k=>$array){
            $component->product[] = $this->getComponent($array, new $this->component);
        }
        return $component;
    }
    public function getComponent($array, $component){
        $component->contractor = isset($array['contractor']) ? $array['contractor'] : " ";
        $component->provider = isset($array['contractor']) ? $array['contractor'] : " ";
        $component->internal = isset($array['internal']) ? $array['internal'] : " ";
        $component->artical = $array['artical'];
        $component->nomencl = $array['nomencl'];
        $component->unit = $array['unit'] ? $array['unit'] : 'шт';
        $component->amount = $array['amount'];
        $component->woVAT = isset($array['woVAT']) ? $array['woVAT'] : " ";
        $component->wiVAT = $array['wiVAT'];
        $component->rateVAT = $array['rateVAT'];
        $component->cost = $array['cost'];
        $component->note = $array['note'];
        
        return $component;
    }

}
