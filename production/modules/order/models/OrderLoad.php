<?php


class OrderLoad extends CFormModel
{
    public static $LOAD_FILE = 'FILE'; # загрузка файла
    public $op_type, $flat, $place, $op_take, $house, $num, $zip, $region, $area,  $street,  $phone, $city, $fio, $agent_id, $op_fragile, $cost_insurance, $op_inventory, $registry, $housing, $floor, $office, $intercom, $wdays, $sat, $sun, $holi, $chm, $thm, $dai, $wd, $cost_public, $cost_delivery, $comment, $op_category;
    public $fileField;
    public $sender, $date_dep, $address_dep, $volume, $contract, $country, $megapolis;
    public $num_count;
    public $log = NULL;
    public $orderload = Array();
    public function attributeLabels()
    {
        return array(
            'sender'        => Yii::t("total", 'отправитель'),
            'date_dep'      => Yii::t("total", 'сдача/прием отправлений'),
            'address_dep'   => Yii::t("total", 'адрес места сдачи'),
            'volume'        => '',
            'contract'      => Yii::t("total", 'номер договора'),
            'megapolis'     => Yii::t("total", 'номер ИдеаЛоджик'),
            'fileField'     => Yii::t("total", 'выбрать файл'),
            'city'          => Yii::t("total", 'пункт доставки/ Город'),
            'country'       => Yii::t("total", 'страна'),
            'num'           => Yii::t("total", 'номер заказа'),
            'zip'           => Yii::t("total", 'индекс'),
            'region'        => Yii::t("total", 'регион /область'),
            'street'        => Yii::t("total", 'улица'),
            'house'         => Yii::t("total", 'дом'),
            'housing'       => Yii::t("total", 'корпус'),
            'floor'         => Yii::t("total", 'этаж'),
            'office'        => Yii::t("total", 'офис/ квартира'),
            'intercom'      => Yii::t("total", 'домофон'),
            'wdays'         => Yii::t("total", 'рабочие дни'),
            'sat'           => Yii::t("total", 'суббота'),
            'sun'           => Yii::t("total", 'воскресенье'),
            'holi'          => Yii::t("total", 'праздничные дни'),
            'chm'           => Yii::t("total", 'с час. мин.'),
            'thm'           => Yii::t("total", 'до час. мин.'),
            'phone'         => Yii::t("total", 'телефон'),
            'dai'           => Yii::t("total", 'описание отправления'),
            'wd'            => Yii::t("total", 'вес отправления, кг'),
            'registry'      => Yii::t("total", 'номер реестра'),
            'comment'       => Yii::t("total", 'комментарий'),
            'fio'           => Yii::t("total", 'Ф.И.О'),
            'agent_id'      => Yii::t("total", 'оператор доставки'),
            'area'          => Yii::t("total", 'район'),
            'op_inventory'  => Yii::t("total", 'наличие описи вложения'),
            'op_type'       => Yii::t("total", 'вид отправления'),
            'op_take'       => Yii::t("total", 'вид приёма'),
            'op_category'   => Yii::t("total", 'категория отправления'),
            'op_fragile'    => Yii::t("total", 'признак хрупкости'),
            'cost_public'   => Yii::t("total", 'объявленная ценность, руб.'),
            'cost_delivery' => Yii::t("total", 'наложенный платеж, руб.'),
            'cost_insurance'=> Yii::t("total", 'сумма страховой ценности, руб'),
        );
    }
	public function rules()
	{
            $request = Yii::app()->request->getParam('OrderLoad');
            if(isset($request['fileField'])){
                return array(
                    array('fileField', 'file', 'types'=> 'xls, xlsx', 'message'=> Yii::t("total", 'пожалуйста загрузите: xls, xlsx')),
		        );
            }
            #$load = current($_POST); # @todo - Костыль - выбираем что именно отдать по тому, что именно нам пришло. Например если не приходит файл, значит нам его и не нужно проверять.
            if(!isset($load['fileField'])){
                return array(
                    array('op_take,house, zip, region,  street, phone, fio,  op_type, op_category,  op_fragile, op_inventory, registry, address_dep, wd', 'required', 'message'=> Yii::t("total", 'введите текст')),
                    array('agent_id', 'required', 'message'=> Yii::t("total", 'введите из списка')),
                    array('num, cost_insurance, housing, registry, floor, office, intercom, wdays, sat, sun, holi, chm, thm, dai, cost_public, comment, contract, sender, megapolis, cost_delivery, volume, date_dep, area, city, flat, place, country', 'safe'),
                    array('cost_public, cost_delivery', 'numerical', 'message'=> Yii::t("total", 'введите число')),
                    array('op_category', 'api', Yii::t("total", 'не правильные данные')),
		);
            }else{ # @todo - грузим форму по проверки файла

            }
	}
    public function api(){

        if($this->op_category == 3){ # Категория отправления - С НП
            if($this->cost_delivery <= 0){ $this->addError('cost_delivery', Yii::t("total", 'оц: Не правильно выбрана сумма'));}
            if($this->cost_public < $this->cost_delivery){
                $this->addError('cost_public', Yii::t("total", 'не может быть меньше Наложенного платежа'));
                $this->addError('cost_delivery',Yii::t("total", 'не может быть больше Оценочной стоимости'));
            }
        }
        if($this->op_category == 4){ # Категория отправления - С ОЦ
            if($this->cost_delivery != 0){ $this->addError('cost_delivery', Yii::t("total", 'оц: Не правильно выбрана сумма'));} # НП
            if($this->cost_public <= 0){ $this->addError('cost_public',Yii::t("total", 'оц: Не правильно выбрана сумма'));}  # ОЦ
        }
        if($this->agent_id == 1){
            if($this->op_category != 5){
                if($this->cost_delivery > 50000){ $this->addError('cost_delivery', Yii::t("total", 'сумма превышает 50.000р'));} # НП
                if($this->cost_public > 50000){ $this->addError('cost_public', Yii::t("total", 'сумма превышает 50.000р'));}  # ОЦ
            }
        }
        if($this->agent_id == 2){
            if($this->cost_delivery > 50000){ $this->addError('cost_delivery', Yii::t("total", 'сумма превышает 50.000р'));} # НП
            if($this->cost_public > 50000){ $this->addError('cost_public', Yii::t("total", 'сумма превышает 50.000р'));}  # ОЦ
        }
		if($this->op_type == 24 || $this->op_type == 27){
			$this->country = 112;
		}

	    if($this->op_type == 22 || $this->op_type == 24 || $this->op_type == 26){
		    $this->country = 112;
	    }elseif($this->op_type == 23 || $this->op_type == 25 || $this->op_type == 27){
		    $this->country = 398;
	    }
    }

    public function beforeValidate(){

        if($this->scenario === self::$LOAD_FILE){

        }else{
            if($this->op_category == 2){ # Категория отправления - Простое
                $this->cost_delivery = 0;
                $this->cost_public = 0;
            }
            if($this->op_category == 4){

                $this->cost_delivery = 0;
            }
            if(empty($this->agent_id)){
                $this->addError('agent_id', Yii::t("total", 'пожалуйста выберите из списка'));
            }

        }

        return $this;
    }
    # чтение из БД
    public function ORDS(ORDS $ORDS){

        $this->attributes = $ORDS->attributes;
        #$COUN = COUN::model()->orders($ORDS->id)->find();

        $this->op_category = $ORDS->opCategory;
        $this->op_type     = $ORDS->opType;
        $this->op_inventory= $ORDS->opInventory;
        $this->agent_id    = $ORDS->idAgent;
        $this->op_fragile  = $ORDS->opFragile;
        $this->cost_public = $ORDS->costPublic;
        $this->cost_delivery = $ORDS->costDelivery;
        $this->housing = $ORDS->building;
        $this->wd = $ORDS->weightEnd ? $ORDS->weightEnd : 0;

        $this->volume = $ORDS->capacity;

        $this->date_dep = $ORDS->dateSurrender;
        $this->intercom = $ORDS->intercom;

        $this->dai = $ORDS->description;
        $this->op_take = $ORDS->opTake;
        $this->address_dep = $ORDS->addressdep;

        return $this;
    }

    public function processing($data){
        $array = array();
        foreach($data as $k=>$v){

            if(is_array($v)){
                foreach($v as $k2=>$v2){

                    $array[$k2][$k] = $v2;
                    $array[$k2]['registry'] = $data['registry'];
                    $array[$k2]['sender'] = $data['sender'];
                    $array[$k2]['date_dep'] = $data['date_dep'];
                    $array[$k2]['op_take'] = $data['op_take'];
                    if($data['op_take'] == 1){
                        if($data['address_dep'] == 1){
                            $data['address_dep'] = Yii::t("total", 'г. Москва, ул. Котляковская д. 3, стр.1');
                        }elseif($data['address_dep'] == 2){
                            $data['address_dep'] = Yii::t("total", 'г. Москва, ул. Ткацкая, д. 5, стр. 5');
                        }
                    }

                    $array[$k2]['address_dep'] = $data['address_dep'];
                    $array[$k2]['contract'] = $data['contract'];
                    $array[$k2]['volume'] = $data['volume'];
                }
            }


        }
        return $array;
    }

}