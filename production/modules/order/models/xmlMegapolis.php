<?php

class xmlMegapolis{

        # @name - getXml
        # @input - array - На вход принимаем массив
        # @proccess - На вход принимаем массив, формируем в XML и отдаем пользователю
        # @return -  XML - выдаем сформированную XML
        public static function getFormatMegapolis(array $array, CWebUser $user){
            unset($array['fileField']);
                $pile['agent_id'] = array( 1 => 'EMS', 2 => 'Почта России');
                $pile['op_type'] = array(
                    1 => 'Отправление EMS',
                    2 => 'Обычное отправление',
                    3 => 'Письмо',
                    4 => 'Почтовая карточка',
                    5 => 'Письмо 1-го класса',
                    6 => 'Бандероль',
                    7 => 'Бандероль 1-го класса',
                    8 => 'Посылка',
                    9 => 'Посылка стандартная',
                    10 => 'Посылка нестандартная',
                    11 => 'Посылка тяжеловесная',
                    12 => 'Посылка тяжеловесная крупногабаритная',
                );
                $pile['op_category'] = array(
                    1 => 'Заказное(ая)',
                    2 => 'Простое',
                    3 => 'С наложенным платежом',
                    4 => 'С объявленной ценностью',
                    5 => 'Со страхованием',
                );
                $pile['op_notification'] = array(
                    1 => 'Заказное',
                    2 => 'Простое',
                );
                $secretKey = $user->auth->secretKey;
                $date = date('d.m.Y H:i:s');
                $pile['xml'] = '';
                
                if(false){ # @test
                    print '----------------------------------------------------------------';
                    print_r($array);
                    exit;
                }
                # Документация в соответствии с документацией загрузки реестра
                $pile['array'] = array(
                    'document' => array(
                        'doc_type' => 100,
                        'doc_date' => $date,
                        'key' => $secretKey,
                    ),
                    'order' => array(
                        'registry' => $array['registry'], # регистр
                        'megapolis' => $array['num'], # Номер мегаполиса
                        'agent_id' => $array['agent_id'], # Вид отправления, подставляется в соответствии со справочником видов отправлений ,
                        'comment' =>  isset($array['comment']) ? $array['comment']: '' ,# комментарий
                        
                        'articles' => array(
                            'article_id' => '',
                            'article_name' => '',
                            'article_price' => '',
                            'article_weight' => '',
                            'article_count' => '',
                        ),
                        'option' => array(
                            'op_inventory' => $array['op_inventory'],
                            'op_category' => $array['op_category'],
                            'op_notification' => '', # ----------- Нет в нашей форме
                            'op_fragile' => $array['op_fragile'],
                            'op_packing' => '',
                            'op_type' => $array['op_type'], # Категория отправления
                            'op_take' => $array['op_take'], # Вид приема
                        ),
                        'cost' => array(
                            'cost_delivery' => $array['cost_delivery'],
                            'cost_insurance' => '',
                            'cost_public' => $array['cost_public'],
                            'cost_article' => '',
                        ),
                        'dimension' => array(
                            'weight_end' => $array['wd'],
                            'weight_article' => $array['wd'],
                            'capacity' => $array['volume'],
                        ),
                        'person' => array(
                            'country'   => $array['country'],
                            'phone' => $array['phone'],
                            'email' => isset($array['email']) ? $array['email']: '' ,
                            'fio' => $array['fio'],
                        ),
                        'address' => array(
                            'zip' => $array['zip'],
                            'region' => $array['region'],
                            'area' => $array['area'],
                            'city' => $array['city'], # Пункт доставки
                            'place' => '',
                            'street' => $array['street'],
                            'house' => $array['house'],
                            'building' =>  $array['housing'],
                            'flat' => $array['office'],
                            'office' => $array['office'],
                            'floor' => $array['floor'],
                            'full' => '',
                        ),
                        'un' => array(
                            'surrend' => $array['date_dep'],
                            'intercom' => $array['intercom'],
                            'description' => $array['dai'],
                            'addressdep'  => $array['address_dep'],
                        ),
                        'client' => array(
                            'client_contract' => $user->id,
                        ),
                       )
                );
                ########################
                # @info - формируем XML

                $pile['data']['xml']['order'] = array( # Пример всех переменных
                    'registry', 'megapolis', 'agent_id', 'comment', 'articles', 'article', 'article_id', 'article_name', 
                    'article_price', 'article_weight', 'article_count', 'option', 'op_inventory', 'op_category', 'op_type',
                    'op_notification', 'op_fragile', 'op_packing', 'op_take', 'cost', 'cost_delivery', 'cost_insurance', 'cost_public',
                    'cost_article', 'dimension', 'weight_end', 'weight_article', 'capacity', 'person', 'fio', 'phone', 'email',
                    'address', 'zip', 'region', 'area', 'city', 'place', 'street', 'house', 'building', 'flat', 'office', 'floor',
                    'full', 'client', 'client_contract',
                );
                
                $xml = new DOMDocument( "1.0", "UTF-8" );
                $pile['xml']['document'] = $xml->createElement( "document" );
                $pile['xml']['doc_type'] = $xml->createElement( "doc_type" , $pile['array']['document']['doc_type']);
                $pile['xml']['key'] = $xml->createElement( "key" , $pile['array']['document']['key']);
                $pile['xml']['doc_date'] = $xml->createElement( "doc_date" , $pile['array']['document']['doc_date']);
                $pile['xml']['order'] = $xml->createElement( "order" );
                
                foreach($pile['array']['order'] as $k=>$v){
                    if(is_array($v)){ # @comment - если переменная массив, то добавляем ветку
                        $pile['xml'][$k] = $xml->createElement($k);
                        foreach($v as $k2=>$v2){
                            $pile['xml'][$k2] = $xml->createElement($k2, $v2);
                            $pile['xml'][$k]->appendChild($pile['xml'][$k2]); 
                        }
                        $pile['xml']['order']->appendChild($pile['xml'][$k]);
                    }else{
                        $pile['xml'][$k] = $xml->createElement($k, $v);
                        $pile['xml']['order']->appendChild($pile['xml'][$k]);
                    }
                }
                
                $pile['xml']['document']->appendChild($pile['xml']['doc_type']);
                $pile['xml']['document']->appendChild($pile['xml']['doc_date']);
                $pile['xml']['document']->appendChild($pile['xml']['key']);
                
                $pile['xml']['document']->appendChild($pile['xml']['order']);
                
                $xml->appendChild($pile['xml']['document']);
                $result = $xml->saveXML();

            return isset($result) ? $result : 0;
        }
}