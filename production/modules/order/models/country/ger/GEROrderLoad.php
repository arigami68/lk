<?php


class GEROrderLoad extends OrderLoad{
    public $export_declaration,
        $invoice,
        $document_select,
        $passport_ser,
        $passport_num,
        $passport_day,
        $passport_give
    ;
    public $sender_name,
        $sender_country,
        $sender_region,
        $sender_zip,
        $sender_area,
        $sender_city,
        $sender_street,
        $sender_house,
        $sender_building,
        $sender_flat

    ;

    public function attributeLabels()
    {
        return array(
            'sender'        => Yii::t("total", 'отправитель'),
            'date_dep'      => Yii::t("total", 'сдача/прием отправлений'),
            'address_dep'   => Yii::t("total", 'адрес места сдачи'),
            'volume'        => '',
            'contract'      => Yii::t("total", 'номер договора'),
            'megapolis'     => Yii::t("total", 'номер ИдеаЛоджик'),
            'fileField'     => Yii::t("total", 'выбрать файл'),
            'city'          => Yii::t("total", 'пункт доставки/ Город'),
            'country'       => Yii::t("total", 'страна'),
            'num'           => Yii::t("total", 'номер заказа'),
            'zip'           => Yii::t("total", 'индекс'),
            'region'        => Yii::t("total", 'регион'),
            'street'        => Yii::t("total", 'улица'),
            'house'         => Yii::t("total", 'дом'),
            'housing'       => Yii::t("total", 'корпус'),
            'floor'         => Yii::t("total", 'этаж'),
            'office'        => Yii::t("total", 'офис/ квартира'),
            'intercom'      => Yii::t("total", 'домофон'),
            'wdays'         => Yii::t("total", 'рабочие дни'),
            'sat'           => Yii::t("total", 'суббота'),
            'sun'           => Yii::t("total", 'воскресенье'),
            'holi'          => Yii::t("total", 'праздничные дни'),
            'chm'           => Yii::t("total", 'с час. мин.'),
            'thm'           => Yii::t("total", 'до час. мин.'),
            'phone'         => Yii::t("total", 'телефон'),
            'dai'           => Yii::t("total", 'описание отправления'),
            'wd'            => Yii::t("total", 'вес отправления, кг'),
            'registry'      => Yii::t("total", 'номер реестра'),
            'comment'       => Yii::t("total", 'комментарий'),
            'fio'           => Yii::t("total", 'Ф.И.О'),
            'agent_id'      => Yii::t("total", 'оператор доставки'),
            'area'          => Yii::t("total", 'район'),
            'op_inventory'  => Yii::t("total", 'наличие описи вложения'),
            'op_type'       => Yii::t("total", 'вид отправления'),
            'op_take'       => Yii::t("total", 'вид приёма'),
            'op_category'   => Yii::t("total", 'категория отправления'),
            'op_fragile'    => Yii::t("total", 'признак хрупкости'),
            'cost_public'   => Yii::t("total", 'объявленная ценность, руб.'),
            'cost_delivery' => Yii::t("total", 'наложенный платеж, руб.'),
            'cost_insurance'=> Yii::t("total", 'сумма страховой ценности, руб'),

            /*
             * @name :: ger
             * @comment :: Для немцев
             */
            'export_declaration'    => Yii::t("total", 'номер экспортной декларации'),
            'invoice'               => Yii::t("total", 'номер инвойса'),
            'document_select'       => Yii::t("total", 'наименование'),

            'passport_ser'          => Yii::t("total", 'серия'),
            'passport_num'          => Yii::t("total", 'номер'),
            'passport_day'          => Yii::t("total", 'дата выдачи паспорта отправителя'),
            'passport_give'         => Yii::t("total", 'Кем выдан документ'),

            'sender_name'           => Yii::t("total", 'наименование'),
            'sender_country'        => Yii::t("total", 'страна'),
            'sender_zip'            => Yii::t("total", 'индекс'),
            'sender_region'         => Yii::t("total", 'регион'),
            'sender_area'           => Yii::t("total", 'район'),
            'sender_city'           => Yii::t("total", 'город'),
            'sender_street'         => Yii::t("total", 'улица'),
            'sender_house'          => Yii::t("total", 'дом'),
            'sender_building'       => Yii::t("total", 'корпус'),
            'sender_flat'           => Yii::t("total", 'номер квартиры'),
        );
    }
    public function rules()
    {
        $request = Yii::app()->request->getParam('OrderLoad');
        if(isset($request['fileField'])){
            return array(
                array('fileField', 'file', 'types'=> 'xls, xlsx', 'message'=> Yii::t("total", 'пожалуйста загрузите: xls, xlsx')),
            );
        }
        #$load = current($_POST); # @todo - Костыль - выбираем что именно отдать по тому, что именно нам пришло. Например если не приходит файл, значит нам его и не нужно проверять.
        if(!isset($load['fileField'])){
            return array(
                array('op_take,house, zip, region,  street, phone, fio,  op_type, op_category,  op_fragile, op_inventory, registry, address_dep, wd', 'required', 'message'=> Yii::t("total", 'введите текст')),
                array('agent_id', 'required', 'message'=> Yii::t("total", 'введите из списка')),
                array('num, cost_insurance, housing, registry, floor, office, intercom, wdays, sat, sun, holi, chm, thm, dai, cost_public, comment, contract, sender, megapolis, cost_delivery, volume, date_dep, area, city ', 'safe'),
                array('cost_public, cost_delivery', 'numerical', 'message'=> Yii::t("total", 'введите число')),
                array('op_category, ', 'api', Yii::t("total", 'не правильные данные')),
            );
        }else{ # @todo - грузим форму по проверки файла

        }
    }

}