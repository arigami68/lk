<?php

class numberMegapolis{
    public static $OPERATION_GET = 'get';
    public static function getDomXml($secret){ # @info - забираем Номера Мегаполиса по Секретному ключу
        $url_numbers_api = MEGAPOLIS_API.'?query&count=1&';
        $answer_numbers = file_get_contents( $url_numbers_api.'secret='.$secret );
        
        # Если пришла xml, смотрим есть ли разрешенный данный номер - пока что забираем. Далее когда будем сохранять форму регистрируем его
        $xml = DomXml::get($answer_numbers);
        
        return (string)$xml->data->text;
    }
    public static function getXml($secret, $count, $get = 'view'){
        $get = 'query='.(($get == 'view') ? 'view' : self::$OPERATION_GET);
        $url_numbers_api = MEGAPOLIS_API.'?count='.$count.'&'.$get.'&';
        $answer_numbers = file_get_contents( $url_numbers_api.'secret='.$secret );
        
        # Если пришла xml, смотрим есть ли разрешенный данный номер - пока что забираем. Далее когда будем сохранять форму регистрируем его
        $xml = DomXml::get($answer_numbers);
        
        foreach($xml->data->text as $k=>$v){
            $array[] = (string)$v;
        }
        return $array;
    }
}