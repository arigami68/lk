<?php
class ResultOrderAction extends CModel{
    public static $MESSAGES_ERROR   = 'error';
    public static $MESSAGES_SUCCESS = 'success';
    public $messages = array(
        'error' => array(),
        'success' => array(),
    );
    public $registry;           # Номер реестра

    public function attributeNames(){}
    public function setMessages($message, $MESSAGES){
        switch($MESSAGES){
            case self::$MESSAGES_ERROR:
                $message = $this->messages[self::$MESSAGES_ERROR][] = $message;
                break;
            case self::$MESSAGES_SUCCESS:
                $message = $this->messages[self::$MESSAGES_SUCCESS][] = $message;
                break;
        }
        return $message;
    }
    public function getMessages(){return $this->messages;}

}
class SettingsOrderAction{
    public $operation;
}
class OrderActionModel extends CModel{
    public static $SCENARIO_SHOW    = 'show';
    public static $SCENARIO_CREATE  = 'create';
    public static $SCENARIO_UPDATE  = 'update';


    public function attributeNames(){}
    public static $instance;

    private function __construct(){}
    private function __clone()    {}
    private function __wakeup()   {}

    public static function get(){ return empty(self::$instance) ? self::$instance = new self() : self::$instance;}
    public $operation;              # Операция которую мы выполняем
    public $cargo       = NULL;     # Реестр или груз
    public $id          = NULL;     # id с которым мы работаем
    public $transfer    = 'json';
    public $flag_file ;             # Загрузка идет через файл или форму 1. файл, 0. форма
    public $form;                   # Форма с которой мы работаем
    public $form_data;
    public $form_error  = NULL;
    public $refresh     = 0;
    public $settings;
    public $messages;               # Сообщения errror, success
    public $result;                 # Результат

    public $count_num   = 0;        # Количество номеров которых мы забрали

    public function setForm(OrderLoad $form, $form_data){
        $this->form = $form;
        $this->form_data = $form_data;
    }
    public function setdb(ORDS $db){$this->db = $db;}
    public function setClient(AuthComponent $client){$this->client = $client;}
    # setCargo() - Устанавливаем Реестр это или Груз и присваиваем номер 
    public function setCargo($id, $registry){
        $result=0;
        if($id){
            $result=1;
            $this->cargo = 'id';
            $this->id = $id;
        }elseif($registry){
            $result=1;
            $this->cargo = 'registry';
            $this->id = $registry;
        }
        return $result;
    }
    public function create(){
        $model = $this->form;
        $message = '';
        if($this->scenario == OrderActionModel::$SCENARIO_SHOW){
            $model->orderload = $model;
            if(!$this->client->range['registry']){
                $registry = FuncMegapolis::Xml($this->client->secretKey, 1, 'view', 2);
                $registry = $registry[0];

                $model->registry = $registry;
            }
            return $model;
        }elseif($this->scenario == OrderActionModel::$SCENARIO_CREATE){
            if($this->flag_file){ # Загрузка файла
                $file = CUploadedFile::getInstance($model,'fileField');
                $model->scenario = OrderLoad::$LOAD_FILE;
				$flag = 1;
                if($model->validate()){
                    $message = $this->loadFile($file);
                    if(!empty($message)){
                        if(!$this->errors){

                        }
                    }else{
						$flag = 3;
                        if($errors = $this->errors){
                            foreach($errors as $k=> $value){
								$message['error'][] = $value[0];
                            }
                        }
                    }
                }else{
					$flag = 3;
					$message['error'][] = 'Ошибка обработки файла';
                }

                $MMRessage = new MegapolisMessageRequest;
                $MMRessage->status(1);
                $MMRessage->processing($message, $flag);

                return $MMRessage;
            }else{ # Загрузка из формы
                $this->form_data['sender']   = $this->client->name;
                $this->form_data['contract'] = $this->client->contract;
                $attributes = $model->processing($this->form_data);
                $error=0;

                foreach($attributes as $k=>$var){
                    $this->form->attributes =  $var;
                    if($this->checkNum($this->form->num, 1)){

                        if($this->form->validate()){

                        }else{

                            $error=1;
                            Yii::app()->user->setFlash('error', 'Одно из отправлений не заполнено!');
                        }

                    }else{

                        $error=1;
                        Yii::app()->user->setFlash('range_cargo', 'Груз: данный диапазон Вам не доступен: '.$this->form->num);
                    }

                    if(!$this->checkNum($this->form->registry, 2)){
                        $error=1;
                        Yii::app()->user->setFlash('range_registry', 'Реестр: данный диапазон Вам не доступен: '.$this->form->registry);
                    }

                    $order[] = clone $model;

                }

                if(!isset($order)){
                    return Yii::app()->user->setFlash('error', 'Отправления отсутсвуют');
                }
                Yii::log(serialize($order), 'lk.create');

                if(!$error){
                    foreach($order as $k=>$var){

                        $attributes = $var->attributes;

                        $xml[] = xmlMegapolis::getFormatMegapolis($attributes, Yii::app()->user);
                    }

                    $message = OrderActionModel::get()->transferToAPI($xml);

                    #$this->refresh=1;
                    #$messages = ApiMessage::message($message, ApiMessage::$MODEL_ORDERACTIONMODEL);
                    /*
                    print_R($messages);
                    exit;
                    if($messages[0] != 'Операция не выполнена!'){
                        $this->refresh=1;
                    }else{
                        $this->form_error = $order;
                        return Yii::app()->user->setFlash('error', 'Операция не выполнена!<br> Данные номера грузов или номера реестров не доступны!');
                    }
                    */

                }else{
                    $this->form_error = $order;
                    #return Yii::app()->user->setFlash('error', 'Операция не выполнена');
                }


                $MMRessage = new MegapolisMessageRequest;
                $MMRessage->status(1);
                $MMRessage->processing($message, 2);

                return $MMRessage;
            }
        }
    }
    public function read(){
        $model = $this->form;
        if($this->scenario == OrderActionModel::$SCENARIO_SHOW){
            $criteria = new CDBcriteria();
            $criteria->order = 'megapolis';
            $ORDS = ORDS::model()->user()->registry(OrderActionModel::get()->id)->findAll($criteria);
            foreach($ORDS as $k=>$v){
                $model->attributes = $model->ORDS($v);
                $model->orderload[] = clone $model;
            }

            return $model;
        }
    }
    public function update(){
        if($this->scenario == OrderActionModel::$SCENARIO_SHOW){
            $array = $this->form;

            $ORDS = ORDS::model()->user()->registry($this->id)->findAll();

            foreach($ORDS as $k=>$v){
                $model = clone $this->form;
                $model->attributes = $model->ORDS($v);
                $model->num = $model->megapolis;

                $array->orderload[] = clone $model;
            }

            return $array;
        }elseif($this->scenario == OrderActionModel::$SCENARIO_CREATE){
            $this->form_data['sender']   = $this->client->name;
            $this->form_data['contract'] = $this->client->contract;
            $model = $this->form;

            $attributes = $model->processing($this->form_data);

            $error=0;
            foreach($attributes as $k=>$var){
                $this->form->attributes =  $var;

                if(!empty($this->form->num)){
                    $this->form->num = $this->form->num;
                }else{
                    $this->form->num = current($key);
                    next($key);
                }
                if($this->form->validate()){
                }else{
                    $error=1;
                    ($e = $this->form->getError('api')) ? Yii::app()->user->setFlash('error', $e) : Yii::app()->user->setFlash('error', 'Одно из отправлений не заполнено/ заполнено не верно!');
                }
                $order[] = clone $model;
            }
            if(!$error){

                foreach($order as $k=>$var){
                    $attributes = $var->attributes;
                    $xml[] = xmlMegapolis::getFormatMegapolis($attributes, Yii::app()->user);
                }
                if(false){ # @test
                    header("Content-Type: text/xml");
                    print_R($xml[0]);
                    exit;
                }
                $message = OrderActionModel::get()->transferToAPI($xml);
                if($this->count_num){
                    FuncMegapolis::Xml($this->client->secretKey, $this->count_num , 'get');
                }
                $this->refresh=1;
                return Yii::app()->user->setFlash('error', 'Обновление прошло успешно!');
            }else{
                $this->form_error = $order;
                return Yii::app()->user->setFlash('error', 'Операция не выполнена');
            }

            return $message;
        }
    }
    public function delete(){
        $o = 'operation='.ORDS::$OPERATION_DELETE;
        $c = 'client[secret]='.$this->client->secretKey.'&client[contract]='.$this->client->contract;
        $ca = $this->cargo.'='.$this->id;
        $message = MEGAPOLIS_API_DATA.'api/api/ORDS?'.$o.'&'.$c.'&'.$ca;

        $get = file_get_contents($message);

        return $get;
    }
    public function transferToAPI($xml){
        $operation  = $this->operation == 'create' ? 'insert' : $this->operation;
        $operation  = $operation == 'update' ? 'update' : $operation;

        $array['data']               = $xml;
        $array['operation']          = $operation;
        $array['client']['contract'] = $this->client->contract;
        $array['client']['secret']   = $this->client->secretKey;

        $history = new DBordersHistory;
        $history->input = json_encode($array['data']);
        $history->contract = $array['client']['contract'];
        $history->date = date('Y-m-d H:i:s');

        $url = 'http://megapolis-old.idea-logic.megapolis.loc/load/load/ORDS';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, array(
            'data'              => json_encode($array['data']),
            'operation'         => $array['operation'],
            'client[contract]'  => $array['client']['contract'],
            'client[secret]'    => $array['client']['secret'],
        ));
        $result = curl_exec ($ch);

        curl_close ($ch);
        $history->output = $result;
        $history->insert();

        return $result;
    }
    public function curl($file, $url){
		#header("Content-type: text/xml");

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $file);
        $result = curl_exec ($ch);

        curl_close ($ch);
        return $result;
    }
    public function checkNum($num, $option){
        if($option == 1){
            if(!$this->client->range['cargo']) return TRUE;
            if(!$COME = COME::model()->user()->operation(1)->find()) return FALSE;
            $max = $COME->step*10000 +  10000;
            $min = $COME->step*10000 +1;

            if($min <= $num AND $num <= $max){
                return TRUE;
            }

        }elseif($option == 2){
            if(!$this->client->range['registry']) return TRUE;
            if(!$COME = COME::model()->user()->operation(2)->find()) return FALSE;

            $max = $COME->step*1000 + 1000;
            $min = $COME->step*1000 +1;
            if($min <= $num AND $num <= $max){
                return TRUE;
            }
        }
        return FALSE;
    }
    public function loadFile(CUploadedFile $file){

        try {
            switch(Yii::app()->user->auth->db->country){
                case 276:
                    $read = new ReadXlsxGer();
                    break;
                default:
                    $read = new ReadXlsx();
            }
            if(!($arrayXml  = $read->read($file))){
				$this->addError('отправления', 'Нет отправлений');
				throw new MegapolisException;
			}

            foreach($arrayXml as $xml){
                $read->push($xml);
            }

            if(!$read->count()) throw new MegapolisException;
            $a=0;
            $megapolisArray = array();
            while($each = $read->next()){

                $megapolis = $each['megapolis'];
                /*
                 * Добавляем товары если такое отправление уже существует
                 */
                if(isset($megapolisArray[$megapolis])){
                    $component = $array[$megapolis];
                }else{
                    $megapolisArray[$megapolis]=1;
                    $component = new OrderComponent;
                }
                $document = $read->getComponent($each, $component);
                $document->validate();
                $array[$megapolis]            = $document;
            }

            $currentArray = current($array);

            if($currentArray->order->getclient()->client_contract != Yii::app()->user->auth->contract){
                #    $this->addError('данные', MegapolisMessage::$CODE_EXC[1]);
            }

            $this->result->registry = $currentArray->order->registry;

            foreach($array as $k=>$order){
                $page = $k+ ReadXlsx::$first_list;
                if($order->errors){
                    foreach($order->errors as $k2=>$value){
                        $this->addError($page.' номер строки', $page.' номер строки :: '.$value[0]);
                    }
                }
            }

            if(!$this->errors){
                $result = $result_order = array();
                $xml = new DOMDocument();
                foreach($array as $i=> $order){
                    if(!isset($first)){ $first =  $order->format($xml);}

                    $result_order[$i] = $order->formatHeart($xml, $order);
                    $first->order[$i] = $result_order[$i];
                    $first->document->appendChild($result_order[$i]);
                }

                $xml->appendChild($first->document);
                $result = $xml->saveXML();

                if(false){ // @test
                    header("Content-Type: text/xml");
                    print_r($result);
                    exit;
                }

                $message = OrderActionModel::get()->curl($result, 'http://old1.megaexp.ru/api.php');

            }
        }catch (MegapolisException $exc){

            $this->addError('данные', 'Ошибка в чтение данных');
        }

        return isset($message) ? $message : FALSE;
    }
}