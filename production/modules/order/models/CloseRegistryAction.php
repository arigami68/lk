<?php
class CloseRegistryAction extends CAction{
    public function run(){
       $r = Yii::app()->request->getQuery('registry');

       $secret    =  Yii::app()->user->auth->secretKey;
       $contract  = Yii::app()->user->auth->contract;
       $operation = 'update';
       $registry  = $r;
       $f = file_get_contents('http://api.hydra.megapolis.loc/api/api/ORDS?registry='.$registry.'&client[secret]='.$secret.'&client[contract]='.$contract.'&operation='.$operation);
       return $this->getController()->redirect('/order');
    }
}
