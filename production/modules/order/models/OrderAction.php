<?php

class OrderAction extends CAction{
    public function run(){
        try{
            if(true){
                OrderActionModel::get()->db         = new ORDS;
                OrderActionModel::get()->setCargo(Yii::app()->request->getQuery('id'), Yii::app()->request->getQuery('registry'));
                OrderActionModel::get()->flag_file  = isset($_POST['OrderLoad']['fileField']) ? 1 : 0;
                OrderActionModel::get()->client     = Yii::app()->user->auth;
                OrderActionModel::get()->setForm(new OrderLoad, isset($_POST['OrderLoad']) ? $_POST['OrderLoad'] : '');
                OrderActionModel::get()->count_num  = isset($_POST['OrderLoad']['num_count']) ? $_POST['OrderLoad']['num_count'] : 0;

                OrderActionModel::get()->settings   = new SettingsOrderAction();
                OrderActionModel::get()->settings->operation = OrderActionModel::get()->operation  = Yii::app()->request->getQuery('operation');
                OrderActionModel::get()->result     = new ResultOrderAction();

                $this->getController()->title = Yii::t("total", 'создание реестра');

                $func = FuncMegapolis::Handbook();

$text1 = <<<EOF
MEG.settings.text1    = '<textarea class="span2 address_dep error" placeholder="Адрес места сдачи" name="OrderLoad[address_dep]" id="OrderLoad_address_dep"></textarea>',
MEG.settings.text2    = '<select class="span2 address_dep error" name="OrderLoad[address_dep]" id="OrderLoad_address_dep"><option value="1" selected="selected">г. Москва, ул. Котляковская д. 3, стр.1</option><option value="2">г. Москва, ул. Ткацкая, д. 5, стр. 5</option></select>',
MEG.settings.address  = 'address_dep',
EOF;

Yii::app()->clientScript->registerScript("Vars","
Message = {
    'delete_order'      : '".Yii::t("total", 'удалить отправление:')."',
    'delete_registry'   : '".Yii::t("total", 'удалить реестр')."',
    'close_registry'    : '".Yii::t("total", 'закрыть реестр')."',
};
MEG.settings.messages = [
        '".Yii::t("total", 'С ОЦ: НП=0, ОЦ>0')."',
        '".Yii::t("total", 'С НП: ОЦ>=НП, НП>0')."',
        '".Yii::t("total", 'orders.Не заполнено ни одного отправления')."',
        '".Yii::t("total", 'orders.следующие ограничения по весу')."',
        '".Yii::t("total", 'orders.минимальные ограничения по весу').":',
        '".Yii::t("total", 'orders.не может превышать 50000')."',
        '".Yii::t("total", 'orders.не может превышать объявленную стоимость')."',
        '".Yii::t("total", 'обязательное поле')."',
        '".Yii::t("total", 'orders.наложенный платеж и Объявленная ценность не может быть равна 0')."',
        '".Yii::t("total", 'orders.в данный момент сервис не доступен, попробуйте повторить попытку позже!')."',
        '".Yii::t("total", 'пожалуйста заполните наложенный платеж')."',
        '".Yii::t("total", 'пожалуйста заполните объявленную ценность')."',
        '".Yii::t("total", 'не все отправления корректно заполнены')."',
        '".Yii::t("total", 'наложенный платеж не может быть равен 0')."',
        '".Yii::t("total", 'объявленная ценность не может быть равна 0')."',
    ];
MEG.settings.od = {
        0 : '',
        1 : 'EMS',
        2 : 'Почта России',
        3 : 'DPD',
        4 : 'ИдеалЛоджик',
        5 : 'Максипост',
};
MEG.settings.exc = [
        'МОСКВА Г',
        'САНКТ-ПЕТЕРБУРГ Г',
        'СЕВАСТОПОЛЬ Г',
        'БАЙКОНУР Г'
];
MEG.settings.name = {
        'OD'       : '".Yii::t("total", 'оператор доставки')."',
        'TYPE'     : '".Yii::t("total", 'вид отправления')."',
        'CATEGORY' : '".Yii::t("total", 'категория отправления')."',
        'FIO'      : '".Yii::t("total", 'Ф.И.О')."',
        'TAKE'     : '".Yii::t("total", 'вид приёма')."',
        'PHONE'    : '".Yii::t("total", 'телефон')."',
        'CITY'     : '".Yii::t("total", 'пункт доставки/ Город')."',
        'REGION'   : '".Yii::t("total", 'регион')."',
        'STREET'   : '".Yii::t("total", 'улица')."',
        'HOUSE'    : '".Yii::t("total", 'дом')."',
        'PUBLIC'   : '".Yii::t("total", 'объявленная ценность')."',
        'WD'       : '".Yii::t("total", 'вес')."',
        'ZIP'      : '".Yii::t("total", 'индекс')."',
        'DELIVERY' : '".Yii::t("total", 'наложенный платеж')."',
};
MEG.settings.type_name = {
        1 :     '".Yii::t("total", 'отправление ems')."',
        2 :     '".Yii::t("total", 'обычное отправление')."',
        3 :     '".Yii::t("total", 'письмо')."',
        4 :     '".Yii::t("total", 'Почтовая карточка')."',
        5 :     '".Yii::t("total", 'Письмо 1-го класса')."',
        6 :     '".Yii::t("total", 'Бандероль')."',
        7 :     '".Yii::t("total", 'Бандероль 1-го класса')."',
        8 :     '".Yii::t("total", 'Посылка')."',
        9 :     '".Yii::t("total", 'Посылка стандартная')."',
        10 :    '".Yii::t("total", 'Посылка нестандартная')."',
        11 :    '".Yii::t("total", 'посылка тяжеловесная')."',
        12 :    '".Yii::t("total", 'посылка тяжеловесная крупногабаритная')."',
        13 :    '".Yii::t("total", 'посылка тяжеловесная крупногабаритная')."',
        14 :    '".Yii::t("total", 'идеаЛоджик')."',
        15 :    '".Yii::t("total", 'dpd consumer')."',
        16 :    '".Yii::t("total", 'dpd classic parcel')."',
        17 :    '".Yii::t("total", 'dpd express')."',
        18 :    '".Yii::t("total", 'dpd classic')."',
        19 :    '".Yii::t("total", 'максипост габаритное')."',
        20 :    '".Yii::t("total", 'максипост крупногабаритное')."',
        21 :    '".Yii::t("total", 'идеаЛоджик ПВЗ')."',
        22 :    '".Yii::t("total", 'Посылка международная авиа Беларусь')."',
        23 :    '".Yii::t("total", 'Посылка международная авиа Казахстан')."',
        24 :    '".Yii::t("total", 'Посылка международная наземная Беларусь')."',
        25 :    '".Yii::t("total", 'Посылка международная наземная Казахстан')."',
        26 :    '".Yii::t("total", 'Отправлениe EMS международное Беларусь')."',
        27 :    '".Yii::t("total", 'Отправление EMS международное Казахстан')."',
        30 :    '".Yii::t("total", 'Посылка-онлайн')."',
        31 :    '".Yii::t("total", 'Курьер-онлайн')."',
};

$text1
", CClientScript::POS_END);
            }

            $result = '';
            switch(OrderActionModel::get()->operation){
                case (ORDS::$OPERATION_CREATE):
                    $output = $this->create();
                    break;
                case (ORDS::$OPERATION_READ):
                    $this->read();
                    break;
                case (ORDS::$OPERATION_UPDATE):
                    $this->update();
                    break;
                case (ORDS::$OPERATION_DELETE):
                    $this->delete();
                    break;
                default: #ORDS::$OPERATION_SHOW:
                    $this->getController()->title = Yii::t("total", 'просмотр реестров');
                    return $this->getController()->render('application.components.views.orders.orders', array('model' => OrderActionModel::get()->db));
                break;
            }

           if(!isset($output)){
                if(is_int($result)){
                    if(isset(MegapolisMessage::$CODE_EXC[$result])){
                        OrderActionModel::get()->form->log = array('error' => MegapolisMessage::$CODE_EXC[$result]);
                    }
                    if(MegapolisMessage::$MEGAPOLIS_TEXT) OrderActionModel::get()->form->log = array('error' => MegapolisMessage::$MEGAPOLIS_TEXT);
                }elseif($result){
                    $message = new MegapolisMessage;

                    $messages = $message->format(MegapolisMessage::$FORMAT_MESSAGE_JSON)->file($result)->model(MegapolisMessage::$MODEL_ORDERACTIONMODEL)->process();
                    OrderActionModel::get()->form->log = $messages;
                }elseif($result === NULL){
                    OrderActionModel::get()->form->log = array('error' => "Файл не обработан.<br> Не соответсвует шаблону. Ссылка на шаблон: <a href=".Yii::app()->assetManager->publish(PRODUCTION.'file'.DS.'general'.DS.'test_registry.xlsx').">тестовый реестр</a>");
                }
           }
            OrderActionModel::get()->form->num_count = OrderActionModel::get()->count_num; # Сохраняем количество добавленных отправлений
            if(isset($output)){
                return $this->output();

            }else{
                switch(OrderActionModel::get()->operation){
                    case (ORDS::$OPERATION_CREATE):
                    case (ORDS::$OPERATION_UPDATE):
                    case (ORDS::$OPERATION_READ):
                        if(OrderActionModel::get()->refresh){
                            if(OrderActionModel::get()->operation == ORDS::$OPERATION_CREATE){
                                Yii::app()->user->setFlash('error', 'Реестр '.OrderActionModel::get()->form->registry.' успешно добавлен! ');
                            }elseif(OrderActionModel::get()->operation == OrderActionModel::$SCENARIO_UPDATE){
                                Yii::app()->user->setFlash('error', 'Обновление Реестра '.OrderActionModel::get()->id.' прошло успешно! ');
                            }
                            Yii::app()->request->redirect('/orders?operation=show');
                        }
                        return $this->getController()->render('application.components.views.order.order', array('model' => OrderActionModel::get()->form, 'load' => isset($load) ? $load : '', 'setting' => isset($setting) ? $setting : '', 'operation' => OrderActionModel::get()->operation));
                    default:
                        return $this->getController()->render('application.components.views.orders.orders', array('model' => OrderActionModel::get()->db));
                    break;
                }
            }
        }catch (MegapolisException $exc){
            $exc->get(MegapolisException::$ERROR_ORDER_CREATE);
        }
    }

    public function create(){
        $idea = $this->getnum();

        # JS обновление Имени
        if(!OrderActionModel::get()->client->range['cargo']){
            foreach($idea as &$v){
                $v = '"'.$v.'"';
            }
            Yii::app()->clientScript->registerScript("Vars","
                                var idea = [".implode(',', $idea)."];
                                var scenario = '".ORDS::$OPERATION_CREATE."';
                                 ", CClientScript::POS_HEAD);
        }

        if(!Yii::app()->request->isPostRequest){
            OrderActionModel::get()->scenario = OrderActionModel::$SCENARIO_SHOW;
            if(OrderActionModel::get()->flag_file){
                $result = OrderActionModel::get()->create();


            }else{
                $model = OrderActionModel::get()->create();
            }

        }else{
            OrderActionModel::get()->scenario = OrderActionModel::$SCENARIO_CREATE;
            $result = OrderActionModel::get()->create();

            $registry = OrderActionModel::get()->result->registry;

            if($result->getcountError()){
                if($errors = $result->getArray()){
                    foreach($errors as $k=> $value){
                        OrderActionModel::get()->result->setMessages($value, ResultOrderAction::$MESSAGES_ERROR);
                    }
                }
            }else{

            #    OrderActionModel::get()->result->setMessages('Успешное создание реестра : ', ResultOrderAction::$MESSAGES_SUCCESS);
            #    OrderActionModel::get()->result->setMessages($registry, ResultOrderAction::$MESSAGES_SUCCESS);
            #    OrderActionModel::get()->result->setMessages('с отправлениями:', ResultOrderAction::$MESSAGES_SUCCESS);

                foreach($result->getArray() as $k=> $value){
                    OrderActionModel::get()->result->setMessages($value, ResultOrderAction::$MESSAGES_SUCCESS);
                }
            }
            OrderActionModel::get()->form_error ? OrderActionModel::get()->form_error : OrderActionModel::get()->form_data;

            if(OrderActionModel::get()->form_error){
                OrderActionModel::get()->operation  = ORDS::$OPERATION_UPDATE;
                OrderActionModel::get()->form->orderload = OrderActionModel::get()->form_error;

                # JS обновление Имени
                if(!OrderActionModel::get()->client->range['cargo']){
                    $idea = $this->getnum();
                    $a = count(OrderActionModel::get()->form->orderload);
                    foreach($idea as $k=>&$v){
                        while($a){ unset($idea[$k]); $a--; continue 2;}
                        $v = '"'.$v.'"';
                    }
                    Yii::app()->clientScript->registerScript("Vars","
                                var idea = [".implode(',', $idea)."];
                                var scenario = '".ORDS::$OPERATION_UPDATE."';
                                 ", CClientScript::POS_HEAD);
                }
            }else{
                OrderActionModel::get()->form->orderload = clone OrderActionModel::get()->form;
            }
        }

        return isset($result) ? $result : NULL;
    }
    public function read(){
        $this->getController()->title = 'Просмотр реестра';
        OrderActionModel::get()->scenario = OrderActionModel::$SCENARIO_SHOW;
        $model = OrderActionModel::get()->read();
    }
    public function update(){
        $this->getController()->title = 'Редактор реестра';
        if(!Yii::app()->request->isPostRequest){
            OrderActionModel::get()->scenario = OrderActionModel::$SCENARIO_SHOW;
            $model = OrderActionModel::get()->update();
        }else{
            OrderActionModel::get()->scenario = OrderActionModel::$SCENARIO_CREATE;
            $model = OrderActionModel::get()->update();

            if(OrderActionModel::get()->form_error){
                OrderActionModel::get()->operation  = ORDS::$OPERATION_UPDATE;
                OrderActionModel::get()->form->orderload = OrderActionModel::get()->form_error;
            }else{
                OrderActionModel::get()->form->orderload = clone OrderActionModel::get()->form;
            }
        }

        if(!OrderActionModel::get()->client->range['cargo']){
            $idea = $this->getnum();
            if(gettype(OrderActionModel::get()->form->orderload) == 'object'){
                if($i = array_search(OrderActionModel::get()->form->orderload->num, $idea) !== FALSE){
                    unset($idea[$i-1]);
                }
            }else{
                foreach(OrderActionModel::get()->form->orderload as $k=>$v){
                    if($i = array_search($v->num, $idea) !== FALSE){
                        unset($idea[$i-1]);

                    }
                }
            }

            foreach($idea as $k=>&$v){
                $v = '"'.$v.'"';
            }
            Yii::app()->clientScript->registerScript("Vars","
                        var idea = [".implode(',', $idea)."];
                        var scenario = '".ORDS::$OPERATION_UPDATE."';
                         ", CClientScript::POS_HEAD);
        }
    }
    public function delete(){
        $result = OrderActionModel::get()->delete();
    }
    public function getnum(){
        if(!$idea = FuncMegapolis::Xml(OrderActionModel::get()->client->secretKey, 700, 'view', 1)) throw new MegapolisException;
        return $idea;
    }
    public function output(){
        switch(OrderActionModel::get()->operation){
            case (ORDS::$OPERATION_CREATE):
                if(OrderActionModel::get()->refresh){
                    Yii::app()->user->setFlash('true', 'Успешная загрузка реестра!');
                    Yii::app()->request->redirect('/orders?operation=show');
                }
                return $this->getController()->render('application.components.views.order.order',
                    array(
                        'model'     => OrderActionModel::get()->form,
                        'load'      => isset($load) ? $load : '',
                        'setting'   => isset($setting) ? $setting : '',
                        'operation' => OrderActionModel::get()->operation,
                        'mSettings' => OrderActionModel::get()->settings,
                        'mResult'   => OrderActionModel::get()->result,
                    ));
            case (ORDS::$OPERATION_UPDATE):
                Yii::app()->user->setFlash('error', 'Обновление Реестра '.OrderActionModel::get()->id.' прошло успешно! ');
            case (ORDS::$OPERATION_READ):
                if(OrderActionModel::get()->refresh){
                    Yii::app()->request->redirect('/orders?operation=show');
                }

                return $this->getController()->render('application.components.views.order.order', array('model' => OrderActionModel::get()->form, 'load' => isset($load) ? $load : '', 'setting' => isset($setting) ? $setting : '', 'operation' => OrderActionModel::get()->operation));
            default:
                return $this->getController()->render('application.components.views.orders.orders', array('model' => OrderActionModel::get()->db));
                break;
        }
    }
}