<?php
abstract class documentOrderAbstract extends CModel{
    public function attributeNames(){}

    public $doc_type    = 100;
    public $key;
    public $doc_date;
    private $order;

    private $format;

    public function setOrder(documentOrderOrder $order){ $this->order = $order;}
    public function getOrder(){ return $this->order;}
    public function format(DOMDocument &$xml){

        $object = new stdClass();

        $object->document   = $xml->createElement("document");

        $object->doc_type   = $xml->createElement('doc_type', $this->doc_type);
        $object->key        = $xml->createElement('key', $this->key);
        $object->doc_date   = $xml->createElement('doc_date', $this->doc_date);

        $object->document->appendChild($object->doc_type);
        $object->document->appendChild($object->key);
        $object->document->appendChild($object->doc_date);

        return $object;
    }
    public function formatHeart($xml, $data){

        $object = new stdClass();

        $object->order = $xml->createElement('order');


        $order = $data->order;
        $object->order->appendChild($object->registry           = $xml->createElement('registry',   $order->registry));
        $object->order->appendChild($object->megapolis          = $xml->createElement('megapolis',  $order->megapolis));
        $object->order->appendChild($object->agent_id           = $xml->createElement('agent_id',   $order->agent_id));
        $object->order->appendChild($object->comment            = $xml->createElement('comment',    $order->comment));
        $object->order->appendChild($object->invoice            = $xml->createElement('invoice',    $order->invoice));
        $object->order->appendChild($object->export_declaration = $xml->createElement('export_declaration', $order->export_declaration));
        $object->order->appendChild($object->consignment        = $xml->createElement('consignment',$order->consignment));
        $object->order->appendChild($object->track_num          = $xml->createElement('track_num',  $order->track_num));

        $object->articles = $xml->createElement('articles');

        if($articles = $order->getarticles()){
            $articleArray = array();
            foreach($articles as $key=> $article){
                $articleArray[$key] = $xml->createElement('article');
                $articleArray[$key]->appendChild($xml->createElement('article_id', $article->article_id));
                $articleArray[$key]->appendChild($xml->createElement('article_name',       $article->article_name));
                $articleArray[$key]->appendChild($xml->createElement('article_price',      $article->article_price));
                $articleArray[$key]->appendChild($xml->createElement('article_price_sum',  $article->article_price_sum));
                $articleArray[$key]->appendChild($xml->createElement('article_weight',     $article->article_weight));
                $articleArray[$key]->appendChild($xml->createElement('article_weight_sum', $article->article_weight_sum));
                $articleArray[$key]->appendChild($xml->createElement('article_count',      $article->article_count));
                $articleArray[$key]->appendChild($xml->createElement('article_web',        $article->article_web));
                $articleArray[$key]->appendChild($xml->createElement('article_price_num',  $article->article_price_num));

                $object->articles->appendChild($articleArray[$key]);
            }
        }

        $object->option = $xml->createElement('option');
        $option = $order->getoption();
        $object->option->appendChild(   $xml->createElement('op_inventory', $option->op_inventory));
        $object->option->appendChild(   $xml->createElement('op_category', $option->op_category));
        $object->option->appendChild(   $xml->createElement('op_type', $option->op_type));
        $object->option->appendChild(   $xml->createElement('op_notification', $option->op_notification));
        $object->option->appendChild(   $xml->createElement('op_fragile', $option->op_fragile));
        $object->option->appendChild(   $xml->createElement('op_packing', $option->op_packing));
        $object->option->appendChild(   $xml->createElement('op_take', $option->op_take));

        $object->cost = $xml->createElement('cost');
        $cost = $order->getcost();
        $object->cost->appendChild(   $xml->createElement('cost_delivery', $cost->cost_delivery));
        $object->cost->appendChild(   $xml->createElement('cost_insurance', $cost->cost_insurance));
        $object->cost->appendChild(   $xml->createElement('cost_public', $cost->cost_public));
        $object->cost->appendChild(   $xml->createElement('cost_article', $cost->cost_article));

        $object->dimension = $xml->createElement('dimension');
        $dimension = $order->getdimension();

        $object->dimension->appendChild(   $xml->createElement('weight_end', $dimension->weight_end));
        $object->dimension->appendChild(   $xml->createElement('weight_article', $dimension->weight_article));
        $object->dimension->appendChild(   $xml->createElement('capacity', $dimension->capacity));
        $object->dimension->appendChild(   $xml->createElement('count', $dimension->count));

        $object->sender = $xml->createElement('sender');
        $sender = $order->getsender();

        $object->sender->appendChild(   $xml->createElement('phone', $sender->phone));
        $object->sender->appendChild(   $xml->createElement('email', $sender->email));
        $object->sender->appendChild(   $xml->createElement('fio', $sender->fio));
        $object->sender->appendChild(   $xml->createElement('name', $sender->name));
        $object->sender->appendChild(   $xml->createElement('surname', $sender->surname));
        $object->sender->appendChild(   $xml->createElement('patronymic', $sender->patronymic));
        $object->sender->appendChild(   $xml->createElement('sex', $sender->sex));
        $object->sender->appendChild(   $xml->createElement('birthday', $sender->birthday));
        $object->sender->appendChild(   $xml->createElement('passport_ser', $sender->passport_ser));
        $object->sender->appendChild(   $xml->createElement('passport_num', $sender->passport_num));
        $object->sender->appendChild(   $xml->createElement('passport_day', $sender->passport_day));
        $object->sender->appendChild(   $xml->createElement('passport_give', $sender->passport_give));

        $object->address_sender = $xml->createElement('address_sender');
        $sender = $order->getaddress_sender();

        $object->address_sender->appendChild(   $xml->createElement('country', $sender->country));
        $object->address_sender->appendChild(   $xml->createElement('zip', $sender->zip));
        $object->address_sender->appendChild(   $xml->createElement('region', $sender->region));
        $object->address_sender->appendChild(   $xml->createElement('area', $sender->area));
        $object->address_sender->appendChild(   $xml->createElement('city', $sender->city));
        $object->address_sender->appendChild(   $xml->createElement('place', $sender->place));
        $object->address_sender->appendChild(   $xml->createElement('street', $sender->street));
        $object->address_sender->appendChild(   $xml->createElement('house', $sender->house));
        $object->address_sender->appendChild(   $xml->createElement('building', $sender->building));
        $object->address_sender->appendChild(   $xml->createElement('flat', $sender->flat));
        $object->address_sender->appendChild(   $xml->createElement('office', $sender->office));
        $object->address_sender->appendChild(   $xml->createElement('floor', $sender->floor));
        $object->address_sender->appendChild(   $xml->createElement('full', $sender->full));

        $object->person = $xml->createElement('person');
        $person = $order->getperson();

        $object->person->appendChild(   $xml->createElement('phone',        $person->phone));
        $object->person->appendChild(   $xml->createElement('email',        $person->email));
        $object->person->appendChild(   $xml->createElement('fio',          $person->fio));
        $object->person->appendChild(   $xml->createElement('name',         $person->name));
        $object->person->appendChild(   $xml->createElement('surname',      $person->surname));
        $object->person->appendChild(   $xml->createElement('patronymic',   $person->patronymic));
        $object->person->appendChild(   $xml->createElement('sex',          $person->sex));
        $object->person->appendChild(   $xml->createElement('birthday',     $person->birthday));
        $object->person->appendChild(   $xml->createElement('passport_ser', $person->passport_ser));
        $object->person->appendChild(   $xml->createElement('passport_num', $person->passport_num));
        $object->person->appendChild(   $xml->createElement('passport_day', $person->passport_day));
        $object->person->appendChild(   $xml->createElement('passport_give',$person->passport_give));

        $object->address = $xml->createElement('address');
        $address = $order->getaddress();

        $object->address->appendChild(   $xml->createElement('country',     $address->country));
        $object->address->appendChild(   $xml->createElement('zip',         $address->zip));
        $object->address->appendChild(   $xml->createElement('region',      $address->region));
        $object->address->appendChild(   $xml->createElement('area',        $address->area));
        $object->address->appendChild(   $xml->createElement('city',        $address->city));
        $object->address->appendChild(   $xml->createElement('place',       $address->place));
        $object->address->appendChild(   $xml->createElement('street',      $address->street));
        $object->address->appendChild(   $xml->createElement('house',       $address->house));
        $object->address->appendChild(   $xml->createElement('building',    $address->building));
        $object->address->appendChild(   $xml->createElement('flat',        $address->flat));
        $object->address->appendChild(   $xml->createElement('office',      $address->office));
        $object->address->appendChild(   $xml->createElement('floor',       $address->floor));
        $object->address->appendChild(   $xml->createElement('full',        $address->full));

        $object->client = $xml->createElement('client');
        $client = $order->getclient();

        $object->client->appendChild(   $xml->createElement('client_contract',     $client->client_contract));

        $object->order->appendChild($object->articles);
        $object->order->appendChild($object->client);
        $object->order->appendChild($object->address);
        $object->order->appendChild($object->person);
        $object->order->appendChild($object->address_sender);
        $object->order->appendChild($object->sender);
        $object->order->appendChild($object->dimension);
        $object->order->appendChild($object->cost);
        $object->order->appendChild($object->option);


        return $object->order;
    }

}