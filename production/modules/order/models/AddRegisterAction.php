<?php
/*
 * @name :: RegisterSettings
 * @comment :: Настройки для вывода информации об отправлении
 */
class RegisterSettings{
    public $country;    # Страна
    public $requestLen; # Возвращаемое значение количества созданных Уже записей
    public $form;       # Форма для добавления
    public $formName;   # Имя формы
    public $formModel;  # Модель формы
    public $HC;         # HandBook
    public $request;    # Отве который приходит в GET или POST
    public $messages;   # Сообщения
    public $articles;   # Вложения
}
class AddRegisterAction extends CAction{
    public static $REGISTER_NEW     = 'new';
    public static $REGISTER_EDIT    = 'edit';
    public static $REGISTER_SHOW    = 'show';
    public static $REGISTER_DELETE  = 'delete';
    public static $FORMAT_JSON      = 'json';
    public static $FORMAT_MODEL     = 'model';

    public function run($json= 'json', $model = NULL, $len = 0){
        if(true){
            $settings = new RegisterSettings();
            $settings->country      = Yii::app()->user->auth->db->country;
            $settings->request      = Yii::app()->request;
            $settings->requestLen   = $len16 = $settings->request->getParam('len16') ? $settings->request->getParam('len16') : 1;
            switch($settings->country){
                case 276:
                    $settings->formName     = Yii::import('application.modules.order.models.country.ger.GEROrderLoad');
                    break;
                default:
                    $settings->formName     = Yii::import('application.modules.order.models.OrderLoad');
            }
            $settings->formModel    = new $settings->formName;
            $settings->formName = 'OrderLoad'; # Костыль. Имя зафиксированно в скрипте
            $settings->articles = new DBarticles;

            ob_start();
            $settings->form = $this->getController()->beginWidget('bootstrap.widgets.TbActiveForm', array(
                'id'=>'location-fo',
                'type'=>'inline',
            ));
            ob_end_clean();
            $settings->HK = FuncMegapolis::Handbook();
            $settings->HK->time = array(
                0 =>  '00:00',
                1 =>  '01:00',
                2 =>  '02:00',
                3 =>  '03:00',
                4 =>  '04:00',
                5 =>  '05:00',
                6 =>  '06:00',
                7 =>  '07:00',
                8 =>  '08:00',
                9 =>  '09:00',
                10 => '10:00',
                11 => '11:00',
                12 => '12:00',
                13 => '13:00',
                14 => '14:00',
                15 => '15:00',
                16 => '16:00',
                17 => '17:00',
                18 => '18:00',
                19 => '19:00',
                20 => '20:00',
                21 => '21:00',
                22 => '22:00',
                23 => '23:00',
            );
            $settings->HK->bool = array(
                1 => 'Да',
                0 => 'Нет',
            );
            $settings->HK->document_select = array(
                0 => '',
                1 => 'Паспорт',
            );
            foreach($settings->HK->agent_id as $key=>$value){
                $settings->HK->agent_id[Yii::t("total", $key)] = Yii::t("total", $value);
            }
            foreach($settings->HK->op_type as $key=>$value){
                $settings->HK->op_type[Yii::t("total", $key)] = Yii::t("total", $value);
            }

            $settings->messages = array(
                'auto'     => Yii::t("total", 'автоматическое присвоение').'.',
                'forward'  => Yii::t("total", 'выбор, с последующим появлением').'.',
                'required' => Yii::t("total", 'обязательное поле').'.',
                'opt'      => Yii::t("total", 'не обязательное поле').'.',
                'reset_add'=> Yii::t("total", 'сброс').'.',
            );
            $settings->HK->country = array(
                'Россия' => 643,
                'Казахстан' => 398,
                'Беларусь' => 112,
            );
        }
        if($model){
            $settings->formModel = $model;
            $settings->requestLen+=1;
        }

        switch($settings->country){
            case 276:
                $result = $this->ger($settings);
                break;
            default:
                $result = $this->rus($settings);
        }



        if($json == self::$FORMAT_JSON){
            $len16 = $settings->requestLen;
            $text = '<div num='.$len16.' id="yw0_tab_'.$len16.'" class="tab-pane">'.$result.'</div>';
            die($text);
        }
        if($json == self::$FORMAT_MODEL) return $result;
    }
    public function ger($settings){
        $register = new stdClass();
        $register->form['model']    = $settings->formModel;
        $register->form['name']     = $settings->formName;
        $register->HK               = $settings->HK;
        $len16                      = $settings->requestLen;
        $labels                     = $settings->formModel->attributeLabels();
        $form                       = $settings->form;
        $messages                   = $settings->messages;
        $articles                   = $settings->articles;
        unset($register->HK->agent_id);
        $register->HK->agent_id['ИдеаЛоджик'] = 4;
        $register->HK->agent_id['Почта России'] = 2;

        $register->text = '';
        $register->text.='<table>';
        $register->text.='<tr>';
        $register->text.='<td>';
        $name='num';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'" style=""><span class="icon-lock"></span>&nbsp</a>';
        if(!Yii::app()->user->auth->range['cargo']){
            $register->text.= $form->textFieldRow($register->form['model'], $name, array('rows'=>2, 'class'=>'span2 '.$name, 'name' => $register->form['name'].'['.$name.']['.$len16.']', 'readonly' => 'readonly'));
        }else{
            $register->text.= $form->textFieldRow($register->form['model'], $name, array('rows'=>2, 'class'=>'span2 '.$name, 'name' => $register->form['name'].'['.$name.']['.$len16.']'));
        }
        $register->text.='</td>';
        $register->text.='<td colspan="2">';
        $register->text.= '<span num='.$len16.' class="delete"><a  rel="tooltip" title="'.Yii::t("total", 'удалить отправление').'" class="zakaz'.$len16.'" href=#>'.Yii::t("total", 'удалить').'</a></span>';
        $register->text.='</td>';
        $register->text.='</tr>';
        $register->text.='<tr>';
        $register->text.='<td>';
        $register->text.= "<div class='megapolis-order-table-header'>".Yii::t("total", 'оператор доставки').":</div>";

        $register->text.='</tr>';
        $register->text.='<tr>';
        $register->text.='<td>';
        $name = 'agent_id';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['required'].'" style=""><span class="icon-pencil"></span>&nbsp</a>';
        $register->text.= $form->dropDownListRow($register->form['model'], $name,  array('')+array_flip($register->HK->$name),  array('rows'=>2, 'class'=>'span2 agent_id',  'name' => $register->form['name'].'['.$name.']['.$len16.']' ,'labelOptions' => array('label' => false)));
        $register->text.='</td>';
        $register->text.='</tr>';
        $register->text.='<tr>';
        $register->text.='<td>';
        $name='op_type';
        $register->text.= '<a class="rel'.$len16.'" href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['required'].'" style=""><span class="icon-pencil"></span>&nbsp</a>';
        $register->text.= '<span class="rel'.$len16.'"  style="margin-right: 84px; font-weight: bold; font-size: 12px;">'.$labels[$name].': </span>';
        $register->text.= $form->dropDownListRow($register->form['model'], $name, array_flip($register->HK->op_type), array('class'=>'span4 op_type', 'name' => $register->form['name'].'['.$name.']['.$len16.']' ,'labelOptions' => array('label' => false)));
        $register->text.='</td>';
        $register->text.='</tr>';
        $register->text.='<tr>';
        $register->text.='<td>';
        $name = 'op_category';
        $register->text.= '<a class="rel'.$len16.'" href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['required'].'" style=""><span class="icon-pencil"></span>&nbsp</a>';
        $register->text.= '<span class="rel'.$len16.'"  style="margin-right: 47px; font-weight: bold; font-size: 12px;">'.$labels[$name].': </span>';
        $register->text.= $form->dropDownListRow($register->form['model'], $name, array_flip($register->HK->$name), array('class'=>'span2 op_category', 'name' => $register->form['name'].'['.$name.']['.$len16.']' ,'labelOptions' => array('label' => false)));
        $register->text.='</td>';
        $register->text.='</tr>';


        $register->text.='<tr>';
            $register->text.='<td>';
                $register->text.= "<div class='megapolis-order-table-header'>".Yii::t("total", 'отправитель').":</div>";
                $name = 'sender_name';
                    $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['required'].'" style=""><span class="icon-pencil"></span>&nbsp</a>';
                    $register->text.= $form->textFieldRow($register->form['model'], $name,array('rows'=>2, 'class'=>'span2 '.$name, 'name' => $register->form['name'].'['.$name.']['.$len16.']'));
                $name = 'sender_country';
                    $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['required'].'" style="margin-left: 10px"><span class="icon-pencil"></span>&nbsp</a>';
                    $register->text.= $form->textFieldRow($register->form['model'], $name,array('rows'=>2, 'class'=>'span2 '.$name, 'name' => $register->form['name'].'['.$name.']['.$len16.']'));
                $name = 'sender_zip';
                    $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['required'].'" style="margin-left: 10px"><span class="icon-pencil"></span>&nbsp</a>';
                    $register->text.= $form->textFieldRow($register->form['model'], $name,array('rows'=>2, 'class'=>'span2 '.$name, 'name' => $register->form['name'].'['.$name.']['.$len16.']'));
            $register->text.='</td>';
        $register->text.='</tr>';
        $register->text.='<tr>';
        $register->text.='<td>';

        $name = 'sender_city';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['required'].'" style=""><span class="icon-pencil"></span>&nbsp</a>';
        $register->text.= $form->textFieldRow($register->form['model'], $name,array('rows'=>2, 'class'=>'span2 '.$name, 'name' => $register->form['name'].'['.$name.']['.$len16.']'));

        $name = 'sender_area';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['required'].'" style="margin-left: 10px"><span class="icon-pencil"></span>&nbsp</a>';
        $register->text.= $form->textFieldRow($register->form['model'], $name,array('rows'=>2, 'class'=>'span2 '.$name, 'name' => $register->form['name'].'['.$name.']['.$len16.']'));

        $name = 'sender_region';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['required'].'" style="margin-left: 10px"><span class="icon-pencil"></span>&nbsp</a>';
        $register->text.= $form->textFieldRow($register->form['model'], $name,array('rows'=>2, 'class'=>'span2 '.$name, 'name' => $register->form['name'].'['.$name.']['.$len16.']'));

        # Отображать загрузку документа
        $register->text.='</td>';
        $register->text.='</tr>';
        $register->text.='<tr>';
            $register->text.='<td>';
            $name = 'sender_street';
            $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['required'].'" style=""><span class="icon-pencil"></span>&nbsp</a>';
            $register->text.= $form->textFieldRow($register->form['model'], $name,array('rows'=>2, 'class'=>'span2 '.$name, 'name' => $register->form['name'].'['.$name.']['.$len16.']'));
            $name = 'sender_house';
            $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['required'].'" style="margin-left: 10px"><span class="icon-pencil"></span>&nbsp</a>';
            $register->text.= $form->textFieldRow($register->form['model'], $name,array('rows'=>2, 'class'=>'span2 '.$name, 'name' => $register->form['name'].'['.$name.']['.$len16.']'));
            $register->text.='</td>';
        $register->text.='</tr>';
        $register->text.='<tr>';
            $register->text.='<td>';

                $name='sender_flat';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['required'].'" style=""><span class="icon-pencil"></span>&nbsp</a>';
                $register->text.= $form->textFieldRow($register->form['model'], $name,array('rows'=>2, 'class'=>'span2', 'name' => $register->form['name'].'['.$name.']['.$len16.']'));
                $name='sender_building';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['required'].'" style="margin-left: 10px"><span class="icon-pencil"></span>&nbsp</a>';
                $register->text.= $form->textFieldRow($register->form['model'], $name, array('rows'=>2, 'class'=>'span2', 'name' => $register->form['name'].'['.$name.']['.$len16.']'));
            $register->text.='</td>';
        $register->text.='</tr>';



















        $register->text.='<tr>';
            $register->text.='<td>';
                $register->text.= "<div class='megapolis-order-table-header'>".Yii::t("total", 'получатель').":</div>";
            $register->text.='</td>';
        $register->text.='</tr>';
        $register->text.='<tr>';
            $register->text.='<td>';
                $name = 'fio';
                $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['required'].'" style=""><span class="icon-pencil"></span>&nbsp</a>';
                $register->text.= $form->textFieldRow($register->form['model'], $name, array('rows'=>2, 'class'=>'span4 '.$name, 'hint'=>'наименование организации, ФИО полностью', 'name' => $register->form['name'].'['.$name.']['.$len16.']'));
                $name = 'phone';
                $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['required'].'" style="margin-left: 10px"><span class="icon-pencil"></span>&nbsp</a>';
                $register->text.= $form->textFieldRow($register->form['model'],$name,array('rows'=>2, 'class'=>'span2 '.$name, 'name' => $register->form['name'].'['.$name.']['.$len16.']'));
            $register->text.='</td>';
        $register->text.='</tr>';
        $register->text.='<tr>';
        $register->text.='<td>';
        $register->text.= "<div class='megapolis-order-table-header'>".Yii::t("total", 'адрес получателя').":</div>";
        $name = 'country';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['auto'].'" style=""><span class="icon-lock"></span>&nbsp</a>';
        $register->text.= $form->textFieldRow($register->form['model'],$name,array('rows'=>2, 'class'=>'span2', 'name' => $register->form['name'].'['.$name.']['.$len16.']', 'value' => 'Россия', 'readonly' => 'readonly'));

        $name = 'zip';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['auto'].'" style="margin-left: 10px"><span class="icon-lock"></span>&nbsp</a>';
        $register->text.= $form->textFieldRow($register->form['model'],$name,array('rows'=>2, 'class'=>'span2 '.$name, 'name' => $register->form['name'].'['.$name.']['.$len16.']', 'readonly' => 'true'));
        $register->text.='</td>';
        $register->text.='</tr>';
        $register->text.='<tr>';
        $register->text.='<td>';

        $name = 'city';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['forward'].'"><span class="icon-forward"></span>&nbsp</a>';
        ob_start();
        $form->widget('application.components.widget.TbTypeahead', array(
            'name'=> $name,
            'attribute' => $register->form['model']->attributes,
            'model' => $register->form['model'],
            'form' => $form,
            'id' => $register->form['name'].'_'.$name.'_'.$len16,
            'options'=>array(
                'items'=> 10,
                'matcher'=>"js:function(item, process) {
                            if(!$('#".$register->form['name'].'_region_'.$len16."').val()){
                                text = item;
                            }else{
                                var text = item.split(';');
                                text = text[1];
                            }
                            return text;
                        }",
                'source'=>"js:function(item, process) {
                            if(item.length > MEG.address.len){
                                if(!$('#".$register->form['name'].'_region_'.$len16."').val()){
                                console.log(2);
                                     $.ajax({
                                        data: {item : item, limit:MEG.address.limit, format: 'json'},
                                        dataType:'json',
                                        type: 'POST',
                                        url: MEG.settings.url1,
                                        success: process,
                                        beforeSend: function(){
                                            if(MEG.main.req){
                                            return false;
                                            }else{
                                                MEG.main.req = 1;
                                            }
                                        },
                                        complete: function(){
                                                MEG.main.req = 0;
                                        }
                                    });
                                }else if(!$('#".$register->form['name'].'_area_'.$len16."').val()){
                                    item=$('#".$register->form['name'].'_region_'.$len16."').val()+','+item;
                                    console.log(2);
                                    $.ajax({
                                        dataType:'json',
                                        data: {item : item, limit: MEG.address.limit, search: 'region_city', format: 'json'},
                                        type: 'POST',
                                        url: MEG.settings.url1,
                                        success: process,
                                        beforeSend: function(){
                                            if(MEG.main.req){
                                            return false;
                                            }else{
                                                MEG.main.req = 1;
                                            }
                                        },
                                        complete: function(){
                                                MEG.main.req = 0;
                                        }
                                    });
                                }else{
                                    item=$('#".$register->form['name'].'_region_'.$len16."').val()+','+$('#".$register->form['name'].'_area_'.$len16."').val()+','+item;
                                    console.log(4);
                                    $.ajax({
                                        dataType:'json',
                                        data: {item : item, limit: MEG.address.limit, search: 'city', format: 'json'},
                                        type: 'POST',
                                        url: MEG.settings.url1,
                                        success: process,
                                        beforeSend: function(){
                                            if(MEG.main.req){
                                            return false;
                                            }else{
                                                MEG.main.req = 1;
                                            }
                                        },
                                        complete: function(){
                                                MEG.main.req = 0;
                                        }
                                    });
                                }
                            }
                        }",
                'highlighter'=>"js:function(item) {
                                if(!$('#".$register->form['name'].'_region_'.$len16."').val()){
                                    var t = item.split(',').splice(2);
                                    item = t.join(', ');

                                }
                                return item;
                            }",

                'updater'=>"js:function(item){
                       if(!$('#".$register->form['name'].'_region_'.$len16."').val()){
                            var zip = item.split(',').splice(0,1);
                            $('#".$register->form['name'].'_zip_'.$len16."').val(zip);
                            var t = item.split(',').splice(2);
                            var item = t.join(', ');
                            var i = t;
                            if(t.length == 3){
                                $('#".$register->form['name'].'_region_'.$len16."').val(i[2]);
                                $('#".$register->form['name'].'_area_'.$len16."').val(i[1]);
                                item = t[0];
                            }else if(t.length == 2){

                                $('#".$register->form['name'].'_region_'.$len16."').val(i[1]);
                                item = t[0];
                            }else if(t.length == 1){
                                $('#".$register->form['name'].'_region_'.$len16."').val(i[0]);

                                item = i[0];
                            }
                            return item;
                       }else{
                            var text = item.split(';');
                            item = text[1];
                            $('#".$register->form['name'].'_zip_'.$len16."').val(text[0]);
                            MEG.address.city[".$len16."] = item;
                            return MEG.address.city[".$len16."];
                       }
                }",

            ),
            'htmlOptions' => array(
                'class' => 'span2 '.$name,
                'name'  => $register->form['name'].'['.$name.']['.$len16.']',
                'autocomplete' => "off",
            ),
        ));
        $out = ob_get_contents();
        ob_end_clean();
        $register->text.= $out;

        $name = 'area';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['forward'].'" style="margin-left: 10px"><span class="icon-forward"></span>&nbsp</a>';
        ob_start();
        $form->widget('application.components.widget.TbTypeahead', array(
            'name'=> $name,
            'attribute' => $register->form['model']->attributes,
            'model' => $register->form['model'],
            'form' => $form,
            'id' => $register->form['name'].'_'.$name.'_'.$len16,
            'options'=>array(
                'items'=> 10,
                'matcher'=>"js:function(item) {
                            return item;
                        }",
                'source'=>'js:function(item, process) {
                            if(item.length > MEG.address.len){
                                item=MEG.address.region['.$len16.']+","+item;
                                $.ajax({
                                    dataType:"json",
                                    data: {item : item, limit: MEG.address.limit, search: "area", format: "json"},
                                    type: "POST",
                                    url: MEG.settings.url1,
                                    success: process,
                                    beforeSend: function(){
                                        if(MEG.main.req){
                                            return false;
                                        }else{
                                            MEG.main.req = 1;
                                        }
                                    },
                                    complete: function(){
                                            MEG.main.req = 0;
                                    },
                                });
                            }
                        }',
                'updater'=>"js:function(item){
                            MEG.address.area[".$len16."] = item;
                            return MEG.address.area[".$len16."];
                        }",
            ),
            'htmlOptions' => array(
                'class' => 'span2 '.$name,
                'name'  => $register->form['name'].'['.$name.']['.$len16.']',
                'autocomplete' => "off",
            ),
        ));
        $out = ob_get_contents();
        ob_end_clean();
        $register->text.= $out;



        $name = 'region';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['forward'].'" style="margin-left: 10px"><span class="icon-forward"></span>&nbsp</a>';
        ob_start();
        $form->widget('application.components.widget.TbTypeahead', array(
            'name'=> $name,
            'attribute' => $register->form['model']->attributes,
            'model' => $register->form['model'],
            'form' => $form,
            'id' => $register->form['name'].'_'.$name.'_'.$len16,
            'options'=>array(
                'items'=> 10,
                'source'=>'js:function(item, process) {
                            if(item.length > MEG.address.len){
                                $.ajax({
                                    dataType:"json",
                                    data: {item : item, limit: MEG.address.limit, search: "region", format: "json"},
                                    type: "POST",
                                    url: MEG.settings.url1,
                                    success: process,
                                    beforeSend: function(){
                                        if(MEG.main.req){
                                            return false;
                                        }else{
                                            MEG.main.req = 1;
                                        }
                                    },
                                    complete: function(){
                                            MEG.main.req = 0;
                                    },
                                });
                            }
                        }',
                'updater'=>"js:function(item) {
                            MEG.address.region[".$len16."] = item;
                            var t = item.split(';').splice(1);
                            var zip = item.split(';');
                            zip = zip[0];
                            var flag=0
                            item = t;
                            if(t == 'МОСКВА Г' || t == 'САНКТ-ПЕТЕРБУРГ Г' || t == 'БАЙКОНУР Г' || t== 'СЕВАСТОПОЛЬ Г') flag=1;
                            MEG.address.region[".$len16."] = item;
                            if(flag){
                                $('#".$register->form['name'].'_zip_'.$len16."').val(zip);
                            }else{
                                $('#".$register->form['name'].'_area_'.$len16."').removeAttr('readOnly');
                            }
                            return t;
                        }",
                'highlighter'=>"js:function(item) {
                                return item.split(';').splice(1);
                            }",
            ),
            'htmlOptions' => array(
                'class' => 'span2 '.$name,
                'name'  => $register->form['name'].'['.$name.']['.$len16.']',
                'autocomplete' => "off",
            ),
        ));
        $out = ob_get_contents();
        ob_end_clean();

        $register->text.= $out;

        $register->text.= '<a href="#" class="reset" name="'.$len16.'" rel="tooltip" title='.$messages['reset_add'].' style="margin-left: 10px"><span class="icon-remove"></span>&nbsp</a>';
        # Отображать загрузку документа
        $register->text.='</td>';
        $register->text.='</tr>';
        $register->text.='<tr>';
        $register->text.='<td>';
        $name = 'street';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['required'].'" style=""><span class="icon-pencil"></span>&nbsp</a>';
        $register->text.= $form->textFieldRow($register->form['model'], $name,array('rows'=>2, 'class'=>'span2 '.$name, 'name' => $register->form['name'].'['.$name.']['.$len16.']'));
        $name='house';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['required'].'" style="margin-left: 10px"><span class="icon-pencil"></span>&nbsp</a>';
        $register->text.= $form->textFieldRow($register->form['model'], $name,array('rows'=>2, 'class'=>'span2 '.$name, 'name' => $register->form['name'].'['.$name.']['.$len16.']'));
        $register->text.='</td>';
        $register->text.='</tr>';
        $register->text.='<tr>';
        $register->text.='<td>';
        $name='office';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['opt'].'" style=""><span class="icon-flag"></span>&nbsp</a>';
        $register->text.= $form->textFieldRow($register->form['model'], $name,array('rows'=>2, 'class'=>'span2', 'name' => $register->form['name'].'['.$name.']['.$len16.']'));
        $name='floor';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['opt'].'" style="margin-left: 10px"><span class="icon-flag"></span>&nbsp</a>';
        $register->text.= $form->textFieldRow($register->form['model'], $name,array('rows'=>2, 'class'=>'span2', 'name' => $register->form['name'].'['.$name.']['.$len16.']'));
        $name='housing';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['opt'].'" style="margin-left: 10px"><span class="icon-flag"></span>&nbsp</a>';
        $register->text.= $form->textFieldRow($register->form['model'], $name, array('rows'=>2, 'class'=>'span2', 'name' => $register->form['name'].'['.$name.']['.$len16.']'));
        $name='intercom';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['opt'].'" style="margin-left: 10px"><span class="icon-flag"></span>&nbsp</a>';
        $register->text.= $form->textFieldRow($register->form['model'],$name,array('rows'=>2, 'class'=>'span2', 'name' => $register->form['name'].'['.$name.']['.$len16.']'));
        $register->text.='</td>';
        $register->text.='</tr>';

        $register->text.='<tr>';
            $register->text.='<td>';
                $register->text.= "<div class='megapolis-order-table-header'>Характеристики отправления:</div>";
            $register->text.='</td>';
        $register->text.='</tr>';
        $register->text.='<tr>';
            $register->text.='<td>';
                $name='export_declaration';
                $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['opt'].'" style=""><span class="icon-flag"></span>&nbsp</a>';
                $register->text.= '<span style="margin-right: 37px; font-weight: bold; font-size: 12px;">'.$labels[$name].': </span>';
                $register->text.= $form->textFieldRow($register->form['model'], $name,array('rows'=>2, 'class'=>'span2', 'name' => $register->form['name'].'['.$name.']['.$len16.']'));
            $register->text.='</td>';
        $register->text.='</tr>';
        $register->text.='<tr>';
            $register->text.='<td>';
                $name='invoice';
                $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['opt'].'" style=""><span class="icon-flag"></span>&nbsp</a>';
                $register->text.= '<span style="margin-right: 37px; font-weight: bold; font-size: 12px;">'.$labels[$name].': </span>';
                $register->text.= $form->textFieldRow($register->form['model'], $name,array('rows'=>2, 'class'=>'span2', 'name' => $register->form['name'].'['.$name.']['.$len16.']'));
            $register->text.='</td>';
        $register->text.='</tr>';
        $register->text.='<tr>';
            $register->text.='<td>';
                $name='op_inventory';
                $register->form['model']->$name = 0;
                $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['opt'].'" style=""><span class="icon-flag"></span>&nbsp</a>';
                $register->text.= '<span style="margin-right: 37px; font-weight: bold; font-size: 12px;">'.$labels[$name].': </span>';
                $register->text.= $form->dropDownListRow($register->form['model'], $name, $register->HK->bool, array('rows'=>2, 'class'=>'span1', 'name' => $register->form['name'].'['.$name.']['.$len16.']', 'labelOptions' => array('label' => false), 'hint'=>'Labels surround all the options for much larger click areas.' ));
            $register->text.='</td>';
        $register->text.='</tr>';
        $register->text.='<tr>';
            $register->text.='<td>';
                $name='op_fragile';
                $register->form['model']->$name = 0;
                $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['required'].'" style=""><span class="icon-pencil"></span>&nbsp</a>';
                $register->text.= '<span style="margin-right: 76px; font-weight: bold; font-size: 12px;">'.$labels[$name].': </span>';
                $register->text.= $form->dropDownListRow($register->form['model'], $name, $register->HK->bool, array('rows'=>2, 'class'=>'span1', 'name' => $register->form['name'].'['.$name.']['.$len16.']', 'labelOptions' => array('label' => false)));
            $register->text.='</td>';
        $register->text.='</tr>';
        $register->text.='<tr>';
            $register->text.='<td>';
                $name='wd';

                $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['opt'].'" style=""><span class="icon-pencil"></span>&nbsp</a>';
                $register->text.= $form->textFieldRow($register->form['model'],$name,array('class'=>'span2 '.$name,  'name' => $register->form['name'].'['.$name.']['.$len16.']', 'hint' => 'округление до 1-тысячной') );
            $register->text.='</td>';
        $register->text.='</tr>';
        $register->text.='<tr>';
        $register->text.='<td>';
        $name='cost_public';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['required'].'"><span class="icon-pencil"></span>&nbsp</a>';
        $register->text.= '<span style="margin-right: 7px; font-weight: bold; font-size: 12px;">'.$labels[$name].': </span>';
        $register->text.= $form->textFieldRow($register->form['model'],$name,array('class'=>'span2 '.$name, 'name' => $register->form['name'].'['.$name.']['.$len16.']'));
        $register->text.='</td>';
        $register->text.='</tr>';
        $register->text.='<tr>';
            $register->text.='<td>';
            $name='cost_delivery';
            $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['required'].'"><span class="icon-pencil"></span>&nbsp</a>';
            $register->text.= '<span style="margin-right: 25px; font-weight: bold; font-size: 12px;">'.$labels[$name].': </span>';
            $register->text.= $form->textFieldRow($register->form['model'],$name,array('class'=>'span2 '.$name, 'name' => $register->form['name'].'['.$name.']['.$len16.']'));
            $register->text.='</td>';
        $register->text.='</tr>';
        $register->text.='<tr>';
            $register->text.='<td>';
                $register->text.= "<div class='megapolis-order-table-header header_document'>Документ, удостоверяющий личность получателя:</div>";
            $register->text.='</td>';
        $register->text.='</tr>';
        $register->text.='<tr>';
            $register->text.='<td>';
                $name='document_select';
                $register->form['model']->$name = 1;
                $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['opt'].'" style=""><span class="icon-flag"></span>&nbsp</a>';
                $register->text.= '<span style="margin-right: 37px; font-weight: bold; font-size: 12px;">'.$labels[$name].': </span>';
                $register->text.= $form->dropDownListRow($register->form['model'], $name, $register->HK->document_select, array('class'=>'span2 '.$name, 'name' => $register->form['name'].'['.$name.']['.$len16.']', 'labelOptions' => array('label' => false), 'readonly' => 'readonly'));
            $register->text.='</td>';
        $register->text.='</tr>';

        $register->text.='<tr>';
            $register->text.='<td>';
                $name='passport_ser';
                $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['opt'].'" style=""><span class="icon-pencil"></span>&nbsp</a>';
                $register->text.= '<span style="margin-right: 25px; font-weight: bold; font-size: 12px;">'.$labels[$name].': </span>';
                $register->text.= $form->textFieldRow($register->form['model'],$name,array('class'=>'span2 '.$name,  'name' => $register->form['name'].'['.$name.']['.$len16.']', 'hint' => 'округление до 1-тысячной') );
            $register->text.='</td>';
        $register->text.='</tr>';
        $register->text.='<tr>';
            $register->text.='<td>';
                $name='passport_num';
                $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['opt'].'" style=""><span class="icon-pencil"></span>&nbsp</a>';
                $register->text.= '<span style="margin-right: 25px; font-weight: bold; font-size: 12px;">'.$labels[$name].': </span>';
                $register->text.= $form->textFieldRow($register->form['model'],$name,array('class'=>'span2 '.$name,  'name' => $register->form['name'].'['.$name.']['.$len16.']') );
            $register->text.='</td>';
        $register->text.='</tr>';
        $register->text.='<tr>';
            $register->text.='<td>';
                $name='passport_day';
                $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['opt'].'" style=""><span class="icon-pencil"></span>&nbsp</a>';
                $register->text.= '<span style="margin-right: 25px; font-weight: bold; font-size: 12px;">'.$labels[$name].': </span>';
                $register->text.= $form->textFieldRow($register->form['model'],$name,array('class'=>'span2 '.$name,  'name' => $register->form['name'].'['.$name.']['.$len16.']') );
            $register->text.='</td>';
        $register->text.='</tr>';
        $register->text.='<tr>';
            $register->text.='<td>';
                $name='passport_give';
                $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['opt'].'" style=""><span class="icon-pencil"></span>&nbsp</a>';
                $register->text.= '<span style="margin-right: 25px; font-weight: bold; font-size: 12px;">'.$labels[$name].': </span>';
                $register->text.= $form->textFieldRow($register->form['model'],$name,array('class'=>'span4 '.$name,  'name' => $register->form['name'].'['.$name.']['.$len16.']') );
            $register->text.='</td>';
        $register->text.='</tr>';

        $register->text.='<tr>';
            $register->text.='<td style="padding-top: 31px;">';
                $name='dai';
                $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['opt'].'" style=""><span class="icon-flag"></span>&nbsp</a>';
                $register->text.= $form->textFieldRow($register->form['model'],$name,array('rows'=>2, 'class'=>'span6', 'name' => $register->form['name'].'['.$name.']['.$len16.']'));
            $register->text.='</td>';
        $register->text.='</tr>';
        $register->text.='<tr>';
            $register->text.='<td>';
                $name='comment';
                $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['opt'].'" style=""><span class="icon-flag"></span>&nbsp</a>';
                $register->text.= $form->textAreaRow($register->form['model'],'comment',array('rows'=>2, 'class'=>'span6', 'name' => $register->form['name'].'['.$name.']['.$len16.']'));
            $register->text.='</td>';
        $register->text.='</tr>';

        $register->text.='<tr>';
            $register->text.='<td>';
                $register->text.= "<div class='megapolis-order-table-header'>Вложения:</div>";
            $register->text.='</td>';
        $register->text.='</tr>';

        $register->text.='<tr>';
            $register->text.='<td>';
                ob_start();
                $this->getController()->widget('bootstrap.widgets.TbGridView', array(
                    'type'=>'striped bordered condensed',
                    'dataProvider'=>  $articles->dataProvider(),
                    'template'=>"{items}\n{pager}",
                    'emptyText' => "<a class='addGoods' link='#'>Добавить товар</a>",
                    'columns'=>array(
                        array(
                            'name'=>'id',
                            'header'=>'№ П.П',
                        ),
                        array(
                            'name'=>'id',
                            'header'=>'Наименование товара',
                        ),
                        array(
                            'name'=>'id',
                            'header'=>'Количество',
                        ),
                        array(
                            'name'=>'id',
                            'header'=>' ВЕС КГ',
                        ),
                        array(
                            'name'=>'id',
                            'header'=>'стоимость Euro',
                        ),
                        array(
                            'name'=>'id',
                            'header'=>'Цена за единицу (вал.)',
                        ),
                        array(
                            'name'=>'id',
                            'header'=>'Стоимость (руб.)',
                        ),
                        array(
                            'name'=>'id',
                            'header'=>'ТН ВЭД',
                        ),
                        array(
                            'name'=>'id',
                            'header'=>'Номер каталога',
                        ),
                        array(
                            'name'=>'id',
                            'header'=>'Артикул ',
                        ),
                        array(
                            'name'=>'WEB',
                            'header'=>'WEB',
                        ),
                    ),
                ));

                $out = ob_get_contents();
                ob_end_clean();

                $register->text.= $out;
            $register->text.='</td>';
        $register->text.='</tr>';

        $register->text.='</table>';
        $name='wdays';

        $name='register_hidden';
        $register->text.= CHtml::hiddenField('hidden', '', array('id' => $register->form['name'].'['.$name.']['.$len16.']'));
        ob_start();
        $this->getController()->endWidget();
        ob_end_clean();

        return $register->text;
    }

    public function rus($settings){

        $register = new stdClass();
        $register->form['model']    = $settings->formModel;
        $register->form['name']     = $settings->formName;
        $register->HK               = $settings->HK;
        $len16                      = $settings->requestLen;
        $labels                     = $settings->formModel->attributeLabels();
        $form                       = $settings->form;
        $messages                   = $settings->messages;


        $register->text = '';
        $register->text.='<table>';
        $register->text.='<tr>';
        $register->text.='<td>';
        $name='num';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'" style=""><span class="icon-lock"></span>&nbsp</a>';
        if(!Yii::app()->user->auth->range['cargo']){
            $register->text.= $form->textFieldRow($register->form['model'], $name, array('rows'=>2, 'class'=>'span2 '.$name, 'name' => $register->form['name'].'['.$name.']['.$len16.']', 'readonly' => 'readonly'));
        }else{
            $register->text.= $form->textFieldRow($register->form['model'], $name, array('rows'=>2, 'class'=>'span2 '.$name, 'name' => $register->form['name'].'['.$name.']['.$len16.']'));
        }
        $register->text.='</td>';
        $register->text.='<td colspan="2">';
        $register->text.= '<span num='.$len16.' class="delete"><a  rel="tooltip" title="'.Yii::t("total", 'удалить отправление').'" class="zakaz'.$len16.'" href=#>'.Yii::t("total", 'удалить').'</a></span>';
        $register->text.='</td>';
        $register->text.='</tr>';
        $register->text.='<tr>';
        $register->text.='<td>';
        $register->text.= "<div class='megapolis-order-table-header'>".Yii::t("total", 'оператор доставки').":</div>";

        $register->text.='</tr>';
        $register->text.='<tr>';
        $register->text.='<td>';
        $name = 'agent_id';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['required'].'" style=""><span class="icon-pencil"></span>&nbsp</a>';
        $register->text.= $form->dropDownListRow($register->form['model'], $name,  array('')+array_flip($register->HK->$name),  array('rows'=>2, 'class'=>'span2 agent_id',  'name' => $register->form['name'].'['.$name.']['.$len16.']' ,'labelOptions' => array('label' => false)));
        $register->text.='</td>';
        $register->text.='</tr>';
        $register->text.='<tr>';
        $register->text.='<td>';
        $name='op_type';
        $register->text.= '<a class="rel'.$len16.'" href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['required'].'" style=""><span class="icon-pencil"></span>&nbsp</a>';
        $register->text.= '<span class="rel'.$len16.'"  style="margin-right: 92px; font-weight: bold; font-size: 12px;">'.$labels[$name].': </span>';
        $register->text.= $form->dropDownListRow($register->form['model'], $name, array_flip($register->HK->op_type), array('class'=>'span4 op_type', 'name' => $register->form['name'].'['.$name.']['.$len16.']' ,'labelOptions' => array('label' => false)));
        $register->text.='</td>';
        $register->text.='</tr>';
        $register->text.='<tr>';
        $register->text.='<td>';
        $name = 'op_category';
        $register->text.= '<a class="rel'.$len16.'" href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['required'].'" style=""><span class="icon-pencil"></span>&nbsp</a>';
        $register->text.= '<span class="rel'.$len16.'"  style="margin-right: 47px; font-weight: bold; font-size: 12px;">'.$labels[$name].': </span>';
        $register->text.= $form->dropDownListRow($register->form['model'], $name, array_flip($register->HK->$name), array('class'=>'span3 op_category', 'name' => $register->form['name'].'['.$name.']['.$len16.']' ,'labelOptions' => array('label' => false)));
        $register->text.='</td>';
        $register->text.='</tr>';

        $register->text.='<tr>';
        $register->text.='<td>';
        $register->text.= "<div class='megapolis-order-table-header'>".Yii::t("total", 'получатель').":</div>";
        $register->text.='</td>';
        $register->text.='</tr>';
        $register->text.='<tr>';
        $register->text.='<td>';
        $name = 'fio';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['required'].'" style=""><span class="icon-pencil"></span>&nbsp</a>';
        $register->text.= $form->textFieldRow($register->form['model'], $name, array('rows'=>2, 'class'=>'span4 '.$name, 'hint'=>'наименование организации, ФИО полностью', 'name' => $register->form['name'].'['.$name.']['.$len16.']'));
        $name = 'phone';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['required'].'" style="margin-left: 10px"><span class="icon-pencil"></span>&nbsp</a>';
        $register->text.= $form->textFieldRow($register->form['model'],$name,array('rows'=>2, 'class'=>'span2 '.$name, 'name' => $register->form['name'].'['.$name.']['.$len16.']'));
        $register->text.='</td>';
        $register->text.='</tr>';
        $register->text.='<tr>';
        $register->text.='<td>';

        $register->text.= "<div class='megapolis-order-table-header'>".Yii::t("total", 'адрес получателя').":</div>";
        $name = 'country';

        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['auto'].'" style=""><span class="icon-lock"></span>&nbsp</a>';
        $register->text.= $form->dropDownListRow($register->form['model'], $name, array_flip($register->HK->$name), array('class'=>'span2 country', 'name' => $register->form['name'].'['.$name.']['.$len16.']' ,'labelOptions' => array('label' => false), 'disabled' => 'disabled'));

        #$register->text.= $form->textFieldRow($register->form['model'],$name,array('rows'=>2, 'class'=>'span2', 'name' => $register->form['name'].'['.$name.']['.$len16.']', 'value' => 'Россия', 'readonly' => 'readonly'));








        $name = 'zip';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['auto'].'" style="margin-left: 10px"><span class="icon-lock"></span>&nbsp</a>';
        $register->text.= $form->textFieldRow($register->form['model'],$name,array('rows'=>2, 'class'=>'span2 '.$name, 'name' => $register->form['name'].'['.$name.']['.$len16.']', 'readonly' => 'true'));
        $register->text.='</td>';
        $register->text.='</tr>';
        $register->text.='<tr>';
        $register->text.='<td>';

        $name = 'city';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['forward'].'"><span class="icon-forward"></span>&nbsp</a>';
        ob_start();
        $form->widget('application.components.widget.TbTypeahead', array(
            'name'=> $name,
            'attribute' => $register->form['model']->attributes,
            'model' => $register->form['model'],
            'form' => $form,
            'id' => $register->form['name'].'_'.$name.'_'.$len16,
            'options'=>array(
                'items'=> 10,
                'matcher'=>"js:function(item, process) {
                            if(!$('#".$register->form['name'].'_region_'.$len16."').val()){
                                text = item;
                            }else{
                                var text = item.split(';');
                                text = text[1];
                            }
                            return text;
                        }",
                'source'=>"js:function(item, process) {
                    var od = $('#".$register->form['name'].'_agent_id_'.$len16."').val();

                    if(!MEG.address.autoComplete) return false;
                            if(item.length > MEG.address.len){
                                if(!$('#".$register->form['name'].'_region_'.$len16."').val()){
                                     $.ajax({
                                        data: {item : item, limit:MEG.address.limit, format: 'json', od : od},
                                        dataType:'json',
                                        type: 'POST',
                                        url: MEG.settings.url1,
                                        success: process,
                                        beforeSend: function(){
                                            if(MEG.main.req){
                                            return false;
                                            }else{
                                                MEG.main.req = 1;
                                            }
                                        },
                                        complete: function(){
                                                MEG.main.req = 0;
                                        }
                                    });
                                }else if(!$('#".$register->form['name'].'_area_'.$len16."').val()){
                                    item=$('#".$register->form['name'].'_region_'.$len16."').val()+','+item;
                                    $.ajax({
                                        dataType:'json',
                                        data: {item : item, limit: MEG.address.limit, search: 'region_city', format: 'json', od : od},
                                        type: 'POST',
                                        url: MEG.settings.url1,
                                        success: process,
                                        beforeSend: function(){
                                            if(MEG.main.req){
                                            return false;
                                            }else{
                                                MEG.main.req = 1;
                                            }
                                        },
                                        complete: function(){
                                                MEG.main.req = 0;
                                        }
                                    });
                                }else{
                                    item=$('#".$register->form['name'].'_region_'.$len16."').val()+','+$('#".$register->form['name'].'_area_'.$len16."').val()+','+item;
                                    $.ajax({
                                        dataType:'json',
                                        data: {item : item, limit: MEG.address.limit, search: 'city', format: 'json', od : od},
                                        type: 'POST',
                                        url: MEG.settings.url1,
                                        success: process,
                                        beforeSend: function(){
                                            if(MEG.main.req){
                                            return false;
                                            }else{
                                                MEG.main.req = 1;
                                            }
                                        },
                                        complete: function(){
                                                MEG.main.req = 0;
                                        }
                                    });
                                }
                            }
                        }",
                'highlighter'=>"js:function(item) {
                                if(!$('#".$register->form['name'].'_region_'.$len16."').val()){
                                    var t = item.split(',').splice(2);
                                    item = t.join(', ');

                                }
                                return item;
                            }",

                'updater'=>"js:function(item){
                       if(!$('#".$register->form['name'].'_region_'.$len16."').val()){
                            var zip = item.split(',').splice(0,1);
                            $('#".$register->form['name'].'_zip_'.$len16."').val(zip);
                            var t = item.split(',').splice(2);
                            var item = t.join(', ');
                            var i = t;
                            if(t.length == 3){
                                $('#".$register->form['name'].'_region_'.$len16."').val(i[2]);
                                $('#".$register->form['name'].'_area_'.$len16."').val(i[1]);
                                item = t[0];
                            }else if(t.length == 2){

                                $('#".$register->form['name'].'_region_'.$len16."').val(i[1]);
                                item = t[0];
                            }else if(t.length == 1){
                                $('#".$register->form['name'].'_region_'.$len16."').val(i[0]);

                                item = i[0];
                            }
                            return item;
                       }else{
                            var text = item.split(';');
                            item = text[1];
                            $('#".$register->form['name'].'_zip_'.$len16."').val(text[0]);
                            MEG.address.city[".$len16."] = item;
                            return MEG.address.city[".$len16."];
                       }
                }",

            ),
            'htmlOptions' => array(
                'class' => 'span2 '.$name,
                'name'  => $register->form['name'].'['.$name.']['.$len16.']',
                'autocomplete' => "off",
            ),
        ));
        $out = ob_get_contents();
        ob_end_clean();
        $register->text.= $out;

        $name = 'area';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['forward'].'" style="margin-left: 10px"><span class="icon-forward"></span>&nbsp</a>';
        ob_start();
        $form->widget('application.components.widget.TbTypeahead', array(
            'name'=> $name,
            'attribute' => $register->form['model']->attributes,
            'model' => $register->form['model'],
            'form' => $form,
            'id' => $register->form['name'].'_'.$name.'_'.$len16,
            'options'=>array(
                'items'=> 10,
                'matcher'=>"js:function(item) {
                            return item;
                        }",
                'source'=>'js:function(item, process) {
                            if(!MEG.address.autoComplete) return false;
                            if(item.length > MEG.address.len){
                                item=MEG.address.region['.$len16.']+","+item;
                                $.ajax({
                                    dataType:"json",
                                    data: {item : item, limit: MEG.address.limit, search: "area", format: "json"},
                                    type: "POST",
                                    url: MEG.settings.url1,
                                    success: process,
                                    beforeSend: function(){
                                        if(MEG.main.req){
                                            return false;
                                        }else{
                                            MEG.main.req = 1;
                                        }
                                    },
                                    complete: function(){
                                            MEG.main.req = 0;
                                    },
                                });
                            }
                        }',
                'updater'=>"js:function(item){
                            MEG.address.area[".$len16."] = item;
                            return MEG.address.area[".$len16."];
                        }",
            ),
            'htmlOptions' => array(
                'class' => 'span2 '.$name,
                'name'  => $register->form['name'].'['.$name.']['.$len16.']',
                'autocomplete' => "off",
            ),
        ));
        $out = ob_get_contents();
        ob_end_clean();
        $register->text.= $out;



        $name = 'region';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['forward'].'" style="margin-left: 10px"><span class="icon-forward"></span>&nbsp</a>';
        ob_start();
        $form->widget('application.components.widget.TbTypeahead', array(
            'name'=> $name,
            'attribute' => $register->form['model']->attributes,
            'model' => $register->form['model'],
            'form' => $form,
            'id' => $register->form['name'].'_'.$name.'_'.$len16,
            'options'=>array(
                'items'=> 10,
                'source'=>'js:function(item, process) {
                            if(!MEG.address.autoComplete) return false;
                            if(item.length > MEG.address.len){
                                $.ajax({
                                    dataType:"json",
                                    data: {item : item, limit: MEG.address.limit, search: "region", format: "json"},
                                    type: "POST",
                                    url: MEG.settings.url1,
                                    success: process,
                                    beforeSend: function(){
                                        if(MEG.main.req){
                                            return false;
                                        }else{
                                            MEG.main.req = 1;
                                        }
                                    },
                                    complete: function(){
                                            MEG.main.req = 0;
                                    },
                                });
                            }
                        }',
                'updater'=>"js:function(item) {
                            MEG.address.region[".$len16."] = item;
                            var t = item.split(';').splice(1);
                            var zip = item.split(';');
                            zip = zip[0];
                            var flag=0
                            item = t;
                            if(t == 'МОСКВА Г' || t == 'САНКТ-ПЕТЕРБУРГ Г' || t == 'БАЙКОНУР Г' || t== 'СЕВАСТОПОЛЬ Г') flag=1;
                            MEG.address.region[".$len16."] = item;
                            if(flag){
                                $('#".$register->form['name'].'_zip_'.$len16."').val(zip);
                            }else{
                                $('#".$register->form['name'].'_area_'.$len16."').removeAttr('readOnly');
                            }
                            return t;
                        }",
                'highlighter'=>"js:function(item) {
                                return item.split(';').splice(1);
                            }",
            ),
            'htmlOptions' => array(
                'class' => 'span2 '.$name,
                'name'  => $register->form['name'].'['.$name.']['.$len16.']',
                'autocomplete' => "off",
            ),
        ));
        $out = ob_get_contents();
        ob_end_clean();

        $register->text.= $out;

        $register->text.= '<a href="#" class="reset" name="'.$len16.'" rel="tooltip" title='.$messages['reset_add'].' style="margin-left: 10px"><span class="icon-remove"></span>&nbsp</a>';
        # Отображать загрузку документа
        $register->text.='</td>';
        $register->text.='</tr>';
        $register->text.='<tr>';
        $register->text.='<td>';
        $name = 'street';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['required'].'" style=""><span class="icon-pencil"></span>&nbsp</a>';
        $register->text.= $form->textFieldRow($register->form['model'], $name,array('rows'=>2, 'class'=>'span2 '.$name, 'name' => $register->form['name'].'['.$name.']['.$len16.']'));
        $name='house';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['required'].'" style="margin-left: 10px"><span class="icon-pencil"></span>&nbsp</a>';
        $register->text.= $form->textFieldRow($register->form['model'], $name,array('rows'=>2, 'class'=>'span2 '.$name, 'name' => $register->form['name'].'['.$name.']['.$len16.']'));
        $register->text.='</td>';
        $register->text.='</tr>';
        $register->text.='<tr>';
        $register->text.='<td>';
        $name='office';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['opt'].'" style=""><span class="icon-flag"></span>&nbsp</a>';
        $register->text.= $form->textFieldRow($register->form['model'], $name,array('rows'=>2, 'class'=>'span2', 'name' => $register->form['name'].'['.$name.']['.$len16.']'));
        $name='floor';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['opt'].'" style="margin-left: 10px"><span class="icon-flag"></span>&nbsp</a>';
        $register->text.= $form->textFieldRow($register->form['model'], $name,array('rows'=>2, 'class'=>'span2', 'name' => $register->form['name'].'['.$name.']['.$len16.']'));
        $name='housing';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['opt'].'" style="margin-left: 10px"><span class="icon-flag"></span>&nbsp</a>';
        $register->text.= $form->textFieldRow($register->form['model'], $name, array('rows'=>2, 'class'=>'span2', 'name' => $register->form['name'].'['.$name.']['.$len16.']'));
        $name='intercom';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['opt'].'" style="margin-left: 10px"><span class="icon-flag"></span>&nbsp</a>';
        $register->text.= $form->textFieldRow($register->form['model'],$name,array('rows'=>2, 'class'=>'span2', 'name' => $register->form['name'].'['.$name.']['.$len16.']'));
        $register->text.='</td>';
        $register->text.='</tr>';

        $register->text.='<tr>';
        $register->text.='<td>';
        $register->text.= "<div class='megapolis-order-table-header'>Характеристики отправления:</div>";
        $register->text.='</td>';
        $register->text.='</tr>';
        $register->text.='<tr>';
        $register->text.='<td>';
        $name='op_inventory';
        $register->form['model']->$name = 0;
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['opt'].'" style=""><span class="icon-flag"></span>&nbsp</a>';
        $register->text.= '<span style="margin-right: 37px; font-weight: bold; font-size: 12px;">'.$labels[$name].': </span>';
        $register->text.= $form->dropDownListRow($register->form['model'], $name, $register->HK->bool, array('rows'=>2, 'class'=>'span1', 'name' => $register->form['name'].'['.$name.']['.$len16.']', 'labelOptions' => array('label' => false), 'hint'=>'Labels surround all the options for much larger click areas.' ));
        $register->text.='</td>';
        $register->text.='</tr>';
        $register->text.='<tr>';
        $register->text.='<td>';
        $name='op_fragile';
        $register->form['model']->$name = 0;
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['required'].'" style=""><span class="icon-pencil"></span>&nbsp</a>';
        $register->text.= '<span style="margin-right: 76px; font-weight: bold; font-size: 12px;">'.$labels[$name].': </span>';
        $register->text.= $form->dropDownListRow($register->form['model'], $name, $register->HK->bool, array('rows'=>2, 'class'=>'span1', 'name' => $register->form['name'].'['.$name.']['.$len16.']', 'labelOptions' => array('label' => false)));
        $register->text.='</td>';
        $register->text.='</tr>';
        $register->text.='<tr>';
        $register->text.='<td>';
        $name='wd';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['opt'].'" style=""><span class="icon-pencil"></span>&nbsp</a>';
        $register->text.= $form->textFieldRow($register->form['model'],$name,array('class'=>'span2 '.$name,  'name' => $register->form['name'].'['.$name.']['.$len16.']', 'hint' => 'округление до 1-тысячной') );
        $register->text.='</td>';
        $register->text.='</tr>';
        $register->text.='<tr>';
        $register->text.='<td>';
        $name='cost_public';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['required'].'"><span class="icon-pencil"></span>&nbsp</a>';
        $register->text.= '<span style="margin-right: 7px; font-weight: bold; font-size: 12px;">'.$labels[$name].': </span>';
        $register->text.= $form->textFieldRow($register->form['model'],$name,array('class'=>'span2 '.$name, 'name' => $register->form['name'].'['.$name.']['.$len16.']'));
        $register->text.='</td>';
        $register->text.='</tr>';
        $register->text.='<tr>';
        $register->text.='<td>';
        $name='cost_delivery';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['required'].'"><span class="icon-pencil"></span>&nbsp</a>';
        $register->text.= '<span style="margin-right: 25px; font-weight: bold; font-size: 12px;">'.$labels[$name].': </span>';
        $register->text.= $form->textFieldRow($register->form['model'],$name,array('class'=>'span2 '.$name, 'name' => $register->form['name'].'['.$name.']['.$len16.']'));
        $register->text.='</td>';
        $register->text.='</tr>';

        $register->text.='</td>';
        $register->text.='</tr>';
        $register->text.='<tr>';
        $register->text.='<td style="padding-top: 31px;">';
        $name='dai';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['opt'].'" style=""><span class="icon-flag"></span>&nbsp</a>';
        $register->text.= $form->textFieldRow($register->form['model'],$name,array('rows'=>2, 'class'=>'span6', 'name' => $register->form['name'].'['.$name.']['.$len16.']'));
        $register->text.='</td>';
        $register->text.='</tr>';
        $register->text.='<tr>';
        $register->text.='<td>';
        $name='comment';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['opt'].'" style=""><span class="icon-flag"></span>&nbsp</a>';
        $register->text.= $form->textAreaRow($register->form['model'],'comment',array('rows'=>2, 'class'=>'span6', 'name' => $register->form['name'].'['.$name.']['.$len16.']'));
        $register->text.='</td>';
        $register->text.='</tr>';
        $register->text.='</table>';
        $name='wdays';

        $name='register_hidden';
        $register->text.= CHtml::hiddenField('hidden', '', array('id' => $register->form['name'].'['.$name.']['.$len16.']'));
        ob_start();
        $this->getController()->endWidget();
        ob_end_clean();

        return $register->text;
    }
}

