<?php

class documentOrderOrder{

    public $registry;
    public $megapolis;
    public $agent_id;
    public $comment;
    public $invoice;
    public $export_declaration;
    public $consignment;
    public $track_num;

    private $articles;
    private $option;
    private $cost;
    private $dimension;
    private $sender;
    private $address_sender;
    private $person;
    private $address;
    private $client;

    public function setarticles(documentOrderArticles   $articles){$this->articles[] = $articles;}
    public function setoption(documentOrderOption       $option){$this->option = $option;}
    public function setcost(documentOrderCost           $cost){$this->cost = $cost;}
    public function setdimension(documentOrderDimension $dimension){$this->dimension = $dimension;}
    public function setsender(documentOrderSender       $sender){$this->sender = $sender;}
    public function setaddress_sender(documentOrderAddress_sender $address_sender){$this->address_sender = $address_sender;}
    public function setperson(documentOrderPerson       $person){$this->person = $person;}
    public function setaddress(documentOrderAddress     $address){$this->address = $address;}
    public function setclient(documentOrderClient   $client){$this->client = $client;}


    public function getarticles(){  return $this->articles;}
    public function getoption(){    return $this->option;}
    public function getcost(){      return $this->cost;}
    public function getdimension(){ return $this->dimension;}
    public function getsender(){    return $this->sender;}
    public function getaddress_sender(){ return $this->address_sender;}
    public function getperson(){    return $this->person;}
    public function getaddress(){   return $this->address;}
    public function getclient(){    return $this->client;}




}
class documentOrderArticles{
    public $article_id;
    public $article_name;
    public $article_price;
    public $article_price_sum;
    public $article_weight;
    public $article_weight_sum;
    public $article_count;
    public $article_web;
}
class documentOrderOption{
    public $op_inventory;
    public $op_category;
    public $op_type;
    public $op_notification;
    public $op_fragile;
    public $op_packing;
    public $op_take;
}
class documentOrderCost{
    public $cost_delivery;
    public $cost_insurance;
    public $cost_public;
    public $cost_article;
    public $cost_total;
}
class documentOrderDimension{
    public $weight_end;
    public $weight_article;
    public $capacity;
    public $count;
}
class documentOrderSender{
    public $fio;
    public $phone;
    public $email;
    public $sex;
    public $name;
    public $surname;
    public $patronymic;
    public $birthday;
    public $passport_ser;
    public $passport_num;
    public $passport_day;
    public $passport_give;
}
class documentOrderAddress_sender{
    public $country;
    public $zip;
    public $region;
    public $area;
    public $city;
    public $place;
    public $street;
    public $house;
    public $building;
    public $flat;
    public $office;
    public $floor;
    public $full;
}
class documentOrderPerson{
    public $fio;
    public $phone;
    public $email;
    public $sex;
    public $name;
    public $surname;
    public $patronymic;
    public $birthday;
    public $passport_ser;
    public $passport_num;
    public $passport_day;
    public $passport_give;
}
class documentOrderAddress{
    public $country;
    public $zip;
    public $region;
    public $area;
    public $city;
    public $place;
    public $street;
    public $house;
    public $building;
    public $flat;
    public $office;
    public $floor;
    public $full;
}
class documentOrderClient{
    public $client_contract;
}