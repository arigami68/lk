<?php

class AddRegisterLockAction extends CAction{
    public static $REGISTER_NEW = 'new';
    public static $REGISTER_EDIT = 'edit';
    public static $REGISTER_SHOW = 'show';
    public static $FORMAT_JSON = 'json';
    public static $FORMAT_MODEL = 'model';

    public function run($json= 'json', $model = NULL, $len = 0){

        if(true){
            $register = new stdClass();
            $register->request = Yii::app()->request;
            $register->form['name'] = Yii::import('application.modules.order.models.OrderLoad');
            $register->form['model'] = new $register->form['name'];
            $len16 = $register->request->getParam('len16') ? $register->request->getParam('len16') : 1;
            $labels = $register->form['model']->attributeLabels();
            $messages = array(
                'auto'     => Yii::t("total", 'автоматическое присвоение'),
                'forward'  => Yii::t("total", 'выбор, с последующим появлением').'.',
                'required' => Yii::t("total", 'обязательное поле').'.',
                'opt'      => Yii::t("total", 'не обязательное поле').'.',
            );
        }
        if($model){
            $register->form['model'] = $model;
            $len16 = $len+1;
            $model->num = $model->megapolis;
        }

        if(true){
            ob_start();
                $form= $this->getController()->beginWidget('bootstrap.widgets.TbActiveForm', array(
                'id'=>'location-fo',
                'type'=>'inline',
                ));
            ob_end_clean();
        }

        if(true){
            $register->HK = FuncMegapolis::Handbook();
            $register->HK->time = array(
                0 =>  '00:00',
                1 =>  '01:00',
                2 =>  '02:00',
                3 =>  '03:00',
                4 =>  '04:00',
                5 =>  '05:00',
                6 =>  '06:00',
                7 =>  '07:00',
                8 =>  '08:00',
                9 =>  '09:00',
                10 => '10:00',
                11 => '11:00',
                12 => '12:00',
                13 => '13:00',
                14 => '14:00',
                15 => '15:00',
                16 => '16:00',
                17 => '17:00',
                18 => '18:00',
                19 => '19:00',
                20 => '20:00',
                21 => '21:00',
                22 => '22:00',
                23 => '23:00',
            );
            $register->HK->bool = array(
                1 => 'Да',
                0 => 'Нет',
            );
            foreach($register->HK->agent_id as $key=>$value){
                $register->HK->agent_id[Yii::t("total", $key)] = Yii::t("total", $value);
            }
            foreach($register->HK->op_type as $key=>$value){
                $register->HK->op_type[Yii::t("total", $key)] = Yii::t("total", $value);
            }
            foreach($register->HK->op_category as $key=>$value){
                $register->HK->op_category[Yii::t("total", $key)] = Yii::t("total", $value);
            }
            foreach($register->HK->bool as $key=>$value){
                $register->HK->bool[Yii::t("total", $key)] = Yii::t("total", $value);
            }

        }

        $register->text = '';
        $register->text.='<table>';
        $register->text.='<tr>';
        $register->text.='<td>';

        $name='num';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'" style=""><span class="icon-lock"></span>&nbsp</a>';
            #$register->text.= $form->hiddenField($register->form['model'], $name, array('rows'=>2, 'class'=>'span7', 'name' => $register->form['name'].'['.$name.']['.$len16.']'));
            $register->text.= $form->textFieldRow($register->form['model'], $name, array('rows'=>2, 'class'=>'span2 '.$name, 'name' => $register->form['name'].'['.$name.']['.$len16.']', 'readonly' => 'readonly'));

        $register->text.='</td>';
        $register->text.='<td colspan="2">';
        $register->text.='</td>';
        $register->text.='</tr>';

        $register->text.='<tr>';
        $register->text.='<td>';
        $name = 'agent_id';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['required'].'" style=""><span class="icon-pencil"></span>&nbsp</a>';
        $register->text.= $form->dropDownListRow($register->form['model'], $name,  array('')+array_flip($register->HK->$name),  array('rows'=>2, 'class'=>'span2 agent_id',  'name' => $register->form['name'].'['.$name.']['.$len16.']' ,'labelOptions' => array('label' => false), 'disabled' => 'disabled'));
        $name='op_type';
        $register->text.= '&nbsp';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['required'].'" style="margin-left: 203px;"><span class="icon-pencil"></span>&nbsp</a>';
        $register->text.= $form->dropDownListRow($register->form['model'], $name, array_flip($register->HK->op_type), array('class'=>'span2 op_type', 'name' => $register->form['name'].'['.$name.']['.$len16.']' ,'labelOptions' => array('label' => false), 'disabled' => 'disabled'));
        $name = 'op_category';
        $register->text.= '&nbsp';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['required'].'" style="margin-left: 10px;"><span class="icon-pencil"></span>&nbsp</a>';
        $register->text.= $form->dropDownListRow($register->form['model'], $name, array_flip($register->HK->$name), array('class'=>'span2 op_category', 'name' => $register->form['name'].'['.$name.']['.$len16.']' ,'labelOptions' => array('label' => false), 'disabled' => 'disabled'));
        $register->text.='</td>';
        $register->text.='</tr>';

        $register->text.='<tr>';
        $register->text.='<td>';
        $register->text.= "<div class='megapolis-order-table-header'>Получатель:</div>";
        $register->text.='</td>';
        $register->text.='</tr>';
        $register->text.='<tr>';
        $register->text.='<td>';
        $name = 'fio';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['required'].'" style=""><span class="icon-pencil"></span>&nbsp</a>';
        $register->text.= $form->textFieldRow($register->form['model'], $name, array('rows'=>2, 'class'=>'span4', 'hint'=>'наименование организации, ФИО полностью', 'name' => $register->form['name'].'['.$name.']['.$len16.']', 'disabled' => 'disabled'));
        $name = 'phone';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['required'].'" style="margin-left: 10px"><span class="icon-pencil"></span>&nbsp</a>';
        $register->text.= $form->textFieldRow($register->form['model'],$name,array('rows'=>2, 'class'=>'span2 '.$name, 'name' => $register->form['name'].'['.$name.']['.$len16.']', 'disabled' => 'disabled'));
        $register->text.='</td>';
        $register->text.='</tr>';
        $register->text.='<tr>';
        $register->text.='<td>';
        $register->text.= "<div class='megapolis-order-table-header'>Адрес получателя:</div>";
        $name = 'country';
	    $value = 'Россия';

	    if($register->form['model']['op_type'] == 22 || $register->form['model']['op_type'] == 24 || $register->form['model']['op_type'] == 26){
		    $value = 'Беларусь';
	    }elseif($register->form['model']['op_type'] == 23 || $register->form['model']['op_type'] == 25 || $register->form['model']['op_type'] == 27){
		    $value = 'Казахстан';
	    }

        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['auto'].'" style=""><span class="icon-lock"></span>&nbsp</a>';
        $register->text.= $form->textFieldRow($register->form['model'],$name,array('rows'=>2, 'class'=>'span2', 'name' => $register->form['name'].'['.$name.']['.$len16.']', 'value' => $value, 'readonly' => 'readonly', 'disabled' => 'disabled'));

        $name = 'zip';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['auto'].'" style="margin-left: 10px"><span class="icon-lock"></span>&nbsp</a>';
        $register->text.= $form->textFieldRow($register->form['model'],$name,array('rows'=>2, 'class'=>'span2 '.$name, 'name' => $register->form['name'].'['.$name.']['.$len16.']', 'readonly' => 'true'));
        $register->text.='</td>';
        $register->text.='</tr>';
        $register->text.='<tr>';
        $register->text.='<td>';
        $name = 'region';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['forward'].'" style=""><span class="icon-forward"></span>&nbsp</a>';

        $register->text.= $form->textFieldRow($register->form['model'],$name,array('rows'=>2, 'class'=>'span2 '.$name, 'name' => $register->form['name'].'['.$name.']['.$len16.']', 'readonly' => 'true'));

        $name = 'area';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['forward'].'" style="margin-left: 10px"><span class="icon-forward"></span>&nbsp</a>';
        ob_start();
        $form->widget('application.components.widget.TbTypeahead', array(
            'name'=> $name,
            'attribute' => $register->form['model']->attributes,
            'model' => $register->form['model'],
            'form' => $form,
            'id' => $register->form['name'].'_'.$name.'_'.$len16,
            'options'=>array(
                'items'=> 10,
                'matcher'=>"js:function(item) {
                            return item;
                        }",
                'source'=>'js:function(item, process) {
                            if(item.length > obj.len){
                                item=obj.output.region['.$len16.']+","+item;
                                $.ajax({
                                    dataType:"json",
                                    data: {item : item, limit: obj.limit, search: "area", format: "json"},
                                    type: "POST",
                                    url: obj.url,
                                    success: process,
                                });
                            }
                        }',
                'updater'=>"js:function(item){
                            obj.output.area[".$len16."] = item;
                            return obj.output.area[".$len16."];
                        }",
            ),
            'htmlOptions' => array(
                'readonly' => 'readonly',
                'class' => 'span2 '.$name,
                'name'  => $register->form['name'].'['.$name.']['.$len16.']',
                'autocomplete' => "off",
            ),
        ));
        $out = ob_get_contents();
        ob_end_clean();
        $register->text.= $out;
        $name = 'city';
        if(empty($register->form['model'][$name])){ $register->form['model']->city = $register->form['model']->place;}
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['forward'].'" style="margin-left: 10px"><span class="icon-forward"></span>&nbsp</a>';
        ob_start();
        $form->widget('application.components.widget.TbTypeahead', array(
            'name'=> $name,
            'attribute' => $register->form['model']->attributes,
            'model' => $register->form['model'],
            'form' => $form,
            'id' => $register->form['name'].'_'.$name.'_'.$len16,
            'options'=>array(
                'items'=> 10,
                'matcher'=>"js:function(item, process) {
                            var text = item.split(';');
                            return text[1];
                        }",
                'source'=>'js:function(item, process) {
                            if(item.length > obj.len){
                                item=obj.output.region['.$len16.']+","+obj.output.area['.$len16.']+","+item;
                                $.ajax({
                                    dataType:"json",
                                    data: {item : item, limit: obj.limit, search: "city", format: "json"},
                                    type: "POST",
                                    url: obj.url,
                                    success: process,
                                });
                            }
                        }',
                'updater'=>"js:function(item){
                            var text = item.split(';');
                            item = text[1];
                            $('#".$register->form['name'].'_zip_'.$len16."').val(text[0]);
                            obj.output.city[".$len16."] = item;
                            return obj.output.city[".$len16."];
                        }",
            ),
            'htmlOptions' => array(
                'readonly' => 'readonly',
                'class' => 'span2 '.$name,
                'name'  => $register->form['name'].'['.$name.']['.$len16.']',
                'autocomplete' => "off",
            ),
        ));
        $out = ob_get_contents();
        ob_end_clean();
        $register->text.= $out;
        $register->text.='</td>';
        $register->text.='</tr>';
        $register->text.='<tr>';
        $register->text.='<td>';
        $name = 'street';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['required'].'" style=""><span class="icon-pencil"></span>&nbsp</a>';
        $register->text.= $form->textFieldRow($register->form['model'], $name,array('rows'=>2, 'class'=>'span2 '.$name, 'name' => $register->form['name'].'['.$name.']['.$len16.']', 'disabled' => 'disabled'));
        $name='house';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['required'].'" style="margin-left: 10px"><span class="icon-pencil"></span>&nbsp</a>';
        $register->text.= $form->textFieldRow($register->form['model'], $name,array('rows'=>2, 'class'=>'span2', 'name' => $register->form['name'].'['.$name.']['.$len16.']', 'disabled' => 'disabled'));
        $register->text.='</td>';
        $register->text.='</tr>';
        $register->text.='<tr>';
        $register->text.='<td>';
        $name='office';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['opt'].'" style=""><span class="icon-flag"></span>&nbsp</a>';
        if(empty($register->form['model'][$name])){ $register->form['model']->office = $register->form['model']->flat;}
        $register->text.= $form->textFieldRow($register->form['model'], $name,array('rows'=>2, 'class'=>'span2', 'name' => $register->form['name'].'['.$name.']['.$len16.']', 'disabled' => 'disabled'));
        $name='floor';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['opt'].'" style="margin-left: 10px"><span class="icon-flag"></span>&nbsp</a>';
        $register->text.= $form->textFieldRow($register->form['model'], $name,array('rows'=>2, 'class'=>'span2', 'name' => $register->form['name'].'['.$name.']['.$len16.']', 'disabled' => 'disabled'));
        $name='housing';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['opt'].'" style="margin-left: 10px"><span class="icon-flag"></span>&nbsp</a>';
        $register->text.= $form->textFieldRow($register->form['model'], $name, array('rows'=>2, 'class'=>'span2', 'name' => $register->form['name'].'['.$name.']['.$len16.']', 'disabled' => 'disabled'));
        $name='intercom';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['opt'].'" style="margin-left: 10px"><span class="icon-flag"></span>&nbsp</a>';
        $register->text.= $form->textFieldRow($register->form['model'],$name,array('rows'=>2, 'class'=>'span2', 'name' => $register->form['name'].'['.$name.']['.$len16.']','disabled' => 'disabled'));
        $register->text.='</td>';
        $register->text.='</tr>';

        $register->text.='<tr>';
        $register->text.='<td>';
        $register->text.= "<div class='megapolis-order-table-header'>Характеристики отправления:</div>";
        $register->text.='</td>';
        $register->text.='</tr>';
        $register->text.='<tr>';
        $register->text.='<td>';
        $name='op_inventory';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['opt'].'" style=""><span class="icon-flag"></span>&nbsp</a>';
        $register->text.= '<span style="margin-right: 37px; font-weight: bold; font-size: 12px;">'.$labels[$name].': </span>';
        $register->text.= $form->dropDownListRow($register->form['model'], $name, $register->HK->bool, array('rows'=>2, 'class'=>'span1', 'name' => $register->form['name'].'['.$name.']['.$len16.']', 'labelOptions' => array('label' => false), 'hint'=>'Labels surround all the options for much larger click areas.', 'disabled' => 'disabled' ));
        $register->text.='</td>';
        $register->text.='</tr>';
        $register->text.='<tr>';
        $register->text.='<td>';
        $name='op_fragile';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['required'].'" style=""><span class="icon-pencil"></span>&nbsp</a>';
        $register->text.= '<span style="margin-right: 76px; font-weight: bold; font-size: 12px;">'.$labels[$name].': </span>';
        $register->text.= $form->dropDownListRow($register->form['model'], $name, $register->HK->bool, array('rows'=>2, 'class'=>'span1', 'name' => $register->form['name'].'['.$name.']['.$len16.']', 'labelOptions' => array('label' => false), 'disabled' => 'disabled'));
        $register->text.='</td>';
        $register->text.='</tr>';
        $register->text.='<tr>';
        $register->text.='<td>';
        $name='wd';

        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['opt'].'" style=""><span class="icon-flag"></span>&nbsp</a>';
        $register->text.= $form->textFieldRow($register->form['model'],$name,array('class'=>'span2',  'name' => $register->form['name'].'['.$name.']['.$len16.']', 'hint' => 'округление до 1-тысячной', 'disabled' => 'disabled') );
        $name='cost_public';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['required'].'" style="margin-left: 10px"><span class="icon-pencil"></span>&nbsp</a>';
        $register->text.= $form->textFieldRow($register->form['model'],$name,array('class'=>'span2 '.$name, 'name' => $register->form['name'].'['.$name.']['.$len16.']', 'disabled' => 'disabled'));
        $name='cost_delivery';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['required'].'" style="margin-left: 10px"><span class="icon-pencil"></span>&nbsp</a>';
        $register->text.= $form->textFieldRow($register->form['model'],$name,array('class'=>'span2 '.$name, 'name' => $register->form['name'].'['.$name.']['.$len16.']', 'disabled' => 'disabled'   ));
        $register->text.='</td>';
        $register->text.='</tr>';

        $register->text.='</td>';
        $register->text.='</tr>';
        $register->text.='<tr>';
        $register->text.='<td style="padding-top: 31px;">';
        $name='dai';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['opt'].'" style=""><span class="icon-flag"></span>&nbsp</a>';
        $register->text.= $form->textFieldRow($register->form['model'],$name,array('rows'=>2, 'class'=>'span6', 'name' => $register->form['name'].'['.$name.']['.$len16.']', 'disabled' => 'disabled'));
        $register->text.='</td>';
        $register->text.='</tr>';
        $register->text.='<tr>';
        $register->text.='<td>';
        $name='comment';
        $register->text.= '<a href="#" rel="tooltip" title="'.$labels[$name].'. '.$messages['opt'].'" style=""><span class="icon-flag"></span>&nbsp</a>';
        $register->text.= $form->textAreaRow($register->form['model'],'comment',array('rows'=>2, 'class'=>'span6', 'name' => $register->form['name'].'['.$name.']['.$len16.']', 'disabled' => 'disabled'));
        $register->text.='</td>';
        $register->text.='</tr>';
        $register->text.='</table>';
        #$register->text.= "</div><div style='font-weight: bold; font-size: 21px; padding-bottom: 10px'>Возможность отправления:</div>";
        $name='wdays';

        $name='register_hidden';
        $register->text.= CHtml::hiddenField('hidden', '', array('id' => $register->form['name'].'['.$name.']['.$len16.']', 'disabled' => 'disabled'));
        ob_start();
        $this->getController()->endWidget();
        ob_end_clean();

        if($json == self::$FORMAT_JSON){
            $len16 = $register->request->getParam('len16');
            $text = '<div id="yw0_tab_'.$len16.'" class="tab-pane">'.$register->text.'</div>';
            die($text);
        }
        if($json == self::$FORMAT_MODEL) return $register->text;
    }

}