<?php
class DomXml{
    public static function get($file, $xpath = ''){
        $dom = new DomDocument();
        $dom->loadXML($file);
        $file = simplexml_import_dom($dom);
        return $file;
    }
}