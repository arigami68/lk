<?php

class ReadXlsxGer extends readXlsxAbstract{
    public $first_list              = 2;
    public $CARGO                   = 2;

    public static $CARGO_EXPRESS    = 1;
    public static $CARGO_PR         = 2;

    public function read(CUploadedFile $file){
        $objPHPExcel = $this->objPHPExcel($file);

        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,false,false,true);

        if($sheetData[1]['BL']){    # Определить что это Експресс груз
            $this->CARGO        = self::$CARGO_EXPRESS;
            $this->first_list   = 3;
        }

        if($this->CARGO == self::$CARGO_PR){
            $result = $this->readPR($sheetData);
        }elseif($this->CARGO == self::$CARGO_EXPRESS){
            $result = $this->readEXPRESS($sheetData);
        }

        return $result;
    }
    public function getComponent(array $array, $component){
        if($this->CARGO == self::$CARGO_PR){
            $result = $this->getComponentPR($array, $component);
        }elseif($this->CARGO == self::$CARGO_EXPRESS){
            $result = $this->getComponentEXPRESS($array, $component);
        }
        return $result;
    }
    public function getComponentPR($array, $document){

        $document->doc_date = date('d.m.Y H:i:s');
        $document->key      = Yii::app()->user->auth->secretKey;
        if(!empty($document->order)){
            $articles = $this->articlesPR($array, new documentOrderArticles);
            $document->order->setarticles($articles);

            return $document;
        }

        $order = new documentOrderOrder;
        $order->registry                = $array['megapolis'];
        $order->megapolis               = $array['megapolis'];
        $order->agent_id;
        $order->invoice                 = $array['invoice'];
        $order->consignment             = $array['consignment'];
        $order->track_num               = $array['megapolis'];
        $order->export_declaration;
        $order->comment;
        $document->order = $order;

        $articles = $this->articlesPR($array, new documentOrderArticles);
        $document->order->setarticles($articles);

        $option = new documentOrderOption;
        $option->op_inventory;
        $option->op_category;
        $option->op_type                = $array['op_type'];
        $option->op_notification;
        $option->op_fragile;
        $option->op_packing;
        $option->op_take;
        $document->order->setoption($option);

        $cost = new documentOrderCost;
        $cost->cost_delivery            = $array['cost_delivery'];
        $cost->cost_insurance;
        $cost->cost_public     ;
        $cost->cost_article     ;
        $cost->cost_total     ;
        $document->order->setcost($cost);

        $dimension = new documentOrderDimension;
        $dimension->weight_end          = $array['weight_end'];
        $dimension->weight_article;
        $dimension->capacity;
        $dimension->count;
        $document->order->setdimension($dimension);

        $sender = new documentOrderSender;
        $sender->fio;
        $sender->phone;
        $sender->email;
        $sender->sex;
        $sender->name;
        $sender->surname;
        $sender->patronymic;
        $sender->birthday;
        $sender->passport_ser ;
        $sender->passport_num;
        $sender->passport_day ;
        $sender->passport_give ;
        $document->order->setsender($sender);

        $address_sender = new documentOrderAddress_sender;
        $address_sender->country        = $array['address_sender.country'];
        $address_sender->zip            = $array['address_sender.zip'];
        $address_sender->region         = $array['address_sender.region'];
        $address_sender->area           = $array['address_sender.area'];
        $address_sender->city           = $array['address_sender.city'];
        $address_sender->place;
        $address_sender->street         = $array['address_sender.street'];
        $address_sender->house          = $array['address_sender.house'];
        $address_sender->building       = $array['address_sender.building'];
        $address_sender->flat           = $array['address_sender.flat'];
        $address_sender->office;
        $address_sender->floor;
        $address_sender->full;
        $document->order->setaddress_sender($address_sender);

        $person = new documentOrderPerson;
        $person->fio                    = $array['person'];
        $person->phone;
        $person->email;
        $person->sex            ;
        $person->name          ;
        $person->surname     ;
        $person->patronymic   ;
        $person->birthday       ;
        $person->passport_ser;
        $person->passport_num;
        $person->passport_day;
        $person->passport_give;
        $document->order->setperson($person);

        $address = new documentOrderAddress;

        $address->country;
        $address->zip                   = $array['zip'];
        $address->region                = $array['region'];
        $address->area                  = $array['area'];
        $address->city                  = $array['city'];
        $address->place;
        $address->street                = $array['street'];
        $address->house                 = $array['house'];
        $address->building              = $array['building'];
        $address->flat                  = $array['flat'];
        $address->office;
        $address->floor;
        $address->full;
        $document->order->setaddress($address);

        $client = new documentOrderClient;
        $client->client_contract = Yii::app()->user->auth->contract;
        $document->order->setclient($client);

        #$component->opTake          = $this->searchMegapolis($array['orders.op_take'], FuncMegapolis::Handbook()->op_take, 1);

        return $document;
    }
    public function getComponentEXPRESS($array, $document){
        $document->doc_date = date('d.m.Y H:i:s');
        $document->key      = Yii::app()->user->auth->secretKey;

        if(!empty($document->order)){
            $articles = $this->articlesEXPRESS($array, new documentOrderArticles);
            $document->order->setarticles($articles);

            return $document;
        }


        $order = new documentOrderOrder;

        $order->registry              = $array['megapolis'];
        $order->megapolis             = $array['megapolis'];
        $order->agent_id;
        $order->invoice               = $array['order.invoice'];
        $order->consignment           = $array['consignment'];
        $order->track_num             = $array['megapolis'];
        $order->export_declaration    = $array['export_declaration'];
        $order->comment;
        $document->order = $order;

        $articles = $this->articlesEXPRESS($array, new documentOrderArticles);
        $document->order->setarticles($articles);

        $option = new documentOrderOption;
        $option->op_inventory;
        $option->op_category;
        $option->op_type;
        $option->op_notification;
        $option->op_fragile;
        $option->op_packing;
        $option->op_take;
        $document->order->setoption($option);

        $cost = new documentOrderCost;
        $cost->cost_delivery    = $array['cost.cost_delivery'];
        $cost->cost_insurance;
        $cost->cost_public      = $array['cost.cost_public'];
        $cost->cost_article     = '';
        $cost->cost_total       = '';
        $document->order->setcost($cost);

        $dimension = new documentOrderDimension;
        $dimension->weight_end      = $array['dimension.weight_end'];
        $dimension->weight_article;
        $dimension->capacity;
        $dimension->count;
        $document->order->setdimension($dimension);

        $sender = new documentOrderSender;
        $sender->fio;
        $sender->phone;
        $sender->email;
        $sender->sex;
        $sender->name;
        $sender->surname;
        $sender->patronymic;
        $sender->birthday;
        $sender->passport_ser = $array['sender.passport_ser'];
        $sender->passport_num = $array['sender.passport_num'];
        $sender->passport_day = $array['sender.passport_day.d'].$array['sender.passport_day.m'].$array['sender.passport_day.Y'];
        $sender->passport_give = $array['sender.passport_give'];
        $document->order->setsender($sender);

        $address_sender = new documentOrderAddress_sender;
        $address_sender->country;
        $address_sender->zip        = $array['address.zip'];
        $address_sender->region     = $array['address.region'];
        $address_sender->area       = $array['address.area'];
        $address_sender->city       = $array['address.city'];
        $address_sender->place;
        $address_sender->street     = $array['address.street'];
        $address_sender->house      = $array['address.house'];
        $address_sender->building   = $array['address.building'];
        $address_sender->flat       = $array['address.flat'];
        $address_sender->office;
        $address_sender->floor;
        $address_sender->full;
        $document->order->setaddress_sender($address_sender);

        $person = new documentOrderPerson;
        $person->fio;
        $person->phone;
        $person->email;
        $person->sex            = $array['person.sex'];
        $person->name           = $array['person.name'];
        $person->surname        = $array['person.surname'];
        $person->patronymic     = $array['person.patronymic'];
        $person->birthday       = $array['person.birthday.d'].$array['person.birthday.m'].$array['person.birthday.Y'];
        $person->passport_ser;
        $person->passport_num;
        $person->passport_day;
        $person->passport_give;
        $document->order->setperson($person);

        $address = new documentOrderAddress;

        $address->country;
        $address->zip       = $array['address.zip'];
        $address->region    = $array['address.region'];
        $address->area      = $array['address.area'];
        $address->city      = $array['address.city'];
        $address->place;
        $address->street    = $array['address.street'];
        $address->house     = $array['address.house'];
        $address->building  = $array['address.building'];
        $address->flat      = $array['address.flat'];
        $address->office;
        $address->floor;
        $address->full;
        $document->order->setaddress($address);

        $client = new documentOrderClient;
        $client->client_contract = Yii::app()->user->auth->contract;
        $document->order->setclient($client);

        #$component->opTake          = $this->searchMegapolis($array['orders.op_take'], FuncMegapolis::Handbook()->op_take, 1);

        return $document;
    }
    public function articlesPR($array, $articles){
        $articles->article_id;
        $articles->article_name         = $array['article_name'];
        $articles->article_price        ;
        $articles->article_price_num    = $array['article_price_sum'];
        $articles->article_weight       = $array['article_weight'];
        $articles->article_weight_sum   ;
        $articles->article_count        = $array['article_count'];
        $articles->article_web          ;

        return clone $articles;
    }
    public function articlesEXPRESS($array, $articles){

        $articles->article_id           = $array['article.article_id'];
        $articles->article_name         = $array['article.article_name'];
        $articles->article_price        = '';
        $articles->article_price_num    = $array['article.article_price_sum'];
        $articles->article_weight       = $array['article.article_weight'];
        $articles->article_weight_sum   = '';
        $articles->article_count        = $array['article.article_count'];
        $articles->article_web          = $array['article.article_web'];

        return clone $articles;
    }
    public function readPR($sheetData){
        $array = array();
        $xml = array();
        $xml_count = array();
        if(!isset($sheetData[1]['A'])) return 0;
        for($a=$this->first_list;$a;$a++){
            if(empty($sheetData[$a]['A'])) break;
            $xml_count[] = $a;
        }

        if(!$xml_count) return 0;

        #$registry   = $this->getNewRegistry();
        #$idea       = $this->getNewNum();

        foreach($xml_count as $v=>$filled){

                $array['client']                    = $sheetData[$filled]['A'];
                $array['contract']                  = $sheetData[$filled]['B'];
                $array['megapolis']                 = $sheetData[$filled]['C'];
                $array['sender']                    = $sheetData[$filled]['D']; # ФИО ОТПРАВИТЕЛЯ

            # <получатель>
                $array['person']                    = $sheetData[$filled]['E']; # ФИО ПОЛУЧАТЕЛЯ
                $array['zip']                       = $sheetData[$filled]['F']; # Индекс получателя
                $array['region']                    = $sheetData[$filled]['G'];
                $array['area']                      = $sheetData[$filled]['H'];
                $array['city']                      = $sheetData[$filled]['I'];
                $array['street']                    = $sheetData[$filled]['J'];
                $array['house']                     = $sheetData[$filled]['K'];
                $array['building']                  = $sheetData[$filled]['L'];
                $array['flat']                      = $sheetData[$filled]['M']; # Квартира получателя
            # </получатель>

                $sheetData[$filled]['N'];   # ТАРИФ ЕВРО
                $sheetData[$filled]['O'];   # ТАРИФ ЕВРОЦЕНТЫ

                $array['weight_end']                = $sheetData[$filled]['P'];   # ВЕС КГ

                $sheetData[$filled]['Q'];   # ВЕС Г

                $array['op_type']                   = $sheetData[$filled]['R'];   # код типа услуги
                $array['cost_delivery']             = $sheetData[$filled]['S'];   # сумма наложенного платежа

                $sheetData[$filled]['T'];   # сумма наложенного платежа копейки
                $sheetData[$filled]['U'];   # номер вложения в месте
            # <вложения>
                $array['article_name']               = $sheetData[$filled]['V'];   # наименование вложения
                $array['article_count']             = $sheetData[$filled]['W'];   # кол-во вложений данного наименования
                $array['article_weight']            = $sheetData[$filled]['X'];   # вес вложения КГ

                $sheetData[$filled]['Y'];   # вес вложения Г

                $array['article_price_sum']         = $sheetData[$filled]['Z'];   # стоимость Euro

                $sheetData[$filled]['AA'];   # стоимость Euro центы
                $sheetData[$filled]['AB'];   # категория отправления
                $sheetData[$filled]['AC'];   # пояснения к категории отправления
            # </вложения>

            $array['invoice']                   = $sheetData[$filled]['AD'];   # номер инвойса

            # <отправитель>
                $array['address_sender.country']                   = $sheetData[$filled]['AE'];   # страна отправителя
                $array['address_sender.zip']        = $sheetData[$filled]['AF'];   # ИНДЕКС отправителя
                $array['address_sender.region']     = $sheetData[$filled]['AG'];   # регион отправителя
                $array['address_sender.area']       = $sheetData[$filled]['AH'];   # район отправителя
                $array['address_sender.city']       = $sheetData[$filled]['AI'];   # город отправителя
                $array['address_sender.street']     = $sheetData[$filled]['AJ'];   # улица отправителя
                $array['address_sender.house']      = $sheetData[$filled]['AK'];   # дом отправителя
                $array['address_sender.building']   = $sheetData[$filled]['AL'];   # корпус отправителя
                $array['address_sender.flat']       = $sheetData[$filled]['AM'];   # квартира отправителя
            # </отправитель>

                $sheetData[$filled]['AN'];   # клиент ПСК
                $array['consignment']               = $sheetData[$filled]['AO'];   # партия
                $sheetData[$filled]['AP'];   # груз


            $xml[] = $array;
        }
        return $xml;
    }

    public function readEXPRESS($sheetData){

        $array = array();
        $xml = array();
        $xml_count = array();
        if(!isset($sheetData[2]['C'])) return 0;

        for($a=$this->first_list;$a;$a++){
            if(empty($sheetData[$a]['C'])) break;
            $xml_count[] = $a;
        }

        if(!$xml_count) return 0;

        #$registry   = $this->getNewRegistry();
        #$idea       = $this->getNewNum();

        foreach($xml_count as $v=>$filled){
            # <Клиент>
                $sheetData[$filled]['C']; # Наименование контрагента
                $array['client_contract']       = $sheetData[$filled]['D']; # Номер договора
            # </Клиент>
                $array['consignment']           = $sheetData[$filled]['E']; # Номер партии
                $array['megapolis']             = $sheetData[$filled]['F']; # Трекинг номер (ШПИ)
            # <Отправитель>
                $array['sender.name']             = $sheetData[$filled]['G']; # Наименование
                $array['address_sender.country']  = $sheetData[$filled]['H']; # Страна
                $array['address_sender.zip']      = $sheetData[$filled]['I']; # Индекс
                $array['address_sender.region']   = $sheetData[$filled]['J']; # Регион
                $array['address_sender.area']     = $sheetData[$filled]['K']; # Район
                $array['address_sender.city']     = $sheetData[$filled]['L']; # Город
                $array['address_sender.street']   = $sheetData[$filled]['M']; # улица
                $array['address_sender.house']    = $sheetData[$filled]['N']; # дом
                $array['address_sender.building'] = $sheetData[$filled]['O']; # корпус
                $array['address_sender.flat']     = $sheetData[$filled]['P']; # квартира
            # </Отправитель>
            # <Получатель>
                $array['person.surname']          = $sheetData[$filled]['Q']; # Фамилия (Кир.)
                $array['person.name']             = $sheetData[$filled]['R']; # Имя (Кир.)
                $array['person.patronymic']       = $sheetData[$filled]['S']; # Отчество
                $sheetData[$filled]['T']; # Имя (англ.)
                $array['person.sex']              = $sheetData[$filled]['U']; # пол
                $array['person.birthday.d']       = $sheetData[$filled]['V']; # День рождения
                $array['person.birthday.m']       = $sheetData[$filled]['W']; # Месяц рождения
                $array['person.birthday.Y']       = $sheetData[$filled]['X']; # Год рождения
            # <Получатель>
            # <Адрес получателя>
                $array['address.zip']             = $sheetData[$filled]['Y']; # ИНДЕКС
                $array['address.region']          = $sheetData[$filled]['Z']; # регион
                $array['address.area']            = $sheetData[$filled]['AA']; # район
                $array['address.city']            = $sheetData[$filled]['AB']; # город (Нас. пункт)
                $array['address.street']          = $sheetData[$filled]['AC']; # улица
                $array['address.house']           = $sheetData[$filled]['AD']; # дом
                $array['address.building']        = $sheetData[$filled]['AE']; # корпус
                $array['address.flat']            = $sheetData[$filled]['AF']; # квартира
                $array['person.phone']            = $sheetData[$filled]['AG']; # Телефон мобильный
                $sheetData[$filled]['AH']; # Доп. телефон для связи
                $array['person.email']            = $sheetData[$filled]['AI']; # e-mail
            # </Адрес получателя>
            # <Документ, удостоверяющий личность получателя>
                $sheetData[$filled]['AJ']; # наименование
                $array['sender.passport_ser']     = $sheetData[$filled]['AK']; # серия
                $array['sender.passport_num']     = $sheetData[$filled]['AL']; # номер
                $array['sender.passport_day.d']   = $sheetData[$filled]['AM']; # День выдачи
                $array['sender.passport_day.m']   = $sheetData[$filled]['AN']; # Месяц выдачи
                $array['sender.passport_day.Y']   = $sheetData[$filled]['AO']; # Год выдачи
                $array['sender.passport_give']    = "РОВД ". $sheetData[$filled]['AB']; # Кем выдан документ
            # </Документ, удостоверяющий личность получателя>
                $sheetData[$filled]['AQ']; # Валюта
                $sheetData[$filled]['AR']; # ТАРИФ
                $array['dimension.weight_end']    = $sheetData[$filled]['AS']; # Вес ОТПРАВЛЕНИЯ КГ
                $sheetData[$filled]['AT']; # Вес ОТПРАВЛЕНИЯ Г
                $sheetData[$filled]['AU']; # Код типа услуги
            # <вложения>
                $array['articles.article']          = $sheetData[$filled]['AV']; # номер в месте
                $array['article.article_name']      = $sheetData[$filled]['AW']; # Наименование товара
                $array['article.article_count']     = $sheetData[$filled]['AX']; # Количество
                $array['article.article_weight']    = $sheetData[$filled]['AY']; # ВЕС КГ
                #$sheetData[$filled]['AZ']; # ВЕС Г
                $sheetData[$filled]['BA']; # стоимость Euro
                $array['article.article_price_sum'] = $sheetData[$filled]['BB'] * $array['article.article_count'];  # Цена за единицу (вал.)

                $sheetData[$filled]['BC']; # Стоимость (руб.)
                $sheetData[$filled]['BD']; # ТН ВЭД
                $sheetData[$filled]['BE']; # Номер каталога
                $array['article.article_id']      = $sheetData[$filled]['BF']; # Артикул
                $array['article.article_web']     = $sheetData[$filled]['BG']; # WEB
            # </вложения>
                $sheetData[$filled]['BH']; # Стоимость общая (евро)
                $array['cost.cost_delivery']      = $sheetData[$filled]['BI']; # сумма наложенного платежа (руб.)
                $array['cost.cost_public']        = $sheetData[$filled]['BJ']; # Стоимость Общая/Объявленная ценность в валюте
                $sheetData[$filled]['BK']; # Сборы
                $array['order.invoice']           = $sheetData[$filled]['BL']; # Номер инвойса
                $sheetData[$filled]['BM']; # ТН ВЭД общий
                $array['export_declaration']     = $sheetData[$filled]['BN']; # Номер экспортной декларации

            $xml[] = $array;
        }
        return $xml;
    }

}