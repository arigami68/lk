<?php

class OrderComponent extends documentOrderAbstract{
    public function load($data){
        foreach($data as $k=>$dep){
            $this->opType = $dep['op_type'];
            $this->opTake = $dep['op_take'];
            $this->house = $dep['house'];
            #$this->num = 1;
            $this->zip = $dep['zip'];
            $this->region = $dep['region'];
            $this->area = $dep['area'];
            $this->street = $dep['street'];
            $this->phone = $dep['phone'];
            #$this->country;
            $this->city = $dep['city'];
            $this->fio = $dep['fio'];
            $this->agent = $dep['agent_id'];
            $this->opFragile = $dep['op_fragile'];
            $this->costInsurance = $dep['cost_insurance'];
            $this->opInventory = $dep['op_inventory'];

        }
    }
    public function validate($attributes=null, $clearErrors=true){
        return true;
    }

}
/*
$to = 'buffalon@yandex.ru';
$subject = 'Привет!';
$message = 'Йа письмецо';
$headers = 'From: test@'. $_SERVER['HTTP_HOST'] . "\r\n" .
    'Reply-To: test@'. $_SERVER['HTTP_HOST'] . "\r\n" .
    'X-Mailer: PHP/' . phpversion();
mail($to,$subject,$message,$headers);
 */