<?php

define( 'CHR0' , chr( 0 ) );// символ с ascii кодом 0
define( 'CHR1' , chr( 1 ) );// символ с ascii кодом 1
abstract class readXlsxAbstract extends CModel{
    public $inputFileType = 'Excel5';

    private $count = 0;
    private $array;
    private $next_key = 0;

    public function push(array $array){
        $this->array[] = $array;
        $this->count+=1;
    }

    public function pull(){ return $this->array;}
    public function count(){ return $this->count;}
    public function next(){
        if(!isset($this->array[$this->next_key])) return false;
        return $this->array[$this->next_key++];
    }

    public function attributeNames(){}
    # Отрезает не нужные пробелы. Спасибо Алексею за эту функцию.
    final public function doTrim( $s , $symbol = ' ' ){ return trim( str_replace( CHR1 , '' , str_replace( CHR1.$symbol , '' , str_replace( $symbol.$symbol , $symbol.CHR1 , $s )))); }
    public function objPHPExcel($file){
        try{
            $objPHPExcel = MegapolisExcel::get()->read($file, MegapolisExcel::$FLAG_PART);
        }catch (PHPExcel_Exception $excel){
            throw new MegapolisException;
        }

        return $objPHPExcel;
    }
    public function getNewRegistry(){
        $registry = FuncMegapolis::Xml( Yii::app()->user->auth->secretKey, 1, 'view', 2);
        return $registry;
    }
    public function getNewNum(){
        $idea = NULL;
        if(!Yii::app()->user->auth->db->num){
            $idea = FuncMegapolis::Xml(Yii::app()->user->auth->secretKey, 2000);
        }
        return $idea;
    }
    /*
     * searchMegapolis()
     * @info   - поиск значений в массиве
     * @data
     * in      - данные пользователя
     * search  - массив для поиска
     * default - значение по умолчанию
     * @data
     */
    public function searchMegapolis($in, $search, $result = 0){
        $in = mb_strtolower(trim($in), 'UTF-8');
        $in = $this->doTrim($in);

        foreach($search as $key=>$value){
            $key = mb_strtolower($key, 'UTF-8');
            if($key == $in){
                $result = $value;
                break;
            }

        };
        return $result;
    }
}