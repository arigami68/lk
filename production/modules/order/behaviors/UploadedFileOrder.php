<?php

class UploadedFileOrder extends CBehavior{
    # Имя файла
    public $name;
    # Временное хранилищеы
    public $tempName;
    # Размер
    public $size;
    # Тип файла
    public $type;
    public $user;
    
    public function setFile(CUploadedFile $file){
        if(!is_object($file)) return FALSE;
        if(!$file->tempName) return FALSE;
        
        $this->tempName = $file->tempName;
        $this->name = $file->name;
        $this->size = $file->size;
        $this->type = $file->type;
        
        return 1;
    }
    public function renderFile(){

        switch ($this->type){
            case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
            case 'application/vnd.ms-excel':
                $format = $this->attachBehavior('extras', 
                    array(
                        'class' => 'application.modules.order.behaviors.ReadXlsx',
                            'component'=> 'FileLoadOrderComponent',
                            'file' => $this->tempName,
                            'inputFileType' => 'Excel5',
                    )
                )->read();
                break;
            case 1:
                break;
            case 2:
                break;
            default:
               $format = 0;
               break;
        }
        return $format;
    }
    public function getFormatMegapolis($render){
        
        foreach($render as $k=>$format){
            $megapolis[] = $format->read();
            
        }
        return $megapolis;
    }
}
