<?php
class ReadXlsx extends readXlsxAbstract{
    public static $first_list = 19;

    public function read(CUploadedFile $file){
        $objPHPExcel = $this->objPHPExcel($file);

        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,false,false,true);
        $array = array();
        $xml = array();
        $xml_count = array();

        if(!isset($sheetData[8]['D']) OR !isset($sheetData[8]['I'])) return 0;

        for($a=self::$first_list;$a;$a++){
            if(empty($sheetData[$a]['A'])) break;
                $xml_count[] = $a;
        }

        $registry = FuncMegapolis::Xml( Yii::app()->user->auth->secretKey, 1, 'view', 2);

        if(!Yii::app()->user->auth->db->num){
            $idea = FuncMegapolis::Xml(OrderActionModel::get()->client->secretKey, 2000);
        }

        $registry = $registry[0];
        if(!$xml_count) return 0;

        foreach($xml_count as $v=>$filled){
            $num = (!Yii::app()->user->auth->db->num) ? array_shift($idea) :  $sheetData[$filled]['B'];
            unset($array);
            $array['format.doc_type']        = 100; # Наименование отправителя
            $array['orders.secretkey']       = Yii::app()->user->auth->secretKey;
            $array['orders.dateCreate']      = time(); # Наименование отправителя
            $array['orders.registry']        = $registry;
            $array['megapolis']              = $sheetData[$filled]['B'] ? $sheetData[$filled]['B'] : $num;
            $array['client.agent_id']        = $sheetData[$filled]['U'];
            $array['orders.comment']         = $sheetData[$filled]['AF'];
            $array['orders.op_inventory']    = $sheetData[$filled]['X'];
            $array['orders.op_category']     = $sheetData[$filled]['W'];
            $array['orders.op_notification'] = $sheetData[$filled]['Y'];
            $array['orders.op_fragile']      = $sheetData[$filled]['Z'];
            $array['orders.op_packing']      = $sheetData[$filled]['AB'];
            $array['orders.op_type']         = $sheetData[$filled]['V'];
            $array['orders.op_take']         = $sheetData[10]['C'];
            $array['orders.cost_delivery']   =  ($str = str_replace(',', '', $sheetData[$filled]['AE'])) ? $str : 0; # Сумма наложенного платежа, руб.
            $array['orders.cost_insurance']  = $sheetData[$filled]['AD']; # Сумма страховой ценности, руб
            $array['orders.cost_public']     = str_replace(',', '', $sheetData[$filled]['AC']);
            $array['orders.cost_article']    = ''; #??????? Спросить у Вани
            $array['orders.weight_end']      = $sheetData[$filled]['T'];
            $array['orders.weight_article']  = '';               # вес товаров в отправлении, кг.
            $array['orders.capacity']        = $sheetData[12]['D'];
            $array['orders.phone']           = $sheetData[$filled]['O'];
            $array['orders.email']           = '';
            $array['orders.fio']             = $sheetData[$filled]['D'];
            $array['orders.zip']             = $sheetData[$filled]['F'];
            $array['orders.region']          = $sheetData[$filled]['G'];
            $array['orders.area']            = $sheetData[$filled]['H'];
            $array['orders.city']            = $sheetData[$filled]['I'];
            $array['orders.place']           = $sheetData[$filled]['I'];
            $array['orders.street']          = $sheetData[$filled]['J'];
            $array['orders.house']           = $sheetData[$filled]['K'];
            $array['orders.building']        = $sheetData[$filled]['L'];
            $array['orders.flat']            = $sheetData[$filled]['N'];
            $array['orders.office']          = $sheetData[$filled]['N'];
            $array['orders.floor']           = $sheetData[$filled]['M'];
            $array['orders.full']            = '';

            $array['date_dep']               = $sheetData[9]['D'];
            $array['dai']                    = '';
            $array['address_dep']            = $sheetData[11]['D'];


            $array['orders.client_contract'] = $sheetData[8]['I'];

            $xml[] = $array;
        }
        return $xml;
    }

    public function getComponent($array, $document){

        $document->doc_date = date('d.m.Y H:i:s');
        $document->key      = Yii::app()->user->auth->secretKey;

        if(!empty($document->order)){
            $articles = $this->articlesPR($array, new documentOrderArticles);
            $document->order->setarticles($articles);

            return $document;
        }

        $order = new documentOrderOrder;
        $order->registry                = $array['orders.registry'];
        $order->megapolis               = $array['megapolis'];
        $order->agent_id                = $this->searchMegapolis($array['client.agent_id'], FuncMegapolis::Handbook()->agent_id, 0);
        $order->invoice;
        $order->consignment;
        $order->track_num;
        $order->export_declaration;
        $order->comment                 = $array['orders.comment'];
        $document->order = $order;


        $option = new documentOrderOption;
        $option->op_inventory           = $this->searchMegapolis($array['orders.op_inventory'], FuncMegapolis::Handbook()->op_inventory, 0);
        $option->op_category            = $this->searchMegapolis($array['orders.op_category'], FuncMegapolis::Handbook()->op_category, '');
        $option->op_type                = $this->searchMegapolis($array['orders.op_type'], FuncMegapolis::Handbook()->op_type, 0);

        $option->op_notification        = $array['orders.op_notification'];
        $option->op_fragile             = $this->searchMegapolis($array['orders.op_fragile'], FuncMegapolis::Handbook()->op_fragile, 0);
        $option->op_packing             = $array['orders.op_packing'];
        $option->op_take                = $this->searchMegapolis($array['orders.op_take'], FuncMegapolis::Handbook()->op_take, 1);
        $document->order->setoption($option);

        $cost = new documentOrderCost;
        $cost->cost_delivery            = $array['orders.cost_delivery'] ? $array['orders.cost_delivery'] : 0;
        $cost->cost_insurance           = $array['orders.cost_insurance'];
        $cost->cost_public              = $array['orders.cost_public'] ? $array['orders.cost_public'] : 0;
        $cost->cost_article             = $array['orders.cost_article'];
        $cost->cost_total;
        $document->order->setcost($cost);

        $dimension = new documentOrderDimension;
        $dimension->weight_end          = $array['orders.weight_end'] ? $array['orders.weight_end'] : 0;
        $dimension->weight_article      = $array['orders.weight_article'];
        $dimension->capacity            = $array['orders.capacity'];
        $dimension->count;
        $document->order->setdimension($dimension);

        $sender = new documentOrderSender;
        $sender->fio                    ;
        $sender->phone                  ;
        $sender->email                  ;
        $sender->sex;
        $sender->name;
        $sender->surname;
        $sender->patronymic;
        $sender->birthday;
        $sender->passport_ser ;
        $sender->passport_num;
        $sender->passport_day ;
        $sender->passport_give ;
        $document->order->setsender($sender);

        $address_sender = new documentOrderAddress_sender;
        $address_sender->country       ;
        $address_sender->zip            ;
        $address_sender->region         ;
        $address_sender->area          ;
        $address_sender->city          ;
        $address_sender->place;
        $address_sender->street    ;
        $address_sender->house         ;
        $address_sender->building   ;
        $address_sender->flat       ;
        $address_sender->office;
        $address_sender->floor;
        $address_sender->full;
        $document->order->setaddress_sender($address_sender);

        $person = new documentOrderPerson;
        $person->fio                    = $array['orders.fio'];
        $person->phone                  = $array['orders.phone'];
        $person->email                  = $array['orders.email'];
        $person->sex            ;
        $person->name          ;
        $person->surname     ;
        $person->patronymic   ;
        $person->birthday       ;
        $person->passport_ser;
        $person->passport_num;
        $person->passport_day;
        $person->passport_give;
        $document->order->setperson($person);

        $address = new documentOrderAddress;

        $address->country;
        $address->zip                   = $array['orders.zip'];
        $address->region                = $array['orders.region'];
        $address->area                  = $array['orders.area'];
        $address->city                  = $array['orders.city'];
        $address->place                 = $array['orders.place'];
        $address->street                = $array['orders.street'];
        $address->house                 = $array['orders.house'];
        $address->building              = $array['orders.building'];
        $address->flat                  = $array['orders.flat'];
        $address->office                = $array['orders.office'];
        $address->floor                 = $array['orders.floor'];
        $address->full                  = $array['orders.full'];
        $document->order->setaddress($address);

        $client = new documentOrderClient;
        $client->client_contract = Yii::app()->user->auth->contract;
        $document->order->setclient($client);


        return $document;
    }

    /*
    public function getComponent(array $array, FileLoadOrderComponent $component){
        $component                  = clone $component;
        $component->docType         = $array['format.doc_type'];
        $component->secretkey       = $array['orders.secretkey'];
        $component->dateCreate      = $array['orders.dateCreate'];
        $component->registry        = $array['orders.registry'];
        $component->megapolis       = $array['orders.megapolis'];
        $component->comment         = $array['orders.comment'];
        $component->opNotification  = $array['orders.op_notification'];
        $component->opPacking       = $array['orders.op_packing'];
        $component->costDelivery    = $array['orders.cost_delivery'] ? $array['orders.cost_delivery'] : 0;
        $component->costInsurance   = $array['orders.cost_insurance'];
        $component->costPublic      = $array['orders.cost_public'] ? $array['orders.cost_public'] : 0;
        $component->costArticle     = $array['orders.cost_article'];
        $component->weightEnd       = $array['orders.weight_end'] ? $array['orders.weight_end'] : 0;
        $component->weightArticle   = $array['orders.weight_article'];
        $component->capacity        = $array['orders.capacity'];
        $component->fio             = $array['orders.fio'];
        $component->phone           = $array['orders.phone'];
        $component->email           = $array['orders.email'];
        $component->zip             = $array['orders.zip'];
        $component->region          = $array['orders.region'];
        $component->area            = $array['orders.area'];
        $component->city            = $array['orders.city'];
        $component->place           = $array['orders.place'];
        $component->street          = $array['orders.street'];
        $component->house           = $array['orders.house'];
        $component->building        = $array['orders.building'];
        $component->flat            = $array['orders.flat'];
        $component->office          = $array['orders.office'];
        $component->floor           = $array['orders.floor'];
        $component->full            = $array['orders.full'];
        $component->opCategory      = $this->searchMegapolis($array['orders.op_category'], FuncMegapolis::Handbook()->op_category, '');
        $component->opFragile       = $this->searchMegapolis($array['orders.op_fragile'], FuncMegapolis::Handbook()->op_fragile, 0);
        $component->opInventory     = $this->searchMegapolis($array['orders.op_inventory'], FuncMegapolis::Handbook()->op_inventory, 0);
        $component->opType          = $this->searchMegapolis($array['orders.op_type'], FuncMegapolis::Handbook()->op_type, 0);

        $component->agent           = $this->searchMegapolis($array['client.agent_id'], FuncMegapolis::Handbook()->agent_id, 0);
        $component->opTake          = $this->searchMegapolis($array['orders.op_take'], FuncMegapolis::Handbook()->op_take, 1);
        $component->clientContract  = $array['orders.client_contract'];

        $component->date_dep        = $array['date_dep'];
        $component->intercom        = '';
        $component->dai             = $array['dai'];
        $component->address_dep     = $array['address_dep'];

        $component      = $this->replace(clone $component);
        return $component;
    }
    */
    /*
    * Присваиваем определенные параметры отправлению
    */
    public function replace($data){
        if(!empty($data->opType)){

            switch($data->opType){
                case 1:
                    $data->agent=1;
                    break;
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                case 11:
                case 13:
                    $data->agent=2;
                    break;
                case 14:
                    $data->agent=4;
                    break;
                default:
                    break;
            };
        }
        return $data;
    }
}
