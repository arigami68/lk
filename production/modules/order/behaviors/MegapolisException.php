<?php

class MegapolisException extends CException{
    public static $ERROR_ORDER_CREATE = 1;

    public static $log;
    public function get($code = NULL){

        switch($code){
            case self::$ERROR_ORDER_CREATE:
                $this->ERROR_ORDER_CREATE();

            break;
        }

    }

    public function ERROR_ORDER_CREATE(){
        Yii::app()->user->setFlash('error', 'Попробуйте воспользоваться данным сервисом позже.');
        Yii::app()->request->redirect('/orders?operation=show');
    }

}