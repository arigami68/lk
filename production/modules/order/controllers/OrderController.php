<?php

class OrderController extends UController
{
    public function init(){
        $this->breadcrumbs['Отправления'] = array('links' => '');
        $this->breadcrumbs['Создание реестра'] = array('links' => '');
    }
    public function actions()
    {

            return array(
                'order'         => array('class'=> 'application.modules.order.models.OrderAction'),
                'addRegister'   => array('class'=> 'application.modules.order.models.AddRegisterAction'),
                'close'         => array('class'=> 'application.modules.order.models.CloseRegistryAction'),
                'idea'          => array('class'=> 'application.modules.order.models.IdeaAction'),
            );
    }
}