<?php

class LoadToOrdersAction extends CAction{
    public $component;
    public $format;
    
    public function run(){
        $component = new $this->component;
        $data = $component->getData();
        
        if(!$data) die('Нет записей');
            
        $format = $component->setFormat($data, $this->format);
        $insert = $component->insertOrders('OrdersDB', $format);
    }
}