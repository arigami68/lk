<?php

class SendMailAction extends CAction{
   public $finished;
    public function run(){
        $mail = new Emailserver();
        $mail->dbCriteria->addCondition('flag = 0');
        foreach($mail->findAll() as $k=>$arr)
        {
            $email = Yii::app()->email;
            foreach($email as $title=>$value) isset($arr[$title]) ? $email->$title = $arr[$title] : false;
            
            $email->send();
            $arr->flag = $this->finished;
            $arr->update();
        }
    }
}