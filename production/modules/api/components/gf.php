<?php

# General Function - добавить как компонент
class gf{
    static function doHash( $str , $precision = 8 ){
	$str = mb_strtolower( trim( ( string ) $str ) );
        
	$str = preg_replace( "/[^0-9]/" , '' , sha1( $str ).sha1( $str.'1' ).sha1( $str.'2' ).sha1( $str.'3' ) );
	if( ( int ) $precision ){
		$str = substr( $str , 0 , $precision );
	}
	$str = preg_replace( "/^[0]*/" , '' , $str ); 
	return $str;
    }
}