<?php

class apiModule extends CWebModule{
    public function init()
    {
        define("STATUS_MAXIPOST_MEGAPOLIS", 8192);
        $this->setModules(array('maxipost'));
        $this->setImport(array(
            'api.modules.maxipost.*',
            'api.modules.*',
            'api.models.*',
            'api.controllers.*',
            'api.components.*',
            'api.modules.maxipost.models.*',
            'api.modules.maxipost.models.record.*',
        ));
    }
}