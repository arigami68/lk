<?php

class LoadController extends Controller
{
    public function actions()
    {
        # Для примера возьмем Maxipost_to_orders. Единый интерфейс для добавления любого компонента. Загрузка в Orders
        return array(
            'orders'=>array(
                'class'=> 'application.modules.api.models.LoadToOrdersAction',
                'component' => 'Maxipost_to_orders', # Для хранения данных ОД
                'format' => 'Maxipost_to_ordersComponent', # Компонент для синхронизации ОД и Orders. В соответсвии с Maxipost_to_ordersComponent
            ),
            'sendmail'=> array(
                'class'=> 'application.modules.api.models.SendMailAction',
                'finished' => 1,
            )
        );
    }
}
