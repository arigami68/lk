<?php

class OrdersDB extends CActiveRecord
{
    public static $tableName = 'orders';
    public static function model(){ return parent::model(static::$class);}
    public function tableName(){ return static::$tableName;}
    public static function getColumns(){
        $array  = array(
            'megapolis',
            'agent',
            'weightEnd',
            'zip',
            'address',
            'costDelivery',
            'costPublic',
            'dateCreate',
            'idAgent',
            'ahash',
            'mhash',
        );
        return $array;
    }
}