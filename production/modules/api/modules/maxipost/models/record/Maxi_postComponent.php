<?php
# Данные пользователя
class Maxi_postComponent extends CComponent
{
    private $_OtpravkaKod;
    private $_Recno;
    private $_Zone;
    private $_PVZNom;
    private $_SkladID;
    private $_VidDost;
    private $_Ind;
    private $_Address;
    private $_Boss;
    private $_Tel;
    private $_TovarOs;
    private $_SummaInkass;
    private $_Ves;
    private $_status;
    private $_datecreate;

    
    public function setOtpravkaKod($OtpravkaKod){ $this->_OtpravkaKod= $OtpravkaKod;}
    public function getOtpravkaKod(){ return $this->_OtpravkaKod;}

    public function setRecno($Recno){ $this->_Recno= $Recno;}
    public function getRecno(){ return $this->_Recno;}

    public function setZone($Zone){ $this->_Zone= $Zone;}
    public function getZone(){ return $this->_Zone;}

    public function setPVZNom($PVZNom){ $this->_PVZNom= $PVZNom;}
    public function getPVZNom(){ return $this->_PVZNom;}

    public function setSkladID($SkladID){ $this->_SkladID= $SkladID;}
    public function getSkladID(){ return $this->_SkladID;}

    public function setVidDost($VidDost){ $this->_VidDost= $VidDost;}
    public function getVidDost(){ return $this->_VidDost;}
    
    public function setInd($Ind){ $this->_Ind= $Ind;}
    public function getInd(){ return $this->_Ind;}
    
    public function setAddress($Address){ $this->_Address= $Address;}
    public function getAddress(){ return $this->_Address;}

    public function setBoss($Boss){ $this->_Boss= $Boss;}
    public function getBoss(){ return $this->_Boss;}
    
    public function setTel($Tel){ $this->_Tel= $Tel;}
    public function getTel(){ return $this->_Tel;}
 
    public function setTovarOs($TovarOs){ $this->_TovarOs= $TovarOs;}
    public function getTovarOs(){ return $this->_TovarOs;}
    
    public function setSummaInkass($SummaInkass){ $this->_SummaInkass= $SummaInkass;}
    public function getSummaInkass(){ return $this->_SummaInkass;}
   
    public function setVes($Ves){ $this->_Ves= $Ves;}
    public function getVes(){ return $this->_Ves;}
    
    public function setstatus($status){ $this->_status= $status;}
    public function getstatus(){ return $this->_status;}

    public function setdatecreate($datecreate){ $this->_datecreate= $datecreate;}
    public function getdatecreate(){ return $this->_datecreate;}
    
}