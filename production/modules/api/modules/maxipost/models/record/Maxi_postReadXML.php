<?php

class Maxi_postReadXML{
    public $flow;
    public $OtpravkaCount;
    public $LoadCount;
    public $DateTime;
    public $EM = array();
    
    public function __construct(SimpleXMLElement $flow){
        $this->flow = $flow;
        $this->flow($this->flow);
    }
    private function flow($flow){
        $this->OtpravkaCount = (int)$flow->attributes()->OtpravkaCount;
        $this->LoadCount = (int)$flow->attributes()->LoadCount;
        $this->DateTime = (int)$flow->attributes()->DateTime;
        
        if($this->OtpravkaCount == $this->LoadCount) return TRUE;

        foreach($flow as $k=>$v){
            foreach($v->ErrorItems->ErrorItem as $k2=>$v2){
                $code = $this->readMessage($v2);
                $key = key($code);
                $this->EM[$key] = $code[$key];
            }
        }
    }
    private function readMessage(SimpleXMLElement $read){
        $Recno = (int)$read->attributes()->Recno;
        $OtpravkaKod = (int)$read->attributes()->OtpravkaKod;
        $ErrCode = (int)$read->attributes()->ErrCode;
        $ErrDescription = (string)$read->attributes()->ErrDescription;
        $EM["$OtpravkaKod"] = array(
            'recno' => $Recno,
            'otpravkaKod' => $OtpravkaKod,
            'errCode' => $ErrCode,
            'errDescription' => $ErrDescription,
        );
        return $EM;
    }
}