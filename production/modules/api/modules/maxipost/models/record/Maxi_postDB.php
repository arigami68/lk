<?php

class Maxi_postDB extends CActiveRecord
{
    CONST FLAG_LOAD = 0;

    public static $tableName = 'maxi_post';
    public static $class = 'Maxi_postDB';
    
    public static function model()
    {
        return parent::model(static::$class);
    }
    public function tableName()
    {
        return static::$tableName;
    }
    # @getLoad
    # return - array
    # Получение отправлений которые еще не были загружены в МаксиПост
    public static function getLoad($db = __CLASS__){
        $criteria = new CDbCriteria();
        $count= $db::model()->count($criteria);
        $criteria->select = "*";
        $criteria->condition = 'status IS NULL';
        $track=new CActiveDataProvider($db,
                array(
                    'criteria' => $criteria,
                    'pagination' => array( 'pageSize' => $count),
                )
        );
        $data = $track->getdata();
        
        return $data;
    }
    public static function setStatus($id, $err, $db = __CLASS__){
        $result = $db::model()->findByPk($id);
        $result->status= $err;
        $result->update();
        print_r($result);
        return true;
    }
    # Колонки для обязательного заполнения в БД
    public static function getColumns(){
        $array  = array(
            'OtpravkaKod',
            'Recno',
            'Zone',
            'PVZNom',
            'SkladID',
            'VidDost',
            'Ind',
            'Address',
            'Boss',
            'Tel',
            'TovarOs',
            'SummaInkass',
            'Ves',
        );
        return $array;
    }
    # Отправления которые были загружены в МаксиПост и нужно загрузить в Orders
    public function getToOrders(){
        $this->getDbCriteria();
        $this->getDbCriteria()->select = "*";
        $this->getDbCriteria()->condition = 'status =1';
        
        return self::model()->findAll($this->getDbCriteria());
    }
    
}