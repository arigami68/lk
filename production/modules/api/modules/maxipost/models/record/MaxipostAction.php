<?php
 # Управляем записями Maxipost
class MaxipostAction extends CAction{
    #public $url = 'http://www.maxipost.ru/api/set_zakaz.php';
    public $url = 'http://www.maxipost.ru/api/set_zakaz_to_db_test.php';
    public $test_1;
    public $test;
    public $maxi_post_order;

    public function run(){
        $secure = date("Y.m.d").'T00:00:00&'.M_PASS;
        $get = 'account='.M_USER.'&secure='.md5($secure);
        try {
            if(!Maxi_postDB::getLoad()) throw new Exception();
        } catch(Exception $e){
            die('Нет номеров для отправления');
           
        }
        if(true){ # Получить заказ
            foreach(Maxi_postDB::getLoad() as $k=>$v){
                $this->maxi_post_order[$k] = new Maxi_postComponent();
                foreach($v as $cell=>$value){
                    if(!method_exists($this->maxi_post_order[$k], $method = "set".$cell)) continue;
                    call_user_func_array(array($this->maxi_post_order[$k], $method), array($value) );
                }
            }
        }
        
        if(true){ # Сформировать XML
            $text='';
            $count = 0;

            foreach($this->maxi_post_order as $k=>$value){
                $text.='<Otpravka ';
                foreach(Maxi_postDB::getColumns() as $k2=>$cell){
                    $method = "get".$cell;
                    $answer = $value->$method();
                    $text.= $cell.'="'.$answer.'"
                        ';
                }
                
                $text.='></Otpravka>';
                $otpravka = $text;
                $count++;
            }
            $post = '<?xml version="1.0" encoding="UTF-8" ?>
                <DeliveryTask  EmailToAnswer="potapov@megapolis-exp.ru" OtpravkaCount="'.$count.'">
                    '.$otpravka.'
                </DeliveryTask>
            ';
        }
        #header('Content-type: application/xml');
        $this->test ? $xml = $this->test_1 : $xml = Remote::maxipost($this->url, $get, $post);
        print_r($xml);
        $MP_readXML = new Maxi_postReadXML(new SimpleXMLElement($xml));
        print_r($MP_readXML);
        
        foreach($this->maxi_post_order as $k=>$MPO){
            $agent = $MPO->getOtpravkaKod();
            isset($MP_readXML->EM[$agent]) ? $flag = $MP_readXML->EM[$agent]['errCode'] : $flag = 1;
            
            Maxi_postDB::setStatus($agent, $flag);
        }
        
                
        #return $this->getController()->render('maxipost', array('model' => $model));
    }
}