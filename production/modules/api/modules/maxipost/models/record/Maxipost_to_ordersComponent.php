<?php

class Maxipost_to_ordersComponent extends CComponent{
    
    private $agent;
    private $megapolis;
    private $zip;
    private $address;
    private $costDelivery;
    private $costPublic;
    private $dateCreate;
    private $weightEnd;
    
    # Поток maxi_post
    private $flow;
    # Постоянная константа, в зависимости от того, какой класс забирает
    private $idAgent = 2;
    # Зависят от того, задан ли $this->agent AND $this->megapolis
    private $ahash;
    private $mhash;
    
    public function setOtpravkaKod($agent){$this->agent=$agent;}
    public function setRecno($megapolis){$this->megapolis=$megapolis;}
    public function setInd($zip){$this->zip=$zip;}
    public function setAddress($address){$this->address=$address;}
    public function setTovarOs($costDelivery){$this->costDelivery=$costDelivery;}
    public function setSummaInkass($costPublic){$this->costPublic=$costPublic;}
    public function setVes($weightEnd){$this->weightEnd=round($weightEnd, 2);}
    
    # Добавляем поток. Для манипуляции данных MaxiPost
    public function setFlow($flow){$this->flow=$flow;}
    public function getFlow(){return $this->flow;}
    
    
    # Добавляем в формате getMegapolis_$table + добавляем в OrdersDB записи
    public function getMegapolis_weightEnd(){return $this->weightEnd;}
    public function getMegapolis_idAgent(){return $this->idAgent;}
    public function getMegapolis_agent(){return $this->agent;}
    public function getMegapolis_megapolis(){return $this->megapolis;}
    public function getMegapolis_zip(){return $this->zip;}
    public function getMegapolis_address(){return $this->address;}
    public function getMegapolis_costDelivery(){return $this->costDelivery;}
    public function getMegapolis_costPublic(){return $this->costPublic;}
    public function getMegapolis_dateCreate(){return $this->dateCreate = time();}
    
    public function getMegapolis_ahash(){return gf::doHash($this->agent);}
    public function getMegapolis_mhash(){return gf::doHash($this->megapolis);}
}