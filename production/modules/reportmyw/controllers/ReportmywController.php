<?php

class ReportmywController extends UController
{
    public function init(){
        $this->breadcrumbs['Склад'] = array('links' => '');
        $this->title = 'Отчет';
    }
    public function actions()
    {
        $this->breadcrumbs['Отчет'] = array('links' => '');
        return array(
            'report'=>array(
              'class'=>
                 'application.modules.reportmyw.models.ReportmywAction',
            ),           
        );
    }
}