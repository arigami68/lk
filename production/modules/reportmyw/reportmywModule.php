<?php

# @info - Отчет:Отправления
class reportmywModule extends CWebModule{
    public function init()
    {
        $this->setImport(array(
            'reportmyw.models.*',
            'reportmyw.components.*',
        ));
    }
}