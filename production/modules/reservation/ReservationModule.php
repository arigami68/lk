<?php

# @info - Отчет:Отправления
class reservationModule extends CWebModule{
    public function init()
    {
        $this->setImport(array(
            'reservation.models.*',
            'reservation.components.*',
        ));
    }
}