<?php

class errorModule extends CWebModule{
    public function init()
    {
        $this->setImport(array(
            'error.models.*',
            'error.components.*',
        ));
    }
}