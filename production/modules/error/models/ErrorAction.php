<?php

class ErrorAction extends CAction{
    
    public function run(){
        $this->getController()->render('application.components.views.error.error');
    }
}