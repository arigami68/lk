<?php

class ErrorController extends Controller
{
    public function actions()
    {
        
        return array(
            'error'=>array(
              'class'=>
                 'application.modules.error.models.ErrorAction',
            ),
        );
    }
}