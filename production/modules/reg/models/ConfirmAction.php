<?php

class ConfirmAction extends CAction{
    public function run($key=0, $contract=0){
        
        $role = ROLE::model()->contract($contract)->find();
        
        # фильтры
        if($role->secret === 1) return $this->getController()->render('RegText', array( 'models' => array('text' => Yii::t("registration", "pcs"))));
        if($role->secret !== $key) return $this->getController()->render('RegText', array( 'models' => array('text' => Yii::t("registration", "pcs"))));
        # если все верно
        
        if($role->secret === $key){
            # 1. Пользователь зарегистрировался
            if($role->role == ROLE::$ROLE_UNREG){
                $role->secret = 1; # переводим в режим пользователя
                
                if($queryUser = CLIT::model()->contract($role->contract)->find()){
                    $role->role = ROLE::$ROLE_FULL; # изменяем роль пользователя
                }elseif($queryUser = TMP_CLIT::model()->contract($role->contract)->find()){
                    $role->role = ROLE::$ROLE_TMP; # изменяем роль пользователя
                }
                $role->update();
                
                return $this->getController()->render('RegText', array( 'models' => array('text' => Yii::t("registration", "ysr"))));
            }
            # 2. Пользователь запросил обновление пароля
            if(true){
                $role->secret = 1; # Сбрасываем секретный ключ
                $role->update();
            }
            if(true){# Находим клиента и обновляем ему пароль
                $recove = new Registration();
                $recove->dbCriteria->addCondition('contract='. $role['contract']);
                $queryRecove = $recove->find();
                $pass = $queryRecove->renderPass();
                $queryRecove->secretKey = md5($pass);
                $queryRecove->update();
            }
            Emailserver::SendModRole('confirm', array('email' => $queryRecove->email, 'pass' => $pass));
            # Обновляем пароль и отсылаем на почту
            return $this->getController()->render('RegText', array( 'models' => array('text' => Yii::t("registration", "ysrRepeat"))));
        }
    }
}