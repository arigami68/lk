<?php
class Registration extends CActiveRecord
{
    public static $tableName = 'clients';
    public $contract;
    public $email;
    public $secretKey;
    public $verifyCode;
  
    public static function model($className= __CLASS__) {return parent::model($className);}
    public function tableName() {return self::$tableName;}
    public function rules()
    {
        return array(
            array('contract, email, secretKey', 'required', 'message'=> Yii::t("total", "обязательное поле")),
            array('email', 'email', 'message'=> Yii::t("total", "не кооректный E-mail")),
            array('secretKey', 'length', 'min' => 6, 'max' => 18,
                'tooShort' => Yii::t("total", "не верное количество символов"),
                'tooLong' => Yii::t("total", "не верное количество символов")
            ),
            array('verifyCode', 'CaptchaExtendedValidator', 'allowEmpty'=>!CCaptcha::checkRequirements(), 'message'=> Yii::t("total", "не верно введен код")),
        );
    }
    public function attributeLabels()
    {
        return array(
            'contract'      => Yii::t("total", "номер договора"),
            'email'         => Yii::t("total", "e-mail"),
            'verifyCode'    => Yii::t("total", "проверочный код"),
            'secretKey'     => Yii::t("total", "пароль"),
        );
    }
    
    public function renderPass($length = 8){
        $gener = '123456789QqWwEeRrTtYyUuIiOoPpaAsSdDFfGgHhJjKkLlZzXxCcVvBbNnMm';
        $length_need = min($length, strlen($gener));
        $result = '';
        while (strlen($result) < $length)
            $result .= substr(str_shuffle($gener), 0, $length_need);
        return $result;
    }
}