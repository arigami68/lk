<?php
# @info: RegAction - Регистрация
class RegAction extends CAction{
    public function run(){
        $model = new Registration();

        if (Yii::app()->request->isPostRequest){
            $model->attributes= $reg = $_POST['Registration'];
            
            if($model->validate()){
                if($contract = CLIT::model()->contract($reg['contract'])->find()){
                }elseif($contract = TMP_CLIT::model()->contract($reg['contract'])->find()){
                }else{
                    $contract = NULL;
                }
                if(!$contract) return $this->getController()->render('application.components.views.reg.RegFalse', array());
                
                $contract->secretKey = md5($model->secretKey);
                if($contract->update()){
                    # Сохраняем роль
                    $role               = new ROLE;
                    $role->secret       = md5(microtime());
                    $role->contract     = $reg['contract'];
                    $role->role         = ROLE::$ROLE_UNREG; # Не подтвержденный пользователь
                    $role->date_create  = date('Y-m-d H:i:s');
                    $role->insert();

                    Emailserver::SendModRole('reg', array('email' => $contract['email'], 'contract' => $role->contract, 'secret' => $role->secret));

                    return $this->getController()->render('application.components.views.reg.RegTrue', array());
                }
            }
            
            return $this->getController()->render('application.components.views.reg.Reg', array('model' => $model));
        }
        
        return $this->getController()->render('application.components.views.reg.Reg', array('model' => $model));
    }
}