<?php

class RegController extends Controller
{
    public function actions()
    {

        return array(
            'reg'=>array(
              'class'=>
                 'application.modules.reg.models.RegAction',
            ),
             'confirm'=>array(
              'class'=>
                 'application.modules.reg.models.ConfirmAction',
            ),
            'captcha'=>array(
                'class'=>'CaptchaExtendedAction',
                'mode'=> CaptchaExtendedAction::MODE_DEFAULT,
                'testLimit'=> 0,
            ),
           
        );
    }
}