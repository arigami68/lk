<H3>Регистрация</H3>
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'horizontalForm',
    'type'=>'horizontal',
));
?>
<div>
    <?php echo $form->textFieldRow($model,'contract',array('rows'=>2, 'class'=>'span7')); ?>
    <?php echo $form->textFieldRow($model,'email',array('rows'=>2, 'class'=>'span7')); ?>
    <?php echo $form->passwordFieldRow($model,'secretKey',array('rows'=>2, 'class'=>'span7', 'hint' => 'от 6-и символов')); ?>
    
    <div style='margin-left: 180px; margin-bottom: 10px'>
        <?php $this->widget('CCaptcha');?>
    </div>
    <?php echo $form->textFieldRow($model,'verifyCode',array('rows'=>2, 'class'=>'span1')); ?>
    <br>
    <div style='width: 8%; float: left; margin-left: 180px'>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type'       => 'primary',
            'label'      => 'Создать',
            'size'       => 'Normal'
        )
    ); ?>
    </div>
</div>
<?php $this->endWidget(); ?>