<?php

class regModule extends CWebModule{
    public function init()
    {
        $this->setImport(array(
            'reg.models.*',
            'reg.components.*',
            'extensions.captchaExtended.*',
            'application.modules.api.modules.maxipost.components.*',
        ));
    }
}