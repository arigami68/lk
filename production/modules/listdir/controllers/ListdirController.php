<?php
class ListdirController extends UController
{
    private $dir = __DIR__;
    public function actions()
    {
        return array(
            'index'=>array(
              'class'=>
                 'application.modules.listdir.models.ListdirAction',
                 'dir' => $this->dir,
           ),
        );
    }
    
}