<?php
return array(
    'регистрация'                   => 'einchecken',
    'восстановление пароля'         => 'Passwort-Wiederherstellung',
    'номер договора'                => 'Vertragsnummer',
    'пароль'                        => 'Passwort',
    'войти в личный кабинет'        => "Zum Mitgliederbereich einloggen",
    'вход'                          => "eingang",
    'ERROR_UNKNOWN'                 => "Falsche Taste und / oder Passwort",
    'ERROR_UNKNOWN_USER'            => "Benutzername oder Passwort stimmt nicht überein",
    'создать'                       => 'Schaffen',
    'e-mail'                        => 'E-mail',
    'проверочный код'               => 'bestätigungs-code',


    'полное наименование организации'   => 'ger',
    'краткое наименование организации'  => 'Краткое наименование организации',
    'организационно-правовая форма'     => 'организационно-правовая форма',
    'адрес юридический' => 'адрес юридический',
    'адрес фактический' => 'адрес фактический',
    'КНН' => 'КНН',
    'КПП (только для ООО)' => 'КПП (только для ООО)',
    'ОКПО' => 'ОКПО',
    'Расчетный счет' => 'Расчетный счет',
    'Корреспондентский счет' => 'Корреспондентский счет',
    'БИК' => 'БИК',
    'ОКАТО' => 'ОКАТО',
    'Ф.И.О. руководителя организации' => 'Ф.И.О. руководителя организации',
    'Ф.И.О. главного бухгалтера' => 'Ф.И.О. главного бухгалтера',
    'Адрес для доставки документов (счетов, с/ф, актов)' => 'Адрес для доставки документов (счетов, с/ф, актов)',
    'телефон' => 'ger',
    'ОКОГУ' => 'ОКОГУ',
    'ОКАТО' => 'ОКАТО',
    'ОКВЭД' => 'ОКВЭД',
    'ОКФС/ОКОПФ' => 'ОКФС/ОКОПФ',
    'ОГРН/ОГРНИП' => 'ОГРН/ОГРНИП',
    'Наименование банка, в т.ч. место (город) нахождения' => 'Наименование банка, в т.ч. место (город) нахождения',
    'Должность руководителя организации' => 'Должность руководителя организации',
    'Факс' => 'Факс',

    'данные о компании'             => 'ger:',
    'договора'                      => 'ger:',
    'реквизиты'                     => 'ger:',
    'настройки личного кабинета'    => 'ger',
    'краткая информация'            => 'ger',
    'сохранить данные'              => 'ger',
    'условные обозначения'          => 'ger',
    'обязательное поле'             => 'ger обязательное поле',
    'не обязательное поле'          => 'ger',


    'Москва'    => 'ger',
    'По России' => 'ger',
    'бесплатно по России' => 'ger',
    'компания'                      => 'ger',
    'дата заключение договора'      => 'ger',
    'договор'                       => 'ger',
    'Выход'                         => 'ger',
    'наложенный платеж'             => 'ger',
    'выберите дату'                 => 'ger',
    'с'                             => 'ger',
    'по'                            => 'ger',
    'Поиск по ШПИ/ № груза'         => 'ger',
    'загрузка файла'                => 'ger',
    'загрузить данные'              => 'ger',
    'закрывающие документы'         => 'ger',
    'по присвоению номерам отправлений' => 'ger',
    'статусы отправлений' => 'ger',
    'возвраты' => 'ger',
    'сформировать' => 'ger',
    'diapazon-31' => 'ger',
    'выберите дату' => 'ger',
    'отчеты' => 'ger',
    'заявка на вызов курьера' => 'ger',
    'создать заявку' => 'ger',
    'просмотр' => 'ger Просмотр',
    'редактирование' => 'ger Редактивароние',
    'удаление' => 'ger Удаление',
    '№ заявки' => '№ ger',
    'дата модификации заявки' => 'ger',
    'дата прибытия курьера' => 'ger',
    'статус заявки' => 'ger',
    'операции' => 'ger операции',
    'действия' => 'ger действия',
    'нет, пропуск не нужен' => 'Нет, пропуск не нужен',
    'да, пропуск нужен' => 'ger',
    'дата' => 'ger',
    'адрес пункта погрузки' => 'ger',
    'контактное лицо' => 'ger',
    'объем (м<sup>3</sup>)' => 'ger (м<sup>3</sup>)',
    'пропуск' => 'ger',
    'примечание' => 'ger',
    'сохранить заявку' => 'ger',
    'составление заявки' => 'ger',
    'комментарий' => 'ger',
    'адрес пункта доставки' => 'ger',
    'нет, пропуск не нужен' => 'ger',
    'да, пропуск нужен' => 'ger',
    'заявки' => 'ger',
    'С' => 'ger',
    'По' => 'ger',
    'объем' => 'ger',

    'город отправления' => 'ger',
    'город доставки' => 'ger',
    'вес, кг' => 'ger',
    'ОЦ' => 'ger',
    'НП' => 'ger',
    'наложенный платеж' => 'ger',
    'объявленная ценность' => 'ger',
    'очистить' => 'ger',
    'рассчитать' => 'ger',
    'выгрузить в excel' => 'ger',
    '№' => '№',
    'ОД' => 'ger',
    'услуга' => 'ger',

    'тариф руб, с НДС' => 'ger',
    'срок доставки, раб.дн.' => 'ger',
    'итого, c НДС' => 'ger',
    'отслеживание отправлений' => 'ger',
    'номер заказа клиента' => 'ger',
    'выбрать файл' => 'ger',
    'номер отправления' => 'ger',
    'отследить' => 'ger',
    'просмотр реестров' => 'ger просмотр реестров',
    'создание реестра' => 'ger Создание реестра',
    'количество реестров' => 'ger Количество реестров',
    'печать штрихкодов'     => 'ger печать',
    'просмотр отправлений' => 'ger просмотр отправлений',
    'печать' => 'ger печать',
    '№ Реестра' => 'ger № Реестра',
    'дата создания' => 'ger Дата создания',
    'количество'    => 'ger Количество',
    'тип приема'    => 'ger Тип приема',
    'статус реестра'    => 'ger Статус реестра',
    'печать реестра'    => 'ger Печать реестра',
    'курьером' => 'ger Курьером',
    'в окне приема' => 'ger В окне приема',
    'закрыть реестр' => 'ger Закрыть реестр',
    'реестр закрыт' => 'ger Реестр закрыт',
    "cообщение о смене пароля" => 'ger На Ваш ящик был отправлен ключ для подтверждения',
    "не верно введен код" => "ger не верно введен код",
    "не кооректный E-mail" => "ger Не кооректный E-mail",
    "от 6-и символов" => "ger От 6-и символов",
    'номер груза' => 'ger Номер груза',
    'идентификатор почтового отправления' => ' ger Идентификатор почтового отправления',
    'дата приема отправления от отправителя' => 'ger Дата приема отправления от отправителя',
    'реестр' => 'ger Реестр',
    'ОЦ, руб.' => 'ger ОЦ, руб.',
    'НП, руб ' => 'ger НП, руб ',
    'адрес доставки' => 'ger Адрес доставки',
    'ФИО получателя' => 'ger ФИО получателя',
    'статус доставки' => 'ger Статус доставки',

    'удалить отправление' => 'ger Удалить отправление',
    'печать отправления' => 'ger Печать отправления',
    'печать штрихкода по отправлению' => 'ger Печать штрихкода по отправлению',
    'штрих код' => 'ger Штрих код',
    'реквизиты успешно сохранены' => 'ger Реквизиты успешно сохранены',
    'не все поля были заполнены' => 'ger Не все поля были заполнены',

    'удалить отправление:' => 'ger Удалить отправление',
    'удалить реестр' => 'ger Удалить реестр?',
    'закрыть реестр' => 'ger Закрыть реестр?',
    'автоматическое присвоение' => '1ger автоматическое присвоение',
    'Почта России' => 'ger Почта России',
    'Да' => 'ger Да',
    'Нет' => 'ger Нет',
    'Простое' => 'ger Простое',
    'Посылка' => 'ger Посылка',
);
