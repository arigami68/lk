<?php

class Emailserver extends CActiveRecord{
    
    public static $tableName = 'mail';
    public static function model($className= __CLASS__) {return parent::model($className);}
    public function tableName() {return self::$tableName;}
    
    public static function SendModRole($flag, array $array){
        $email              = new Emailserver();
        $email->date_create = date('Y-m-d H:i:s');
        
        # @info: reg - Регистрация
        if($flag == 'reg'){
            if(!isset($array['email']) OR !isset($array['contract']) OR !isset($array['contract'])) throw new Exception('Обратитесь в службу поддержки. #1000');
            
            $email->to          = $array['email'];
            $email->from        = 'info@megapolis-exp.ru';
            $email->subject     = 'Вы успешно зарегистрировались!';
            $email->message     = 'Ваш номер договора: '.$array['contract'].' <br>Подтвердите E-mail : <a href="/reg/Reg/confirm?contract='.$array['contract'].'&key='.$array['secret'].'">по этой ссылке</a>';
        }
        
        # @info: confirm - Высылаем новый пароль
        if($flag == 'confirm'){
            if(!isset($array['email']) OR !isset($array['pass'])) throw new Exception('Email. Обратитесь в службу поддержки. #1001');
            
            $email->to = $array['email'];
            $email->from = 'info@megapolis-exp.ru';
            $email->subject = 'Восстановление пароля';
            $email->message = 'Ваш новый пароль: '. $array['pass'];
            
        }
        
        # @info: recove - Восстановление пароля. Высылаем ключ для подтверждения.
        if($flag == 'recove'){
            if(!isset($array['email']) OR !isset($array['key']) OR  !isset($array['contract']) ) throw new Exception('Email. Обратитесь в службу поддержки. #1002');
            
               $email->to = $array['email'];
               $email->from = 'info@megapolis-exp.ru';
               $email->subject = 'Восстановление пароля';
               $email->message = 'Кликните : <a href="/reg/Reg/confirm?contract='.$array['contract'].'&key='.$array['key'].'">по этой ссылке</a> если вы запрашивали смену пароля';
        }        

        $email->insert();
        Remote::mail("/api/load/sendmail");
    }
}