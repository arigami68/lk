<?php

class TRAC extends CActiveRecord{
    private static $tableName = 'trace';
    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){return self::$tableName;}
	public function last(){
		$this->getDbCriteria()->mergeWith(
			array(
				'order'=> 'id desc',)
		);
		return $this;
	}
	public function code($id){$this->getDbCriteria()->mergeWith(array('condition'=> 'code = "'.$id.'"',));return $this;}
}