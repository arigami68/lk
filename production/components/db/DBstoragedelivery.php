<?php

class DBstoragedelivery extends CActiveRecord{
	private static $tableName = 'storagedelivery';
	public static function model($className= __CLASS__){return parent::model($className);}
	public function tableName(){return self::$tableName;}
	public function rules()
	{
		return array(
			array('provider,client,creatinonDate,editionDate,number', 'safe'),
		);
	}
}