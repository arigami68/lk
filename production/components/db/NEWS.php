<?php

class NEWS extends CActiveRecord{
    private static $tableName = 'news';
    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){return self::$tableName;}
    public function search(){
        $criteria = new CDbCriteria();
        $pagination = new CPagination();
        $sort = new CSort();
        $pagination->pageVar = 'page';
        $pagination->pageSize = 10;
        $sort->sortVar = 'sort';
        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
            'pagination'=> $pagination,
            'sort' => $sort,
        ));
    }
    public static function getNews($data){
        $text = '';
        $text.= '<div id="megapolis_time">'. date('Y-m-d', strtotime($data->date)).'</div>';
        $text.= '<div id="megapolis_top"><a href=news?id='.$data->id.'>'. $data->themes.'</a></div>';
        #$text.= '<div id="megapolis_intro">'. mb_strcut($data->text, 0, 200, 'UTF-8').'...</div>';

        return $text;
    }
}