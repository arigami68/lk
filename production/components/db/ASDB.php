<?php

class ASDB extends CActiveRecord{
    public static $APP_VIEW = 0;
    public static $APP_EDIT = 1;
    
    private static $tableName = 'app_storage';
    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){return self::$tableName;}
    public function relations(){
        return array(
            'idstorage'=>array(self::HAS_MANY, 'AHDB', 'idstorage'),
        );
    }
    public function search_getiw(){
        
        $array = array();
        $criteria = new CDbCriteria;
        $criteria->compare('user', Yii::app()->user->auth->id);
        
        $criteria->select ='id, dateupdate, provider';
        $all = ASDB::model()->findAll($criteria);
        
        $array = array();
        
        foreach($all as $cell){
            foreach($cell->idstorage as $k=>$storage){
                if($apti = APTI::model()->user()->find('id_idapplication=:id_idapplication', array('id_idapplication' => $storage->idapplication))){
                    if($apti->status == APTI::$APP_DELETE) continue;
                }
                if(!$apti) continue;
                $array[$storage->idapplication][$storage->idstorage]=$this->getApps($storage);
                $array[$storage->idapplication][$storage->idstorage]['date'] = $cell->dateupdate;
                $array[$storage->idapplication][$storage->idstorage]['provider'] = $cell->provider;
                $array[$storage->idapplication][$storage->idstorage]['date_shipping_fact'] = $apti->date_shipping_fact;
            }
        }
        
        $arr = array_map(function($array){
            
            foreach($array as $k=>$v){
                $storage = (string)$v['storage'];
                $id = $v['id'];
                $sum[$id] = array(
                        'id' => $id,
                        'amount' => isset($sum[$id]) ? $sum[$id]['amount'] + $v['number'] : $v['number'],
                        'date' => date('Y-m-d H:i', $v['date']),
                        'provider' => $v['provider'],
                        'date_shipping_fact' => $v['date_shipping_fact'],
                    );
            }
            return $sum[$id];
        }, $array);
        
        
        $pagination = new CPagination();
        $pagination->pageVar = 'page';
        $pagination->pageSize = 10;
        
        $sort = new CSort();
        $sort->sortVar = 'sort';
        
        return new CArrayDataProvider($arr, array(
            'pagination'=> $pagination,
            'sort' => $sort,
        ));
    }
    
    public static function view_operation($data){
        $id = $data['id'];
        $img['read'] = '/megapolis-status-pin--arrow.png';
        $img['edit'] = '/megapolis-status-pin--pencil.png';
        $img['delete'] = '/megapolis-status-pin--minus.png';
        $img['edit_status'] = '/megapolis-edit-status.png';
        $apti = APTI::model()->find('id_idapplication=:id_idapplication', array('id_idapplication' => $id));
        $array = array(
            'read' => array(
                'src' =>  CHtml::image(Yii::getPathOfAlias('link_images').$img['read']),
                'url' => '/iw?ASDB[id]='.$id.'&ASDB[status]='.self::$APP_VIEW,
                'htmloption' =>  array('title' => 'Просмотр заявки'),
            ),
            'refresh' => array(
                'src' =>  CHtml::image(Yii::getPathOfAlias('link_images').$img['edit']),
                'url' => '/iw?ASDB[id]='.$id.'&ASDB[status]='.self::$APP_EDIT,
                'htmloption' =>  array('title' => 'Редактирование заявки'),
            ),

            'delete' => array(
                'src' =>  CHtml::image(Yii::getPathOfAlias('link_images').$img['delete']),
                'url' => '/deleteiw?APTI[id]='.$id,
                'htmloption' =>  array('title' => 'Удаление заявки'),
            ),
            'edit' => array(
                'src' =>  CHtml::image(Yii::getPathOfAlias('link_images').$img['edit_status']),
                'url' => '/editiw?APTI[id]='.$id,
                'htmloption' =>  array('title' => 'Изменения статуса'),
            ),
        );
        return (
                $apti->status ? CHtml::link($array['read']['src'], $array['read']['url'], $array['read']['htmloption']) : 
                CHtml::link($array['read']['src'], $array['read']['url'], $array['read']['htmloption'])
                .' '.
                 CHtml::link($array['refresh']['src'], $array['refresh']['url'], $array['refresh']['htmloption'])
                .' '.
                 CHtml::link($array['delete']['src'], $array['delete']['url'], $array['delete']['htmloption'])
                .' '.
                CHtml::link($array['edit']['src'], $array['edit']['url'], $array['edit']['htmloption']) 
        );
    }
    public function getApps(AHDB $storage){
            $array = array(
                'id' => $storage->idapplication,
                'storage' => $storage->idstorage,
                'number' => $storage->number,
            );  
        
        return $array;
    }
    public function scopes()
    {
        return array(
            'user'=>array(
                'condition'=> 'user = '.Yii::app()->user->auth->id,
            ),
        );
    }
}