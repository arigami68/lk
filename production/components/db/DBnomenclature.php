<?php

class DBnomenclature extends CActiveRecord{

    private static $tableName = 'storagenomenclature';
    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){return self::$tableName;}
	public function rules()
	{
		return array(
			array('article, code, name, print, handbookunitId, rate, weight, volume, length, width, height, comment, group', 'safe'),
		);
	}

	public function beforeSave(){
		parent::beforeSave();
		/**
		 * Check for user belongs this group
		 */
		if(isset($this->group)){
			$group = $this->group;
			return DBnomenclaturegroupname::model()->id($group)->user()->count();
		}
		return true;
	}
	public function afterSave(){
		parent::afterSave();
	}
	public function search(){
		$criteria = new CDbCriteria();
		$pagination = new MegapolisPagination();
		$sort = new CSort();
		$criteria->scopes = array('user');
		$pagination->pageVar = 'page';
		$pagination->pageSize = 10;
		$sort->sortVar = 'sort';

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
			'pagination'=> $pagination,
			'sort' => $sort,
		));
	}
	public function article($id)
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition'=> 'article= "'.$id.'"',
		));

		return $this;
	}
	public function warehouse($article){

		$this->getDbCriteria()->mergeWith(array(
			'condition' => ' article LIKE "'. $article.'%"',
			'limit'             => 10,
			'select'            => 'article'
		));
	}
	public function scopes(){
		return array(
			'user'=>array(
				'condition'=> 'clientID = '.Yii::app()->user->auth->id,
			),
		);
	}
	public function relations()
	{
		return array(
			'unit'=>array(self::BELONGS_TO, 'DBhandbookUnit', array('handbookunitId' => 'id'))
		);
	}
}