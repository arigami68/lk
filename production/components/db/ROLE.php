<?php

class ROLE extends CActiveRecord
{
    public static $ROLE_UNREG = 4; # Не зарегистрированный пользователь
    public static $ROLE_TMP   = 7; # Временный пользователь
    public static $ROLE_FULL  = 8; # Зарегистрированный пользователь со всеми правами
    public static $tableName = 'clients_role';
    
    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){ return self::$tableName;}
    public function contract($id)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition'=> 'contract= '.$id,
        ));
        return $this;
    }
    public function relations()
    {
        return array(
            'clientrole'=>array(self::BELONGS_TO, 'RoleClients', 'role'),
        );
    }
}