<?php

class DBnomenclaturegroupname extends CActiveRecord{
	private static $tableName = 'storagenomenclaturegroupname';
	public static function model($className= __CLASS__){return parent::model($className);}
	public function tableName(){return self::$tableName;}
	public function search($prev = ' AND n.prev=0'){
		$criteria = new CDbCriteria();
		$criteria->alias = 't';
		$pagination = new MegapolisPagination();
		$sort = new CSort();

		$criteria->join = 'LEFT JOIN storagenomenclaturegroup n ON n.current=t.id where  t.clientID = '.Yii::app()->user->auth->id.$prev;

		$pagination->pageVar = 'page';
		$pagination->pageSize = 300;
		$sort->sortVar = 'sort';

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
			'pagination'=> $pagination,
			'sort' => $sort,
		));
	}
	public function searchFilter(CActiveDataProvider $provider){
		$array = array();
		foreach($provider->getData() as $k=>$v){
			$array[$v->id] = $v->name;
		}
		return $array;
	}
	public function id($id)
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition'=> 'id = '.$id,
		));
		return $this;
	}
	public function name($id)
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition'=> 'name = "'.$id.'"',
		));
		return $this;
	}

	/**
	 * Building hierarchy
	 */
	public static function getData(){
		$db     = new DBnomenclaturegroupname;
		$search = $db->search();
		$data   = $search->getData();
		$result = array();
		$three;
		// Мой дорогой друг, у тебя есть шанс организовать рекурсию. Посмотр за окном, там салют в твою честь. А я сделаю пару вложенностей и никто не спалит этого, потому что всем пофиг.

		foreach($data as $k=>$v){
			$id     = $v->id;
			$text   = $v->name;

			$result[$id] = array();
			$result[$id]['id'] = $id;
			$result[$id]['text'] = $text;

			if($group = DBnomenclaturegroup::model()->prev($id)->findAll()){

				foreach($group as $k2=>$v2){
					$idCurrent     = $v2->currentName->id;
					$name   = $v2->currentName->name;

					$result[$id]['children'][$idCurrent]['id']     = $idCurrent;
					$result[$id]['children'][$idCurrent]['text']   = $name;
				}

			}
		}


		return $result;
	}


	public function getView(){
		/*

$this->widget('bootstrap.widgets.TbGridView', array(
	'type'=>'striped bordered condensed',
	'dataProvider'=>  $data['model']->search(),
	'template'=>"{items}\n{pager}",
	'columns'=>array(
		array(
			'name'=>'name',
			'header'=>'Наименование',
			'htmlOptions'=>array('style'=>'width: 10px'),
		),
		array(
			'name'=>'article',
			'header'=>'Артикула',
			'htmlOptions'=>array('style'=>'width: 10px'),
			'type' => 'raw',
			'value' => function($data){
					$article = $data['article'];
					return CHTML::link($article, '/mywN?operation=read&art='.$article);
				},
		),
		array(
			'name'=>'',
			'header'=>'Качество',
			'htmlOptions'=>array('style'=>'width: 10px'),
		),
		array(
			'name'=>'2',
			'header'=>'Ед.изм.',
			'htmlOptions'=>array('style'=>'width: 10px'),
		),
		array(
			'name'=>'1',
			'header'=>'Остаток по номенклатуре (в ниличии)',
			'htmlOptions'=>array('style'=>'width: 10px'),
		),
		array(
			'name'=>'1',
			'header'=>'Операции',
			'htmlOptions'=>array('style'=>'width: 10px'),
		),
	),
	'pager' => array(
		'class' => 'CLinkPager',
		'cssFile' => false,
		'header' => false,
		'firstPageLabel' => '<<',
		'prevPageLabel' => '<',
		'nextPageLabel' => '>',
		'lastPageLabel' => '>>',
	),
));

		 */
	}
	public function scopes(){
		return array(
			'user'=>array(
				'condition'=> 'clientID = '.Yii::app()->user->auth->id,
			),
		);
	}
}