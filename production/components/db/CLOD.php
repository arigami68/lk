<?php

class CLOD extends CActiveRecord{
    public static $OD_EMS        = 1;
    public static $OD_PR         = 2;
    public static $OD_DPD        = 3;
    public static $OD_IDEA       = 4;
	public static $OD_MAXIPOST   = 5;


    public static $OD_IDEAPvz       = 1006;

	public static $OD_PRBanderol    = 5005;
	public static $OD_PRAviaBelorus = 5006;
	public static $OD_PRNazemBelorus= 5007;
	public static $OD_PRAviaKazah   = 5008;
	public static $OD_PRNazemKazah  = 5009;
	public static $OD_POSILKA       = 5010;
	public static $OD_COURIER       = 5011;

    private static $tableName = 'calc_od';
    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){return self::$tableName;}
}