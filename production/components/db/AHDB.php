<?php

class AHDB extends CActiveRecord{
   
    private static $tableName = 'app_history';
    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){return self::$tableName;}
    public static function undertow($id){
        $criteria = new CDbCriteria();
        $criteria->compare('idapplication', $id);
        if(AHDB::model()->deleteAll($criteria))
            return 1;
        
        return 1;
    }
    
}