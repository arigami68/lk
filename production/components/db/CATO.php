<?php

class CATO extends CActiveRecord{
    private static $tableName = 'calc_total';
    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){return self::$tableName;}
    public function relations()
    {
        return array(
            'CLOD'=>array(self::BELONGS_TO, 'CLOD', array('od' => 'id')),
            'CLPR'=>array(self::HAS_MANY, 'CLPR', array('zone_id' => 'tz')),
            'CLPR_IDEA'=>array(self::BELONGS_TO, 'CLPR_IDEA', array('idkladr' => 'kladr')),
            'CLPR_DPD'=>array(self::HAS_MANY, 'CLPR_DPD', array('tz' => 'tz')),
            'CLKL'=>array(self::HAS_MANY, 'CLKL', array('kladr' => 'idkladr')),
        );
    }
    public function kladr($id)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition'=> 'idkladr = "'.$id.'"',
        ));
        return $this;
    }
    public function od($id)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition'=> 'od ='.$id,
        ));
        return $this;
    }
}