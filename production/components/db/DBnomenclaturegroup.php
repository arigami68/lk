<?php

class DBnomenclaturegroup extends CActiveRecord{
	private static $tableName = 'storagenomenclaturegroup';
	public static function model($className= __CLASS__){return parent::model($className);}
	public function tableName(){return self::$tableName;}
	public function prev($id)
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition'=> 'prev = "'.$id.'"',
		));
		return $this;
	}
	public function relations()
	{
		return array(
			'currentName'=>array(self::BELONGS_TO, 'DBnomenclaturegroupname', array('current'=> 'id')),
		);
	}
}