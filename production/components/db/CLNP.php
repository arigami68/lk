<?php

class CLNP extends CActiveRecord{
    private static $tableName = 'client_np';
    public static $SCENARIO_FILE = 'file';
    public static $STATUS_SCHEDULED = 1; # Запланировано к оплате
    public static $STATUS_CANCEL = 2;    # Аннулировано
    public static $STATUS_PAID = 3;      #  Оплачено
    public static $STATUS_CANCELPAID = 4;# Аннулированая оплата

    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){return self::$tableName;}
    public function beforeFind(){
        $this->getDbCriteria()->mergeWith(array( 'condition'=> ' cod_status ='.self::$STATUS_PAID));
        return $this;
    }
    public function search($criteria, $sort){
        $agent    = Yii::app()->request->getQuery('agent');
        $criteria->scopes = array('contract');

        $criteria->mergeWith(array( 'condition'=> ' cod_status ='.self::$STATUS_PAID));

        if($this->scenario !== self::$SCENARIO_FILE){
            $s = Yii::app()->request->getQuery('s') ? MegapolisTime::get()->timeToConvert(MegapolisTime::$TIME_DAY, MegapolisTime::$TIME_YEAR, new DateTime(Yii::app()->request->getQuery('s'))) : NULL;
            $f = Yii::app()->request->getQuery('f') ? MegapolisTime::get()->timeToConvert(MegapolisTime::$TIME_DAY, MegapolisTime::$TIME_YEAR, new DateTime(Yii::app()->request->getQuery('f'))) : NULL;


            $criteria->compare('agent', $agent, 'false', 'AND');
            $criteria->compare('mailing_id', $agent,'false', 'OR');

            if($f) $criteria->addBetweenCondition('reception_date', $s, $f, 'AND');
        }

        $pagination = new MegapolisPagination();


        $pagination->pageVar = 'page';
        $pagination->pageSize = 10;
        $sort->sortVar = 'sort';

        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
            'pagination'=> $pagination,
            'sort' => $sort,
        ));
    }
    public static function view_status($data){
        $status = '';
        if($data->cod_status == 1){ $status = 'Запланировано к оплате';}
        elseif($data->cod_status == 2){ $status = 'Аннулировано';}
        elseif($data->cod_status == 3){ $status = 'Оплачено';}
        elseif($data->cod_status == 4){ $status = 'Анулирование оплаты';}
        print $status;
    }

    public function relations()
    {
        return array(
            #'role'=>array(self::BELONGS_TO, 'Role', 'contract'),
        );
    }
    public function scopes()
    {
        return array(
            'contract'=>array(
                'condition'=> 'contract = '.Yii::app()->user->auth->contract,
            ),
        );
    }
}