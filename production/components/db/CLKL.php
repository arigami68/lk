<?php

class CLKL extends CActiveRecord{
    private static $tableName = 'calc_kladrindex';
    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){return self::$tableName;}
    public function zip($zip){
        $this->getDbCriteria()->mergeWith(array(
            'condition'=> 'zip = "'.$zip.'"',
        ));
        return $this;
    }
}