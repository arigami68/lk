<?php

class MAKE extends CActiveRecord{
    private static $tableName = 'make';
    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){return self::$tableName;}
    public function search(){
        $criteria = new CDbCriteria();
        $pagination = new CPagination();
        $sort = new CSort();
        $pagination->pageVar = 'page';
        $pagination->pageSize = 10;
        $sort->sortVar = 'sort';
        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
            'pagination'=> $pagination,
            'sort' => $sort,
        ));
    }
    public function scopes()
    {
        return array(
            'user'=>array( 'condition'=> 'user = '.Yii::app()->user->auth->id),
        );
    }
}