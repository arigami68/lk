<?php

class DBcalc_difficult_pr extends CActiveRecord{
    public static $deliveryType_CLOSE = 1;
    public static $deliveryType_EARTH = 2;
    public static $deliveryType_SKY   = 3;

    private static $tableName = 'calc_difficult_pr';
    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){return self::$tableName;}


    public function zip_ops($id)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition'=> 'zip_ops = '.$id,
        ));
        return $this;
    }
}