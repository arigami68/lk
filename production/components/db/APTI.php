<?php

class APTI extends CActiveRecord{
    public static $APP_CREATE = 0;
    public static $APP_TO_ENTER = 1;
    public static $APP_DELETE = 3;
    public static $APP_ENTERED_OF_1C = 9;
    public static $APP_ENTERED_OF_1C_full = 4;
    public static $APP_ENTERED_OF_1C_part = 8;
    
    private static $tableName = 'app_time';
    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){return self::$tableName;}
    public function relations()
    {
        return array(
            'APH1'=>array(self::HAS_MANY, 'APH1', array('id_idapplication' => 'id_idapplication')),
        );
    }
    public static function status($data){
        $id_app = $data['id'];
        $criteria = new CDbCriteria();
        $criteria->scopes = array('user');
        $criteria->compare('id_idapplication', $id_app);
        
        if(is_object($data)){
            $APTI = $data;
        }else{
            if(!$APTI = APTI::model()->find($criteria)) return false;
        }
        
        if($APTI->status == self::$APP_CREATE){
            $array =  array(
                       'src' =>  CHtml::image(Yii::getPathOfAlias('link_images').'/megapolis-status-pin-small.png'),
                       'htmloption' =>  array('title' => 'создан'),
           );
        }
        if($APTI->status == self::$APP_TO_ENTER){
            $array =  array(
                       'src' =>  CHtml::image(Yii::getPathOfAlias('link_images').'/megapolis-status-pin--arrow.png'),
                       'htmloption' =>  array('title' => 'к поступлению'),
           );
        }
        if($APTI->status == self::$APP_ENTERED_OF_1C){
            if($APTI->status_conversion == self::$APP_ENTERED_OF_1C_part){
                $array =  array(
                           'src' =>  CHtml::image(Yii::getPathOfAlias('link_images').'/megapolis-status-pin--exclamation.png'),
                           'htmloption' =>  array('title' => 'поступил частично'),
                );
            }elseif($APTI->status_conversion == self::$APP_ENTERED_OF_1C_full){
                $array =  array(
                           'src' =>  CHtml::image(Yii::getPathOfAlias('link_images').'/megapolis-status-pin--plus.png'),
                           'htmloption' =>  array('title' => 'поступил полностью'),
                );
            }else{
                $array =  array(
                           'src' =>  CHtml::image(Yii::getPathOfAlias('link_images').'/megapolis-status-pin.png'),
                           'htmloption' =>  array('title' => ' к поступлению'),
                );
            }
            
        }
        return CHtml::link($array['src'], '' ,$array['htmloption']);
    }
    public static function date_delivery($data){
        $id_app = $data['id'];
        
        $criteria = new CDbCriteria();
        $criteria->compare('id_idapplication', $id_app);
        
        $APTI = APTI::model()->user()->find($criteria);
        return $APTI->status ? date('Y-m-d', strtotime($APTI->date_shipping)) : '';
    }
    public function search(){
        $criteria = new CDbCriteria();
        $criteria->scopes = array('user', 'status_ap1c');
        
        $pagination = new CPagination();
        $pagination->pageVar = 'page';
        $pagination->pageSize = 10;
        
        $sort = new CSort();
        $sort->sortVar = 'sort';
        $sort->attributes = array( 'shipping' => array('shipping'),
            'id' => array('id'),
            );
        
        $apti = self::model()->with('APH1')->findAll($criteria);
        $stack = array();
        # Подсчитываем количество отправлений
        if(true){
            
          
            foreach($apti as $k=>$v){
                if($v->APH1){
                    foreach($v->APH1 as $k2=>$v2){
                        if(!isset($stack[$v2->id_idapplication])){
                            $stack[$v2->id_idapplication] = array();
                        }
                        $stack[$v2->id_idapplication][$v2->id_storage]['total'] = $v2->number_fact;
                        $stack[$v2->id_idapplication][$v2->id_storage]['woVAT'] = $v2->woVAT;
                        $stack[$v2->id_idapplication][$v2->id_storage]['wiVAT'] = $v2->wiVAT;
                        $stack[$v2->id_idapplication][$v2->id_storage]['shipping'] = $v->date_shipping_fact;
                    }
                }
            }
        }
       
        # Пересчитываем товар
        if(true){
            $storage = function($array){
                $a=1;
                $push = array();
                foreach($array as $k=>$v){
                    foreach($v as $k2=>$v2){
                        $store = ASDB::model()->findByPk($k2);
                        $push[$a]['id'] = $a;
                        $push[$a]['application'] = $k;
                        $push[$a]['artical'] = $store->artical;
                        $push[$a]['nomencl'] = $store->nomencl;
                        $push[$a]['unit']    = $store->unit;
                        $push[$a]['note']    = $store->note;
                        $push[$a]['total'] = $v2['total'];
                        $push[$a]['wiVAT'] = $v2['wiVAT'];
                        $push[$a]['woVAT'] = $v2['woVAT'];
                        $push[$a]['shipping'] = $v2['shipping'];
                        $a++;
                    }
                }
                return $push;
            };
            $array = $storage($stack);
        }
        
        return new CArrayDataProvider($array, array(
            'pagination'=> $pagination,
            'sort' => $sort,
        ));
    }
    public function viewAction($data){
        $id = $data['id'];
        $array =  array(
           'src' =>  CHtml::image(Yii::getPathOfAlias('link_images').'/megapolis-add.png'),
           'htmloption' =>  array('title' => 'добавить в заявку', 'class'=>'button'),
        );
        
        return CHtml::link($array['src'], '' ,$array['htmloption']);
    }
    public function scopes()
    {
        return array(
            'user'=>array(
                'condition'=> 'id_clients = '.Yii::app()->user->auth->id,
            ),
             'status_ap1c'=>array(
                'condition'=> 'status = '.self::$APP_ENTERED_OF_1C,
            ),
        );
    }
}