<?php

class RCDB extends CActiveRecord{
    public static $RC_STATUS_REQUEST_OPEN     = 0; # CREATE
    public static $RC_STATUS_REQUEST_MODIFIED = 2; # UPDATE
    public static $RC_STATUS_REQUEST_DELETE   = 3; # DELETE
    public static $RC_STATUS_REQUEST_LOCK     = 5; # LOCK   Добавлена 28.10.14
    
    private static $tableName = 'requestсarrier';
    public $form;
    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){return self::$tableName;}
    public function rules()
    {
        return array(
            array('dateApp, timeStart, timeEnd, addressOfLoad, contactName, phone, shouldBeSkipped, note, occupancy, statusRequest', 'safe'),
        );
    }
    # Возможность редактировать заявки
    public function afterFind () {
        $this->dateApp = date('d-m-Y H:i', strtotime($this->dateApp));
        $this->timeStart = date('d-m-Y H:i', strtotime($this->timeStart));
        $this->timeEnd = date('d-m-Y H:i', strtotime($this->timeEnd));
    }
    public function beforeSave() {
        if(parent::beforeSave() === false) return false; # Ошибка
        if(isset($this->statusRequest)){
            if(!self::oppMode($this)){
                $this->addError('date', 'Пожалуйста измените дату на более поздний срок!');
                return false;

            }
        }else{
            return false;
        }

        $this->dateApp = date('Y-m-d H:i:s', strtotime($this->dateApp));
        $this->timeStart = date('Y-m-d H:i:s', strtotime($this->timeStart));
        $this->timeEnd = date('Y-m-d H:i:s', strtotime($this->timeEnd));

        return true;
    }
    public static function oppMode(RCDB $object){
        $time = time();
        $date_xxx = strtotime($object->timeStart);
        $themDay = date('Hi');
        $date_xxx-= ($themDay > 1630) ? 3600 * 24 : 0;
        $obj_id = self::model()->findByPk($object->id);

        if($object->statusRequest === RCDB::$RC_STATUS_REQUEST_OPEN){
            if($object->occupancy >= 5){
                if($time > ($date_xxx + 3600*9 - 2*24*3600)){
                    return false;
                }
            }else{

                if($time > ($date_xxx - 3600*15)){
                    return false;
                }
            }
        }elseif($object->statusRequest === RCDB::$RC_STATUS_REQUEST_DELETE){
            if($obj_id->statusRequest == self::$RC_STATUS_REQUEST_LOCK){
                return false;
            }
            $object->statusRequest = self::$RC_STATUS_REQUEST_DELETE;

        }elseif($object->statusRequest === RCDB::$RC_STATUS_REQUEST_MODIFIED){
            if($object->occupancy >= 5){
                if($time > ($date_xxx + 3600*9 - 2*24*3600)){
                    return false;
                }
            }else{
                if($time > ($date_xxx - 3600*15)){
                    return false;
                }
            }
        }elseif($object->statusRequest === RCDB::$RC_STATUS_REQUEST_LOCK){
            if($object->occupancy >= 5){
                if($time > ($date_xxx + 3600*9 - 2*24*3600)){
                    return false;
                }
            }else{
                if($time > ($date_xxx - 3600*15)){
                    return false;
                }
            }
        }
        return true;
    }
    public function scopes()
    {
        return array(
            'user'=>array(
                'condition'=> 'client = '.Yii::app()->user->auth->id,
            ),
        );
    }
}