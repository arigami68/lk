<?php

class PRTI extends CActiveRecord{
    public static $OPERATION_SHOW = 'show';
    public static $OPERATION_EDIT = 'edit';
    public static $OPERATION_DELETE = 'delete';
    private static $tableName = 'prod_time';
    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){return self::$tableName;}
    public function relations()
    {
        return array(
            'PRHS'=>array(self::HAS_MANY, 'PRHS', array('idapplication' => 'id_idapplication')),
        );
    }
    public function search(){
        $criteria = new CDbCriteria();
        $pagination = new CPagination();
        $sort = new CSort();
        $criteria->scopes = array('user');
        $pagination->pageVar = 'page';
        $pagination->pageSize = 10;
        $sort->sortVar = 'sort';
        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
            'pagination'=> $pagination,
            'sort' => $sort,
        ));
    }
    
    public static function status($data){
        $array['s'] =  array(
                   'src' =>  CHtml::image(Yii::getPathOfAlias('link_images').'/megapolis-status-pin--arrow.png'),
                   'htmloption' =>  array('title' => 'просмотр'),
                   'link' => '?action=operation&operation=show&id='.$data->id_idapplication,
        );
        $array['r'] =  array(
                   'src' =>  CHtml::image(Yii::getPathOfAlias('link_images').'/megapolis-status-pin--minus.png'),
                   'htmloption' =>  array('title' => 'удалить'),
                   'link' => '?action=operation&operation=delete&id='.$data->id_idapplication,
        );   
        $array['e'] =  array(
                   'src' =>  CHtml::image(Yii::getPathOfAlias('link_images').'/megapolis-status-pin--pencil.png'),
                   'htmloption' =>  array('title' => 'редактировать'),
                   'link' => '?action=operation&operation=edit&id='.$data->id_idapplication,
        );
        $array['m'] =  array(
            'src' =>  CHtml::image(Yii::getPathOfAlias('link_images').'/megapolis-edit-status.png'),
            'htmloption' =>  array('title' => 'Изменения статуса'),
            'link' => '/com?action=status&id='.$data->id_idapplication,
            
        );
            
        return (
               $data->status ? CHtml::link($array['s']['src'], $array['s']['link'] ,$array['s']['htmloption']) :
               CHtml::link($array['s']['src'], $array['s']['link'] ,$array['s']['htmloption']).' '.
               CHtml::link($array['e']['src'], $array['e']['link'] ,$array['e']['htmloption']).' '.
               CHtml::link($array['r']['src'], $array['r']['link'] ,$array['r']['htmloption']).' '.
               CHtml::link($array['m']['src'], $array['m']['link'] ,$array['m']['htmloption'])
               );
    }
    # Планируемая дата поступления
    public static function date_delivery($data){
        $date = $data->date_shipping;
        
        return $date ? $date : NULL;
    }
    public function application($id)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition'=> 'id_idapplication= '.$id,
        ));
        return $this;
    }
    
    public function scopes()
    {
        return array(
            'user'=>array( 'condition'=> 'id_clients = '.Yii::app()->user->auth->id),
        );
    }
}