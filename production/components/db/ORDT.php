<?php

class ORDT extends CActiveRecord{
    private static $tableName = 'orderstest';
    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){return self::$tableName;}
    public function search($id){
        $ORDT = new stdClass();
        $ORDT->db =  new stdClass();
        $criteria = new CDbCriteria();
        $criteria->scopes = array('user');
        $ORDT->array = new stdClass();
        $pagination = new CPagination();
        $pagination->pageVar = 'page';
        $pagination->pageSize = 20;
        $sort = new CSort();
        $sort->sortVar = 'sort';
        $sort->attributes = array( 'shipping' => array('shipping'),
            'id' => array('id'),
        );
        
        $ORDT->db->ORDT = ORDT::model()->findAll($criteria);
        
        if($ORDT->db->ORDT){
            $db = function($ORDT){
                $array = new stdClass();

                foreach($ORDT as $k=>$v){
                    
                    $megapolis = $v->megapolis;
                    $array->orders["$megapolis"]["registry"] = $v->registry;
                    $array->orders["$megapolis"]["dateCreate"] = $v->dateCreate;
                    $array->orders["$megapolis"]["np"] = $v->costDelivery;
                    $array->orders["$megapolis"]["address"] = $v->address;
                    $array->megapolis[] = $v->megapolis;
                }  
                return $array;
            };
            $ORDT->array->ORDT = $db($ORDT->db->ORDT);
            $process = function($ORDT){
                $array = Array();
                $i=1;
                foreach($ORDT->orders as $k=>$v){
                    $array[$i]["id"] = $i;
                    $array[$i]["megapolis"] = $k;
                    $array[$i]["dateCreate"] = date('Y-m-d H:i', $ORDT->orders[$k]['dateCreate']);
                    $array[$i]["registry"] = $ORDT->orders[$k]['registry'];
                    $array[$i]["np"] = $ORDT->orders[$k]['np'];
                    $array[$i]["address"] = $ORDT->orders[$k]['address'];
                    $array[$i]["steps"] = '';
                    $i++;
                }  
                return $array;
            };
            
            $array = $process($ORDT->array->ORDT);
        }else{
            $array = Array();
        }
        return new CArrayDataProvider($array, array(
            'pagination'=> $pagination,
            'sort' => $sort,
        ));
    }
    public function relations()
    {
        return array(
            'track'=>array(self::BELONGS_TO, 'TRA2', 'id'),
            
        );
    }
    public function scopes()
    {
        return array(
            'user'=>array(
                'condition'=> 'idClient = '.Yii::app()->user->auth->id,
            ),
        );
    }
}