<?php

class TRA2 extends CActiveRecord{
    private static $tableName = 'track2';
    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){return self::$tableName;}
    public static function processing(TRA2 $track){
        $pro = new stdClass();
        
        $pro->com['name'] = Yii::import('application.components.component.TrackComponent');
        $pro->com['track'] = new $pro->com['name'];
        
        $pro->com['track']->id = $track->id_order;
        $pro->com['track']->done = $track->done;

        $pro->result = $pro->com['track']->setData($track->data);
        return $pro->result;
    }
}