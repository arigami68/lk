<?php

class RCDB extends CActiveRecord{
    public static $RC_STATUS_REQUEST_OPEN     = 0; # CREATE
    public static $RC_STATUS_REQUEST_MODIFIED = 2; # UPDATE
    public static $RC_STATUS_REQUEST_DELETE   = 3; # DELETE
    public static $RC_STATUS_REQUEST_LOCK     = 5; # LOCK   Добавлена 28.10.14
    
    private static $tableName = 'request_carrier';
    public $form;
    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){return self::$tableName;}
    public function rules()
    {
        return array(
            array('date_app, time_start, time_end, address_of_load, contact_name, phone, should_be_skipped, note, occupancy, status_request', 'safe'),
        );
    }
    # Возможность редактировать заявки
    public function afterFind () {
        $this->date_app = date('d-m-Y H:i', strtotime($this->date_app));
        $this->time_start = date('d-m-Y H:i', strtotime($this->time_start));
        $this->time_end = date('d-m-Y H:i', strtotime($this->time_end));
    }
    public function beforeSave() {
        if(parent::beforeSave() === false) return false; # Ошибка
        if(isset($this->status_request)){
            if(!self::oppMode($this)){
                $this->addError('date', 'Пожалуйста измените дату на более поздний срок!');
                return false;

            }
        }else{
            return false;
        }

        $this->date_app = date('Y-m-d H:i:s', strtotime($this->date_app));
        $this->time_start = date('Y-m-d H:i:s', strtotime($this->time_start));
        $this->time_end = date('Y-m-d H:i:s', strtotime($this->time_end));

        return true;
    }
    public static function oppMode(RCDB $object){
        $time = time();
        $date_xxx = strtotime($object->time_start);
        $themDay = date('Hi');
        $date_xxx-= ($themDay > 1630) ? 3600 * 24 : 0;
        $obj_id = self::model()->findByPk($object->id);

        if($object->status_request === RCDB::$RC_STATUS_REQUEST_OPEN){
            if($object->occupancy >= 5){
                if($time > ($date_xxx + 3600*9 - 2*24*3600)){
                    return false;
                }
            }else{
                if($time > ($date_xxx - 3600*15)){
                    return false;
                }
            }
        }elseif($object->status_request === RCDB::$RC_STATUS_REQUEST_DELETE){
            if($obj_id->status_request == self::$RC_STATUS_REQUEST_LOCK){
                return false;
            }
            $object->status_request = self::$RC_STATUS_REQUEST_DELETE;

        }elseif($object->status_request === RCDB::$RC_STATUS_REQUEST_MODIFIED){
            if($object->occupancy >= 5){
                if($time > ($date_xxx + 3600*9 - 2*24*3600)){
                    return false;
                }
            }else{
                if($time > ($date_xxx - 3600*15)){
                    return false;
                }
            }
        }elseif($object->status_request === RCDB::$RC_STATUS_REQUEST_LOCK){
            if($object->occupancy >= 5){
                if($time > ($date_xxx + 3600*9 - 2*24*3600)){
                    return false;
                }
            }else{
                if($time > ($date_xxx - 3600*15)){
                    return false;
                }
            }
        }
        return true;
    }
    public function scopes()
    {
        return array(
            'user'=>array(
                'condition'=> 'client = '.Yii::app()->user->auth->id,
            ),
        );
    }
}