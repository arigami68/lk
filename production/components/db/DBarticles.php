<?php

class DBarticles extends CActiveRecord{
    private static $tableName = 'articles';
    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){return self::$tableName;}


    public function dataProvider(){
        $criteria   = new CDbCriteria();
        $pagination = new CPagination();
        $sort       = new CSort();

        $pagination->pageVar = 'page';
        $pagination->pageSize = 10;
        $criteria->limit = 0;

        $sort->sortVar = 'sort';

        return new CArrayDataProvider(array(), array(
            'pagination'=> $pagination,
            'sort' => $sort,
        ));
    }
    public function idOrder($id)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition'=> 'idOrder = '.$id,
        ));
        return $this;
    }
}