<?php
class ORDS extends CActiveRecord{
    public static $OPERATION_CREATE = 'create';
    public static $OPERATION_READ   = 'read';
    public static $OPERATION_UPDATE = 'update';
    public static $OPERATION_DELETE = 'delete';
    
    public static $STATUS_CREATE = 0;
    public static $STATUS_UPDATE = 2;
    public static $STATUS_DELETE = 4;

    public static $SEARCH_RETURNS   = 1; # отчет - по возвратам
    public static $SEARCH_AGENT     = 2; # отчет - по присвоеным номерам
    public static $SEARCH_STATUS    = 3; # отчет - по статусам отправлений
    public static $SEARCH_REGISTRY  = 4; # отчет - по реестрам
    public static $SEARCH_DEPARTURE = 5; # отчет - в рамках реестра
    public static $SEARCH_AGENTS    = 6; # отчет - по отчетам на складе

    public static $autoinc    = 1;
    private static $tableName = 'orders';

    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){return self::$tableName;}
    public function beforeFind(){
        $this->getDbCriteria()->mergeWith(array(
            'condition'=> ' (id1s IS NULL) OR (id1s <> '.self::$STATUS_DELETE.')',
        ));

        return $this;
    }
    public function QuSe($find, $count = NULL, $id = NULL, $criteria = NULL){
        $criteria = empty($criteria) ?  new CDbCriteria() : $criteria;
        $pagination = new MegapolisPagination();

        $sort = new CSort();
        if(true){ # CDBcriteria
            if($find == self::$SEARCH_RETURNS){
                $criteria->scopes = array('user');
            }elseif($find == self::$SEARCH_AGENT){
                $criteria->addCondition('agent != ""');
                $criteria->scopes = array('user');
            }elseif($find == self::$SEARCH_STATUS){
                $criteria->scopes = array('user');
                $criteria->addCondition('agent != ""');
            }elseif($find == self::$SEARCH_REGISTRY){
                $criteria->mergeWith(array('condition'=> ' (id1s IS NULL) OR (id1s <> '.self::$STATUS_DELETE.')'));
                $criteria->scopes = array('user');
            }elseif($find == self::$SEARCH_DEPARTURE){
                $criteria->compare('registry', $id);
                $criteria->scopes = array('user');
                $pagination->pageVar = 'page';
                $pagination->pageSize = 1000;
                $sort->sortVar = 'sort';
            }elseif($find == self::$SEARCH_AGENTS){
                $criteria->scopes = array('user');
            }
        }


        if($find == self::$SEARCH_RETURNS){
            return HelpSearchWidget::ORDS_return($criteria, $pagination, $sort, $this);
        }elseif($find == self::$SEARCH_AGENT){
            return HelpSearchWidget::ORDS_agent($criteria, $pagination, $sort, $this);
        }elseif($find == self::$SEARCH_STATUS){
            return HelpSearchWidget::reORDS_status($criteria, $pagination, $sort, $this);
        }elseif($find == self::$SEARCH_REGISTRY){
            return HelpSearchWidget::ORDS_registry($criteria, $pagination, $sort, $this, $count);
        }elseif($find == self::$SEARCH_DEPARTURE){
            return HelpSearchWidget::ORDS_departure($criteria, $pagination, $sort, $this, $id);
        }elseif($find == self::$SEARCH_AGENTS){
            return HelpSearchWidget::ORDS_agents($criteria, $pagination, $sort, $this);
        }
    }

    public function relations()
    {
        return array(
            'track'=>array(self::BELONGS_TO, 'TRA2', 'id'),
            'unique'=>array(self::BELONGS_TO, 'COUN', array('id' => 'orders')),
        );
    }
    public function registry($id)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition'=> 'registry = "'.$id.'"',
        ));
        return $this;
    }
    public function megapolis($id)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition'=> 'megapolis = "'.$id.'"',
        ));
        return $this;
    }
	public function agent($id)
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition'=> 'agent = "'.$id.'"',
		));
		return $this;
	}
    public function scopes()
    {
        return array(
            'user'=>array(
                'condition'=> 'idClient = '.Yii::app()->user->auth->id,
            ),
        );
    }
}


