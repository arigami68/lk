<?php

class CLIT extends CActiveRecord{
    private static $tableName = 'clients';
    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){return self::$tableName;}
    public function contract($id)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition'=> 'contract= "'.$id.'"',
        ));
        return $this;
    }
    public function mail($id)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition'=> 'email= "'.$id.'"',
        ));
        return $this;
    }
    public function secret($id)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition'=> 'pass= "'.$id.'"',
        ));
        return $this;
    }
    public function relations()
    {
        return array(
            'role'=>array(self::BELONGS_TO, 'ROLE', 'contract'),
            'clientrole'=>array(self::HAS_ONE,'RoleClients',array('role'=>'id'),'through'=>'role'),
        );
    }
}