<?php

class COME extends CActiveRecord{
    public static $OPERATION_CARGO    = 1;
    public static $OPERATION_REGISTRY = 2;
    
    private static $tableName = 'contractor_megapolis';
    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){return self::$tableName;}
    public function step($id)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition'=> 'step = "'.$id.'"',
        ));
        return $this;
    }
    public function operation($id)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition'=> 'operation = "'.$id.'"',
        ));
        return $this;
    }
    public function scopes()
    {
        return array(
            'user'=>array(
                'condition'=> 'user = '.Yii::app()->user->auth->id,
            ),
        );
    }
}