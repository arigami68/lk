<?php

class DETA extends CActiveRecord{
    private static $tableName = 'details';
    public $fullname, $shortname, $orgform, $addresslegal, $addressfact, $inn, $kpp, $okpo, $calculatedinvoice, $correspondentinvoice, $bik, $fiohead, $fioaccountant, $addressdocuments, $tel;
    public $okogy, $okato, $okbed, $okfc, $ogph, $namebank, $positionhead, $fax, $mail_np, $mail_notice, $www;

    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){return self::$tableName;}
    public function user($id)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition'=> 'user= '.$id,
        ));
        return $this;
    }
    public function rules()
    {
        return array(
            array('fullname, shortname, orgform, addresslegal, addressfact, inn, kpp, okpo, calculatedinvoice, correspondentinvoice, bik, fiohead, fioaccountant, addressdocuments, tel, okogy, okato, okbed, okfc, ogph, namebank, positionhead, fax, mail_np, mail_notice, www', 'safe'),
        );
    }
}