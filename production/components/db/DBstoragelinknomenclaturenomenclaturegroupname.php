<?php

class DBstoragelinknomenclaturenomenclaturegroupname extends CActiveRecord{
	private static $tableName = 'storagelinknomenclaturenomenclaturegroupname';
	public static function model($className= __CLASS__){return parent::model($className);}
	public function tableName(){return self::$tableName;}
	public function nomenclaturegroup($id){
		$this->getDbCriteria()->mergeWith(array(
			'condition'=> 'nomenclaturegroupnameID = '.$id,
		));
		return $this;
	}
	public function relations(){
		return array(
			'nomenclature'=>array(self::BELONGS_TO, 'DBnomenclature', array('nomenclatureID'=> 'id')),
		);
	}
}