<?php

class PhpAuthManager extends CPhpAuthManager{
    public static $contract = NULL;
    public $roles;
    public function init(){

        // Иерархию ролей расположим в файле auth.php в директории config приложения
        if($this->authFile===null){
            $this->authFile=Yii::getPathOfAlias('application.config.auth').'.php';
        }
        parent::init();
        // Для гостей у нас и так роль по умолчанию guest.

        if(!Yii::app()->user->isGuest){
            $this->getContract();
            if(!$this->getContract()) return Yii::app()->user->logout(); # Нет пользователя в БАЗЕ
            $this->assign(Yii::app()->user->role, Yii::app()->user->id);
            $this->roles = Yii::app()->user->role;

            Yii::app()->user->auth = $this->getAuth(AuthComponent::get());
        }
    }
    # @info: Номер договора
    public function getContract(){
        
        $id = Yii::app()->user->id;
        if(!static::$contract){
            if(!static::$contract = CLIT::model()->findByPk($id, array('select' => '*'))){
                static::$contract = TMP_CLIT::model()->findByPk($id, array('select' => '*'));
            }
        }
        
        return static::$contract;
    }
    public function getAuth(AuthComponent $object){
        $object::get()->db = $this->getContract();
        $object::get()->id = $this->getContract()->id;
        $object::get()->contract = $this->getContract()->contract;
        $object::get()->test = $this->getContract()->testregim ? 1 : 0;
        $object::get()->name = $this->getContract()->name;
        $object::get()->comment = $this->getContract()->comment;
        $object::get()->mail = $this->getContract()->email;
        $object::get()->range = array(
                'registry' => $this->getContract()->registry,
                'cargo'    => $this->getContract()->num,
        );
        $object::get()->secretkey = $this->getContract()->secretKey;
        
        return $object::get();
    }
}