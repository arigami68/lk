<?php

class MegapolisFunction{
    public static $API_QUERY = 'query';

    public static $instance;
    public static function get(){ return empty(self::$instance) ? self::$instance = new self() : self::$instance;}

    /*
     * Отдаем полный адрес.
     * INPUT: регион, область и город.
     */
    public function implodeAddress($region, $area, $city, $ds = ' '){
        if($region == '-') $region = '';
        return $region.$ds.$area.$ds.$city;
    }
    public function api($secret, $count, $get = 'view', $option = 1){
        $get = self::$API_QUERY.'='.(($get == 'view') ? 'view' : 'get');
        $option = 'option='.$option;
        #$url_numbers_api = MEGAPOLIS_API.'?count='.$count.'&'.$get.'&'.$option.'&'.'secret='.$secret;
        $url_numbers_api = 'http://api.hydra.megapolis.loc/togetnum'.'?count='.$count.'&'.$get.'&'.$option.'&'.'secret='.$secret;
        $answer_numbers = file_get_contents( $url_numbers_api);
        # Если пришла xml, смотрим есть ли разрешенный данный номер - пока что забираем. Далее когда будем сохранять форму регистрируем его
        $xml = self::mget($answer_numbers);
        foreach($xml->data->text as $k=>$v){
            $array[] = (string)$v;
        }
        return $array;
    }
    public static function mget($file, $xpath = ''){
        $dom = new DomDocument();
        $dom->loadXML($file);
        $file = simplexml_import_dom($dom);
        return $file;
    }
}