<?php
///var/www/lk/extensions/php-excel/Classes#
class MegapolisExcel{
    public static $FLAG_PART = 2;
    public static $instance;
    public static function get(){ return empty(self::$instance) ? self::$instance = new self() : self::$instance;}
    /*
     * чтение EXCEL файла
     */
    public function read($file, $flag = 1){

        spl_autoload_unregister(array('YiiBase', 'autoload'));
        $phpExcelPath = Yii::getPathOfAlias('extensions.php-excel.Classes');

        include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');

        spl_autoload_register(array('YiiBase', 'autoload'));
        $objReader = PHPExcel_IOFactory::createReader('Excel2007'); # 'Excel2007'

        $objReader->setLoadAllSheets();
        $objPHPExcel = PHPExcel_IOFactory::load($file->tempName);

        if($flag == self::$FLAG_PART){
            $sheetData = $objPHPExcel;
        }else{
            $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
        }
        return $sheetData;
    }

    public function write(){
        spl_autoload_unregister(array('YiiBase', 'autoload'));
        $phpExcelPath = Yii::getPathOfAlias('extensions.php-excel.Classes');
        include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');
        spl_autoload_register(array('YiiBase', 'autoload'));

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator("user")
            ->setLastModifiedBy("ИдеаЛоджик")
            ->setTitle("ИдеаЛоджик")
            ->setSubject("Отчет об отправлениях")
            ->setDescription("Отчет")
            ->setKeywords("repost")
            ->setCategory("report");
        return $objPHPExcel;
    }
    public function output($objPHPExcel){

        $objPHPExcel = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");;
        header("Content-Disposition: attachment;filename=ideaLogic ".date('d-m-Y H:i').".xls");
        header("Content-Transfer-Encoding: binary ");
        return $objPHPExcel->save('php://output');
    }

}