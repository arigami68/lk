<?php

class FuncMegapolis{
     # Разбор XML
    public static $API_MEGAPOLIS_NUMBER_REPEATED = 128;
    public static $API_MEGAPOLIS_NUMBER_TRUE = 129;
    public static $API_MEGAPOLIS_NUMBER_FALSE = 130;
    public static $REGO = 'http://api.hydra.megapolis.loc/api/api/REGO';

    /*
    public static function REGO($secret, $operation = 'get'){
        $operation = 'operation'.$operation;
        $secret = 'secret='.$secret;
        
        print(self::$REGO.$secret.$operation);
        exit;
    }
    */
    
    public static function getXml($file, $xpath = ''){
        $dom = new DomDocument();
        $dom->loadXML($file);
        $file = simplexml_import_dom($dom);
        return $file;
    }
    public static function pushXml($file, $url, $get=''){ # @info - отправляем по API Мегаполиса
        ini_set("max_execution_time", 60*60*5);
        
        $get = '?'.$get;
        $ch = curl_init();
        /*
        header("Content-type: text/xml");
        print_r($file);
        exit;
         
         * 
         */
        curl_setopt($ch, CURLOPT_URL, $url.$get);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $file);
        $result = curl_exec ($ch);
        print_r($result);
		exit;
        curl_close ($ch);
        return $result;
    }
    public static function Handbook(){
        $att = CLOD::model()->findAll();
        $agent_id = array();
        foreach($att as $k=>$v){
            $agent_id[trim($v->od)] = $v->id;
        }
        $book = new StdClass();
        $book->agent_id = $agent_id;

        $type = CATY::model()->findAll();
        foreach($type as $value){ $book->op_type[$value->type] = $value->id;}

        $book->op_category = array(
            'Заказное(ая)' => 1,
            'Простое' => 2,
            'С наложенным платежом' => 3,
            'С объявленной ценностью' => 4,
            'Со страхованием' => 5,
        );
        $book->op_notification = array(
            'Заказное' => 1,
            'Простое'  => 2,
        );
        $book->op_take = array(
            'В окне приема' => 1,
            'Курьером'  => 2,
        );
        $book->op_inventory = array(
            'Да' => 1,
            'Нет'  => 0,
        );
        $book-> op_fragile = array(
            'Да' => 1,
            'Нет'  => 0,
        );

        $book->default_address = array(
            1 =>  'г. Москва, ул. Котляковская д. 3, стр.1',
            2  => 'г. Москва, ул. Ткацкая, д. 5, стр. 5',
        );
        return $book;
    }
    public static function ParseXML($file){
        $doc = simplexml_load_string($file);
        $error = '';
        
        if((int)$doc->error == 0) return $error = array((string)$doc->orders->order->megapolis => 'success');
        
        if($message = $doc->messages->message){
            
            if((string)$message == 'не указан номер договора'){
                $error['::']['error'][]= (string)$message;
                return $error;
            }
            
            foreach($doc->messages->message as $k=>$text){
                $text = (string)$text;
                if(stristr($text, 'ошибки валидации документа, ни одного заказа не импортировано')) continue;
                
                preg_match('|заказ "(\S+)"|', $text, $match);
                if(!isset($match[1])) continue;
                $code = $match[1];
                
                if(stristr($text, 'такой заказ уже есть в БД') !== FALSE){
                    return $error = array($code => 'info');
                }

                $error[$code]['error'][]= $text;
            }
            return $error;
        }
        if((int)$doc->error != 0) $error['error'] = 'Ошибка загрузки документа<br>';
        return 0;
    }
    public static function Xml($secret, $count, $get = 'view', $option = 1, $url = 'http://megapolis-old.idea-logic.ru'){ #MEGAPOLIS_API){
        $get = 'query='.(($get == 'view') ? 'view' : 'get');
        $option = 'option='.$option;
        $url_numbers_api = $url.'/togetnum?count='.$count.'&'.$get.'&'.$option.'&'.'secret='.$secret;

        $answer_numbers = file_get_contents( $url_numbers_api);

        # Если пришла xml, смотрим есть ли разрешенный данный номер - пока что забираем. Далее когда будем сохранять форму регистрируем его
        $xml = self::get($answer_numbers);
        foreach($xml->data->text as $k=>$v){
            $array[] = (string)$v;
        }

        return $array;
    }
    public static function get($file, $xpath = ''){
        $dom = new DomDocument();
        $dom->loadXML($file);
        $file = simplexml_import_dom($dom);
        return $file;
    }

    /**
     * Возвращает сумму прописью
     * @author runcore
     * @uses morph(...)
     */
    function num2str($num) {
        $nul='ноль';
        $ten=array(
            array('','один','два','три','четыре','пять','шесть','семь', 'восемь','девять'),
            array('','одна','две','три','четыре','пять','шесть','семь', 'восемь','девять'),
        );
        $a20=array('десять','одиннадцать','двенадцать','тринадцать','четырнадцать' ,'пятнадцать','шестнадцать','семнадцать','восемнадцать','девятнадцать');
        $tens=array(2=>'двадцать','тридцать','сорок','пятьдесят','шестьдесят','семьдесят' ,'восемьдесят','девяносто');
        $hundred=array('','сто','двести','триста','четыреста','пятьсот','шестьсот', 'семьсот','восемьсот','девятьсот');
        $unit=array( // Units
            array('копейка' ,'копейки' ,'копеек',	 1),
            array('рубль'   ,'рубля'   ,'рублей'    ,0),
            array('тысяча'  ,'тысячи'  ,'тысяч'     ,1),
            array('миллион' ,'миллиона','миллионов' ,0),
            array('миллиард','милиарда','миллиардов',0),
        );

        $morph = function($n, $f1, $f2, $f5){
            $n = abs(intval($n)) % 100;
            if ($n>10 && $n<20) return $f5;
            $n = $n % 10;
            if ($n>1 && $n<5) return $f2;
            if ($n==1) return $f1;
            return $f5;
        };

        //
        list($rub,$kop) = explode('.',sprintf("%015.2f", floatval($num)));
        $out = array();
        if (intval($rub)>0) {
            foreach(str_split($rub,3) as $uk=>$v) { // by 3 symbols
                if (!intval($v)) continue;
                $uk = sizeof($unit)-$uk-1; // unit key
                $gender = $unit[$uk][3];
                list($i1,$i2,$i3) = array_map('intval',str_split($v,1));
                // mega-logic
                $out[] = $hundred[$i1]; # 1xx-9xx
                if ($i2>1) $out[]= $tens[$i2].' '.$ten[$gender][$i3]; # 20-99
                else $out[]= $i2>0 ? $a20[$i3] : $ten[$gender][$i3]; # 10-19 | 1-9
                // units without rub & kop
                if ($uk>1) $out[]= $morph($v,$unit[$uk][0],$unit[$uk][1],$unit[$uk][2]);
            } //foreach
        }
        else $out[] = $nul;
        $out[] = $morph(intval($rub), $unit[1][0],$unit[1][1],$unit[1][2]); // rub
        $out[] = $kop.' '.$morph($kop,$unit[0][0],$unit[0][1],$unit[0][2]); // kop
        return trim(preg_replace('/ {2,}/', ' ', join(' ',$out)));
    }
// Название месяца по метке UNIX
    public static function  getMonthName($unixTimeStamp){
        $monthAr = array(
            1 => array('Январь', 'Января'),
            2 => array('Февраль', 'Февраля'),
            3 => array('Март', 'Марта'),
            4 => array('Апрель', 'Апреля'),
            5 => array('Май', 'Мая'),
            6 => array('Июнь', 'Июня'),
            7 => array('Июль', 'Июля'),
            8 => array('Август', 'Августа'),
            9 => array('Сентябрь', 'Сентября'),
            10=> array('Октябрь', 'Октября'),
            11=> array('Ноябрь', 'Ноября'),
            12=> array('Декабрь', 'Декабря')
        );
        return $monthAr[$unixTimeStamp][1];
    }


}
