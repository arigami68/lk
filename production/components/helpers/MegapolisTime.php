<?php

/**
 * Class MegapolisTime :: для работы со временем
 *
 */
class MegapolisTime{
    public static $TIME_YEAR            = 'Y-m-d';
    public static $TIME_YEAR_FULL       = 'Y-m-d H:i:s';
    public static $TIME_DAY             = 'd-m-Y';
    public static $TIME_DEFAULT         = 'd-m-Y';
    public static $TIME_DEFAULT_FULL    = 'd-m-Y H:i:s';
    public static $instance;

    public static function get(){ return empty(self::$instance) ? self::$instance = new self() : self::$instance;}
    /*
     * Конвертация времени.
     */
    public function timeToConvert($in, $to, DateTime $date){

        if($in == self::$TIME_YEAR){
            if($to == self::$TIME_DEFAULT OR $to == self::$TIME_DEFAULT_FULL){
                $time = date_format($date, $to);
            };
        }elseif($in == self::$TIME_DAY){
            if($to == self::$TIME_YEAR){
                $time = date_format($date, $to);
            };
        }

        return $time;
    }
}