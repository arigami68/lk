<?php

class FpsBehavior extends CActiveRecordBehavior{
    public function getWarehouseCriteria($client){
        
        $criteria = new CdbCriteria();
        $criteria->select = "id, keeper, nomencl, artical, implprice, amount, coder";
        $criteria->compare('user', $client);
        
        return $criteria;
    }
    public function paymentgoods($array){
        foreach($array as $k=>$v){
            $keeper = $v['keeper'];
            $nomencl = $v['nomencl'];
            $artical = $v['artical'];
            $implprice = $v['implprice'];
            $amount = $v['amount'];
            $coder = $v['coder'];
            $id = $v['id'];

            isset($r[$coder]) ? FALSE :  $r[$coder] = new FpsComponent();
            
            $r[$coder]->setId($id);
            $r[$coder]->setKeeper($keeper);
            $r[$coder]->setArtical($artical);
            $r[$coder]->setNomencl($nomencl);
            $r[$coder]->setImplprice($implprice);
            $r[$coder]->setAmount($amount);
            $r[$coder]->setCost();
            
        }

        return $r;
        
    }
}