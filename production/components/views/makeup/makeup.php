<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'createOrder',
    'type'=>'horizontal',
));
?>    
<?php $this->widget('bootstrap.widgets.TbGridView', array(
        'type'=>'striped bordered condensed',
        'dataProvider'=> $model->QuSe($model::$SEARCH_RETURNS),
        'template'=>"{items}\n{pager}",
        'ajaxVar' => '',
        'columns'=>array(
            array(
                'name'=>'id',
                'header' => '№ заявки',
            ),
            array(
                'name'=>'megapolis',
                'header' => 'Номер мегаполис',
            ),
            array(
                'name'=>'np',
                'header' => 'НП',
            ),
            array(
                'name'=>'oc',
                'header' => 'ОЦ',
            ),
            array(
                'name'=>'address',
                'header' => 'Адрес получателя',
            ),                        
        ),
        'enablePagination' => true,
        'pager' => array(
                'class' => 'CLinkPager',
                'cssFile' => false,
                'header' => false,
                'firstPageLabel' => 'Начало',
                'prevPageLabel' => 'Назад',
                'nextPageLabel' => 'Вперед',
                'lastPageLabel' => 'Конец',
         ),
    )); ?>
<div class="load-automatic-form-actions">
   <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        'type'=>'null', 
        'label'=>'Загрузить',
        'htmlOptions'=>array('name'=>'excel', 'value' => '1'),)
        ); 
   ?>
</div>
<?php $this->endWidget(); ?>