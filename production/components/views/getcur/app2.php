<?php
    if(true){
        $model->date ? TRUE : $model->shouldBeSkipped = 0;
        //$model->timeEnd = 18;
        $array = Array(
            'time_start' => array(
                9 =>  '09:00',
                10 => '10:00',
                11 => '11:00',
                12 => '12:00',
                13 => '13:00',
                14 => '14:00',
                15 => '15:00',
            ),
            'time_end' => array(
                12 => '12:00',
                13 => '13:00',
                14 => '14:00',
                15 => '15:00',
                16 => '16:00',
                17 => '17:00',
                18 => '18:00',
            ),
            'sbs'      => array(
                0 => Yii::t("total", 'нет, пропуск не нужен'),
                1 => Yii::t("total", 'да, пропуск нужен'),
            ),
        );
    }
?>

<div>
    <?php  $this->widget('bootstrap.widgets.TbButton', array(
        'label'=> Yii::t("total", 'заявки'),
        'type'=> 'info', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'url' => Yii::app()->createUrl('/getcur'),
        'htmlOptions'=>array('id'=>'appLink'),
    )); ?>
</div>
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'application',
    'type'=>'inline',
)); ?>

        <table style="margin-top: 30px">
            <tr>
                <td>
                    <span>
                        <?=Yii::t("total", 'дата')?>*
                    </span>
                    <span style="padding-left: 128px">
                        <?php echo $form->textFieldRow($model,'date',array( 'class'=>'span2')); ?>
                    </span>
                        <?php echo $form->dropDownListRow($model, 'timeStart', $array['time_start'] ,array( 'class'=>'span2 megapolis-span1'));?>
                        <?php echo $form->dropDownListRow($model, 'timeEnd'  , $array['time_end']   ,array( 'class'=>'span2 megapolis-span1'));?>
                    </span>
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                       <tr>
                            <td>
                                <?=Yii::t("total", 'адрес пункта погрузки')?>*
                            </td>

                            <td>
                                <?php echo $form->textFieldRow($model,'addressOfLoad',array('class'=>'span3')); ?>
                            </td>
                       </tr>
                       <tr>
                            <td>
                                <?=Yii::t("total", 'телефон')?>*
                            </td>
                            <td>
                                <?php echo $form->textFieldRow($model,'phone',array('class'=>'span2')); ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <?=Yii::t("total", 'контактное лицо')?>*
                            </td>
                            <td>
                                   <?php echo $form->textFieldRow($model,'contactName',array('class'=>'span3')); ?>
                            </td>
                        </tr>
                     </table>
                </td>
            </tr>
            <tr>

            </tr>
            <tr>

                        <td >
                            <span>
                                <?=Yii::t("total", 'объем').'(м<sup>3</sup>)'?>*
                            </span>
                            <span style="padding-left: 87px">
                                <?php echo $form->textFieldRow($model,'occupancy',array('class'=>'span1', 'hint'=>'<small>в (куб.м). Пример: 5.00 или 3 </small>')); ?>
                            </span>
                        </td>

            </tr>
            <tr>
                <td>
                     <span>
                        <?=Yii::t("total", 'пропуск')?>
                     </span>
                     <span style="padding-left: 116px">
                         <?php echo $form->dropDownListRow($model,'shouldBeSkipped', $array['sbs']);?>
                    </span>

                </td>
            </tr>
            <tr>
                <td>
                     <span>
                        <?=Yii::t("total", 'примечание')?>
                     </span>
                     <span style="padding-left: 152px;">
                        <?php echo $form->textAreaRow($model,'note',array('class'=>'span8')); ?>
                    </span>

                </td>
            </tr>
            <tr>
                <td>
                    <?php $this->widget('bootstrap.widgets.TbButton', array(
                            'buttonType' => 'submit',
                            'type'       => 'primary',
                            'label'      => Yii::t("total", 'сохранить заявку'),
                            'size'       => 'Normal',
                        )
                    ); ?>
                </td>
            </tr>
        </table>
<?php $this->endWidget(); ?>