<div id="parentView">
    <?php $this->widget('bootstrap.widgets.TbGridView', array(
        'type'=>'striped bordered condensed',
        'dataProvider'=> $model->search(),
        'template'=>"{items}\n{pager}",
        'ajaxVar' => '',
        'columns'=>array(
            array(
                'name'=>'id',
                'header' => Yii::t("total", '№ заявки'),
                'htmlOptions'=> array('style'=>'width: 30px;'),
                #'value'  => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + $row + 1',
            ),
            array(
                'name'=> 'dateApp',
                'header'=> Yii::t("total", 'дата модификации заявки'),
            ),
            array(
                'name'=> 'timeEnd',
                'header'=> Yii::t("total", 'дата прибытия курьера'),
            ),
             array(
                'name'=> 'timeEnd',
                'header'=> Yii::t("total", 'действия'),
                'type'  => 'raw',
                'value' => function($data){
                        $img = array(0 => '/megapolis-stop.gif', 1 => '/megapolis_state_tick.png');
                        $oppmode = RCDB::oppMode($data);
                        
                        $src =  ($data->statusRequest == 5) ? '' : 's_read';
                        $text = ($data->statusRequest == 5) ? 'Заявка закрыта' : 'Закрыть заявку';
                        $array = array(
                            'read' => array(
                                'url' => '/getcur?RCDB[id]='.$data->id.'&RCDB[status]=5',
                                'htmloption' =>  array('title' => $text, 'class' => $src),
                            ),
                        );
                        if($data->statusRequest == RCDB::$RC_STATUS_REQUEST_LOCK){
                            $text = CHtml::link($array['read']['htmloption']['title'], '', $array['read']['htmloption']);
                        }else{

                            $text = CHtml::link($array['read']['htmloption']['title'], $array['read']['url'], $array['read']['htmloption']);
                        }
                        return $text;
                    },
            ),
            array(
                'name'=>'status_reques',
                'header'=> Yii::t("total", 'операции'),
                'htmlOptions'=>array('style'=>'width:80px; text-align:center'),
                'type'  => 'raw',
                'value' => function($data){
                        $flag = FALSE;
                        $array = array(
                            'del' => array(
                                'url' => Yii::app()->controller->createUrl('getcur', array(get_class($data).'[id]'=>$data->id, get_class($data).'[status]'=> RCDB::$RC_STATUS_REQUEST_DELETE)),
                                'htmloption' =>  array('title' => 'Удалить', 'class' => 's_delete icon-trash'),
                            ),
                            'mod' => array(
                                'url' => Yii::app()->controller->createUrl('app', array(get_class($data).'[id]'=>$data->id, get_class($data).'[status]'=> RCDB::$RC_STATUS_REQUEST_MODIFIED)),
                                'htmloption' =>  array('title' => 'Редактировать', 'class' => 's_update icon-edit'),
                            ),
                            'read' => array(
                                'src' =>  CHtml::image(Yii::getPathOfAlias('link_images').'/megapolis-open.gif'),
                                'url' => Yii::app()->controller->createUrl('getcur', array(get_class($data).'[id]'=>$data->id, get_class($data).'[status]'=> RCDB::$RC_STATUS_REQUEST_OPEN)),
                                'htmloption' =>  array('title' => 'Просмотр', 'class' => 'icon-eye-open'),
                            ),
                        );
                        if($data->statusRequest == 5){
                            $flag = TRUE;
                        }

                        $result =  $flag ?
                        (
                                CHtml::link('', $array['read']['url'], $array['read']['htmloption']).
                                str_repeat('&nbsp', 10)
                        )
                            .' '
                            :
                        (
                            CHtml::link('', $array['read']['url'], $array['read']['htmloption'])
                            .' '.
                            CHtml::link('', $array['mod']['url'], $array['mod']['htmloption'])
                            .' '.
                            CHtml::link('', $array['del']['url'], $array['del']['htmloption'])
                            .str_repeat('&nbsp', 1)
                        )

                        ;
                        return $result;
                },
            ),
            array(
                'name'=> 'time_end',
                'header'=> Yii::t("total", 'статус заявки'),
                'type'  => 'raw',
                'value' => function($data){
                        $img = array(0 => '/megapolis-non-executed.gif', 1 => '/megapolis_state_tick.png');
                        $array = array(
                            'read' => array(
                                'src' =>  CHtml::image(Yii::getPathOfAlias('link_images').$img[$data->status1c ? 1 : 0]),
                                'htmloption' =>  array('title' => $data->status1c ? 'Исполнена' : 'Еще не исполнена', 'class' => 'icon-stop'),
                                'text' => Yii::t("total", 'создана'),
                            ),
                        );

                        return ($array['read']['text']);
                },
            ),
        ),
        'enablePagination' => true,
        'pager' => array(
            'class' => 'CLinkPager',
            'cssFile' => false,
            'header' => false,
            'firstPageLabel' => '<<',
            'prevPageLabel' => '<',
            'nextPageLabel' => '>',
            'lastPageLabel' => '>>',
        ),
    )); ?>
</div>