<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('js').DS.'/megapolis.app.js'));?>
<?php $model->shouldBeSkipped = !$model->shouldBeSkipped ? Yii::t("total", 'нет, пропуск не нужен') : Yii::t("total", 'да, пропуск нужен');?>
<?php  $this->widget('bootstrap.widgets.TbButton', array(
    'label'=> Yii::t("total", 'заявки'),
    'type'=> 'info', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
    'url' => 'getcur',
)); ?>
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'application',
    'type'=>'horizontal',

));
$words = [
    'application'   => Yii::t("total", 'составление заявки'),
    'volume'        => Yii::t("total", 'объем ').'(м<sup>3</sup>)',
    'from'          => Yii::t("total", 'дата прибытия курьера'),
    'to'            => Yii::t("total", 'дата прибытия курьера'),
    'address'       => Yii::t("total", 'адрес пункта доставки'),
    'contact'       => Yii::t("total", 'контактное лицо'),
    'phone'         => Yii::t("total", 'телефон'),
    'skip'          => Yii::t("total", 'пропуск'),
    'comment'       => Yii::t("total", 'комментарий'),
];
?>
    <div>
        <?php echo $form->textFieldRow($model,'dateApp',  array('rows'=>2, 'class'=>'span7','disabled'=>true, 'labelOptions'        => array('label' =>  $words['application'])) ); ?>
        <?php echo $form->textFieldRow($model,'occupancy',  array('rows'=>2, 'class'=>'span7','disabled'=>true, 'labelOptions'      => array('label' =>  $words['volume'])) ); ?>
        <?php echo $form->textFieldRow($model,'timeStart',  array('rows'=>2, 'class'=>'span7','disabled'=>true, 'labelOptions'      => array('label' =>  $words['from'])) ); ?>
        <?php echo $form->textFieldRow($model,'timeEnd', array('rows'=>2, 'class'=>'span7','disabled'=>true, 'labelOptions'         => array('label' =>  $words['to'])) ); ?>
        <?php echo $form->textFieldRow($model,'addressOfLoad', array('rows'=>2, 'class'=>'span7','disabled'=>true, 'labelOptions'   => array('label' =>  $words['address'])) ); ?>
        <?php echo $form->textFieldRow($model,'contactName', array('rows'=>2, 'class'=>'span7','disabled'=>true, 'labelOptions'     => array('label' =>  $words['contact'])) ); ?>
        <?php echo $form->textFieldRow($model,'phone', array('rows'=>2, 'class'=>'span7','disabled'=>true, 'labelOptions'           => array('label' =>  $words['phone'])) ); ?>
        <?php echo $form->textFieldRow($model,'shouldBeSkipped', array('rows'=>2, 'class'=>'span7', 'disabled'=>true, 'labelOptions'=> array('label' =>  $words['skip'])) ); ?>
        <?php echo $form->textAreaRow($model,'note', array('class'=>'span7', 'rows'=>6, 'disabled'=>true, 'labelOptions'            => array('label' =>  $words['comment'])) ); ?>
     </div>
<?php $this->endWidget(); ?>
