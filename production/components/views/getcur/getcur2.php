<div>
    <?php  $this->widget('bootstrap.widgets.TbButton', array(
        'label'=> Yii::t("total", 'создать заявку'),
        'type'=> 'info', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'url' => Yii::app()->createUrl('/app'),
    )); ?>
</div>

<?php $this->renderPartial('application.components.views.getcur._getcur',array(
        'model'=>$model,
      )); 
?>
<?php $this->widget('application.components.component.MegapolisDesign',array(
    'array' => array(
        array('icon-eye-open',  Yii::t("total", 'создать заявку')),
        array('icon-edit',      Yii::t("total", 'редактирование')),
        array('icon-trash',     Yii::t("total", 'удаление')),
    ),
));