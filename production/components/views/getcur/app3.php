<?php
    if(true){
        $model->date ? TRUE : $model->shouldBeSkipped = 0;
        $model->time_end = 18;  
    }
?>

<div>
    <?php  $this->widget('bootstrap.widgets.TbButton', array(
        'label'=>'Заявки',
        'type'=> 'info', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'url' => Yii::app()->createUrl('/getcur'),
        'htmlOptions'=>array('id'=>'appLink'),
    )); ?>
</div>
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'application',
    'type'=>'horizontal',
)); ?>
    <div>
        <?php echo $form->textFieldRow($model,'date',array( 'class'=>'span2')); ?>
        <?php echo $form->textFieldRow($model,'occupancy',array('class'=>'span1', 'hint'=>'<small>в (куб.м). Пример: 5.00 или 3 </small>')); ?>
        <?php echo $form->dropDownListRow($model, 'timeStart', array(
                                9 =>  '09:00',
                                10 => '10:00',
                                11 => '11:00',
                                12 => '12:00',
                                13 => '13:00',
                                14 => '14:00',
                                15 => '15:00',
                       ),array( 'class'=>'span1 megapolis-span1'));
            ?>
            <?php echo $form->dropDownListRow($model, 'timeEnd', array(
                                12 => '12:00',
                                13 => '13:00',
                                14 => '14:00',
                                15 => '15:00',
                                16 => '16:00',
                                17 => '17:00',
                                18 => '18:00',
                       ),array( 'class'=>'span1 megapolis-span1'));
            ?>
            <?php echo $form->textFieldRow($model,'addressOfLoad',array('class'=>'span3')); ?>
            <?php echo $form->textFieldRow($model,'contactName',array('class'=>'span3')); ?>
            <?php echo $form->textFieldRow($model,'phone',array('class'=>'span2')); ?>
            <?php echo $form->radioButtonListRow($model,'shouldBeSkipped',
                    array(0 => 'Нет, пропуск не нужен',
                          1 => 'Да, пропуск нужен',
                        ));
            ?>
            <?php echo $form->textAreaRow($model,'note',array('class'=>'span7', 'rows'=>6)); ?>
        
            <div style='width: 671px; float: left; margin-right: 18px; margin-left: 180px'>
                <?php $this->widget('bootstrap.widgets.TbButton', array(
                        'buttonType' => 'submit',
                        'type'       => 'primary',
                        'label'      => 'Сохранить заявку',
                        'size'       => 'Normal',
                    )
                ); ?>
            </div>
    </div>


<?php $this->endWidget(); ?>