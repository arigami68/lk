<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'createOrder',
    'type'=>'inline',
));
?>
<div style="float: right;">
    <?php
    $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
           
            'label'=>'Сохранить в excel',
            'htmlOptions'=>array('name'=>'excel'),)
    );
    ?>
</div>
<?php $this->endWidget(); ?>
<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
    'dataProvider'=> $model->QuSe($model::$SEARCH_RETURNS),
    'template'=>"{items}\n{pager}",
    'ajaxVar' => '',
    'columns'=>array(
        array(
            'name'=>'id',
            'header' => '№ заявки',
        ),
        array(
            'name'=>'agent',
            'header' => 'Идентификатор почтового отправления',
        ),
        array(
            'name'=>'megapolis',
            'header' => 'Идентификационный номер, присвоенный отправлению Клиентом',
        ),
        array(
            'name'=>'datelast',
            'header' => 'Дата приема отправления от отправителя',
        ),
        array(
            'name'=>'np',
            'header' => 'НП',
        ),
        array(
            'name'=>'oc',
            'header' => 'ОЦ',
        ),
        array(
            'name'=>'fio',
            'header' => 'Наименование получателя',
        ),
        array(
            'name'=>'address',
            'header' => 'Адрес получателя',
            'type' => 'raw',
            'value' => function($data){
                    print $data['address'];
                },
        ),
        array(
            'name'=>'phone',
            'header' => 'Телефон',
        ),
    ),
    'enablePagination' => true,
    'pager' => array(
        'class' => 'CLinkPager',
        'cssFile' => false,
        'header' => false,
        'firstPageLabel' => '<<',
        'prevPageLabel' => '<',
        'nextPageLabel' => '>',
        'lastPageLabel' => '>>',
    ),
    'htmlOptions'=>array(),
)); ?>
<div style="float: right; margin-top: -21px;">
    <?php
    $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Сохранить в excel',
            'htmlOptions'=>array('name'=>'excel'),)
    );
    ?>
</div>