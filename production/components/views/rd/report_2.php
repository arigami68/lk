<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'createOrder',
    'type'=>'inline',
));
$criteria = new CDbCriteria();

?>
<div style="float: right;">
    <?php
    $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Сохранить в excel',
            'htmlOptions'=>array('name'=>'excel'),)
    );
    ?>
</div>
<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
    'dataProvider'=> $model->QuSe($model::$SEARCH_STATUS, NULL, NULL, $criteria),
    'template'=>"{items}\n{pager}",
#    'enableSorting' => false,
    'ajaxVar' => '',
    'columns'=>array(
        array(
            'header'=> '№ п/п',
            'value' => function($data, $row, $column) {
                    $grid = $column->grid;
                    $pages = $grid->dataProvider->getPagination();
                    $start = ($grid->enablePagination === false) ? 0 : $pages->getCurrentPage(false) * $pages->getPageSize();
                    return $start + $row + 1;
                },

        ),
        array(
            'name'  => 'agent',
            'header'=> 'Номер ОД',
        ),

        array(
            'name'  => 'megapolis',
            'header'=> 'Клиентский номер',
        ),
        array(
            'name'  => 'dateCreate',
            'header'=> 'Дата приема',
            'type'  => 'raw',
            'value' => function($data){
                    print date('d-m-Y H:i', $data->dateCreate);
                },
        ),
        array(
            'name'  => 'name',
            'header'=> 'Наименование отправителя',
            'type'  => 'raw',
            'value' => 'Yii::app()->user->auth->name',

        ),
        array(
            'name'  => 'costPublic',
            'header'=> 'ОЦ, руб.',
        ),
        array(
            'name'  => 'costDelivery',
            'header'=> 'НП, руб ',
        ),
        array(
            'name'  => 'fio',
            'header'=> 'ФИО получателя',
        ),
        array(
            'name'  => 'address',
            'header'=> 'Адрес доставки',
            'type'  => 'raw',
            'value' => '$data->region." ".$data->area." ".$data->city',
        ),
        array(
            'name'  => 'phone',
            'header'=> 'Телефон',
        ),

        array(
            'name'  => 'status',
            'header'=> 'Статус отправления',
            'type'  => 'raw',
            'value' => function($data){
		            $trace = TRAC::model()->code($data->agent)->last()->find();
                    return  $trace ? $trace->status : '';
            },
        ),
        array(
            'name'  => 'status_date',
            'header'=> 'Дата статуса отправления',
            'type'  => 'raw',
            'value' => function($data){
		            $trace = TRAC::model()->code($data->agent)->last()->find();
		            return  $trace ? date('m-d-Y', strtotime($trace->dateD)) : '';
             },
        ),
    ),
    'pager' => array(
        'class' => 'CLinkPager',
        'cssFile' => false,
        'header' => false,
        'firstPageLabel' => '<<',
        'prevPageLabel' => '<',
        'nextPageLabel' => '>',
        'lastPageLabel' => '>>',
    ),
    'htmlOptions'=>array(),
));
?>
<div style="float: right; margin-top: -21px;">
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'label'=>'Сохранить в excel',
                'htmlOptions'=>array('name'=>'excel'),)
        );
        ?>
    </div>
<?php $this->endWidget(); ?>