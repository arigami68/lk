
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('js').DS.'/datepicker/jquery-ui.js'));?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('js').DS.'/datepicker/jquery.ui.datepicker-ru.js'));?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('js').DS.'megapolis.rd.js'));?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('css').DS.'/datepicker/jquery-ui.css')); ?>
<?php
    $list = $model->list;
    $model->list = Yii::app()->request->getQuery('list')-1;
?>
<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'createOrder',
    'type'=>'inline',
));
?>
<div style="padding-bottom: 20px">
    <?=Yii::t("total", "отчеты")?><?php echo str_repeat('&nbsp', 20);?>

<?php echo $form->dropDownListRow($model,'list', $list); ?>
</div>
<div style="padding-bottom: 20px">
    <?=Yii::t("total", "выберите дату")?><?php echo str_repeat('&nbsp', 6);?>
    <?php echo $form->textFieldRow($model,'date_s',array('rows'=>1, 'class'=>'span2'));?>
    <?php echo $form->textFieldRow($model,'date_f',array('rows'=>1, 'class'=>'span2'));?>
</div>
<span id="summary" style="color: red"><?=Yii::t("total", "diapazon-31")?></span>
<br><br>
<?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        'type'=>'primary',
        'label'=> Yii::t("total", "сформировать"),
        'htmlOptions'=>array('name'=>'submit', 'id' => 'Ekaterinburg'),)
);
?>
<?php
/*
if(isset($value)){
    if($value == 1){
        require_once Yii::getPathOfAlias('application.components.views.rd').DS.'report_1.php';
    }elseif($value == 2){
        require_once Yii::getPathOfAlias('application.components.views.rd').DS.'report_2.php';
    }elseif($value == 3){
        require_once Yii::getPathOfAlias('application.components.views.rd').DS.'report_3.php';
    }
}

?>


*/
?>
<?php $this->endWidget(); ?>