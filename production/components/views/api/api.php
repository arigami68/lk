<?php Yii::app()->clientScript->registerCssFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('css').DS.'megapolis.api.css'));?>

<table class="contentpaneopen">
    <tr>
        <td valign="top">
            <div style="margin: 0 30px 0 30px; text-align: justify;">
                <p>Мы стараемся сделать наше сотрудничество более удобным для Вас. Для этого ИдеаЛоджик разрабатывает различные IT-решения, позволяющие экономить ваше время и сделать наше сотрудничество более эффективным.</p>
                <p><b>КАЛЬКУЛЯТОР</b></p>
                <p>На нашем сайте установлен <a style="color: #0000ff;" href="/calc">КАЛЬКУЛЯТОР</a>, на котором Вы сможете рассчитать стоимость доставки вашего отправления из Москвы в любой населенный пункт России. Расчёты можно производить как с учетом суммы наложенного платежа, так и без него.</p>
                <p><b>API</b></p>
                <p>Используя API "ИдеаЛоджик" Вы можете рассчитывать стоимость и сроки доставки автоматически, без участия пользователя. <strong>API&nbsp;</strong>открыт по адресу&nbsp;<span color="#0000ff" style="color: #0000ff;"><span style="text-decoration: underline;"><span face="Times New Roman, serif" style="font-family: 'Times New Roman', serif;"><span size="3" style="font-size: small;"><a class="western" href="http://www.megaexp.ru/api.php">http</a><a class="western" href="http://www.megaexp.ru/api.php">://</a><a class="western" href="http://www.megaexp.ru/api.php">www</a><a class="western" href="http://www.megaexp.ru/api.php">.</a><a class="western" href="http://www.megaexp.ru/api.php">megaexp</a><a class="western" href="http://www.megaexp.ru/api.php">.</a><a class="western" href="http://www.megaexp.ru/api.php">ru</a><a class="western" href="http://www.megaexp.ru/api.php">/</a><a class="western" href="http://www.megaexp.ru/api.php">api</a><a class="western" href="http://www.megaexp.ru/api.php">.</a><a class="western" href="http://www.megaexp.ru/api.php">php</a>.</span></span></span></span></p>
                <p><a style="color: #0000ff;" href="http://www.megapolis-exp.ru/files/API_megapolis-exp_ru.doc">Скачать документацию API (с примерами)</a>.</p>
                <p><b>ФРЕЙМ</b></p>
                <p>Для расчёта стоимости Ваших отправлений на сайте можно также воспользоваться <a style="color: #0000ff;" href="http://www.megapolis-exp.ru/calc6/">Фреймом</a>.</p>
                <p>По этому адресу Вы можете получить фрейм, который используется у нас на сайте (<a style="color: #0000ff;" href="http://www.megapolis-exp.ru/calc6/">Калькулятор</a>) или персанализировать его под свои собственные требования.</p>
                <p>Как показывает практика, нашим клиентам не всегда требуется полный калькулятор со всеми функциями. Чаще необходимо работать с заранее определенными параметрами. Загружая фрейм, Вы можете предусмотреть все необходимые условия и ограничения в момент его построения. Например, можно установить фильтр на вес отправления и тип тарифа. При использовании фильтров уже на этапе запроса к нашему серверу они будут учтены при построении калькулятора. <a style="color: #0000ff;" href="http://www.megapolis-exp.ru/index.php/informaciya/893-2011-06-09-16-03-15">Подробнее</a>.</p>
                <p><b>MEGAPOLIS Document Offline Generator</b></p>
                <p>Для удобства формирования реестров отправлений мы предлагаем Вам воспользоваться программой MEGAPOLIS Document Offline Generator. Эта программа позволит Вам в считанные минуты создавать необходимые для отправки формы документов и исключить вероятность появления ошибок в процессе оформления. Готовые реестры Вы можете отправить по электронной почте Вашему менеджеру или клиенту, прямо не выходя из программы.</p>
                <p>Для использования программы необходимо предварительно получить логин и пароль, пройдя регистрацию у нас на сайте.</p>
                <p>Минимальные требования: Операционная система Windows XP и выше, .NET Framework 3.5SP1, установленный пакет Microsoft Office 97 и выше.</p>
                <p><a style="color: #0000ff;" href="http://www.megapolis-exp.ru/MegapolisOfflineDocumentGenerator/app.publish.zip">Скачать MEGAPOLIS Document Offline Generator</a>.</p>
            </div>
        </td>
    </tr>

</table>