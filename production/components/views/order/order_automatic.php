<div id="load-automatic-text">
    <div id="file-error">
    <?php

        if(isset($mResult)){
            if($mResult->messages[$mResult::$MESSAGES_ERROR]){
                $css = array('class' => 'alert alert-block alert-error');
                $model->clearErrors();
                $model->addErrors($mResult->messages[$mResult::$MESSAGES_ERROR]);
                echo $form->errorSummary($model, '', NULL, $css);
            }
            if($mResult->messages[$mResult::$MESSAGES_SUCCESS]){
                $css        = array('class' => 'alert alert-block');
                $model->clearErrors();
                $model->addErrors($mResult->messages[$mResult::$MESSAGES_SUCCESS]);
                echo $form->errorSummary($model, '', NULL, $css);
            }
        }else{
            if(count($model->log)){
                foreach($model->log as $k=>$v){
                    if($k == MegapolisMessage::$OPERATION_ERROR){
                        $text_alert = Yii::t("total", "ошибка");
                        $css = array('class' => 'alert alert-block alert-error');
                        $model->clearErrors();
                        $model->addError(MegapolisMessage::$OPERATION_ERROR, $v);
                        echo $form->errorSummary($model,$text_alert, NULL, $css);
                    }elseif($k == MegapolisMessage::$OPERATION_INSERT){

                        $css        = array('class' => 'alert alert-block');
                        $text_alert = Yii::t("total", "успешное добавление").':';
                        $model->clearErrors();

                        $model->addError(MegapolisMessage::$OPERATION_INSERT, $v);
                        echo $form->errorSummary($model,$text_alert, NULL, $css);
                    }

                }
            }else{
                $text_alert = Yii::t("total", "Необходимо исправить ошибки").':';
                $css        = array('class' => 'alert alert-block alert-error');
                if($model->hasErrors()){;
                    echo $form->errorSummary($model,$text_alert, NULL, $css);
                }
            }
        }
    print '</div>';
        $text='<table><tr><td><H5>'.Yii::t("total", "действия при загрузке").'</H5></td><td></tr><tr><td>';
        if($load){
            $color='';
            if(isset($load['allow'])){
                if($load['allow'] == 'error'){$color='red';}elseif($load['allow'] == 'success'){$color='orange';}
                unset($load['allow']);
            }
            foreach($load as $k=>$v){
                $text.='<div >'.$k.':: <b style="color: '.$color.'">'.$v.'</b></div>';
            }
            $text.='</td></tr></table><br>';
            print $text;
        }

print '</div>';
switch(Yii::app()->user->auth->db->country){
    case 276:
        $href[] = '<a href='.Yii::app()->assetManager->publish(PRODUCTION.'file'.DS.'general'.DS.'test_registry_pr.xlsx').'> '.Yii::t("total", 'шаблон для загрузки Почты Росии').'</a>';
        $href[] = '<a href='.Yii::app()->assetManager->publish(PRODUCTION.'file'.DS.'general'.DS.'test_registry_express.xlsx').'> '.Yii::t("total", 'шаблон для загрузки Эксперсс грузов').'</a>';
        break;
    default:
        $href[] = '<a href='.Yii::app()->assetManager->publish(PRODUCTION.'file'.DS.'general'.DS.'test_registry.xlsx').'> '.Yii::t("total", 'шаблон для загрузки').'</a>';
}
foreach($href as $value) print '<p>'.$value.'</p>';

?>

<div id="fileLoad">
    <?php echo $form->fileFieldRow($model, 'fileField');?>
    <span id="fileformlabel"></span>
</div>

<div class="load-automatic-form-actions">
    <div id="yt1"></div>
   <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit',
        'type'=>'null', 
        'label'=> Yii::t("total", 'загрузить'))
        ); 
   ?>

</div>
