<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('js').DS.'/datepicker/jquery-ui.js'));
    if((Yii::app()->request->getQuery('operation')) == 'create'){
        $this->widget('bootstrap.widgets.TbButtonGroup', array(
            'type' => 'primary',
            'toggle' => 'radio',
            'buttons' => array(
                array('label'=> Yii::t("total", 'вручную'), 'active' => 'true', 'htmlOptions'=>array('id'=>'check_manual', )),
                array('label'=> Yii::t("total", 'загрузить из файла'), 'htmlOptions'=>array('id'=>'check_automatic',)),
            ),
        ));
        print '<br><br>';
    }
?>

<div id='manual' style='display: block'>
    <?php require Yii::getPathOfAlias('views').DS.'order'.DS.'order_manual.php';?>
</div>

<?php
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id'=>'loadForm',
        'htmlOptions'=>array('enctype'=>'multipart/form-data'),
        'type'=>'horizontal',
    )); 
?>

<div id='automatic' style='display:none'>
    <?php require Yii::getPathOfAlias('views').DS.'order'.DS.'order_automatic.php';?>
</div>

<?php $this->endWidget(); ?>
