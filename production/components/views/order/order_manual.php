<?php
    $model->sender = Yii::app()->user->auth->name;
    $model->contract = Yii::app()->user->auth->contract;

    $model->volume = isset($model->orderload[0]) ? $model->orderload[0]->volume : '';
    $model->date_dep = isset($model->orderload[0]) ? $model->orderload[0]->date_dep : '';

    $model->op_take = isset($model->orderload[0]) ? $model->orderload[0]->op_take : 1;
    $model->address_dep = isset($model->orderload[0]) ? $model->orderload[0]->address_dep : '';
    $manual = new stdClass();
    $manual->action['name']   = Yii::import('application.modules.order.models.AddRegisterAction');
    $manual->action['namelock']   = Yii::import('application.modules.order.models.AddRegisterLockAction');
    $manual->content= '';
    $labels = $model->attributeLabels();

    $messages = array(
        'auto'                  => Yii::t("total", 'автоматическое присвоение'),
        'date_contract'         => Yii::t("total", 'дата заключение договора'),
        'example_volume'        => Yii::t("total", "примерный объем отправления.").'в м<sup>3</sup>',
        'all_filled'            => Yii::t("total", 'все поля заполнены'),
        'save_order'            => Yii::t("total", 'сохранить заказ'),
        'lock_field'            => Yii::t("total", 'заблокированное поле'),
        'mandatory_field'       => Yii::t("total", 'обязательное поле'),
        'not_mandatory_field'   => Yii::t("total", 'не обязательное поле'),
        'reset_field'           => Yii::t("total", 'сброс полей адресата'),
        'load_field'            => Yii::t("total", 'подгружаемые данные'),
    );

    if(!isset($operation)) $operation = 'new';

?>
<?php
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id'=>'createOrder',
        'type'=>'inline',
    ));
?>
<div>
<?php
if(ORDS::$OPERATION_READ == $operation){
    $disabled = 'disabled';
}else{
    $disabled = "";
}
            $array = array();
            $array[0] = $form->textFieldRowDisabled($model,'sender',array('rows'=>3, 'class'=>'span2'));
            $array[1] = $form->textFieldRow($model,'date_dep',array('class'=>'span2', 'disabled' => $disabled));

            $array[2] = $form->dropDownListRow($model, 'op_take', array_flip(FuncMegapolis::Handbook()->op_take), array('class' => 'span2 op_take',  'labelOptions' => array('label' => false), 'disabled' => $disabled));
            #$register->text.= $form->dropDownListRow($register->form['model'], $name,  array('')+array_flip($register->HK->$name),  array('rows'=>2, 'class'=>'span2 agent_id',  'name' => $register->form['name'].'['.$name.']['.$len16.']'));
            if($model->op_take == 2){
                $array[3] = $form->textAreaRow($model,'address_dep',array('class'=>'span2 address_dep', 'disabled' => $disabled));
            }else{
                $key = array_search($model->address_dep, FuncMegapolis::Handbook()->default_address);
                $model->address_dep = $key;
                $array[3] = $form->dropDownListRow($model, 'address_dep', FuncMegapolis::Handbook()->default_address, array('class' => 'span2 address_dep',  'labelOptions' => array('label' => false), 'disabled' => $disabled)
                );
            }
            $array[4] = $form->textFieldRow($model,'volume',array('class'=>'span1', 'disabled' => $disabled));

            $array[5] = $form->textFieldRowDisabled($model,'contract',array('rows'=>2, 'class'=>'span2'));
            $array[6] = $form->hiddenField($model, 'num_count',array('id'=>'num_count'));
            if($operation == ORDS::$OPERATION_CREATE){
                $array[7] = $form->textFieldRow($model,'registry',array('rows'=>2, 'class'=>'span2', 'readonly' => 'readonly'));
            }elseif($operation == ORDS::$OPERATION_UPDATE){
                $model->registry = $model->orderload[0]->registry;

                $array[7] = $form->textFieldRow($model,'registry',array('rows'=>2, 'class'=>'span2', 'readonly' => 'readonly'));
            }else{
                $array[7] = $form->textFieldRow($model,'registry',array('rows'=>2, 'class'=>'span2', 'readonly' => 'readonly'));
            }
            echo $array[6];
switch(Yii::app()->user->auth->db->country){
    case 276:
        $take = array_flip(FuncMegapolis::Handbook()->op_take);
        $array[2] = $form->dropDownListRow($model, 'op_take', array($take[1]), array('class' => 'span2 op_take',  'labelOptions' => array('label' => false), 'readonly' => 'readonly'));

        $array[3] = $form->dropDownListRow($model, 'address_dep', array('Grossbeerenstraße 146 Halle 3.12 GewerbePark Marienfelde '), array('class' => 'span3 address_dep',  'labelOptions' => array('label' => false), 'readonly' => 'readonly'));
        break;
    default:

}
?>

<table style="width: 100%">
    <tr>
        <td>
            <a href="#" rel="tooltip" title="<?php print $labels['sender'].'. '.$messages['auto']; ?>"><span class="icon-lock"></span></a>
            <?php print $labels['sender'];?>:
        </td>
        <td>
            <?php echo $array[0];?>
        </td>
        <td  class="megapolis-order-table-right">
            <?php print $labels['date_dep'];?>:
        </td>
        <td>
            <?php echo $array[1];?>
        </td>
    </tr>
    <tr>
        <td>
            <a href="#" rel="tooltip" title="<?php print $labels['contract'].'. '.$messages['auto']; ?>" style="display: inline"><span class="icon-lock"></span>&nbsp</a>
            <?php print $labels['contract'];?>:
        </td>
        <td>
            <?php echo $array[5];?>
        </td>
    </tr>
    <tr>
        <td>
            <a href="#" rel="tooltip" title="<?php print $labels['registry'].'. '.$messages['auto']; ?>" style="display: inline"><span class="icon-lock"></span>&nbsp</a>
            <?php print $labels['registry'];?>:
        </td>
        <td>
            <?php echo $array[7];?>
        </td>
        <td  class="megapolis-order-table-right">
            <?php print $labels['op_take'];?>:
        </td>
        <td>
            <?php echo $array[2];?>
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td>

        </td>
        <td  class="megapolis-order-table-right">
            <?php print $labels['address_dep'];?>:
        </td>
        <td>
            <?php echo $array[3];?>
        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td>

        </td>
        <td class="megapolis-order-table-right" style="padding-top: 20px;">
            <?=$messages['date_contract']?>:
        </td>
        <td style="padding-top: 20px; font-weight: bold">
            <?php echo date('d-m-Y', strtotime(Yii::app()->user->auth->db->contractDate));?>
        </td>
    </tr>

    <tr>
        <td>
            <p><?=$messages['example_volume']?></p>
        </td>
        <td style="margin-bottom: 10px">
            <?php echo $array[4];?>
        </td>
    </tr>

    </table>

<?php

            $tbtabs[]= array('label'=>'+', 'content'=> '', 'linkOptions' => array('id'=>'addRegister'));

        if(ORDS::$OPERATION_CREATE == $operation){
            print '</div>';
        }elseif(ORDS::$OPERATION_UPDATE == $operation){
            foreach($model->orderload as $k=>$v){
                if(!    empty($v->num)){
                    $manual->action['action'] = new $manual->action['name']($this->action->controller, $this->id);
                    $manual->content= $manual->action['action']->run('model', $v, $k);
                    $tbtabs[]= array('label'=>$v->num, 'content'=> $manual->content, 'linkOptions' => array('id' => 'zakaz'.($k+1), 'num' => $k+1));
                }
            }

        }elseif(ORDS::$OPERATION_READ == $operation){

            $tbtabs = array();

            foreach($model->orderload as $k=>$v){
                $megapolis = $v->megapolis;
                $manual->action['action'] = new $manual->action['namelock']($this->action->controller, $this->id);
                $manual->content= $manual->action['action']->run('model', $v, $k);

                $tbtabs[]= array('label'=>$megapolis, 'content'=> $manual->content, 'linkOptions' => array('id' => 'zakaz'.($k+1)));
            }
        }

        $this->widget('bootstrap.widgets.TbTabs', array(
            'type'=>'tabs',
            'placement'=>'top',
            'tabs'=> $tbtabs,
        ));

    ?>

<div style="margin-left: 100%; display: none">
    <?php $this->widget('bootstrap.widgets.TbLabel', array(
        'type'=>'success', // 'success', 'warning', 'important', 'info' or 'inverse'
        'label'=> $messages['all_filled'],
        'htmlOptions'=>array('id'=>'full'),
    )); ?>
</div>
<? if(ORDS::$OPERATION_READ != $operation){ ?>
<div style='padding-top: 10px; margin-left: 18px; float: left;'>
<?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type'       => 'primary',
        'label'      => $messages['save_order'],
        'size'       => 'Normal',
        'htmlOptions'=>array('id'=>'save'),
    )
); ?>
<?};?>
</div>
<?php $this->endWidget() ?>
<?php

$this->widget('application.components.component.MegapolisDesign',array(
    'array' => array(
        array("icon-lock",      $messages['lock_field']),
        array('icon-pencil',    $messages['mandatory_field']),
        array('icon-flag',      $messages['not_mandatory_field']),
        array('icon-remove',    $messages['reset_field']),
        array('icon-forward',   $messages['load_field']),
    ),
    'style' => "width: 200px; margin-top: 70px",
));

?>
