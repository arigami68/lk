<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'horizontalForm',
	'type'=>'horizontal',
)); ?>

<div class="top-menu" style="margin-bottom: 40px;">
	<div style="margin-bottom: 20px;">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType' => 'submit',
				'type'       => 'primary',
				'label'      =>  Yii::t("total", "задать товарные ограничения"),
				'size'       => 'Normal',
			)
		); ?>
	</div>
	<div style="margin-bottom: 20px;">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType' => 'submit',
				'type'       => 'primary',
				'label'      =>  Yii::t("total", "сформировать график колебания остатков"),
				'size'       => 'Normal',
			)
		); ?>
	</div>
	<div>
		<?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType' => 'submit',
				'type'       => 'primary',
				'label'      =>  Yii::t("total", "получать уведомления"),
				'size'       => 'Normal',
			)
		); ?>
	</div>

</div>

<?php echo $form->textFieldRow($data['model'],'prefix_delivery',array('class'=>'span1')); ?>
<?php echo $form->textFieldRow($data['model'],'prefix_options', array('class'=>'span1')); ?>
<?php echo $form->textFieldRow($data['model'],'prefix_goods',   array('class'=>'span1')); ?>
<?php echo $form->textFieldRow($data['model'],'packing',        array('class'=>'span1')); ?>

<?php $this->endWidget(); ?>
