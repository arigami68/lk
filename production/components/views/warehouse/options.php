<span style="margin-bottom: 20px;">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'       => 'primary',
			'label'      =>  Yii::t("total", "Создать заявку на комплектацию"),
			'size'       => 'Normal',
		)
	); ?>
</span>
<span style="margin-bottom: 20px;">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'       => 'primary',
			'label'      =>  Yii::t("total", "Фильтры"),
			'size'       => 'Normal',
		)
	); ?>
</span>
<span style="float: right;">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'       => 'primary',
			'label'      =>  Yii::t("total", "Выгрузить данные"),
			'size'       => 'Normal',
		)
	); ?>
</span>
