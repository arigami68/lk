<div>
	<?php

	$this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'link',
			'type'       => 'primary',
			'label'      =>  Yii::t("total", "Номенклатуры"),
			'size'       => 'Normal',
			'url'        => '/mywNG',
		)
	); ?>

</div>
<br>
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'horizontalForm',
	'type'=>'inline',

)); ?>

<style>

	table tr  td{
		text-align: right;
		width:200px;
		border:  0px double black;
	}
	#NomenclatureForm_comment{
		width: 90%;
	}
	.menu-center-right .btn{
		width: 200px;
		padding: 0px;
		border-style: unset;
		height: 22px;
		margin-top: 2px;
	}
	input.span1, textarea.span1, .uneditable-input.span1{
		width: 50px;
	}
</style>
<table>
	<?php
	print
		'<tr>'.
		'<td ><label>'.$data['model']->attributeLabels()[$data['model']->attributeNames()[2]].'</label></td>'.
		'<td>'.$form->textFieldRow($data['model'], $data['model']->attributeNames()[2],array('class'=>'span3')).'</td>'.
		'<td><label>'.$data['model']->attributeLabels()[$data['model']->attributeNames()[3]].'</label></td>'.
		'<td>'.$form->textFieldRow($data['model'], $data['model']->attributeNames()[3],array('class'=>'span3')).'</td>'.
		'</tr>'.
		'<tr>'.
		'<td><label>'.$data['model']->attributeLabels()[$data['model']->attributeNames()[4]].'</label></td>'.
		'<td >'.$form->textFieldRow($data['model'], $data['model']->attributeNames()[4],array('class'=>'span3')).'</td>'.
		'</tr>'.
		'<tr>'.
		'<td><label>'.$data['model']->attributeLabels()[$data['model']->attributeNames()[5]].'</label></td>'.
		'<td >'.$form->textFieldRow($data['model'], $data['model']->attributeNames()[5],array('class'=>'span3')).'</td>'.
		'</tr>'.
		'<tr>'.
		'<td><label>'.$data['model']->attributeLabels()[$data['model']->attributeNames()[6]].'</label></td>'.
		'<td >'.$form->textFieldRow($data['model'], $data['model']->attributeNames()[6],array('class'=>'span3')).'</td>'.
		'<td><label>'.$data['model']->attributeLabels()[$data['model']->attributeNames()[7]].'</label></td>'.
		'<td >'.$form->textFieldRow($data['model'], $data['model']->attributeNames()[7],array('class'=>'span3')).'</td>'.
		'</tr>'.
		'<tr>'.
		'<td><label>'.$data['model']->attributeLabels()[$data['model']->attributeNames()[8]].'</label></td>'.
		'<td >'.$form->textFieldRow($data['model'], $data['model']->attributeNames()[8],array('class'=>'span3')).'</td>'.

		'<td><label>'.$data['model']->attributeLabels()[$data['model']->attributeNames()[9]].'</label></td>'.
		'<td >'.$form->textFieldRow($data['model'], $data['model']->attributeNames()[9],array('class'=>'span3')).'</td>'.
		'</tr>'.
		'<tr>'.
		'<td><label>'.$data['model']->attributeLabels()[$data['model']->attributeNames()[10]].'</label></td>'.
		'<td style="text-align: left">'.$form->textFieldRow($data['model'], $data['model']->attributeNames()[10],array('class'=>'span2')).'</td>'.
		'<td><label>'.$data['model']->attributeLabels()[$data['model']->attributeNames()[11]].'</label></td>'.
		'<td style="text-align: left" >'.$form->textFieldRow($data['model'], $data['model']->attributeNames()[11],array('class'=>'span2')).'</td>'.
		'<td><label>'.$data['model']->attributeLabels()[$data['model']->attributeNames()[12]].'</label></td>'.
		'<td style="text-align: left">'.$form->textFieldRow($data['model'], $data['model']->attributeNames()[12],array('class'=>'span2')).'</td>'.
		'</tr>'.
		'<tr  >'.
		'<td colspan=2>';

	$this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'       => 'primary',
			'label'      =>  Yii::t("total", "сохранить"),
			'size'       => 'Normal',
			'htmlOptions'=> array('style' => 'float: left; margin: 0 0 0 110px'),
		)
	);
	print
		'</td>'.
		'</tr>'
	;
	?>
</table>

<?php $this->endWidget(); ?>
