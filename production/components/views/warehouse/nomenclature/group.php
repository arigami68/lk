

<div id="dialog" style="display: none" title="Действия над группой">
	<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
		'id'=>'horizontalForm',
		'type'=>'vertical',
	)); ?>
	<div >
		<?=$form->textFieldRow($data['group'], 'name' ,array('class'=>'span3'));?>
	</div>
	<div>
		<div style="padding-bottom: 5px;">Вложение в группу (если пусто, значит группа находится в корневом разделе)</div>
		<?=$form->dropDownList($data['group'], 'link' ,$data['link'], array('class'=>'span3'));?>
	</div>
	<?=$form->textFieldRow($data['group'], 'comment' ,array('class'=>'span3'));?>
	<div>
			<? $this->widget('bootstrap.widgets.TbButton', array(
					'buttonType' => 'submit',
					'type'       => 'primary',
					'label'      =>  Yii::t("total", "Создать номенклатуру"),
					'size'       => 'Normal',
				)
			); ?>
	</div>
	<?php $this->endWidget(); ?>
</div>
<span>
	<?php

	$this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'button',
			'type'       => 'primary',
			'label'      =>  Yii::t("total", "Создать группу"),
			'size'       => 'Normal',
			'htmlOptions'=> array('id' => 'group'),

		)
	); ?>

</span>
<div style="padding-top: 10px">
<? $this->widget('CTreeView', array('url' => array('AjaxFillTree'),'data' => DBnomenclaturegroupname::getData(), 'htmlOptions' => array('class' => 'treeview-group')));?>
</div>
<span>
	<?php

	$this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'link',
			'type'       => 'primary',
			'label'      =>  Yii::t("total", "Создать номенклатуру"),
			'size'       => 'Normal',
			'url'        => '/mywN?operation=create',
		)
	); ?>
</span>

<?php
$r = new DBnomenclaturegroupname;

$this->widget('bootstrap.widgets.TbGridView', array(
	'type'=>'striped bordered condensed',
	'dataProvider'=> $r->search(' AND FALSE'),
	'template'=>"{items}\n{pager}",
	'ajaxVar' => '',
	'columns'=>array(
		array(
			'name'=>'id',
			'header' => 'Наименование',
		),
		array(
			'name'=>'id',
			'header' => 'Артикул',
		),
		array(
			'name'=>'id',
			'header' => 'Качество',
		),
		array(
			'name'=>'id',
			'header' => 'Ед.измерения',
		),
		array(
			'name'=>'id',
			'header' => 'Остаток по номенклатуре (в наличии)',
		),
	),
	'enablePagination' => true,
	'pager' => array(
		'class' => 'CLinkPager',
		'cssFile' => false,
		'header' => false,
		'firstPageLabel' => 'Начало',
		'prevPageLabel' => 'Назад',
		'nextPageLabel' => 'Вперед',
		'lastPageLabel' => 'Конец',
	),
));
?>