<div>
	<?php

	$this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'link',
			'type'       => 'primary',
			'label'      =>  Yii::t("total", "Просмотр заявок на поставку"),
			'size'       => 'Normal',
			'url'        => '/mywDG',
		)
	); ?>

</div>
<br><br>
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'horizontalForm',
	'type'=>'inline',
)); ?>

<style>

	table tr  td{
		text-align: right;
		width:200px;
		border:  0px double black;
	}
	#NomenclatureForm_comment{
		width: 90%;
	}
	.menu-center-right .btn{
		width: 200px;
		padding: 0px;
		border-style: unset;
		height: 22px;
		margin-top: 2px;
	}
	input.span1, textarea.span1, .uneditable-input.span1{
		width: 50px;
	}
</style>
<table>
	<?php

	print
		'<tr>'.
		'<td><label>'.$data['model']->attributeLabels()['number'].'</label></td>'.
		'<td >'.$form->textFieldRow($data['model'], 'number',array('class'=>'span3')).'</td>'.

		'<td><label>'.$data['model']->attributeLabels()['creationDate'].'</label></td>'.
		'<td >'.$form->textFieldRow($data['model'], 'creationDate',array('class'=>'span3')).'</td>'.

		'</tr>'.
		'<tr>'.
		'<td ><label>'.$data['model']->attributeLabels()['provider'].'</label></td>'.
		'<td>'.$form->textFieldRow($data['model'], 'provider',array('class'=>'span3')).'</td>'.
		'<td><label>'.$data['model']->attributeLabels()['client'].'</label></td>'.
		'<td>'.$form->textFieldRow($data['model'], 'client',array('class'=>'span3')).'</td>'.
		'</tr>'.
		'<tr>'.

		'<td colspan=2>';
		$this->widget('bootstrap.widgets.TbButton', array(
				'buttonType' => 'link',
				'type'       => 'primary',
				'label'      =>  Yii::t("total", "Заявка на подачу транспорта"),
				'size'       => 'Normal',
				'htmlOptions'=> array('style' => 'float: left; margin: 0 0 0 110px'),
				'url'        => '/getcur',
			)
		);
		print '</td>'.
		'</tr>'.


		'</tr>'
	;
	?>
</table>

<br><br>
Выбор Артикула:
<?=$form->dropDownList($data['model'], 'article' ,$data['data']['article'], array('class'=>'span3 choose'));?>
<button class="add">Добавить в заявку</button>

<?php

$r = new DBnomenclaturegroupname;
$this->widget('bootstrap.widgets.TbGridView', array(
	'type'=>'striped bordered condensed',
	'dataProvider'=> $r->search(' AND FALSE'),
	'template'=>"{items}\n{pager}",
	'ajaxVar' => '',
	'columns'=>array(
		array(
			'name'=>'id',
			'header' => '№ п/п',
		),
		array(
			'name'=>'id',
			'header' => 'Наименование',
		),
		array(
			'name'=>'id',
			'header' => 'Артикул',
		),
		array(
			'name'=>'id',
			'header' => 'Ед.измерения',
		),
		array(
			'name'=>'id',
			'header' => 'Количество (по заявке)',
		),
		array(
			'name'=>'id',
			'header' => 'Цена с НДС',
		),
		array(
			'name'=>'id',
			'header' => 'Сумма с НДС',
		),
		array(
			'name'=>'id',
			'header' => 'Ставка НДС',
		),
		array(
			'name'=>'id',
			'header' => 'НДС',
		),
	),
	'enablePagination' => true,
	'pager' => array(
		'class' => 'CLinkPager',
		'cssFile' => false,
		'header' => false,
		'firstPageLabel' => 'Начало',
		'prevPageLabel' => 'Назад',
		'nextPageLabel' => 'Вперед',
		'lastPageLabel' => 'Конец',
	),
));
?>
<?php $this->endWidget(); ?>
<?php
$this->widget('bootstrap.widgets.TbButton', array(
		'buttonType' => 'submit',
		'type'       => 'primary',
		'label'      =>  Yii::t("total", "сохранить"),
		'size'       => 'Normal',
		'htmlOptions'=> array('style' => 'float: left; margin: 0 0 0 110px'),
	)
);
