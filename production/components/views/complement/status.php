<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('js').DS.'/datepicker/jquery-ui.js'));?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('js').DS.'/datepicker/jquery.ui.datepicker-ru.js'));?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('js').DS.'/megapolis.complement.js'));?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('css').DS.'/datepicker/jquery-ui.css')); ?>
<H2>Изменение статус заявки</H2>
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'File',
    'htmlOptions'=>array('enctype'=>'multipart/form-data'),
    'type'=>'horizontal',
)); 
?>

<?php echo $form->textFieldRow($model, 'id', array('class'=>'span1','disabled'=>true,)); ?>
<?php echo $form->textFieldRow($model, 'date_shipping'); ?>
<?php echo $form->dropDownListRow($model, 'edit', array('', 'к отгрузке')); ?>

<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit',
    'type'=>'null', 
    'label'=>'Загрузить')
    ); 
?>
<?php $this->endWidget(); ?>