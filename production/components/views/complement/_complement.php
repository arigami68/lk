
<?php
    $this->widget('bootstrap.widgets.TbGridView', array(
        'type'=>'striped bordered condensed',
        'dataProvider'=>  $model->search(),
        'template'=>"{items}\n{pager}",
        'columns'=>array(
            array(
                'name'=>'id_idapplication',
                'header'=>'№ пл'
            ),
            array(
                'name'=>'date_create',
                'header'=>'Дата загрузки',
            ),
            array(
                'name'=> '',
                'header'=>'Планируемая дата поступления',
                'value' => 'PRTI::date_delivery($data)',
                'type' => 'raw',
                
            ),
            array(
                'name'=>'status',
                'header'=>'Действия',
                'value' => 'PRTI::status($data)',
                'type'  => 'raw',
            ),
        ),
));
?>