<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('js').DS.'/megapolis.complement.js'));?>
<?php Yii::app()->clientScript->registerCssFile('/production/css/megapolis.complement.css');?>
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'File',
    'htmlOptions'=>array('enctype'=>'multipart/form-data'),
    'type'=>'horizontal',
)); ?>

<?php $form->widget('bootstrap.widgets.TbAlert', array(
    'closeText'=> false, // close link text - if set to false, no close link is displayed
    'alerts'=>array( // configurations per alert type
        'htmlOptions'=>array('class'=>'span1'),
        'error'=>array('block'=>true, 'fade'=>true, ), // success, info, warning, error or danger
    ),
)); ?>
<div id="fileLoad">
    <?php echo $form->fileFieldRow($model, 'file'); ?>
    <span id="fileformlabel"></span>
</div>
<div class="load-automatic-form-actions">
    <div id="yt1"></div>
   <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit',
        'type'=>'null', 
        'label'=>'Загрузить')
        ); 
   ?>

</div>
<?php $this->endWidget(); ?>