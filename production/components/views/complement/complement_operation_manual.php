<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('js').DS.'/megapolis.complement.js'));?>
<?php Yii::app()->clientScript->registerCssFile('/production/css/megapolis.complement.css');?>

<?
if(true){
    $com = new stdClass();
    $com->tbtabs = new stdClass();
    $com->process = new stdClass();
    $com->process->main = array('name' => 'main');
    $com->text = new stdClass();
    $model->bailor = Yii::app()->user->auth->name;
}
?>
<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'createOrder',
    'type'=>'horizontal',
));

?>
<?php

    ($operation != 'show') ? $com->tbtabs->main[] = array('label'=>'+', 'linkOptions' => array('id'=>'addOrders')) : NULL;
    $com->tbtabs->add_name = array('label'=>'+', 'linkOptions' => array('id'=>'addGoods'));
    echo ($operation != 'show') ? $form->textFieldRow($model,'id',array('rows'=>2, 'class'=>'span7')) : $form->textFieldRowDisabled($model,'id',array('rows'=>2, 'class'=>'span7'));
    echo ($operation != 'show') ? $form->textFieldRow($model,'keeper',array('rows'=>2, 'class'=>'span7')) : $form->textFieldRowDisabled($model,'keeper',array('rows'=>2, 'class'=>'span7'));
    echo ($operation != 'show') ? $form->textFieldRow($model,'bailor',array('rows'=>2, 'class'=>'span7')) : $form->textFieldRowDisabled($model,'bailor',array('rows'=>2, 'class'=>'span7'));
    $product = $model;
    $c=0;
    $cname = '';
    
    if($model->product){
    foreach($model->product as $k=>$v){
        
            $key = $v->act;
            $AI = ++$k;
            $model = $v;
            $com->text->main[$key] = '';
            if($key != $cname) $c++;
            $cname = $key;

            ($operation != 'show') ? $com->text->main[$key].= '<div class="deleteOrders"><a name=yw9_tab_'.($c+1).' class=order'.($c).' href=#>Удалить</a></div>' : NULL;

            $com->name = 'act';
            $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($model).'['.$com->process->main['name'].']'.'['.$AI.']['.$com->name.']');
            $com->text->main[$key].= ($operation != 'show') ? $form->textFieldRow($model,$com->name, $htmloption) : $form->textFieldRowDisabled($model,$com->name, $htmloption);

            $com->name = 'surname';
            $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($model).'['.$com->process->main['name'].']'.'['.$AI.']['.$com->name.']');
            $com->text->main[$key].= ($operation != 'show') ? $form->textFieldRow($model,$com->name, $htmloption) : $form->textFieldRowDisabled($model,$com->name, $htmloption);

            $com->name = 'name';
            $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($model).'['.$com->process->main['name'].']'.'['.$AI.']['.$com->name.']');
            $com->text->main[$key].= ($operation != 'show') ? $form->textFieldRow($model,$com->name, $htmloption) : $form->textFieldRowDisabled($model,$com->name, $htmloption);

            $com->name = 'lastname';
            $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($model).'['.$com->process->main['name'].']'.'['.$AI.']['.$com->name.']');
            $com->text->main[$key].= ($operation != 'show') ? $form->textFieldRow($model,$com->name, $htmloption) : $form->textFieldRowDisabled($model,$com->name, $htmloption);

            $com->name = 'phone';
            $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($model).'['.$com->process->main['name'].']'.'['.$AI.']['.$com->name.']');
            $com->text->main[$key].= ($operation != 'show') ? $form->textFieldRow($model,$com->name, $htmloption) : $form->textFieldRowDisabled($model,$com->name, $htmloption);

            $com->name = 'country';
            $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($model).'['.$com->process->main['name'].']'.'['.$AI.']['.$com->name.']');
            $com->text->main[$key].= ($operation != 'show') ? $form->textFieldRow($model,$com->name, $htmloption) : $form->textFieldRowDisabled($model,$com->name, $htmloption);

            $com->name = 'region';
            $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($model).'['.$com->process->main['name'].']'.'['.$AI.']['.$com->name.']');
            $com->text->main[$key].= ($operation != 'show') ? $form->textFieldRow($model,$com->name, $htmloption) : $form->textFieldRowDisabled($model,$com->name, $htmloption);

            $com->name = 'area';
            $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($model).'['.$com->process->main['name'].']'.'['.$AI.']['.$com->name.']');
            $com->text->main[$key].= ($operation != 'show') ? $form->textFieldRow($model,$com->name, $htmloption) : $form->textFieldRowDisabled($model,$com->name, $htmloption);

            $com->name = 'delivery';
            $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($model).'['.$com->process->main['name'].']'.'['.$AI.']['.$com->name.']');
            $com->text->main[$key].= ($operation != 'show') ? $form->textFieldRow($model,$com->name, $htmloption) : $form->textFieldRowDisabled($model,$com->name, $htmloption);

            $com->name = 'zip';
            $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($model).'['.$com->process->main['name'].']'.'['.$AI.']['.$com->name.']');
            $com->text->main[$key].= ($operation != 'show') ? $form->textFieldRow($model,$com->name, $htmloption) : $form->textFieldRowDisabled($model,$com->name, $htmloption);

            $com->name = 'street';
            $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($model).'['.$com->process->main['name'].']'.'['.$AI.']['.$com->name.']');
            $com->text->main[$key].= ($operation != 'show') ? $form->textFieldRow($model,$com->name, $htmloption) : $form->textFieldRowDisabled($model,$com->name, $htmloption);

            $com->name = 'house';
            $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($model).'['.$com->process->main['name'].']'.'['.$AI.']['.$com->name.']');
            $com->text->main[$key].= ($operation != 'show') ? $form->textFieldRow($model,$com->name, $htmloption) : $form->textFieldRowDisabled($model,$com->name, $htmloption);

            $com->name = 'nod';
            $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($model).'['.$com->process->main['name'].']'.'['.$AI.']['.$com->name.']');
            $com->text->main[$key].= ($operation != 'show') ? $form->textFieldRow($model,$com->name, $htmloption) : $form->textFieldRowDisabled($model,$com->name, $htmloption);

            $com->name = 'viewdep';
            $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($model).'['.$com->process->main['name'].']'.'['.$AI.']['.$com->name.']');
            $com->text->main[$key].= ($operation != 'show') ? $form->textFieldRow($model,$com->name, $htmloption) : $form->textFieldRowDisabled($model,$com->name, $htmloption);

            $com->name = 'coder';
            $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($model).'['.$com->process->main['name'].']'.'['.$AI.']['.$com->name.']');
            $com->text->main[$key].= ($operation != 'show') ? $form->textFieldRow($model,$com->name, $htmloption) : $form->textFieldRowDisabled($model,$com->name, $htmloption);

            $com->name = 'body';
            $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($model).'['.$com->process->main['name'].']'.'['.$AI.']['.$com->name.']');
            $com->text->main[$key].= ($operation != 'show') ? $form->textFieldRow($model,$com->name, $htmloption) : $form->textFieldRowDisabled($model,$com->name, $htmloption);

            $com->name = 'floor';
            $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($model).'['.$com->process->main['name'].']'.'['.$AI.']['.$com->name.']');
            $com->text->main[$key].= ($operation != 'show') ? $form->textFieldRow($model,$com->name, $htmloption) : $form->textFieldRowDisabled($model,$com->name, $htmloption);

            $com->name = 'office';
            $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($model).'['.$com->process->main['name'].']'.'['.$AI.']['.$com->name.']');
            $com->text->main[$key].= ($operation != 'show') ? $form->textFieldRow($model,$com->name, $htmloption) : $form->textFieldRowDisabled($model,$com->name, $htmloption);

            $com->name = 'categorydep';
            $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($model).'['.$com->process->main['name'].']'.'['.$AI.']['.$com->name.']');
            $com->text->main[$key].= ($operation != 'show') ? $form->textFieldRow($model,$com->name, $htmloption) : $form->textFieldRowDisabled($model,$com->name, $htmloption);

            $com->name = 'aoii';
            $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($model).'['.$com->process->main['name'].']'.'['.$AI.']['.$com->name.']');
            $com->text->main[$key].= ($operation != 'show') ? $form->textFieldRow($model,$com->name, $htmloption) : $form->textFieldRowDisabled($model,$com->name, $htmloption);

            $com->name = 'flagnotif';
            $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($model).'['.$com->process->main['name'].']'.'['.$AI.']['.$com->name.']');
            $com->text->main[$key].= ($operation != 'show') ? $form->textFieldRow($model,$com->name, $htmloption) : $form->textFieldRowDisabled($model,$com->name, $htmloption);

            $com->name = 'viewnotif';
            $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($model).'['.$com->process->main['name'].']'.'['.$AI.']['.$com->name.']');
            $com->text->main[$key].= ($operation != 'show') ? $form->textFieldRow($model,$com->name, $htmloption) : $form->textFieldRowDisabled($model,$com->name, $htmloption);
            
            $com->name = 'fragility';
            $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($model).'['.$com->process->main['name'].']'.'['.$AI.']['.$com->name.']');
            $com->text->main[$key].= ($operation != 'show') ? $form->textFieldRow($model,$com->name, $htmloption) : $form->textFieldRowDisabled($model,$com->name, $htmloption);

            $com->name = 'note';
            $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($model).'['.$com->process->main['name'].']'.'['.$AI.']['.$com->name.']');
            $com->text->main[$key].= ($operation != 'show') ? $form->textFieldRow($model,$com->name, $htmloption) : $form->textFieldRowDisabled($model,$com->name, $htmloption);

            $com->name = 'codei';
            $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($model).'['.$com->process->main['name'].']'.'['.$AI.']['.$com->name.']');
            $com->text->main[$key].= ($operation != 'show') ? $form->textFieldRow($model,$com->name, $htmloption) : $form->textFieldRowDisabled($model,$com->name, $htmloption);

            $b=0;
            unset($com->tbtabs->add);
            ($operation != 'show') ? $com->tbtabs->add[] = $com->tbtabs->add_name : NULL;
            $AI2 = $AI;
            foreach($product->product as $k=>$v2){
                if($v2->act != $key) continue;
                    
                    $key_add =$b++;
                    $goods = new stdClass();
                    $goods->model = new stdClass();
                    $goods->model->active = $v2;
                    $goods->process->main['name'] = 'add';
                    $goods->array['orders'] = $AI;
                    ++$AI2;
                    $com->text->add[$key][$key_add] = '';
                    ($operation != 'show') ? $com->text->add[$key][$key_add].= '<div class="deleteGoods"><a class=goods'.$key_add.' href=#>Удалить</a></div>' : NULL;

                    $goods->name = 'tdv';
                    $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($goods->model->active).'['.$goods->process->main['name'].']'.'['.$goods->array['orders'].']'.'['.$AI2.']['.$goods->name.']');
                    $com->text->add[$key][$key_add].= ($operation != 'show') ? $form->textFieldRow($goods->model->active,$goods->name, $htmloption) : $form->textFieldRowDisabled($goods->model->active,$goods->name, $htmloption);

                    $goods->name = 'codamount';
                    $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($goods->model->active).'['.$goods->process->main['name'].']'.'['.$goods->array['orders'].']'.'['.$AI2.']['.$goods->name.']');
                    $com->text->add[$key][$key_add].= ($operation != 'show') ? $form->textFieldRow($goods->model->active,$goods->name, $htmloption) : $form->textFieldRowDisabled($goods->model->active,$goods->name, $htmloption);

                    $goods->name = 'accpack';
                    $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($goods->model->active).'['.$goods->process->main['name'].']'.'['.$goods->array['orders'].']'.'['.$AI2.']['.$goods->name.']');
                    $com->text->add[$key][$key_add].= ($operation != 'show') ? $form->textFieldRow($goods->model->active,$goods->name, $htmloption) : $form->textFieldRowDisabled($goods->model->active,$goods->name, $htmloption);

                    $goods->name = 'viewaccpack';
                    $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($goods->model->active).'['.$goods->process->main['name'].']'.'['.$goods->array['orders'].']'.'['.$AI2.']['.$goods->name.']');
                    $com->text->add[$key][$key_add].= ($operation != 'show') ? $form->textFieldRow($goods->model->active,$goods->name, $htmloption) : $form->textFieldRowDisabled($goods->model->active,$goods->name, $htmloption);

                    $goods->name = 'weightdep';
                    $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($goods->model->active).'['.$goods->process->main['name'].']'.'['.$goods->array['orders'].']'.'['.$AI2.']['.$goods->name.']');
                    $com->text->add[$key][$key_add].= ($operation != 'show') ? $form->textFieldRow($goods->model->active,$goods->name, $htmloption) : $form->textFieldRowDisabled($goods->model->active,$goods->name, $htmloption);

                    $goods->name = 'artical';
                    $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($goods->model->active).'['.$goods->process->main['name'].']'.'['.$goods->array['orders'].']'.'['.$AI2.']['.$goods->name.']');
                    $com->text->add[$key][$key_add].= ($operation != 'show') ? $form->textFieldRow($goods->model->active,$goods->name, $htmloption) : $form->textFieldRowDisabled($goods->model->active,$goods->name, $htmloption);

                    $goods->name = 'attachments';
                    $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($goods->model->active).'['.$goods->process->main['name'].']'.'['.$goods->array['orders'].']'.'['.$AI2.']['.$goods->name.']');
                    $com->text->add[$key][$key_add].= ($operation != 'show') ? $form->textFieldRow($goods->model->active,$goods->name, $htmloption) : $form->textFieldRowDisabled($goods->model->active,$goods->name, $htmloption);

                    $goods->name = 'nomencl';
                    $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($goods->model->active).'['.$goods->process->main['name'].']'.'['.$goods->array['orders'].']'.'['.$AI2.']['.$goods->name.']');
                    $com->text->add[$key][$key_add].= ($operation != 'show') ? $form->textFieldRow($goods->model->active,$goods->name, $htmloption) : $form->textFieldRowDisabled($goods->model->active,$goods->name, $htmloption);

                    $goods->name = 'unit';
                    $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($goods->model->active).'['.$goods->process->main['name'].']'.'['.$goods->array['orders'].']'.'['.$AI2.']['.$goods->name.']');
                    $com->text->add[$key][$key_add].= ($operation != 'show') ? $form->textFieldRow($goods->model->active,$goods->name, $htmloption) : $form->textFieldRowDisabled($goods->model->active,$goods->name, $htmloption);

                    $goods->name = 'amount';
                    $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($goods->model->active).'['.$goods->process->main['name'].']'.'['.$goods->array['orders'].']'.'['.$AI2.']['.$goods->name.']');
                    $com->text->add[$key][$key_add].= ($operation != 'show') ? $form->textFieldRow($goods->model->active,$goods->name, $htmloption) : $form->textFieldRowDisabled($goods->model->active,$goods->name, $htmloption);

                    $goods->name = 'implprice';
                    $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($goods->model->active).'['.$goods->process->main['name'].']'.'['.$goods->array['orders'].']'.'['.$AI2.']['.$goods->name.']');
                    $com->text->add[$key][$key_add].= ($operation != 'show') ? $form->textFieldRow($goods->model->active,$goods->name, $htmloption) : $form->textFieldRowDisabled($goods->model->active,$goods->name, $htmloption);

                    $goods->name = 'implsum';
                    $htmloption = array('rows'=>2, 'class'=>'span7','name' => get_class($goods->model->active).'['.$goods->process->main['name'].']'.'['.$goods->array['orders'].']'.'['.$AI2.']['.$goods->name.']');
                    $com->text->add[$key][$key_add].= ($operation != 'show') ? $form->textFieldRow($goods->model->active,$goods->name, $htmloption) : $form->textFieldRowDisabled($goods->model->active,$goods->name, $htmloption);
                    
                    $com->tbtabs->add[] = array('label'=>'Товар'.($key_add+1), 'content' => $com->text->add[$key][$key_add], 'linkOptions' => array('id' => 'goods'.$key_add));
            }
            
            
            ob_start();
            $this->widget('bootstrap.widgets.TbTabs', array(
             'id' => 'yw'.($AI+15).'_tab_'.$AI,
             'type'=>'tabs',
             'placement'=>'top', // 'above', 'right', 'below' or 'left'
             'tabs'=> isset($com->tbtabs->add) ? $com->tbtabs->add : array(),
            ));
            $out[$key] = ob_get_contents();
            ob_end_clean();
            
            $com->text->main[$key].=$out[$key];
            
            #$com->text->main[$key].= '<div id="yw'.$goods->array['orders'].'_tab_'.$AI.'"  class="tab-pane">'.$out[$key].'</div>';
        }
        
        $f=0;
        foreach($com->text->main as $k=>$v){
            $order['num'] = ++$f;
            $order['name'] = 'Заказ №';
            $order['link'] = 'order';
            $order['content'] = $v;
            # Вывод накопленных сообщений
            $com->tbtabs->main[]= array('label'=>$order['name'].$order['num'], 'content' => $order['content'], 'active'=> ($order['num'] == 1 ? true : false), 'linkOptions' => array('id' => $order['link'].$order['num']));
        }
    }
    
    $this->widget('bootstrap.widgets.TbTabs', array(
            'type'=>'tabs',
            'placement'=>'top', // 'above', 'right', 'below' or 'left'
            'tabs'=> isset($com->tbtabs->main) ? $com->tbtabs->main : array(),
    ));
    print "<div style='width: 671px; float: left; margin-right: 18px; margin-left: 180px'>";
    ($operation != 'show') ? (
            $this->widget('bootstrap.widgets.TbButton', array(
                    'buttonType' => 'submit',
                    'type'       => 'primary',
                    'label'      => 'Сохранить заявку',
                    'size'       => 'Normal',
                )
            )        
    )
    : '';
    print '</div>';
?>

<?php $this->endWidget(); ?>