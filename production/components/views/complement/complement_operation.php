<?php Yii::app()->clientScript->registerCssFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('css').DS.'jquery.jgrowl.min.css'));?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('js').DS.'jquery.jgrowl.min.js'));?>

<?php
$url= '/com?action=operation';
?>
<div>
    <?php  $this->widget('bootstrap.widgets.TbButton', array(
        'label'=>'Просмотр заданий',
        'type'=> 'info', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'url' => Yii::app()->createUrl('/com?action=show'),
    )); ?>
</div>
<br>

<?php $this->widget('bootstrap.widgets.TbButtonGroup', array(
    'type' => 'primary',
    'toggle' => 'radio',
    'buttons'=>array(
        array('label'=>'Вручную', 'url'=> Yii::app()->createUrl($url.'&load=manual') , 'htmlOptions'=>array('id'=> 'check_manual'),),
        array('label'=>'Загрузить файл', 'url'=> Yii::app()->createUrl($url.'&load=auto'),  'htmlOptions'=>array('class'=> 'load', 'id'=> 'check_automatic'),),
    ),
));
?>
<br><br><br>
<?php
    if(Yii::app()->request->getQuery('load') == 'auto'){
       
        $this->beginContent('views.complement.complement_operation_auto', array('model' => $model));
        $this->endContent();
        return TRUE;
    }
     $this->beginContent('views.complement.complement_operation_manual', array('model' => $model, 'operation' => isset($operation) ? $operation : 'new'));
     $this->endContent();
?>