<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('js').DS.'jquery.jgrowl.min.js'));?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('css').DS.'jquery.jgrowl.min.css'));?>

<div>
    <?php  $this->widget('bootstrap.widgets.TbButton', array(
        'label'=>'Загрузка заданий',
        'type'=> 'info', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'url' => Yii::app()->createUrl('/com?action=operation'),
    )); ?>
</div>
<?php $this->renderPartial('application.components.views.complement._complement',array(
        'model'=>$model,
      ));
?>