<?php

$this->widget('bootstrap.widgets.TbButtonGroup', array(
	'type' => 'primary',
	'toggle' => 'radio',
	'buttons'=>array(
		array('label'=> Yii::t("total", 'Создать шаблон'), 		'url'=>'?f=settings' ,'htmlOptions'=>array('class'=> 'load', 'id'=> 'check_automatic'),),
		array('label'=> Yii::t("total", 'В работе'), 		'url'=>'?f=stat' , 'htmlOptions'=>array('id'=> 'check_manual'),),
		array('label'=> Yii::t("total", 'Подробный отчет'), 'url'=>'?f=report' ,'htmlOptions'=>array('class'=> 'load', 'id'=> 'check_automatic'),),
	),
));

$path = __DIR__.DS.$array['link'].'.php';
$h =	$array['default']['HH'];
$m = 	$array['default']['MM'];

if($file = file_exists($path)){
	require_once $path;
}