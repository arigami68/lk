<?php
$db = $array['db'];
$this->widget('bootstrap.widgets.TbGridView', array(
	'type'=>'striped bordered condensed',
	'dataProvider'=>  $db->view(),
	'template'=>"{items}\n{pager}",
	'columns'=>array(
		array(
			'name'=>'id',
			'header'=>'№ п/п',
			'htmlOptions'=>array('style'=>'width: 10px'),
		),
		array(
			'name'=>'od',
			'header'=>'Оператор доставки',
			'value' => function($data){
					if(!isset($data->valueod->od)) return false;
					return $data->valueod->od;
			 },
			'htmlOptions'=>array('style'=>'width: 20px '),
		),
		array(
			'name'=>'status',
			'header'=>'Статус',
			'value' => function($data){
					if(!isset($data->valuestatus->status)) return false;
					return $data->valuestatus->status;
			 },
			'htmlOptions'=>array('style'=>'width: 20px '),
		),
		array(
			'name'=>'number',
			'header'=>'Номер SMS по счету',
			'htmlOptions'=>array('style'=>'width: 20px '),
		),
		array(
			'name'=>'text',
			'header'=>'Текст',
		),
		array(
			'name'=>'timeUTC',
			'header'=>'Местное время',
		),
		array(
			'name'=>'work',
			'header'=>'Состояние',
			'value' => function($data){
				return $data->work ? 'Работает' : 'Остановлено';
			},
		),
		array(
			'name'	=>'timeShipping',
			'header'=>'Время отправления',
			'value' => function($data){
					$array = DBMSmsStatus::getConvertTime($data);
					$fromHH = $array['fromHH'];
					$fromMM = $array['fromMM'];
					$toHH 	= $array['toHH'];
					$toMM 	= $array['toMM'];

					$fromHH = strlen($fromHH) == 1 ? '0'.$fromHH : $fromHH;
					$fromMM = strlen($fromMM) == 1 ? '0'.$fromMM : $fromMM;
					$toHH = strlen($toHH) == 1 ? '0'.$toHH : $toHH;
					$toMM = strlen($toMM) == 1 ? '0'.$toMM : $toMM;
					$string = $fromHH.':'.$fromMM.' - '.$toHH.':'.$toMM;

					return $string;
				},
		),
		array(
			'name'=>'operation',
			'header'=>'Операции',
			'value' => function($data){
					$g1 	= '&od='.$data->od.'&status='.$data->status.'&sms_num='.$data->number;
					$s1 	= '?f=settings';
					$s2 	= '?f=stat';
					$work 	= '&work='.($data->work ? 0 : 1);
					$from   = '&from='.$data->timeFrom;
					$to   	= '&to='.$data->timeTo;
					$delete = '&delete=1';

					$lang = array();
					$lang['work'] = $data->work ? "Выключение данной рассылки" : "Включение данной рассылки";
					$text = '';
					$text.= '<a href="'.$s1.$g1.$from.$to.'" class="icon-pencil" rel="tooltip" title="Редактирование статусов" ></a>';
					$text.= '&nbsp';
					$text.= '<a href="'.$s2.$g1.$work.'" class="icon-off" rel="tooltip" title="'.$lang['work'].'" ></a>';
					$text.= '&nbsp';
					$text.= '<a href="'.$s2.$g1.$delete.'" class="icon-remove" rel="tooltip" title="Удаление шаблона" ></a>';

					return $text;
			},
			'type' => 'raw',
			'htmlOptions'=>array('style'=>'width: 30px '),
		),
	),
	'pager' => array(
		'class' => 'CLinkPager',
		'cssFile' => false,
		'header' => false,
		'firstPageLabel' => '<<',
		'prevPageLabel' => '<',
		'nextPageLabel' => '>',
		'lastPageLabel' => '>>',
	),
));