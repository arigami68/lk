<?php
$db = $array['db'];
$this->widget('bootstrap.widgets.TbGridView', array(
	'type'=>'striped bordered condensed',
	'dataProvider'=>  $db->report(),
	'template'=>"{items}\n{pager}",
	'columns'=>array(
		array(
			'name'=>'id',
			'header'=>'№ п/п',
			'htmlOptions'=>array('style'=>'width: 10px'),
		),
		array(
			'name'=>'agent',
			'header'=>'Номер ОД',
		),
		array(
			'name'=>'megapolis',
			'header'=>'Клиентский номер',
		),
		array(
			'name'=>'dateRecept',
			'header'=>'Дата приема',
			'htmlOptions'=>array('style'=>'width: 10px'),
		),
		array(
			'name'=>'nameClient',
			'header'=>'Наименование отправителя',
		),
		array(
			'name'=>'OC',
			'header'=>'ОЦ, руб',
		),
		array(
			'name'=>'NP',
			'header'=>'НП, руб',
		),
		array(
			'name'=>'fio',
			'header'=>'ФИО получателя',
		),
		array(
			'name'=>'addressDelivery',
			'header'=>'Адрес доставки',
		),
		array(
			'name'=>'phone',
			'header'=>'Телефон',
		),
		array(
			'name'=>'status',
			'header'=>'Статус отправления',
		),
		array(
			'name'=>'dateStatus',
			'header'=>'Дата статуса отправления',
		),
		array(
			'name'=>'amount',
			'header'=>'Количество СМС',
		),
		array(
			'name'=>'amount',
			'header'=>'Стоимость за доставку СМС',
			'type' => 'raw',
			'value' => function($data){
					$v = ' руб.';
					return $data->num * Yii::app()->user->auth->db->costSms.$v;
			}
		),
	),
	'pager' => array(
		'class' => 'CLinkPager',
		'cssFile' => false,
		'header' => false,
		'firstPageLabel' => '<<',
		'prevPageLabel' => '<',
		'nextPageLabel' => '>',
		'lastPageLabel' => '>>',
	),
));