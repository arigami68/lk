<?php
	$arrayTime = DBSmsStatus::getConvertTime($array['default']);

?>

<div class="settings">
	<div id="message" role="alert"></div>
		<div class="param">
			<div class="select">
				<div class="select-text">Выберите Оператора Доставки:</div>
				<?=CHtml::dropDownList('listname_od', $array['default']['od'], $array['od'], array ('onChange' => "save_template(0, 3)"));?>
			</div>
			<div class="select">
				<div class="select-text">Выберите Статус:</div>
				<?=CHtml::dropDownList('listname_status', $array['default']['status'], $array['status'], array ('onChange' => "save_template(0, 3)"));?>
			</div>
			<div class="select input_num">
				<div class="select-text">Через сколько дней отправить<br> SMS повторно:</div>
				<?=CHtml::textField('input_num_text',$array['default']['sms_num'], array('class' => 'span1', 'onKeyup' => "save_template(0, 3)"));?> дней
			</div>
			<div class="select">
				<div  class="select-text"><?=CHtml::CheckBox('check_time','', array('checked'=>'checked','value'=>'on'));?>&nbspПо местному времени абонента</div>
			</div>
			<div class="intval select" style="padding-top: 20px;">
				<div class="select-text" >Время отправки смс:</div>
				<div>
					<span style="padding-left: 10px">С <?=CHtml::dropDownList('fromHH', $arrayTime['fromHH'], $h, array('class' => 'span1'));?></span> чч
					<span style="font-weight: bold">:</span>
					<span><?=CHtml::dropDownList('fromMM',  $arrayTime['fromMM'], $m, array('class' => 'span1'));?></span> мм
				</div>
				<div>
					<span>До <?=CHtml::dropDownList('toHH', $arrayTime['toHH'], $h, array('class' => 'span1'));?></span> чч
					<span style="font-weight: bold">:</span>
					<span><?=CHtml::dropDownList('toMM', $arrayTime['toMM'], $m, array('class' => 'span1'));?></span> мм
				</div>
			</div>

		</div>
		<div class="text">
					<textarea
						id='text_sms'
						onblur="set_val(this, 'Введите текст Вашей СМС');"
						onfocus="reset_val(this, 'Введите текст Вашей СМС');"
						onkeyup="set_len_sms(1);"
					><?=$array['default']['text']?></textarea>

					<div class="past">
						<div class="title">Вставка:</div>
						<ul>
							<li><a href="javascript:set_value('text_sms', '#idealogic#')">Номер отправления</a></li>
							<li><a href="javascript:set_value('text_sms', '#number#')">Номер телефона</a></li>
							<li><a href="javascript:set_value('text_sms', '#fio#')">ФИО</a></li>
							<li><a href="javascript:set_value('text_sms', '#od#')">Оператор Доставки</a></li>
							<li><a href="javascript:set_value('text_sms', '#status#')">Статус</a></li>
						</ul>
					</div>

				<div class="text-info">
					<div class="info">Введено <span id="letters_sms">0</span> символов, <span id="num_sms">0</span> СМС.</div>
					<a title="&quot;Транслитерация&quot; &mdash; передача информации на русском языке с помощью латинских символов. Необходима для телефонов, &quot;не понимающих&quot; русские буквы." class="top_link" href="javascript:translit_sms();">Транслитерация</a>
				</div>
			<div class="button">
				<button class="save_template btn btn-primary" onclick="save_template(0, 2)">Сохранить шаблон</button>
			</div>
		</div>



</div>