<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('js').DS.'megapolis.settings.js'));?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('css').DS.'megapolis.settings.css'));?>
<?php
    $this->widget('application.components.widget.settingsWidget',array(
        'model' => $model,
        'setting' => $setting,
    ));
?>