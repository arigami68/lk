<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'createOrder',
    'type'=>'horizontal',
));
?>
<div id="parentView">
    <?php $this->widget('bootstrap.widgets.TbGridView', array(
        'type'=>'striped bordered condensed',
        'dataProvider'=> $model->QuSe($model::$SEARCH_AGENTS),
        'template'=>"{items}\n{pager}",
        'ajaxVar' => '',
        'columns'=>array(
            array(
                'name'  => 'dateCreate',
                'header'=> 'Период',
            ),
            array(
                'name'  => 'megapolis',
                'header'=> 'Номенклатура',
            ),
            array(
                'name'  => 'megapolis',
                'header'=> 'Документ',
            ),
            array(
                'name'  => 'megapolis',
                'header'=> 'Остатки на начало периода',
            ),
            array(
                'name'  => 'megapolis',
                'header'=> 'Оборот (приход)',
            ),
            array(
                'name'  => 'megapolis',
                'header'=> 'Оборот (расход)',
            ),
            array(
                'name'  => 'megapolis',
                'header'=> 'Остатки на конец периода',
            ),
            array(
                'name'  => 'megapolis',
                'header'=> 'Итого',
            ),
            array(
                'name'  => 'megapolis',
                'header'=> 'Шапка',
            ),
        ),
        'enablePagination' => true,
        'pager' => array(
                'class' => 'CLinkPager',
                'cssFile' => false,
                'header' => false,
                'firstPageLabel' => 'Начало',
                'prevPageLabel' => 'Назад',
                'nextPageLabel' => 'Вперед',
                'lastPageLabel' => 'Конец',
         ),
    )); ?>
</div>
<?php $this->endWidget(); ?>