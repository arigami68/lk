<?php Yii::app()->clientScript->registerCssFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('css').DS.'jquery.jgrowl.min.css'));?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('js').DS.'jquery.jgrowl.min.js'));?>

<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'createOrder',
    'type'=>'inline',
));


$messages = Array(
    'full' => '<br><b>Наш оператор с Вами свяжется!<br>Вы можете отредактировать заявку</b><br><br>',
    'feed' => '<br><b>Пожалуйста заполните Вашу информацию. <br> После заполнения Наш менеджер проверит информацию!</b><br><br>',
    'required' => Yii::t("total", "обязательное поле"),
    'opt'      => Yii::t("total", "не обязательное поле"),
);
$labels = $model->attributeLabels();
?>
<?php
/*
            'fullname'              => 'Полное наименование организации',
            'shortname'             => 'Краткое наименование организации',
            'orgform'               => 'Организационно-правовая форма',
            'addressLegal'          => 'Адрес бридический',
            'addressFact'           => 'Адрес фактический',
            'inn'                   => 'ИНН',
            'kpp'                   => 'КПП (только для ООО)',
            'okpo'                  => 'ОКПО',
            'calculatedInvoice'     => 'Расчетный счет',
            'correspondentInvoice'  => 'Корреспондентский счет',
            'bik'                   => 'БИК',
            'fioHEad'               => 'Ф.И.О. руководителя организации',
            'fioAccountant'         => 'Ф.И.О. главного бухгалтера',
            'addressDocuments'      => 'Адрес для доставки документов (счетов, с/ф, актов)',
            'tel'                   => 'Телефон',
            'okgy'                  => 'ОКОГУ',
            'okat'                  => 'ОКАТО',
            'okbed'                 => 'ОКВЭД',
            'okfc'                  => 'ОКФС/ОКОПФ',
            'ogph'                  => 'ОГРН/ОГРНИП',
            'nameBank'              => 'Наименование банка, в т.ч. место (город) нахождения',
            'positionHead'          => 'Должность руководителя организации',
            'fax'                   => 'Факс',
            'mail'                  => 'E-mail',
            'www'                   => 'www',
 */
?>
<?php
if(Yii::app()->user->auth->range['cargo']){
    if($come = COME::model()->user()->operation(COME::$OPERATION_CARGO)->findAll()){
        print '<table>
            <tr>
                <td style="font-weight: bold">
                    <H4>Ваш Диапазон номеров:</H4>
                </td>
            </tr>';

        foreach($come as $v){
            print '
            <tr>
                <td >
                    '.number_format($v->step*10000).' - '.number_format($v->step*10000 + 10000).'
                </td>
            </tr>
            ';
        }
        print'</table>';
    }
}
?>
<table>
<tr>
    <td style="font-weight: bold">
        <H4><?=Yii::t("total", "договора")?></H4>
    </td>
</tr>
<tr>
    <td  style="padding-bottom: 20px">
        <?php print $string;?>
    </td>
</tr>
<tr>
    <td style="font-weight: bold">
        <H4><?=Yii::t("total", "реквизиты")?></H4>
    </td>
</tr>
<tr>
    <td>
        <? #print  (!empty($model->fullname)) ?  $messages['feed'] : $messages['full'] ;?>
    </td>
</tr>
<tr>
    <td style="font-weight: bold; padding: 10px 0px 5px 5px">
        <?=Yii::t("total", "данные о компании")?>
    </td>
</tr>
<tr>

    <td>
       <a href="#" rel="tooltip" title="<?php print $labels['fullname'].'. '.$messages['required']?>" style="margin-left: 10px"><span class="icon-pencil"></span></a>
        <?php echo $form->textFieldRow($model,'fullname',             array('class'=>'span3'));?>
        <a href="#" rel="tooltip" title="<?php print $labels['shortname'].'. '.$messages['required']?>" style="margin-left: 10px"><span class="icon-pencil"></span></a>
        <?php echo $form->textFieldRow($model,'shortname',            array('class'=>'span3'));?>
        <br>
        <a href="#" rel="tooltip" title="<?php print $labels['orgform'].'. '.$messages['required']?>" style="margin-left: 10px"><span class="icon-pencil"></span></a>
        <?php echo $form->textFieldRow($model,'orgform',              array('class'=>'span3'));?>
        <a href="#" rel="tooltip" title="<?php print $labels['addresslegal'].'. '.$messages['required']?>" style="margin-left: 10px"><span class="icon-pencil"></span></a>
        <?php echo $form->textFieldRow($model,'addresslegal',         array('class'=>'span3'));?>
        <br>
        <a href="#" rel="tooltip" title="<?php print $labels['addressfact'].'. '.$messages['required']?>" style="margin-left: 10px"><span class="icon-pencil"></span></a>
        <?php echo $form->textFieldRow($model,'addressfact',          array('class'=>'span3'));?>
        <a href="#" rel="tooltip" title="<?php print $labels['addressdocuments'].'. '.$messages['required']?>" style="margin-left: 10px"><span class="icon-pencil"></span></a>
        <?php echo $form->textFieldRow($model,'addressdocuments',     array('class'=>'span3'));?>
    </td>
</tr>
<tr>
    <td style="font-weight: bold; padding: 10px 0px 5px 5px">
        <?=Yii::t("total", "реквизиты")?>
    </td>
</tr>
<tr>
    <td>
        <a href="#" rel="tooltip" title="<?php print $labels['inn'].'. '.$messages['required']?>" style="margin-left: 10px"><span class="icon-pencil"></span></a>
        <?php echo $form->textFieldRow($model,'inn',                  array('class'=>'span3'));?>
        <a href="#" rel="tooltip" title="<?php print $labels['kpp'].'. '.$messages['required']?>" style="margin-left: 10px"><span class="icon-pencil"></span></a>
        <?php echo $form->textFieldRow($model,'kpp',                  array('class'=>'span3'));?>
        <br>
        <a href="#" rel="tooltip" title="<?php print $labels['okpo'].'. '.$messages['required']?>" style="margin-left: 10px"><span class="icon-pencil"></span></a>
        <?php echo $form->textFieldRow($model,'okpo',                 array('class'=>'span3'));?>
        <a href="#" rel="tooltip" title="<?php print $labels['okogy'].'. '.$messages['opt']?>" style="margin-left: 10px"><span class="icon-flag"></span></a>
        <?php echo $form->textFieldRow($model,'okogy',                array('class'=>'span3'));?>
        <br>
        <a href="#" rel="tooltip" title="<?php print $labels['okato'].'. '.$messages['opt']?>" style="margin-left: 10px"><span class="icon-flag"></span></a>
        <?php echo $form->textFieldRow($model,'okato',                array('class'=>'span3'));?>
        <a href="#" rel="tooltip" title="<?php print $labels['okbed'].'. '.$messages['opt']?>" style="margin-left: 10px"><span class="icon-flag"></span></a>
        <?php echo $form->textFieldRow($model,'okbed',                array('class'=>'span3'));?>
        <br>
        <a href="#" rel="tooltip" title="<?php print $labels['okfc'].'. '.$messages['opt']?>" style="margin-left: 10px"><span class="icon-flag"></span></a>
        <?php echo $form->textFieldRow($model,'okfc',                 array('class'=>'span3'));?>
        <a href="#" rel="tooltip" title="<?php print $labels['ogph'].'. '.$messages['opt']?>" style="margin-left: 10px"><span class="icon-flag"></span></a>
        <?php echo $form->textFieldRow($model,'ogph',                 array('class'=>'span3'));?>
        <br>
        <a href="#" rel="tooltip" title="<?php print $labels['namebank'].'. '.$messages['opt']?>" style="margin-left: 10px"><span class="icon-flag"></span></a>
        <?php echo $form->textFieldRow($model,'namebank',             array('class'=>'span3'));?>
        <a href="#" rel="tooltip" title="<?php print $labels['calculatedinvoice'].'. '.$messages['required']?>" style="margin-left: 10px"><span class="icon-pencil"></span></a>
        <?php echo $form->textFieldRow($model,'calculatedinvoice',    array('class'=>'span3'));?>
        <br>
        <a href="#" rel="tooltip" title="<?php print $labels['correspondentinvoice'].'. '.$messages['required']?>" style="margin-left: 10px"><span class="icon-pencil"></span></a>
        <?php echo $form->textFieldRow($model,'correspondentinvoice', array('class'=>'span3'));?>
        <a href="#" rel="tooltip" title="<?php print $labels['bik'].'. '.$messages['required']?>" style="margin-left: 10px"><span class="icon-pencil"></span></a>
        <?php echo $form->textFieldRow($model,'bik',                  array('class'=>'span3'));?>
    </td>
</tr>
<tr>
    <td style="font-weight: bold; padding: 10px 0px 5px 5px">
        <?=Yii::t("total", "краткая информация")?>
    </td>
</tr>
<tr>
    <td>
        <a href="#" rel="tooltip" title="<?php print $labels['positionhead'].'. '.$messages['opt']?>" style="margin-left: 10px"><span class="icon-flag"></span></a>
        <?php echo $form->textFieldRow($model,'positionhead',         array('class'=>'span3'));?>
        <a href="#" rel="tooltip" title="<?php print $labels['fiohead'].'. '.$messages['required']?>" style="margin-left: 10px"><span class="icon-pencil"></span></a>
        <?php echo $form->textFieldRow($model,'fiohead',              array('class'=>'span3'));?>
        <br>
        <a href="#" rel="tooltip" title="<?php print $labels['fioaccountant'].'. '.$messages['required']?>" style="margin-left: 10px"><span class="icon-pencil"></span></a>
        <?php echo $form->textFieldRow($model,'fioaccountant',        array('class'=>'span3'));?>
        <a href="#" rel="tooltip" title="<?php print $labels['tel'].'. '.$messages['required']?>" style="margin-left: 10px"><span class="icon-pencil"></span></a>
        <?php echo $form->textFieldRow($model,'tel',                  array('class'=>'span3'));?>
        <br>
        <a href="#" rel="tooltip" title="<?php print $labels['mail_np'].'. '.$messages['opt']?>" style="margin-left: 10px"><span class="icon-flag"></span></a>
        <?php echo $form->textFieldRow($model,'mail_np',                 array('class'=>'span3'));?>
        <a href="#" rel="tooltip" title="<?php print $labels['mail_notice'].'. '.$messages['opt']?>" style="margin-left: 10px"><span class="icon-flag"></span></a>
        <?php echo $form->textFieldRow($model,'mail_notice',                 array('class'=>'span3'));?>
        <br>
        <a href="#" rel="tooltip" title="<?php print $labels['fax'].'. '.$messages['opt']?>" style="margin-left: 10px"><span class="icon-flag"></span></a>
        <?php echo $form->textFieldRow($model,'fax',                  array('class'=>'span3'));?>
        <a href="#" rel="tooltip" title="<?php print $labels['www'].'. '.$messages['opt']?>" style="margin-left: 10px"><span class="icon-flag"></span></a>
        <?php echo $form->textFieldRow($model,'www',                  array('class'=>'span3'));?>
    </td>
</tr>
<tr>
    <td style="padding-top: 10px">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType' => 'submit',
                'type'       => 'primary',
                'label'      => Yii::t("total", "сохранить данные"),
                'size'       => 'Normal',
            )
        ); ?>
    </td>
</tr>

</table>
<?php $this->endWidget(); ?>
<?php $this->widget('application.components.component.MegapolisDesign',
    array('array' => array(
        array('icon-pencil', Yii::t("total", "обязательное поле"),),
        array('icon-flag'  , Yii::t("total", "не обязательное поле"),),
    ),


    ));
?>