<?php Yii::app()->clientScript->registerCssFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('css').DS.'megapolis.ident.css'));?>

<div id='megapoils_middle'>
    <div id='megapolis_themes'>
        <div id='megapolis_header-news'><H3><?=Yii::t("total", 'новости компании')?></H3><hr></div>
        <div id="parentView">
            <?php $this->widget('bootstrap.widgets.TbGridView', array(
                'type'=>'striped bordered condensed',
                'dataProvider'=> $model->search(),
                'template'=>"{items}\n{pager}",
                'ajaxVar' => '',
                'columns'=>array(
                    array(
                        'name'  => 'id',
                        'header'=> Yii::t("total", 'новости'),
                        'value' => 'NEWS::getNews($data)',
                        'type'  => 'raw',
                    ),
                ),
                'enablePagination' => true,
                'pager' => array(
                        'class' => 'CLinkPager',
                        'cssFile' => false,
                        'header' => false,
                        'firstPageLabel' => 'Начало',
                        'prevPageLabel' => 'Назад',
                        'nextPageLabel' => 'Вперед',
                        'lastPageLabel' => 'Конец',
                 ),
                'htmlOptions'=>array('style'=>'width: 800px '),
            )); ?>
        </div>
</div>    
    