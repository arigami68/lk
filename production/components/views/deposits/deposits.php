<?php
    $this->widget('bootstrap.widgets.TbGridView', array(
        'type'=>'striped bordered condensed',
        'dataProvider'=>  $model->search(),
        'template'=>"{items}\n{pager}",
        'columns'=>array(
            array(
                'name'=>'id',
                'header'=>'№ п.п.'
            ),
            array(
                'name'=>'inflow_date',
                'header'=>'Дата поступления ДС на депозит',
            ),
            array(
                'name'=>'inflow_amount',
                'header'=>'Сумма поступления ДС на депозит',
            ),
            array(
                'name'=>'register',
                'header'=>'№ реестра отправлений',
            ),
            array(
                'name'=>'deposit_funds_date',
                'header'=>'Дата списания ДС',
            ),
            array(
                'name'=>'deposit_funds_amount',
                'header'=>'Сумма списания ДС',
            ),                        
            array(
                'name'=>'residue',
                'header'=>'Оставшиеся ДС на депозите',
            ),                        
        ),
        'htmlOptions'=>array('style'=>'width: 1400px '),
));

?>
<?php
$form1 = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
'id'=>'DepositsForm',
'type'=>'horizontal',
));

echo $form1->textFieldRow($form,'sum',array('rows'=>2, 'class'=>'span2'));

?>

<div class="load-automatic-form-actions">
   <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        'type'=>'null',
        'label'=>'Сформировать счет',
        'htmlOptions'=>array('name'=>'excel', 'value' => '2'),)
        );
   ?>
</div>

<?php $this->endWidget(); ?>