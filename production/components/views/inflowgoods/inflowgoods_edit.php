<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('js').DS.'/datepicker/jquery-ui.js'));?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('js').DS.'/datepicker/jquery.ui.datepicker-ru.js'));?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('js').DS.'megapolis.inflowgoods.js'));?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('css').DS.'/datepicker/jquery-ui.css')); ?>
<div>
    <?php  $this->widget('bootstrap.widgets.TbButton', array(
        'label'=>'Просмотр заявок',
        'type'=> 'info', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'url' => Yii::app()->createUrl('/getiw'),
    )); ?>
</div>

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'File',
    'htmlOptions'=>array('enctype'=>'multipart/form-data'),
    'type'=>'horizontal',
)); 
?>

<?php echo $form->textFieldRow($model, 'id', array('class'=>'span1','disabled'=>true,)); ?>
<?php echo $form->textFieldRow($model, 'date_shipping'); ?>
<?php echo $form->dropDownListRow($model, 'edit', array('', 'к поступлению')); ?>


<div class="load-automatic-form-actions">
    <div id="yt1"></div>
   <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit',
        'type'=>'null', 
        'label'=>'Сохранить')
        ); 
   ?>

</div>
<?php $this->endWidget(); ?>