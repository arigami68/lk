<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('js').DS.'/datepicker/jquery-ui.js'));?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('js').DS.'/datepicker/jquery.ui.datepicker-ru.js'));?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('js').DS.'megapolis.inflowgoods.js'));?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('css').DS.'jquery.jgrowl.min.css'));?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('js').DS.'jquery.jgrowl.min.js'));?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('css').DS.'megapolis.inflowgoods.css'));?>

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'File',
    'htmlOptions'=>array('enctype'=>'multipart/form-data'),
    'type'=>'horizontal',
)); ?>

<div id="fileLoad">
    <?php echo $form->fileFieldRow($model, 'file'); ?>
    <span id="fileformlabel"></span>
</div>


<div class="load-automatic-form-actions">
    <div id="yt1"></div>
   <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit',
        'type'=>'null', 
        'label'=>'Загрузить')
        ); 
   ?>

</div>
<?php $this->endWidget(); ?>