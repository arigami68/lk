<div id="parentView">
    <?php $this->widget('bootstrap.widgets.TbGridView', array(
        'type'=>'striped bordered condensed',
        'dataProvider'=> $model->search_getiw(),
        'template'=>"{items}\n{pager}",
        'ajaxVar' => '',
        'columns'=>array(
            array(
                'name'=>'id',
                'header' => '№ заявки',
                'htmlOptions'=> array('style'=>'width: 30px;'),
            ),
            array(
                'name'=> 'date',
                'header'=>'Дата заявки',
            ),
            array(
                'name'=> 'provider',
                'header'=>'Поставщик контрагента',
            ),
            array(
                'name'=> 'amount',
                'header'=>'Общее количество',
            ),
            array(
                'name'=> '',
                'header'=>'Планируемая дата поступления',
                'value' => 'APTI::date_delivery($data)',
                'type' => 'raw',
                
            ),
            array(
                'name'=> 'date_shipping_fact',
                'header'=>'Фактическая дата поступления',
            ),
            array(
                'name'=> '',
                'header'=>'Статус заявки',
                'value' => 'APTI::Status($data)',
                'type' => 'raw',
                
            ),
            array(
                'name'=> '',
                'header'=>'Операции',
                'value' => 'ASDB::view_operation($data)',
                'type'  => 'raw',
            ),
        ),
        'enablePagination' => true,
        'pager' => array(
                'class' => 'CLinkPager',
                'cssFile' => false,
                'header' => false,
                'firstPageLabel' => 'Начало',
                'prevPageLabel' => 'Назад',
                'nextPageLabel' => 'Вперед',
                'lastPageLabel' => 'Конец',
         ),
    )); ?>
</div>