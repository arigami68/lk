<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('js').DS.'/datepicker/jquery-ui.js'));?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('js').DS.'/datepicker/jquery.ui.datepicker-ru.js'));?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('js').DS.'/megapolis.inflowgoods.js'));?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('css').DS.'/datepicker/jquery-ui.css')); ?>
<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'createOrder',
    'type'=>'horizontal',
));
?>

<?php
    
    echo $form->textFieldRow($model,'id',array('rows'=>2, 'class'=>'span7', 'disabled' => 'disabled'));
    echo $form->hiddenField($model,'id');
    echo $form->textFieldRow($model,'contractor',array('rows'=>2, 'class'=>'span7',));
    echo $form->textFieldRow($model,'provider',array('rows'=>2, 'class'=>'span7',));
    $tbtabs[] = array('label'=>'+', 'linkOptions' => array('id'=>'addGoods'));
    
    if(true){ # возвращаем в форму
        foreach($model->attributes as $k=>$v){
            if(is_array($v)){
                foreach($v as $k2=>$v2){
                    $dep[$k2][$k] = $v2;
                }
            }
        }
    }
 
        $a=0;
    foreach($dep as $k=>$v){
        
        if(isset($errors[$k])){
            $model->clearErrors();
            $model->addErrors($errors[$k]);
        }else{
            $model->clearErrors();
        }
        $a+=1;
        $k=$a;
        $model->attributes = $v;

        $text[$k] = '';
        $text[$k].= $k !== 1 ? '<div class="delete"><a class=tovar'.$k.' href=#>Удалить</a></div>' : '';
        $text[$k].= $form->textFieldRow($model,'internal',array('rows'=>2, 'class'=>'span7', 'name' => 'IwForm[internal]['.$k.']', ));
        $text[$k].= $form->textFieldRow($model,'artical',array('rows'=>2, 'class'=>'span7', 'name' => 'IwForm[artical]['.$k.']'));
        $text[$k].= $form->textFieldRow($model,'nomencl',array('rows'=>2, 'class'=>'span7', 'name' => 'IwForm[nomencl]['.$k.']', ));
        $text[$k].= $form->textFieldRow($model,'unit',array('rows'=>2, 'class'=>'span7', 'name' => 'IwForm[unit]['.$k.']', ));
        $text[$k].= $form->textFieldRow($model,'amount',array('rows'=>2, 'class'=>'span7', 'name' => 'IwForm[amount]['.$k.']', ));
        $text[$k].= $form->textFieldRow($model,'woVAT',array('rows'=>2, 'class'=>'span7', 'name' => 'IwForm[woVAT]['.$k.']',));
        $text[$k].= $form->textFieldRow($model,'wiVAT',array('rows'=>2, 'class'=>'span7', 'name' => 'IwForm[wiVAT]['.$k.']', ));
        $text[$k].= $form->textFieldRow($model,'rateVAT',array('rows'=>2, 'class'=>'span7', 'name' => 'IwForm[rateVAT]['.$k.']',));
        $text[$k].= $form->textFieldRow($model,'cost',array('rows'=>2, 'class'=>'span7', 'name' => 'IwForm[cost]['.$k.']',));
        $text[$k].= $form->textFieldRow($model,'note',array('rows'=>2, 'class'=>'span7', 'name' => 'IwForm[note]['.$k.']',));
    }
    foreach($text as $k=>$v){
        $tbtabs[]= array('label'=>'Товар #'.$k, 'content'=> $v, 'active'=> ($k == 1 ? true : false), 'linkOptions' => array('id' => 'tovar'.$k));
    }
    $this->widget('bootstrap.widgets.TbTabs', array(
        'type'=>'tabs',
        'placement'=>'top', // 'above', 'right', 'below' or 'left'
        'tabs'=> $tbtabs,
    ));
    ?>
<?
    Yii::app()->clientScript->registerScript("addGoods", "
        $('.delete a').live('click', function(data){
            var data_id = data.target.className;
            $('#'+data_id)
            .remove();
            $('.'+data_id)
            .parent()
            .parent()
            .remove();
            $('#yw4 .active').removeClass('active')

            addClass('active');
            $('#yw4 li').
            eq(1).
            addClass('active');
        });

        $('body').delegate('#addGoods', 'click', function(){
            var len = $('#yw4 li').length+1; 
            $.ajax({
                data: {'goods': len},
                type: 'POST',
                url: '/inflowgoods/inflowgoods/addgoods',
                success: function(e){
                  $('.tab-content').append(e);
                },
                 beforeSend: function(){},
                 complete: function(){},
                 error: function(){},
              });
        $('#yw4').html($('#yw4').html()+'<li ><a id=tovar'+len+' data-toggle=tab href=#yw3_tab_'+len+'>Товар #'+(len-1)+'</a></li>');
         return false;
    })");
?>
<div style='width: 671px; float: left; margin-right: 18px; margin-left: 180px'>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type'       => 'primary',
            'label'      => 'Сохранить заявку',
            'size'       => 'Normal',
        )
    ); ?>
</div>

<?php $this->endWidget(); ?>