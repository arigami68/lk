<div>
    <?php  $this->widget('bootstrap.widgets.TbButton', array(
        'label'=>'Просмотр заявок',
        'type'=> 'info', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'url' => Yii::app()->createUrl('/getiw'),
    )); ?>
</div>

<?
if($ASDB = Yii::app()->request->getParam('ASDB')){
    if(is_array($ASDB)){
        if($ASDB['status'] == ASDB::$APP_VIEW){
            print '<br><br><br>';
            $this->beginContent('views.inflowgoods.inflowgoods_manual_view', array('model' => $model, 'dep' => isset($dep) ? $dep : NULL, 'errors' => isset($errors) ? $errors : NULL ));
            $this->endContent();
            return 1;
        }
        if($ASDB['status'] == ASDB::$APP_EDIT){
            print '<br><br><br>';
            $this->beginContent('views.inflowgoods.inflowgoods_manual_edit', array('model' => $model, 'dep' => isset($dep) ? $dep : NULL, 'errors' => isset($errors) ? $errors : NULL ));
            $this->endContent();
            return 1;
        }
    }
        return $this->redirect('getiw');
}
?>
<br>

<?php $this->widget('bootstrap.widgets.TbButtonGroup', array(
    'type' => 'primary',
    'toggle' => 'radio',
    'buttons'=>array(
        array('label'=>'Вручную', 'url'=>'?manual' , 'htmlOptions'=>array('id'=> 'check_manual'),),
        array('label'=>'Загрузить файл', 'url'=>'?auto',  'htmlOptions'=>array('class'=> 'load', 'id'=> 'check_automatic'),),
    ),
));
?>
<br><br><br>
<?php
 
    if(Yii::app()->request->getQuery('auto') !== NULL){
        $this->beginContent('views.inflowgoods.inflowgoods_auto', array('model' => $model));
        $this->endContent();
        return 1;
    }
        $this->beginContent('views.inflowgoods.inflowgoods_manual', array('model' => $model, 'dep' => isset($dep) ? $dep : NULL, 'errors' => isset($errors) ? $errors : NULL ));
        $this->endContent();
    ?>