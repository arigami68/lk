<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'createOrder',
    'type'=>'horizontal',
));
?>

<?php
    
    echo $form->textFieldRow($model,'id',array('rows'=>2, 'class'=>'span7', 'disabled' => 'disabled')); 
    echo $form->textFieldRow($model,'contractor',array('rows'=>2, 'class'=>'span7', 'disabled' => 'disabled'));
    echo $form->textFieldRow($model,'provider',array('rows'=>2, 'class'=>'span7', 'disabled' => 'disabled'));
            
            if(true){ # возвращаем в форму
                foreach($model->attributes as $k=>$v){
                    if(is_array($v)){
                        foreach($v as $k2=>$v2){
                            $dep[$k2][$k] = $v2;
                        }
                    }
                }
            }
                $a=0;
            foreach($dep as $k=>$v){
    
                $a+=1;
                $k=$a;
                $model->attributes = $v;
                
                
                $text[$k] = '';
                $text[$k].= $form->textFieldRow($model,'internal',array('rows'=>2, 'class'=>'span7', 'name' => 'IwForm[internal]['.$k.']', 'disabled' => 'disabled'));
                $text[$k].= $form->textFieldRow($model,'artical',array('rows'=>2, 'class'=>'span7', 'name' => 'IwForm[artical]['.$k.']', 'disabled' => 'disabled'));
                $text[$k].= $form->textFieldRow($model,'nomencl',array('rows'=>2, 'class'=>'span7', 'name' => 'IwForm[nomencl]['.$k.']', 'disabled' => 'disabled'));
                $text[$k].= $form->textFieldRow($model,'unit',array('rows'=>2, 'class'=>'span7', 'name' => 'IwForm[unit]['.$k.']', 'disabled' => 'disabled'));
                $text[$k].= $form->textFieldRow($model,'amount',array('rows'=>2, 'class'=>'span7', 'name' => 'IwForm[amount]['.$k.']', 'disabled' => 'disabled'));
                $text[$k].= $form->textFieldRow($model,'woVAT',array('rows'=>2, 'class'=>'span7', 'name' => 'IwForm[woVAT]['.$k.']', 'disabled' => 'disabled'));
                $text[$k].= $form->textFieldRow($model,'wiVAT',array('rows'=>2, 'class'=>'span7', 'name' => 'IwForm[wiVAT]['.$k.']', 'disabled' => 'disabled'));
                $text[$k].= $form->textFieldRow($model,'rateVAT',array('rows'=>2, 'class'=>'span7', 'name' => 'IwForm[rateVAT]['.$k.']', 'disabled' => 'disabled'));
                $text[$k].= $form->textFieldRow($model,'cost',array('rows'=>2, 'class'=>'span7', 'name' => 'IwForm[cost]['.$k.']', 'disabled' => 'disabled'));
                $text[$k].= $form->textFieldRow($model,'note',array('rows'=>2, 'class'=>'span7', 'name' => 'IwForm[note]['.$k.']', 'disabled' => 'disabled'));
            }
            foreach($text as $k=>$v){
                $tbtabs[]= array('label'=>'Товар #'.$k, 'content'=> $v, 'active'=> ($k == 1 ? true : false), 'linkOptions' => array('id' => 'tovar'.$k));
            }
            $this->widget('bootstrap.widgets.TbTabs', array(
                'type'=>'tabs',
                'placement'=>'top', // 'above', 'right', 'below' or 'left'
                'tabs'=> $tbtabs,
            ));

    ?>
<?php $this->endWidget(); ?>