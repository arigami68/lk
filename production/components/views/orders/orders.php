<? if(true){
        Yii::app()->clientScript->registerCssFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('css').DS.'megapolis.orders.css'));
        Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('js').DS.'/megapolis.orders.js'));
        Yii::app()->getClientScript()->registerCoreScript( 'jquery.ui' );
        Yii::app()->clientScript->registerCssFile( Yii::app()->clientScript->getCoreScriptUrl(). '/jui/css/base/jquery-ui.css');
    }
    $count = Yii::app()->request->getParam('count');

    if($count < 10) $count=10;
    $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
                    'id' => 'megapolis',
                    'options' => array(
                        'title' => 'Отправления по данному реестру',
                        'autoOpen'=>false,
                        'modal'=>true,
                        'width'=>'auto',
                        'height'=>'auto',
                        'resizable'=>'auto',
                    ),
                ));
    $this->endWidget('zii.widgets.jui.CJuiDialog');


?>

<span style="font-weight: bold"><?=Yii::t("total", 'количество реестров')?></span> <?php echo CHtml::dropDownList('orders-count', $model, array('10', '20', '30', '40', '50'), array('class' => 'span1')); ?>


<div id="loadGif" style="display: none"><b><?php print CHtml::image(Yii::getPathOfAlias('link_images').'/loading.gif'); ?><small>Обработка запроса</small></b></div>
<div id="parentView">
    <?php $this->widget('bootstrap.widgets.TbGridView', array(
        'type'=>'striped bordered condensed',
        'dataProvider'=> $model->QuSe($model::$SEARCH_REGISTRY, $count),
        'template'=>"{items}\n{pager}",
        'ajaxVar' => '',
        'columns'=>array(
            #№ п.п	Дата приема	Номера Мегаполис	Номер Реестра	Сумма НП	Последний статус	Адрес доставки
            array(
                'name'=> 'registry',
                'header'=> Yii::t("total", '№ Реестра'),
                'htmlOptions' => array('style'=>'width: 1x'),
            ),
            array(
                'name'  => 'dateCreate',
                'header'=> Yii::t("total", 'дата создания'),
                'type'  => 'raw',
                'value' => 'HelpViewWidget::searchdateCreate($data)',
            ),
            array(
                'name'  => 'megapolis',
                'header'=> Yii::t("total", 'Количество'),
                'htmlOptions' => array('style'=>'width: 5px'),
            ),
            array(
                'name'  => 'opTake',
                'header'=> Yii::t("total", 'тип приема'),
                'type'  => 'raw',
                'value' => 'HelpViewWidget::ORDS_opTake($data)',
                'htmlOptions' => array('style'=>'width: 110px'),
            ),
            array(
                'name'  => 'action',
                'header'=> Yii::t("total", 'статус реестра').' <a href="#" rel="tooltip" title="'.Yii::t("total", 'статус реестра').'" style="display: inline"><img src='.Yii::app()->assetManager->publish(Yii::getPathOfAlias('images').DS.'question.png').'></a>',
                'type'  => 'raw',
                'value' => 'HelpViewWidget::ORDS_action_operation_register($data)',
                'htmlOptions' => array('style'=>'width: 150px'),
            ),
            array(
                'name'  => 'operation',
                'header'=> Yii::t("total", 'операции').' <a href="#" rel="tooltip" title="'.Yii::t("total", 'операции').'" style="display: inline"><img src='.Yii::app()->assetManager->publish(Yii::getPathOfAlias('images').DS.'question.png').'></a>',
                'type'  => 'raw',
                'value' => function($data){
                    $word = array(
                        'print_registry_in_reestr'  => Yii::t("total", 'печать штрихкодов'),
                        'print_registry'            => Yii::t("total", 'печать реестра'),
                        'show_order'                => Yii::t("total", 'просмотр отправлений'),
                        'read'                      => Yii::t("total", 'просмотр'),
                        'update'                    => Yii::t("total", 'редактирование'),
                        'delete'                    => Yii::t("total", 'удаление'),
                    );
                            $flag = empty($data->dateSend) ? 1 : 0;
                            $glyphicons = '/img/glyphicons-halflings.png';
                            $array['print'] =  array(
                                'htmloption' =>  array('title' => $word['print_registry_in_reestr'], 'class' => 'printR icon-barcode'),
                            );
                            $array['print2'] =  array(
                                'htmloption' =>  array('title' => $word['print_registry'], 'class' => 'icon-print'),
                                'url' => '/orders?registry='.$data['registry'].'&operation=print',
                            );
                            $img['read'] = '/megapolis-open.gif';
                            $id=$data->registry;
                            $array['registry'] = array(
                                'src' =>  CHtml::image(Yii::getPathOfAlias('link_images').$img['read']),
                                'url' => '/ordes/orders/addDep?id='.$id,
                                'htmloption' =>  array('title' => $word['show_order'], 'class' => 'button icon-th-large'),
                            );
                            if($flag){
                                $array['s'] =  array(
                                    'htmloption' =>  array('title' => $word['read'], 'class' => 'icon-eye-open'),
                                    'url' => '/order?id='.$data['registry'].'&operation='.ORDS::$OPERATION_READ,
                                );
                                $array['e'] =  array(
                                    'htmloption' =>  array('title' => $word['update'], 'class' => 's_update icon-edit'),
                                    'url' => '/order?id='.$data['registry'].'&operation='.ORDS::$OPERATION_UPDATE,
                                );
                                $array['d2'] =  array(
                                    'htmloption' =>  array('title' => $word['delete'], 'class' => 's_delete icon-trash'),
                                    'url' => '/order?registry='.$data['registry'].'&operation=delete',
                                );


                                return
                                    CHtml::link('', $array['s']['url'] ,$array['s']['htmloption']).
                                    ' '.
                                    CHtml::link('', $array['e']['url'] ,$array['e']['htmloption']).
                                    ' '.
                                    CHtml::link('', $array['d2']['url'] ,$array['d2']['htmloption']).
                                    '&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp'.
                                    #CHtml::link('', '' ,$array['print']['htmloption']).
                                    '&nbsp&nbsp&nbsp'.
                                    CHtml::link('', '' ,$array['registry']['htmloption'])
                                    #'&nbsp&nbsp&nbsp'.
                                    #CHtml::link('', $array['print2']['url'] ,$array['print2']['htmloption'])
                                    ;
                            }else{
                                $array['s'] =  array(
                                    'htmloption' =>  array('title' => $word['read'], 'class' => 'icon-eye-open'),
                                    'url' => '/order?id='.$data['registry'].'&operation='.ORDS::$OPERATION_READ,
                                );
                                return
                                    CHtml::link('', $array['s']['url'] ,$array['s']['htmloption']).
                                    str_repeat('&nbsp', 12).
                                    CHtml::link('', '' ,$array['print']['htmloption']).
                                    '&nbsp&nbsp&nbsp'.
                                    CHtml::link('', '' ,$array['registry']['htmloption']).
                                    '&nbsp&nbsp&nbsp'.
                                    CHtml::link('', $array['print2']['url'] ,$array['print2']['htmloption'])
                                    ;
                            }
                },
            ),
        ),
        'enablePagination' => true,
        'pager' => array(
            'class' => 'CLinkPager',
            'cssFile' => false,
            'header' => false,
            'firstPageLabel' => '<<',
            'prevPageLabel' => '<',
            'nextPageLabel' => '>',
            'lastPageLabel' => '>>',
        ),
    )); ?>
    <?php $this->widget('application.components.component.MegapolisDesign',array('array' => array(
        array("icon-eye-open", Yii::t("total", 'просмотр')),
        array('icon-edit',     Yii::t("total", 'редактирование') ),
        array('icon-trash',    Yii::t("total", 'удаление')),
        array('icon-barcode',  Yii::t("total", 'печать штрихкодов')),
        array('icon-th-large', Yii::t("total", 'просмотр отправлений')),
        array('icon-print',    Yii::t("total", 'печать')),
    )));
    ?>