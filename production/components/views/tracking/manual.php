<div id='manual' style='display:none'>
    <div style="display: inline;">
        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id'=>'loadForm',
            'htmlOptions'=>array('enctype'=>'multipart/form-data'),
            'type'=>'horizontal',
        )); ?>
        <?php echo $form->fileFieldRow($model, 'fileField'); ?>
        <div class="load-automatic-form-actions">
            <div id="yt1"></div>
           <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit',
                'type'=>'null', 
                'label'=>'Загрузить','htmlOptions'=>array('class'=> 'load', 'id'=> 'check_manual'))
                );
           ?>
        </div>
    </div>
    
    <div id="tableManualShow"><?php isset($manual) ? print $manual : '';?></div>
    <div id="loadGifManual" style="display: none"><b> <?php print CHtml::image(Yii::getPathOfAlias('link_images').'/loading.gif'); ?><small>Обработка запроса</small></b></div>
</div>
 <?php $this->endWidget(); ?>