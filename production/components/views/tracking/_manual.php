<div id='automatic'>
    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id'=>'loadForm',
            'htmlOptions'=> array('enctype'=>'multipart/form-data'),
            'type'=>'horizontal',
    )); 
    ?>
    <?php echo $form->textFieldRow($model, 'text', array('id' => 'agent'));?>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'button',
                'type'=>'null',
                'htmlOptions'=>array('id'=>'getagent', 'onclick'=>"getAgent(agent.value)",),
                'label'=> Yii::t("total", 'отследить'),
          ));
    ?>
    <?php $this->endWidget(); ?>
    <br>
        <div id="loadGif" style="display: none"><b><?php print CHtml::image(Yii::getPathOfAlias('link_images').'/loading.gif'); ?><small>Обработка запроса</small></b></div>
    <br>
    <div id="tableErrorShow"></div>
    <div id="tableSourceShow"></div>
    <div id="tableAddressShow"></div>
    <div id="tableOperationShow"></div>
    <br>
</div>