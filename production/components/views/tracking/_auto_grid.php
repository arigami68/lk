<div id="megapolis-loadGif" style="display: none"><b><?php print CHtml::image(Yii::getPathOfAlias('link_images').'/loading.gif'); ?><small>Получение номеров отправлений... </small></b></div>
<div id="parentView">
    
    <?php $this->widget('bootstrap.widgets.TbGridView', array(
        'type'=>'striped bordered condensed',
        'template'=>"{items}\n{pager}",
        'dataProvider'=> AutoAction::autoDataProvider(),
        'ajaxVar' => '',
        'ajaxUpdate' => 'childView',
        'columns'=>array(
            array(
                'name'=> 'inc',
                'header' => 'Отправление',
            ),
            array(
                'name'=> 'id',
                'header' => 'Оператор доставки',
            ),
            array(
                'name'=> 'address',
                'header' => 'Адрес доставки',
            ),
            array(
                'name'=> 'zip',
                'header' => 'Индекс',
            ),
            array(
                'name'=> 'oc',
                'header' => 'Объявленная ценность',
            ),
            array(
                'name'=> 'np',
                'header' => 'Наложенный платеж',
            ),
            array(
                'name'=> 'last_operation',
                'header'=>'Место операции',
            ),
            array(
                'name'=> 'last_operation',
                'header'=>'Дата последней операции',
            ),
            array(
                'name'=> 'status',
                'header'=>'Статус',
            ),
            array(
                'name'=> 'handed',
                'header'=>'Вручено',
            ),
        ),
        'enablePagination' => true,
        'pager' => array(
                'class' => 'CLinkPager',
                'cssFile' => false,
                'header' => false,
                'firstPageLabel' => 'Начало',
                'prevPageLabel' => 'Назад',
                'nextPageLabel' => 'Вперед',
                'lastPageLabel' => 'Конец',
         ),
    )); ?>
</div>