<div style="display: inline; ">
    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id'=>'loadForm',
            'htmlOptions'=> array('enctype'=>'multipart/form-data'),
            'type'=>'horizontal',
          ));

    ?>
    <p><a href=<?php print Yii::app()->assetManager->publish(PRODUCTION.'file'.DS.'general'.DS.'test_tracking.xlsx');?>>Шаблон для загрузки</a></p>
    <div id="fileLoad">
        <?php echo $form->fileFieldRow($model, 'fileField'); ?>
        <span id="fileformlabel"></span>
    </div>

    <div class="load-automatic-form-actions">
        <span>
        <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit',
            'type'=>'primary',
            'label'=>'Загрузить','htmlOptions'=>array('class'=> 'load', 'id'=> 'check_manual'))
            );
        ?>
        <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'button',
                'type'=>'primary',
                'label'=>'Остановить трекинг',
                'htmlOptions'=>array(
                    'onclick'=> 'tracking.stoptrack = 1',
                )
              ));
        ?>
        </span>
        <span style="float: right">
        <?php
            $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType' => 'submit',
                'type'       => 'primary',
                'label'      => 'Выгрузить в excel',
                'size'       => 'Normal',
                'htmlOptions'=>array('id'=> 'excel', 'name' => 'excel'),
                )
            );
        ?>
        </span>

    <?php $this->endWidget(); ?>
</div>

    
<?php $this->renderPartial('application.components.views.tracking._auto_grid',array(
        'model'=>$model,
      )); 
?>
    