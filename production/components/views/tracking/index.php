<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('js').DS.'megapolis.tracking.js')); ?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('css').DS.'megapolis.tracking.css')); ?>
<?php
if(isset($state['manual'])){
    $manual = '<H3 style="color: orange">Последние статусы Отправлений</H3>';
    $manual.= '<table  border=1>';
    $manual.= '<tr style="background-color: rgb(153, 153, 153);color: rgb(255, 255, 255); text-align: center"><td>Код</td><td>Дата</td><td>Статус</td><td>Вручени</td><td>Адрес</td></tr>';
    foreach($state['manual'] as $k=>$v){
        $manual.= '<tr>';
        $manual.= '<td>'.$v["code"].'</td>';
        $manual.= '<td>'.$v["date"].'</td>';
        $manual.= '<td>'.$v["status"].'</td>';
        $manual.= '<td>'.$v["done"].'</td>';
        $manual.= '<td>'.$v["address"].'</td>';
        $manual.= '</tr>';
    }
    $manual.= '</table>';
}
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup', array(
    'type' => 'primary',
    'toggle' => 'radio',
    'buttons'=>array(
        array('label'=> Yii::t("total", 'номер заказа клиента'), 'url'=>'?manual' , 'htmlOptions'=>array('id'=> 'check_manual'),),
        array('label'=> Yii::t("total", 'загрузка файла'), 'url'=>'?auto',  'htmlOptions'=>array('class'=> 'load', 'id'=> 'check_automatic'),),
    ),
));
?>
<br><br><br>
<?php
    if(Yii::app()->request->getQuery('auto') !== NULL){
        $this->beginContent('views.tracking._auto', array('model' => $model));
        $this->endContent();
        return 1;
    }
    $this->beginContent('views.tracking._manual', array('model' => $model)); 
    $this->endContent();
        
?>