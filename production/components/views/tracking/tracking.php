
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('css').DS.'megapolis.tracking.css')); ?>
<?php

/* Отслеживание отправлений. Не работает функция загрузки "много отправлений"
 */
if(isset($state['manual'])){
    $manual = '<H3 style="color: orange">Последние статусы Отправлений</H3>';
    $manual.= '<table  border=1>';
    $manual.= '<tr style="background-color: rgb(153, 153, 153);color: rgb(255, 255, 255); text-align: center"><td>Код</td><td>Дата</td><td>Статус</td><td>Вручени</td><td>Адрес</td></tr>';
    foreach($state['manual'] as $k=>$v){
        $manual.= '<tr>';
        $manual.= '<td>'.$v["code"].'</td>';
        $manual.= '<td>'.$v["date"].'</td>';
        $manual.= '<td>'.$v["status"].'</td>';
        $manual.= '<td>'.$v["done"].'</td>';
        $manual.= '<td>'.$v["address"].'</td>';
        $manual.= '</tr>';
    }
    $manual.= '</table>';
}
?>
<?php

$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'horizontalForm',
    'type'=>'horizontal',
)); ?>

<?php

$this->widget('bootstrap.widgets.TbButtonGroup', array(
    'type' => 'primary',
    'toggle' => 'radio',
    'buttons'=>array(
        array('label'=> Yii::t("total", 'номер заказа клиента'), 'htmlOptions'=>array('id'=> 'check_manual'),),
        array('label'=> Yii::t("total", 'загрузка файла'),'htmlOptions'=>array('class'=> 'load', 'id'=> 'check_automatic'),),
    ),
));
?>
<?php $this->endWidget(); ?>
<!-- ************************************ -->
<div id='automatic'>
    <?php echo $form->textFieldRow($model, 'text', array('hint' => '<strong><sub>Отправление</sub></strong>', 'id' => 'agent'));
    ?>
        <?php
        $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit',
                    'type'=>'null',
                    'htmlOptions'=>array('id'=>'getagent', 'onclick'=>"getAgent(agent.value)",),
                    'label'=>'Мне повезет'
            )
        );
      ?>

    <br>
        <div id="loadGif" style="display: none"><b><?php print CHtml::image(Yii::getPathOfAlias('link_images').'/loading.gif'); ?><small>Обработка запроса</small></b></div>
    <br>
    <div id="tableErrorShow"></div>
    <div id="tableSourceShow"></div>
    <div id="tableAddressShow"></div>
    <div id="tableOperationShow"></div>
    <br>
</div>

<!-- ************************************************************************************* -->
<div id='manual' style='display:none'>
    <div style="display: inline;">
        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id'=>'loadForm',
            'htmlOptions'=>array('enctype'=>'multipart/form-data'),
            'type'=>'horizontal',
        )); ?>

        <?php echo $form->fileFieldRow($model, 'fileField'); ?>
        <div class="load-automatic-form-actions">
            <div id="yt1"></div>
           <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit',
                'type'=>'null', 
                'label'=>'Загрузить',
                'htmlOptions'=>array('class'=> 'load', 'id'=> 'check_manual', ))
                );
           ?>
        </div>
    </div>
    
    <div id="tableManualShow"><?php isset($manual) ? print $manual : '';?></div>
    <div id="loadGifManual" style="display: none"><b> <?php print CHtml::image(Yii::getPathOfAlias('link_images').'/loading.gif'); ?><small>Обработка запроса</small></b></div>
</div>
<!-- ************************************************************************************* -->
 <?php $this->endWidget(); ?>
