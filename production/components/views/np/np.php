<?php Yii::app()->clientScript->registerCssFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('css').DS.'/np.css')); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('js').DS.'/datepicker/jquery-ui.js'));?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('js').DS.'/datepicker/jquery.ui.datepicker-ru.js'));?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('css').DS.'/datepicker/jquery-ui.css')); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('js').DS.'/megapolis.np.js'));?>
<?php

$multi = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'createOrder',
    'htmlOptions'=>array('enctype'=>'multipart/form-data'),
    'type'=>'inline',
));
if($model->scenario !== CLNP::$SCENARIO_FILE){
    $s = Yii::app()->request->getQuery('s');
    $f = Yii::app()->request->getQuery('f');
    $list = Yii::app()->request->getQuery('agent');

    if($s) $form->date_s = $s;
    if($f) $form->date_f = $f;
    if($list) $form->list = $list;
}
?>
<div class="megapolis-table">
    <div class="megapolis-tr">
        <div class="megapolis-td">
            <?=Yii::t("total", "выберите дату")?>
        </div>
        <div class="megapolis-td" style="padding-bottom: 15px">
            <?php echo $multi->textFieldRow($form,'date_s',array('rows'=>1, 'class'=>'span2'));?>
            <?php echo $multi->textFieldRow($form,'date_f',array('rows'=>1, 'class'=>'span2'));?>
        </div>
    </div>
    <div class="megapolis-tr">
        <div class="megapolis-td">
            <?=Yii::t("total", "Поиск по ШПИ/ № груза")?>
        </div>
        <div class="megapolis-td" style="padding-bottom: 15px">
            <?php echo $multi->textFieldRow($form,'list',array('rows'=>1, 'class'=>'span2'));?>
        </div>
    </div>

    <div class="megapolis-tr">
        <div class="megapolis-td">
            <?=Yii::t("total", "загрузка файла")?>
        </div>
        <div class="megapolis-td">
            <p><a href=<?php print Yii::app()->assetManager->publish(PRODUCTION.'file'.DS.'general'.DS.'test_np.xls');?>>Шаблон для загрузки</a></p>
            <div id="fileLoad">
                <?php echo $multi->fileFieldRow($form, 'file'); ?>
                <span id="fileformlabel"></span>
            </div>


        </div>
    </div>
    <div class="megapolis-tr">
        <div class="megapolis-td">
        </div>
        <div class="megapolis-td megapolis-sub" >
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                    'buttonType'=>'submit',
                    'type'=>'primary',
                    'label'=> Yii::t("total", "загрузить данные"),
                    'htmlOptions'=>array('name'=>'submit', 'id' => 'Permy'),)
            );
            ?>
        </div>
    </div>
</div>

<?php
$this->endWidget();
if(Yii::app()->request->getQuery('agent') || Yii::app()->request->getQuery('to')){
    $this->widget('bootstrap.widgets.TbGridView', array(
        'type'=>'striped bordered condensed',
        'dataProvider'=>  $model->search($criteria, $sort),
        'template'=>"{items}\n{pager}",
        'columns'=>array(
            array(
                'name'=>'id_reestr',
                'header'=>'№ реестра переводных платежей',
                'htmlOptions'=>array('style'=>'width: 10px'),
            ),
            array(
                'name'=>'reception_date',
                'header'=>'Дата приема',
                'type' => 'raw',
                'value' => function($data){ print $time =  MegapolisTime::get()->timeToConvert(MegapolisTime::$TIME_YEAR, MegapolisTime::$TIME_DEFAULT_FULL, new DateTime($data->reception_date));},
            ),
            array(
                'name'=>'agent',
                'header'=>'ШПИ',
            ),
            array(
                'name'=>'mailing_id',
                'header'=>'Номер груза',
            ),
            array(
                'name'=>'destination_region',
                'header'=>'Адрес доставки',
                'value' => '$data->destination_region." ".$data->destination_place',
                'type' => 'raw',
            ),
            array(
                'name'=>'recipient_name',
                'header'=>'Получатель',
            ),
            array(
                'name'=>'date_payment',
                'header'=>'Дата оплаты',
                'value' => '$data->date_payment != "0000-00-00 00:00:00" ? date("d-m-Y", strtotime($data->date_payment)) : ""',
                'htmlOptions'=>array('style'=>'width: 70px '),
                'type' => 'raw',
            ),
            array(
                'name'=>'cod_status',
                'header'=>'Статус доставки',
                'value' => 'CLNP::view_status($data)',
                'type' => 'raw',
            ),
            array(
                'name'=>'cod_amount',
                'header'=>'Сумма НП',
            ),

        ),
        'pager' => array(
            'class' => 'CLinkPager',
            'cssFile' => false,
            'header' => false,
            'firstPageLabel' => '<<',
            'prevPageLabel' => '<',
            'nextPageLabel' => '>',
            'lastPageLabel' => '>>',
        ),
    ));
}