<?php
class UserIdentity extends CUserIdentity{
	private $_id;
    private $_name;
	public function authenticate()
	{

        $ERROR_UNKNOWN         = Yii::t("total", "ERROR_UNKNOWN");
        $ERROR_UNKNOWN_USER    = Yii::t("total", "ERROR_UNKNOWN_USER");

        $password = md5($this->password);
        # Сбрасывать секретный ключ, если он установлен. Если был успешный вход

        if($queryUser = CLIT::model()->secret($password)->contract($this->username)->find()){
            }elseif($queryUser = TMP_CLIT::model()->secret($password)->contract($this->username)->find()){}

            if(!isset($queryUser->role)){ # Если данные из clients верны, но пользователя нет в Ролях
                $this->errorCode = $ERROR_UNKNOWN_USER;
            }else{

                if( $queryUser===null || $queryUser->role->role == 4 || !isset($queryUser->role) ){

                    $this->errorCode = $ERROR_UNKNOWN;
                }
                else
                {
                    # Обновляем время входа в систему
                    if(true){
                        $queryUser->role->date_update = date('Y-m-d H:i:s');
                        $queryUser->role->update();
                    }
                    $this->_id= $queryUser->id;
                    $this->_name= $queryUser->name;

                    $this->errorCode= self::ERROR_NONE;
                }
            }
            return !$this->errorCode;
	}

	public function getId(){return $this->_id;}
    public function getName(){return $this->_name;}
}