<?php

class Controller extends CController
{
    public static $THEMES_CLASSIC       = 'classic';
    public static $THEMES_TABLE         = 'themes_table';
    public static $THEMES_CLASSIC_LAST  = 'classic_last';

    public $layout = '';
	public $menu=array();
	public $breadcrumbs=array('');
    public $title = '';
    public function __construct(){
        parent::__construct($this->id, $this->module);
        $this->widget('application.components.widget.MegapolisLanguageWidget');

        if(true){
            Yii::app()->themeManager->basePath = Yii::getPathOfAlias('themes');

            Yii::setPathOfAlias('js', Yii::app()->theme->basePath.DS.'js');
            Yii::setPathOfAlias('css', Yii::app()->theme->basePath.DS.'css');
        }

        $this->layout = 'application.themes.'.Yii::app()->theme->name.'.layouts.init';


        if(false){
            if(Yii::app()->theme->name === self::$THEMES_CLASSIC){
                $this->layout = 'application.themes.classic.layouts.column';
            }elseif(Yii::app()->theme->name === self::$THEMES_TABLE){
                $this->layout = 'application.themes.themes_table.layouts.init';
            }elseif(Yii::app()->theme->name === self::$THEMES_CLASSIC_LAST){
                $this->layout = 'application.themes.classic_last.layouts.column';
            }
        }

    }

}