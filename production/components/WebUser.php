<?php

class WebUser extends CWebUser {
    public $auth;
    public function getRole(){
        
        if (!$this->isGuest){
            if($contract = PhpAuthManager::$contract) $role = $this->getModel($contract);
        }
        return isset($role) ? $role : Yii::app()->user->guestName;
    }
    private function getModel($contract){
    
        if(!isset($contract->clientrole->role)) return FALSE;
        return $contract->clientrole->role;
    }
}