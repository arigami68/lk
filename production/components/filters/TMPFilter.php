<?php

 class TMPFilter extends CFilter
{
    public function preFilter($filterChain){
       
       if(Yii::app()->user->checkAccess('tmp')){
            return $filterChain->run();
       }elseif(Yii::app()->user->checkAccess('user')){
           return $filterChain->run();
       }else{
           return Yii::app()->request->redirect(Yii::app()->user->returnUrl.Yii::app()->user->loginUrl);
       }
    }
}