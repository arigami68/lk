<?php

 class GetFilter extends CFilter   
{
    public function preFilter($filterChain){
        $request = new stdClass();
        $request->request = Yii::app()->request;
        
        $filterChain->run();
    }
}