<?php

 class AuthFilter extends CFilter   
{
    public function preFilter($filterChain){
     if(!Yii::app()->user->checkAccess('user')){
         return Yii::app()->request->redirect(Yii::app()->user->returnUrl.Yii::app()->user->loginUrl);
     }else{
        $filterChain->run();
     }
    }
}