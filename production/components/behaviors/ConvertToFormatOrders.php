<?php

 class ConvertToFormatOrders extends CBehavior{
    public $format;
    public function read(){
        if(get_class($this->owner) === 'FileLoadOrderComponent'){
            return $this->xlsToXml($this->owner);
        }
        if(get_class($this->owner) === 'App_storageComponent'){
            return $this->owner->convertToXML();
        }
    }
    public function xlsToXml($owner){return ConvertToXml::getFormatMegapolis($owner);}
 }