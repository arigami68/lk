<?php

class Remote{
    public static function maxipost($url, $get, $post){
        $get = '?'.$get;
        $post = array('data' => $post);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url.$get);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        $result = curl_exec ($ch);
        curl_close ($ch);
        
        return $result;
    }
    public static function mail($url){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1);
        $result = curl_exec ($ch);
        
        curl_close ($ch);
        
        return $result;
    }
}

?>