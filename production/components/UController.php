<?php
class UController extends Controller
{
        public function filters()
        {
            $array[] = array('application.components.filters.TMPFilter  + details, settings');
            $array[] = array('application.components.filters.AuthFilter - login, reg, logout, pr, captcha, settings, details, payment, find, ideaFind, ideaPayment');
            
            //$array[] = array('application.components.filters.GetFilter');
            if(Yii::app()->request->isPostRequest){
                $array[] = array('application.components.filters.PostFilter + getcur');
            }
            return $array;
        }
	public function ideaAddInfo($text){
		return $info = '<a href="#" rel="tooltip" title="'.Yii::t("total", $text).'"><img src='.Yii::app()->assetManager->publish(Yii::getPathOfAlias('images').DS.'question.png').'></a>';
	}
}