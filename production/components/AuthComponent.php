<?php
class AuthComponent extends CModel
{
    private static $instance;
    private function __construct(){}
    private function __clone()    {}
    private function __wakeup()   {}
    
    public function attributeNames(){}
    public static function get(){ return empty(self::$instance) ? self::$instance = new self() : self::$instance;}
    
    public $db;
    public $secretKey;
    public $mail;
    public $id;
    public $test;
    public $name;
    public $contract;
    public $range;
    public $comment;
    
    public function setDb($db){$this->db = $db;}
    public function setSecretKey($secretKey){$this->secretKey = $secretKey;}
    public function setMail($mail){$this->mail = $mail;}
    public function setId($id){$this->id = $id;}
    public function setTest($test){ $this->test = $test;}
    public function setName($name){ $this->name = $name;}
    public function setContract($contract){ $this->contract = $contract;}
    public function setRange($array){ $this->range = $array;}
    public function setComment($comment){ $this->comment = $comment;}
}