<?php
class PDFWidget extends CWidget {
    public $params = array();
    private $text;
    public function run() {
        if (!function_exists('mb_ucfirst') && extension_loaded('mbstring'))
        {
            /**
             * mb_ucfirst - преобразует первый символ в верхний регистр
             * @param string $str - строка
             * @param string $encoding - кодировка, по-умолчанию UTF-8
             * @return string
             */
            function mb_ucfirst($str, $encoding='UTF-8')
            {
                $str = mb_ereg_replace('^[\ ]+', '', $str);
                $str = mb_strtoupper(mb_substr($str, 0, 1, $encoding), $encoding).
                    mb_substr($str, 1, mb_strlen($str), $encoding);
                return $str;
            }
        }
        $pdf = Yii::createComponent('extensions.tcpdf.ETcPdf', 'P', 'cm', 'A4', true, 'UTF-8');
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor("Потапов Геннадий");
        $pdf->SetTitle("Orders");
        $pdf->SetSubject("Orders");
        $pdf->SetKeywords("Orders");
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        $pdf->AddPage();
        $pdf->SetFont('freeserif', '', 14);
        $this->text = '<div align="center">Внимание! Оплата данного счета означает согласие с условиями поставки товара. Уведомление об оплате
 обязательно, в противном случае не гарантируется наличие товара на складе. Товар отпускается по факту
 прихода денег на р/с Поставщика, самовывозом, при наличии доверенности и паспорта.</div>';
        $this->text.= ' <table style="width: 500px; border: 1px solid black;">
            <tr>
                 <td style="border: 1px solid black;" colspan="2">
                 ВТБ24 (ЗАО) Г.МОСКВА
                 </td>
                 <td style="border: 1px solid black;" rowspan="1">
                 БИК
                 <hr width="105%">
                 Сч. №
                 </td>

                 <td style="border: 1px solid black;">
                 044525716<br>
                 30101810100000000716
                 </td>

            </tr>
            <tr>
                 <td style="border: 1px solid black;">
                 ИНН 7724548764
                 </td>
                 <td style="border: 1px solid black;">
                 КПП 771901001
                 </td>
                 <td style="border: 1px solid black;" rowspan="2">
                 Сч. №
                 </td>
                 <td style="border: 1px solid black;" rowspan="2">
                 40702810100000075126
                 </td>
            </tr>
            <tr>
                 <td style="border: 1px solid black;" colspan="2">
                 ООО "ИдеаЛоджик"<br>
                 Получатель
                 </td>

            </tr>
          </table>';
        $this->text.= '<H2><b>Счет на оплату № '.$this->params->number.' от '.$this->params->date2.' г.</b></H2><br>';
        $this->text.= '<hr>';
        $this->text.= 'Поставщик: <b>ООО "ИдеаЛоджик", ИНН 7724548764, КПП 771901001, 105318, Москва г, Ткацкая ул, дом № 5, строение 5, тел.: (495) 228-14-95</b>';
        $this->text.= '<br><br>';
        $this->text.= 'Покупатель: <b>'.$this->params->details.'</b>';
        $this->text.= '<br><br>';

        $this->text.= '<table>';

            $this->text.='<tr>';
                $this->text.='<td style="border: 1px solid black; width: 20px">';
                    $this->text.='№';
                $this->text.='</td>';
                $this->text.='<td  style="border: 1px solid black;width: 300px">';
                    $this->text.='Товары (работы, услуги)';
                $this->text.='</td>';
                $this->text.='<td  style="border: 1px solid black; width: 20px">';
                    $this->text.='Кол-во';
                $this->text.='</td>';
        $this->text.='<td  style="border: 1px solid black; width: 20px">';
        $this->text.='Ед';
        $this->text.='</td>';
        $this->text.='<td  style="border: 1px solid black;">';
        $this->text.='Цена';
        $this->text.='</td>';
        $this->text.='<td  style="border: 1px solid black;">';
        $this->text.='Сумма';
        $this->text.='</td>';

            $this->text.='</tr>';

        $this->text.='<tr>';
            $this->text.='<td  style="border: 1px solid black;">';
                $this->text.='1';
            $this->text.='</td>';
            $this->text.='<td  style="border: 1px solid black; width: 300px">';
                $this->text.='Возмещение расходов , понесенные по исполнению договора №'.$this->params->contract.' от '. $this->params->date1;
            $this->text.='</td>';
        $this->text.='<td  style="border: 1px solid black;">';
        $this->text.='1';
        $this->text.='</td>';
        $this->text.='<td  style="border: 1px solid black;">';
        $this->text.='';
        $this->text.='</td>';
        $this->text.='<td  style="border: 1px solid black;">';
        $this->text.=$this->params->sum;
        $this->text.='</td>';
        $this->text.='<td  style="border: 1px solid black;">';
        $this->text.=$this->params->sum;
        $this->text.='</td>';
        $this->text.='</tr>';

        $this->text.= '</table>';
        $this->text.= '<br><br>';
        $this->text.= '<div style="font-weight: bold; text-align: right">';
            $this->text.= 'Итого: '.round($this->params->sum, 2);
            $this->text.= '<br>';
            $this->text.= 'В том числе НДС: '.round(($this->params->sum/118)*18, 2);
            $this->text.= '<br>';
            $this->text.= 'Всего к оплате: '.$this->params->sum;
            $this->text.= '<br>';
        $this->text.= '</div>';
        $this->text.='Всего наименований 1, на сумму '.round($this->params->sum, 2).' руб.';
        $this->text.='<br>';
        $this->text.= mb_ucfirst($this->params->sum_propis, $encoding='UTF-8');
        $this->text.='<br>';
        $this->text.='<hr>';
        $this->text.='<div></div><div></div><div></div>';


        $this->text.='<div>';
            $this->text.='<span style="float: left; text-align: left">';
                $this->text.='<span>';
                $this->text.='Руководитель ';
                $this->text.='</span>';
                $this->text.='<span style="text-decoration: underline;">';
                $this->text.= str_repeat('_', 10);
                $this->text.='Татарникова М.Г.';
                $this->text.='</span>';
            $this->text.='</span>  ';
            $this->text.='<span style="float: right;text-align: right">';
                $this->text.='<span style="white-space: pre;">';
                $this->text.=' Бухгалтер';
                $this->text.='</span>  ';
                $this->text.='<span style="text-decoration: underline">';
                $this->text.= str_repeat('_', 10);
                $this->text.='Митякова Ф.Р.';
                $this->text.='</span>';
            $this->text.='</span>';
        $this->text.='</div>';


        $tbl = "" . date('d.m.Y', time()) . "<br>";
        $pdf->SetFont('freeserif', '', 10);
        $tbl .= '';
        $printOrders = null;
        $tbl .= "<table style='padding: 5px;' border='1'><tbody><tr bgcolor='#ccc'><th width='30%'><strong>Клиент</strong></th></tr></tbody></table>'";

        $pdf->writeHTML($this->text, true, false, false, false, '');

        $pdf->Output('orders.pdf', 'I');

        exit;
    }
}