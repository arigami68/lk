<?php

class TbTypeahead extends CInputWidget
{
    /**
     * @var array the options for the Bootstrap Javascript plugin.
     */
    public $options = array();
    public $form;

    /**
     * Initializes the widget.
     */
    public function init()
    {
        $this->htmlOptions['type'] = 'text';
        $this->htmlOptions['data-provide'] = 'typeahead';

    }

    /**
     * Runs the widget.
     */
    public function run()
    {

        list($name, $id) = $this->resolveNameID();
        $id = $this->id;
        if (isset($this->htmlOptions['id']))
            $id = $this->htmlOptions['id'];
        else
            $this->htmlOptions['id'] = $id;

        if (isset($this->htmlOptions['name']))
            $name = $this->htmlOptions['name'];

        if ($this->hasModel()){
            echo $this->form->textFieldRow($this->model,$this->name, $this->htmlOptions);
            #echo CHtml::activeTextField($this->model, $this->attribute, $this->htmlOptions);
        }else{
            echo CHtml::textField($name, $this->value, $this->htmlOptions);
        }

        $options = !empty($this->options) ? CJavaScript::encode($this->options) : '';
        echo '<script type="text/javascript">';
        print '/*<![CDATA[*/
jQuery(function($) {';
        echo "jQuery('#{$id}').typeahead({$options});";
        print "jQuery('#yw0').tab('show');
jQuery('body').tooltip({'selector':'a[rel=tooltip]'});
jQuery('body').popover({'selector':'a[rel=popover]'});
});
    /*]]>*/";
        print '</script>';
        #Yii::app()->clientScript->registerScript(__CLASS__.'#'.$id, "jQuery('#{$id}').typeahead({$options});");

    }
}
