<?php
class CalcWidget extends CWidget {
    public $model;
    private $text = '';

    public function run() {
        $words = array(
            'from'              => Yii::t("total", 'город отправления'),
            'to'                => Yii::t("total", 'пункт назначения'),
	        'country'           => Yii::t("total", 'cтрана назначения'),
            'weight'            => Yii::t("total", 'вес, кг'),
            'oc'                => Yii::t("total", 'ОЦ'),
            'np'                => Yii::t("total", 'НП'),
            'od'                => Yii::t("total", 'ОД'),
            'np_info'           => Yii::t("total", 'наложенный платеж'),
            'weight_info'       => Yii::t("total", 'вес, кг'),
            'oc_info'           => Yii::t("total", 'объявленная ценность'),
            'clear'             => Yii::t("total", 'очистить'),
            'calculation'       => Yii::t("total", 'рассчитать'),
            'load_to_excel'     => Yii::t("total", 'выгрузить в excel'),
            '№'                 => Yii::t("total", '№'),
            'service'           => Yii::t("total", 'услуга'),
            'tarif_nds'         => Yii::t("total", 'тариф руб, с НДС'),
            'delivery_period'   => Yii::t("total", 'срок доставки, раб.дн.'),
            'total_nds'         => Yii::t("total", 'итого, c НДС'),
            '[x]'               => Yii::t("total", '[x]'),
        );

        $model = $this->model;
        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id'=>'createOrder',
            'type'=>'horizontal',
        ));
            $this->text.= $this->top_info($words);
            $this->text.= $this->top_search($model, $words);
            $this->text.= '<br></br>';
            $this->text.= $this->output($words);

            print $this->text;

        $this->endWidget();

    }
    public function top_info($words){
        $text='
    <table id="Moskow">
        <tr class="tr1">
            <td>
                <div>
                    <p class="class1">'.$words['from'].' <a href="#" rel="tooltip" title="'.$words['from'].'" class="main-question"> <img src='.Yii::app()->assetManager->publish(Yii::getPathOfAlias('images').DS.'question.png').'></a></p>
                </div>
            </td>
            <td>

                <p class="class1">
                    Страна назначения
				</p>
            </td>
            <td>
                <div>
                    <p class="class1">'.$words['to'].'<a href="#" rel="tooltip" title="'.$words['to'].'" class="main-question"> <img src='.Yii::app()->assetManager->publish(Yii::getPathOfAlias('images').DS.'question.png').'></a></p>
                </div>
            </td>
            <td>
                <div>
                    <p class="class1">'.$words['weight'].'<a href="#" rel="tooltip" title="'.$words['weight_info'].'" class="main-question"><img src='.Yii::app()->assetManager->publish(Yii::getPathOfAlias('images').DS.'question.png').'></a></p>
                </div>
            </td>
            <td>
                <div>
                    <p class="class1">'.$words['oc'].' <a href="#" rel="tooltip" title="'.$words['oc_info'].'" class="main-question"><img src='.Yii::app()->assetManager->publish(Yii::getPathOfAlias('images').DS.'question.png').'></a></p>
                </div>
            </td>
            <td>
                <div>
                    <p class="class1">'.$words['np'].' <a href="#" rel="tooltip" title="'.$words['np_info'].'" class="main-question"><img src='.Yii::app()->assetManager->publish(Yii::getPathOfAlias('images').DS.'question.png').'></a></p>
                </div>
            </td>

        </tr>
        <tr>
            <td class="sold">
         ';
        return $text;
    }
    public function top_search($model, $words){
        $text = '';
        if(Yii::app()->user->checkAccess('user')){
            switch(Yii::app()->user->auth->db->country){
                case 276:
                    $text.= CHtml::activeTextField($model,'typeahead2', array('value' => 'Berlin', 'readonly' => 'readonly'));
                    break;
                default:
                    $text.= CHtml::activeTextField($model,'typeahead2', array('value' => 'Москва', 'readonly' => 'readonly'));
            }
        }

	    $text.= '<td class="sold">';
	    $text.='<div>
					<select class="toСountry" style="width: 150px;">
					<option value="643"" >Россия</option>
					<option value="112" >Беларусь</option>
					<option value="398"" >Казахстан</option>
					</secect>
          		</div>';
	    $text.= '</td>';


        $text.= '</td>';
        $text.= '<td class="sold">';
        Yii::app()->clientScript->registerScript("Vars","
            var npR = '".(Yii::app()->user->auth->db->np*100)."';
        ", CClientScript::POS_HEAD);

        ob_start();
        $this->widget('bootstrap.widgets.TbTypeahead', array(
            'name'=>'typeahead',
            'options'=>array(
                'items'=> 10,
                'matcher'=>"js:function(item, process) {
                            return item;
                        }",
                'source'=>'js:function(item, process) {
                               if(item.length > obj.len){
                                    $.ajax({
                                        data: {item : item, limit: obj.limit, format: "json"},
                                        dataType:"json",
                                        type: "POST",
                                        url: obj.url,
                                         success: process,

                                        beforeSend: function(){
                                            if(MEG.calc.settings.req){
                                            return false;
                                            }else{
                                                MEG.calc.settings.req = 1;
                                            }
                                        },
                                        complete: function(){
                                            MEG.calc.settings.req = 0;
                                        },});

                               }
                            }',
                'updater'=>"js:function(item) {

                                var kladr = item.split(',').splice(1,1);
                                $('#hidden_to').val(kladr);
                                var t = item.split(',').splice(2, 2);
                                var item = t.join(', ');

                                return item;
                            }",
                'highlighter'=>"js:function(item) {
                                var t = item.split(',').splice(2, 2);
                                item = t.join(', ');
                                return item;
                }",

            ),
            'htmlOptions' => array(
                'autocomplete' => "off",
            ),
        ));
        $out = ob_get_contents();

        ob_end_clean();
        $text.= $out;
        $text.= '</td>';

        $text.= '<td class="sold">';
        $text.= CHtml::activeTextField($model,'weight', array('class'=>'span1'));
        $text.= '</td>';
        $text.= '<td class="sold">';
        $text.= CHtml::activeTextField($model,'oc', array('class'=>'span1 oc', 'maxlength'=>32));
        $text.= '</td>';
        $text.= '<td class="sold">';
        $text.= CHtml::activeTextField($model,'np', array('class'=>'span1 np', 'maxlength'=>32, 'autocomplete' => "off",));
        $text.= '</td>';
        $text.= '</tr>';
        $text.= '<tr>';
        $text.= '<td colspan="6">';
        $text.= '<table>';
        $text.= '<tr>';
        $text.= '<td width="50" align="left" height="50" style="">';
        $text.= '<div>';
        ob_start();
        $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType' => 'reset',
                'type'       => '',
                'label'      => $words['clear'],
                'size'       => 'Normal',
                'htmlOptions'=>array('id'=> 'get_clean', 'class' => 'btn1'),
            )
        );
        $out = ob_get_contents();
        ob_end_clean();
        $text.= $out;

        $text.= '</div>';
        $text.= '</td>';
        $text.= '<td width="100%" align="right">';
        $text.= '<div>';
        ob_start();
        $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType' => 'button',
                'type'       => 'primary',
                'label'      => $words['calculation'],
                'size'       => 'Normal',
                'htmlOptions'=>array('id'=> 'get_payment', 'class' => 'btn1'),
            )
        );
        $out = ob_get_contents();
        ob_end_clean();
        $text.= $out;
        $text.= '</div>';
        $text.= '</td>';
        $text.= '</tr>';
        $text.= '</table>';
        $text.= '</td>';
        $text.= '</tr>';
        $text.= '<tr>';
        $text.= '</tr>';
        $text.= '<td  height="30px"></td>';

        $text.= '</table>';

        return $text;
    }
    public function output($words){


        $text = '';
        $text.= '<div id="put_vladivostok"></div>';
        $text.= '<table id="Xabarovsk" border=1>';
        $text.= '<tr>';
        $text.= '<td class="sold">';
        $text.= '<div>';
        $text.= $words['№'];
        $text.= '</div>';
        $text.= '</td>';
        $text.= '<td class="sold">';
        $text.= '<div>';
        $text.= $words['from'];
        $text.= '</div>';
        $text.= '</td>';
        $text.= '<td class="sold">';
        $text.= '<div>';
        $text.= $words['to'];
        $text.= '</div>';
        $text.= '</td class="sold">';
        $text.= '<td class="sold">';
        $text.= '<div>';
        $text.= $words['od'];
        $text.= '</div>';
        $text.= '</td>';
        $text.= '<td class="sold">';
        $text.= '<div>';
        $text.= $words['service'];
        $text.= '</div>';
        $text.= '</td>';
        $text.= '<td class="sold">';
        $text.= '<div>';
        $text.= $words['weight_info'];
        $text.= '</div>';
        $text.= '</td>';
        $text.= '<td class="sold">';
        $text.= '<div>';
        $text.= $words['tarif_nds'];
        $text.= '</div>';
        $text.= '</td>';
        $text.= '<td class="sold">';
        $text.= '<div>';
        $text.= $words['delivery_period'];
        $text.= '</div>';
        $text.= '</td>';
        $text.= '<td class="sold">';
        $text.= '<div>';
        $text.= $words['[x]'];
        $text.= '</div>';
        $text.= '</td>';
        $text.= '</tr>';
        $text.= '</table>';
        $text.= '<table id="Egorievsk" class="sold">';
        $text.= '<tr>';
        $text.= '<td>';

        $text.= '<div id="non-nds" class="Panki" style="height: 20px">';
        # $this->text.= '<p class="itog">';
        # $this->text.= '0';
        # $this->text.= '</p>';
        #$this->text.= '<p class="itog-txt">';
        # $this->text.= 'Итого, без НДС';
        #$this->text.= '</p>';
        # $this->text.= '</div>';
        #  $this->text.= '<div id="nds" class="Panki " style="height: 20px">';
        $text.= '<p class="itog">';
        $text.= '0';
        $text.= '</p>';
        $text.= '<p class="itog-txt">';
        $text.= $words['total_nds'];
        $text.= '</p>';
        $text.= '</div>';

        $text.= '</td>';
        $text.= '</tr>';
        $text.= '</table>';

        $text.= CHtml::hiddenField('hidden', '1', array('id' => 'hidden_from'));
        $text.= CHtml::hiddenField('hidden', '', array('id' => 'hidden_to'));
        ob_start();
        $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType' => 'submit',
                'type'       => 'primary',
                'label'      => $words['load_to_excel'],
                'size'       => 'Normal',
                'htmlOptions'=>array('id'=> 'excel', 'name' => 'excel', 'class' => 'btn1'),
            )
        );
        $out = ob_get_contents();
        ob_end_clean();
        $text.= $out;

        return $text;
    }
}