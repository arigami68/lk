<?php
class HelpSearchWidget extends CWidget {
    public static function ORDS_departure($criteria, $pagination, $sort, $class, $id){
        $sort->attributes = array( 'shipping' => array('shipping'),
            'id' => array('id'),
        );
        if($find = ORDS::model()->findAll($criteria)){
            $array = array();
            foreach($find as $k=>$v){
                $i = $k+1;
                $array[$i]['agent']          = $v->agent;
                $array[$i]['megapolis']      = $v->megapolis;
                $array[$i]['registry']       = $v->registry;

                $array[$i]['dateCreate']     = date('d-m-Y H:i', $v->dateCreate);
                $array[$i]['dateSend']       = $v->dateSend;
                $array[$i]['idorders']       = $v->id;

                $array[$i]['id']             = $i;
                $array[$i]['np']             = $v->costDelivery;
                $array[$i]['oc']             = $v->costPublic;
                $array[$i]['region']         = $v->region;
                $array[$i]['area']           = $v->area;
                $array[$i]['city']           = $v->city;
                $array[$i]['fio']            = $v->fio;
                $array[$i]['steps']          = isset($v->track->last) ? $v->track->last : '';
            }
        }

        return new CArrayDataProvider($array, array(
            'pagination'=> $pagination,
            'sort' => $sort,
        ));
    }
    public static function ORDS_registry($criteria, $pagination, $sort, $class, $count = 10){
        $criteria->scopes = array('user');
        $criteria->select = 'registry, dateCreate, dateSend, COUNT(megapolis) AS megapolis, opTake';
        $criteria->group = 'registry';

        $pagination->pageVar = 'page';

        if($count <= 50){
            $pagination->pageSize = $count;
        }else{
            $pagination->pageSize = 10;
        }

        $sort->sortVar = 'sort';
        $sort->attributes = array('id' => array('id'));
        $sort->defaultOrder='dateCreate DESC';

        return new CActiveDataProvider(get_class($class), array(
            'criteria' => $criteria,
            'pagination'=> $pagination,
            'sort' => $sort,
        ));
    }
    public static function reORDS_status($criteria, $pagination, $sort, $class){
        if(true){
            $pageSize = 30;
            $pagination->pageVar = 'page';
            $pagination->pageSize = $pageSize;
            if($s = Yii::app()->request->getQuery('s')){ $criteria->compare('dateCreate >', strtotime($s)); }
            if($f =Yii::app()->request->getQuery('f')){  $criteria->compare('dateCreate <', strtotime($f)); }
            if(!$f OR !$s){
                return new CArrayDataProvider(array(), array(
                    'pagination'=> $pagination,
                    'sort' => $sort,
                ));
            }
            $criteria->select = 'id, registry, dateCreate, costDelivery, costPublic, phone, fio, region, area, city, agent, megapolis';
            $sort->attributes = array('megapolis', 'costPublic', 'costDelivery', 'id', 'dateCreate');
        }
        return new CActiveDataProvider(get_class($class), array(
            'criteria'   => $criteria,
            'pagination' => $pagination,
            'sort'       => $sort,
        ));
    }
    public static function ORDS_status($criteria, $pagination, $sort, $this){

        $pageSize = 20;
        $size = MegapolisPagination::params()->MEGgetPage()*$pageSize-$pageSize;
        $pagination->pageVar = 'page';
        $pagination->pageSize = $pageSize;

        $criteria->limit= $pageSize;

        $ORDS = new stdClass();
        $ORDS->db =  new stdClass();
        $ORDS->array = new stdClass();

        if($s = Yii::app()->request->getQuery('s')){ $criteria->compare('dateCreate >', strtotime($s)); }
        if($f =Yii::app()->request->getQuery('f')){  $criteria->compare('dateCreate <', strtotime($f)); }
        if(!$f OR !$s){
            return new CArrayDataProvider(array(), array(
                'pagination'=> $pagination,
                'sort' => $sort,
            ));
        }
        $criteria->offset= $size;


        $sort->attributes = array( 'shipping' => array('shipping'),
            'id' => array('id'),
        );
        $count = ORDS::model()->count($criteria);

        $ORDS->db->ORDS = ORDS::model()->findAll($criteria);
        if($ORDS->db->ORDS){
            $db = function($ORDS){
                $array = new stdClass();

                foreach($ORDS as $k=>$v){
                    $megapolis = $v->megapolis;
                    $array->orders["$megapolis"]["registry"] = $v->registry;
                    $array->orders["$megapolis"]["idorder"] = $v->id;
                    $array->orders["$megapolis"]["dateCreate"] = $v->dateCreate;
                    $array->orders["$megapolis"]["np"] = $v->costDelivery;
                    $array->orders["$megapolis"]["oc"] = $v->costPublic;
                    $array->orders["$megapolis"]["phone"] = $v->phone;
                    $array->orders["$megapolis"]["fio"] = $v->fio;
                    $array->orders["$megapolis"]["region"] = $v->region;
                    $array->orders["$megapolis"]["area"] = $v->area;
                    $array->orders["$megapolis"]["city"] = $v->city;
                    $array->orders["$megapolis"]["agent"] = $v->agent;
                    $array->orders["$megapolis"]["db"] = $v;
                    $array->megapolis[] = $v->megapolis;
                }
                return $array;
            };
            $ORDS->array->ORDS = $db($ORDS->db->ORDS);
            unset($ORDS->db->ORDS);
            $criteria->addInCondition('megapolis', $ORDS->array->ORDS->megapolis);
            $ORDS->db->TRA2 = ORDS::model()->findAll($criteria);

            foreach($ORDS->db->TRA2 as $k=>$v){
                $id = $v->id;
                $megapolis = $v->megapolis;
                if(!$v->track) continue;
                if($v->track->data){
                    $ORDS->array->ORDS->track[$megapolis] = TRA2::processing($v->track);
                }
            }
            $process = function($ORDS, $size){
                $array = Array();
                $i=$size;

                foreach($ORDS->orders as $k=>$v){
                    if(true){
                        $last = $v['db']->track->last ? $v['db']->track->last : '-';
                        $datelast = $v['db']->track->dateLast ? $v['db']->track->dateLast : '-';
                        $agent = $v['agent'] ? $v['agent'] : '-';
                        $address = $v['region'].$v['area'].$v['city'];
                    }
                        $array[$i]["id"] = $i;
                        $array[$i]["idorders"] = $ORDS->orders[$k]['idorder'];
                        $array[$i]["megapolis"] = $k;
                        $array[$i]["agent"] = $agent;
                        $array[$i]["dateCreate"] = date('Y-m-d H:i', $ORDS->orders[$k]['dateCreate']);
                        $array[$i]["registry"] = $ORDS->orders[$k]['registry'];
                        $array[$i]["phone"] = $ORDS->orders[$k]['phone'];
                        $array[$i]["fio"] = $ORDS->orders[$k]['fio'];
                        $array[$i]["np"] = isset($ORDS->orders[$k]['np']) ? $ORDS->orders[$k]['np'] : 0;
                        $array[$i]["oc"] = isset($ORDS->orders[$k]['oc']) ? $ORDS->orders[$k]['oc'] : 0;
                        $array[$i]["address"] = $address;
                        $array[$i]["status"] = $last;
                        $array[$i]["status_date"] = $datelast;
                        $i++;
                }
                return $array;
            };
        }else{
            $array = Array();
        }

        if(isset($ORDS->array->ORDS)){
            $array = $process($ORDS->array->ORDS, $size >= $pageSize ? $size : 0);
            if(count($array) <= $size){
                $empty = array_fill(0, $size, 1);
                $result = array_merge($empty, $array);
                $array = $result;
            }
        }

        $data = new CArrayDataProvider($array, array(
            'pagination'=> $pagination,
            'sort' => $sort,
            'TotalItemCount' => $count,
        ));
        
        return $data;
    }

    public static function ORDS_agent($criteria, $pagination, $sort, $class){
        $criteria->select = 'agent, megapolis, dateCreate, costPublic, costDelivery, fio, address, phone, region, area, city';
        if($s = Yii::app()->request->getQuery('s')){ $criteria->compare('dateCreate >', strtotime($s)); }
        if($f =Yii::app()->request->getQuery('f')) { $criteria->compare('dateCreate <', strtotime($f)); }
        if(!$f OR !$s){
            return new CArrayDataProvider(array(), array(
                'pagination'=> $pagination,
                'sort' => $sort,
            ));
        }
        $pagination->pageVar = 'page';
        $pagination->pageSize = 30;
        $sort->sortVar = 'sort';

        return new CActiveDataProvider($class, array(
            'criteria' => $criteria,
            'pagination'=> $pagination,
            'sort' => $sort,
        ));
    }
    public static function ORDS_return($criteria, $pagination, $sort, $class){
        if($s = Yii::app()->request->getQuery('s')){ $criteria->compare('dateCreate >', strtotime($s));}
        if($f =Yii::app()->request->getQuery('f')){ $criteria->compare('dateCreate <', strtotime($f));}

        if(!$f OR !$s){
            return new CArrayDataProvider(array(), array(
                'pagination'=> $pagination,
                'sort' => $sort,
            ));
        }
        if(true){
            $pagination->pageVar = 'page';
            $pagination->pageSize = 20;
            $sort->sortVar = 'sort';
            $sort->attributes = array( 'shipping' => array('shipping'),
                'id' => array('id'),
            );
            $criteria->select = 'id,megapolis, dateCreate, costDelivery, costPublic, fio, phone, region, area, city, t.agent';
        }
        $criteria->join = 'LEFT JOIN track2 t2 ON t2.id_order=t.id';
        $criteria->addCondition('t2.refund=1');

        $FIND = ORDS::model()->findAll($criteria);

        if($FIND){
            $db = function($ORDS){
                $array = new stdClass();
                foreach($ORDS as $k=>$v){
                    $megapolis = $v->megapolis;
                    $array->orders["$megapolis"]["registry"] = $v->registry;
                    $array->orders["$megapolis"]["db"] = $v;
                    $array->orders["$megapolis"]["idorder"] = $v->id;
                    $array->orders["$megapolis"]["dateCreate"] = $v->dateCreate;
                    $array->orders["$megapolis"]["np"] = $v->costDelivery;
                    $array->orders["$megapolis"]["oc"] = $v->costPublic;
                    $array->orders["$megapolis"]["phone"] = $v->phone;
                    $array->orders["$megapolis"]["fio"] = $v->fio;
                    $array->orders["$megapolis"]["region"] = $v->region;
                    $array->orders["$megapolis"]["area"] = $v->area;
                    $array->orders["$megapolis"]["city"] = $v->city;
                    $array->orders["$megapolis"]["agent"] = $v->agent;
                    $array->megapolis[] = $v->megapolis;
                }
                return $array;
            };

            $array = $db($FIND);

            $process = function($ORDS){
                $array = Array();
                $i=1;
                foreach($ORDS->orders as $k=>$v){
                    $array[$i]["id"] = $i;
                    $array[$i]["idorders"] = $ORDS->orders[$k]['idorder'];
                    $array[$i]["megapolis"] = $k;
                    $array[$i]["agent"] = isset($ORDS->track[$k]->code) ? $ORDS->track[$k]->code : '';
                    $array[$i]["dateCreate"] = date('Y-m-d H:i', $ORDS->orders[$k]['dateCreate']);
                    $array[$i]["registry"] = $ORDS->orders[$k]['registry'];
                    $array[$i]["phone"] = $ORDS->orders[$k]['phone'];
                    $array[$i]["fio"] = $ORDS->orders[$k]['fio'];

                    $array[$i]["datelast"] = date('d-m-Y', strtotime($v['db']->track->dateLast));
                    $array[$i]["agent"] = $ORDS->orders[$k]['agent'];
                    $array[$i]["np"] = isset($ORDS->orders[$k]['np']) ? $ORDS->orders[$k]['np'] : 0;
                    $array[$i]["oc"] = isset($ORDS->orders[$k]['oc']) ? $ORDS->orders[$k]['oc'] : 0;
                    $array[$i]["address"] = $v['db']['region'].' '.$v['db']['area'].' '.$v['db']['city'];
                    $array[$i]["status"] = isset($step) ? $step : '';
                    $array[$i]["status_date"] = isset($step_date) ? $step_date : '';
                    $i++;
                }

                return $array;
            };

            $provider = $process($array);
        }else{ $provider = Array();
        }

        return new CArrayDataProvider($provider, array(
            'pagination'=> $pagination,
            'sort' => $sort,
        ));
    }
    public function ORDS_agents($criteria, $pagination, $sort, $class){

        $criteria->scopes = array('user');
        $criteria->select = 'id, megapolis, dateCreate';
        $pagination->pageVar = 'page';
        $pagination->pageSize = 10;
        $sort->sortVar = 'sort';
        return new CArrayDataProvider(array(), array(
            #'criteria' => $criteria,
            'pagination'=> $pagination,
            'sort' => $sort,
        ));
    }
}