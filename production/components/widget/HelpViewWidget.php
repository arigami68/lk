<?php
class HelpViewWidget extends CWidget {
    public static function ORDS_action_operation_register($data){
        $flag = empty($data->dateSend) ? 1 : 0;
        if($flag){ $array['s'] =  array('htmloption' =>  array('title' => Yii::t("total", 'закрыть реестр'), 'class' => 's_read'),'url' => '/order/order/close?registry='.$data['registry']);
        }else{ $array['s'] =  array('htmloption' =>  array('title' => Yii::t("total", 'реестр закрыт'), 'class' => ''), 'url' => '');
        }
        return CHtml::link($array['s']['htmloption']['title'], $array['s']['url'] ,$array['s']['htmloption']);
    }
    public static function ORDS_status_operation_register($data){
        $flag = empty($data->dateSend) ? 1 : 0;
        $glyphicons = '/img/glyphicons-halflings.png';
        $array['print'] =  array(
            'htmloption' =>  array('title' => 'Печать штрихкодов по реестру', 'class' => 'printR icon-barcode'),
        );
        $array['print2'] =  array(
            'htmloption' =>  array('title' => 'Печать реестра', 'class' => 'icon-print'),
            'url' => '/orders?registry='.$data['registry'].'&operation=print',
        );
        $img['read'] = '/megapolis-open.gif';
        $id=$data->registry;
        $array['registry'] = array(
            'src' =>  CHtml::image(Yii::getPathOfAlias('link_images').$img['read']),
            'url' => '/ordes/orders/addDep?id='.$id,
            'htmloption' =>  array('title' => 'Просмотр отправлений', 'class' => 'button icon-th-large'),
        );
        if($flag){
            $array['s'] =  array(
                'htmloption' =>  array('title' => 'Просмотр', 'class' => 'icon-eye-open'),
                'url' => '/order?id='.$data['registry'].'&operation='.ORDS::$OPERATION_READ,
            );
            $array['e'] =  array(
                'htmloption' =>  array('title' => 'Редактировать', 'class' => 's_update icon-edit'),
                'url' => '/order?id='.$data['registry'].'&operation='.ORDS::$OPERATION_UPDATE,
            );
            $array['d2'] =  array(
                'htmloption' =>  array('title' => 'Удалить реестр', 'class' => 's_delete icon-trash'),
                'url' => '/order?registry='.$data['registry'].'&operation=delete',
            );


            return
                CHtml::link('', $array['s']['url'] ,$array['s']['htmloption']).
                ' '.
                CHtml::link('', $array['e']['url'] ,$array['e']['htmloption']).
                ' '.
                CHtml::link('', $array['d2']['url'] ,$array['d2']['htmloption']).
                '&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp'.
                #CHtml::link('', '' ,$array['print']['htmloption']).
                '&nbsp&nbsp&nbsp'.
                CHtml::link('', '' ,$array['registry']['htmloption'])
                #'&nbsp&nbsp&nbsp'.
                #CHtml::link('', $array['print2']['url'] ,$array['print2']['htmloption'])
                ;
        }else{
            $array['s'] =  array(
                'htmloption' =>  array('title' => 'Просмотр', 'class' => 'icon-eye-open'),
                'url' => '/order?id='.$data['registry'].'&operation='.ORDS::$OPERATION_READ,
            );
            return
                CHtml::link('', $array['s']['url'] ,$array['s']['htmloption']).
                str_repeat('&nbsp', 12).
                CHtml::link('', '' ,$array['print']['htmloption']).
                '&nbsp&nbsp&nbsp'.
                CHtml::link('', '' ,$array['registry']['htmloption']).
                '&nbsp&nbsp&nbsp'.
                CHtml::link('', $array['print2']['url'] ,$array['print2']['htmloption'])
                ;
        }
    }
    public static function ORDS_opTake($data){
        $array = array(
            'currier' => array( 'htmloption' =>  array('title' => Yii::t("total", 'курьером'))),
            'window'  => array( 'htmloption' =>  array('title' => Yii::t("total", 'в окне приема'))),
        );
        return $data->opTake > 1 ? CHtml::label($array['currier']['htmloption']['title'], 's') : CHtml::label($array['window']['htmloption']['title'], 'f');
    }
    public static function ORDS_status_operation($data){
        $array['d'] =  array(
            'htmloption' =>  array('title' => 'Удалить отправление', 'class' => 's2_delete icon-trash'),
            'url' => '/order?id='.$data['idorders'].'&operation=delete',
        );
        $array['print'] =  array(
            'htmloption' =>  array('title' => 'Печать штрихкода по отправлению', 'class' => 'printO icon-print', 'onclick'=>'
               window.open("/orders/orders/print?order='.$data['idorders'].'",
               "Штрих код",
               "width=420,height=230,resizable=yes,scrollbars=yes,status=yes"
            )'),
        );
        return
            !empty($data->dateSend) ? CHtml::link('', $array['d']['url'] ,$array['d']['htmloption']).'&nbsp&nbsp&nbsp' : ''.
                CHtml::link('', '' ,$array['print']['htmloption'])
            ;
    }
    public static function ORDS_address_dep($data){
        $result = '';
        if($data['region']) $result.= $data['region'].', ';
        if($data['area'])   $result.= $data['area'].', ';
        if($data['area'])   $result.= $data['city'].', ';
        $result = mb_substr($result, 0, -2);
        return $result;
    }
    public static function searchdateCreate($data){
        $date = date('d-m-Y H:i', $data->dateCreate);
        return $date;
    }
}