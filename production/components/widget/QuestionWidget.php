<?php
# Отображение инфо в нужном месте
class QuestionWidget extends CWidget {
    public static $PAGE_SETTING = 'settings';
    public static $PAGE_ORDER = 'order';
    public static $PAGE_ORDERT = 'ordert';
    public static $TITLE_ORDER = 'Загрузка реестра';
    public $page;
    public $title;

    public function run($r = NULL) {
        $array = array(
            self::$PAGE_SETTING => array(
                array('top' => '200px', 'left' => '500px', 't' => 'Информация')
            ),
            self::$PAGE_ORDER => array(
                #array('top' => '256px', 'left' => '864px', 't' => 'Операции над отправлениями'),
                #array('top' => '256px', 'left' => '700px', 't' => 'Действия. В частности закрытие реестра'),
            ),
            self::$PAGE_ORDERT => array(
                /*
                array('top' => '306px', 'left' => '635px', 't' => 'Имя Вашей организации'),
                array('top' => '356px', 'left' => '635px', 't' => 'Дата сдачи/приема отправлений'),
                array('top' => '419px', 'left' => '835px', 't' => 'Вид приёма'),
                array('top' => '496px', 'left' => '835px', 't' => 'Адрес места сдачи'),
                array('top' => '547px', 'left' => '547px', 't' => 'Объем. В цифрах'),
                array('top' => '617px', 'left' => '635px', 't' => 'Номер договора'),
                array('top' => '669px', 'left' => '635px', 't' => 'Номер реестра'),
                */
            ),

        );
        $x='-100px';
        $y='-100px';
        $t='';
        $text = function($value){
            $text = '';
            foreach($value as $array){
                $text.= '<p style="position: absolute; top:'.$array['top'].'; left: '.$array['left'].'; z-index: 1;">
                        <a href="#" rel="tooltip" title="'.$array['t'].'" >
                            <img src='.Yii::app()->assetManager->publish(Yii::getPathOfAlias('images').DS.'question.png').'>
                        </a>
                    </p>';
            }
            return $text;
        };
        switch($this->page){
            case self::$PAGE_SETTING:

            break;
            case self::$PAGE_ORDER:
                if($this->title == self::$TITLE_ORDER){
                    print $text($array[self::$PAGE_ORDERT]);
                }else{
                    print $text($array[self::$PAGE_ORDER]);
                }
            break;
        }
    }
}