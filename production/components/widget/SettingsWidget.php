<?php

class SettingsWidget extends CWidget {
    public $model;
    public $setting;
    private $text = '';

    public function run() {
        $setting = $this->setting;
        $model = $this->model;

        ob_start();
        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id'=>'createOrder',
            'type'=>'horizontal',
        ));
        $out = ob_get_contents();
        ob_end_clean();
        $this->text.= $out;
        $this->text.='<br>';
            if(Yii::app()->user->checkAccess('user')){
                 $this->text.=$form->dropDownListRow($model, 'getnum', array(
                        0 =>  'Автоматически',
                        1 =>  'Из диапазона',)
                );
                $this->text.='<div id="range" style="display: none">';
                $this->text.= $form->textFieldRow($model,'range',array('rows'=>2, 'class'=>'span2', 'maxlength'=>32, 'hint' => '<small>x10.000 номеров | <span id=span_range>10.001 - 20.001</span></small>'));
                $this->text.='</div>';
                $this->text.='<div>';
                $model->address_dep = 1;
                $this->text.=$form->dropDownListRowDisabled($model, 'address_dep', array(
                            1 =>  ' г. Москва, ул. Котляковская д. 3, стр.1',
                        )
                        ,array('rows'=>2, 'class'=>'span4')
                );
                #$this->text.=$form->textFieldRow($model,'address_dep2',array('rows'=>2, 'class'=>'span4'));
                #$this->text.= $form->dropDownListRow($model, 'dropdown_ad2', $model->dropdown_ad2);
                $this->text.='</div>';
                $model->np = $setting['np'].' %';
                $this->text.= $form->textFieldRowDisabled($model,'np',array('rows'=>2, 'class'=>'span1', 'maxlength'=>32));
                ob_start();
                $this->widget('bootstrap.widgets.TbButton', array(
                    'type'=>'success',
                    'label'=>'Cохранить/ добавить',
                    'htmlOptions'=>array('id'=>'save'),
                ));
                $out = ob_get_contents();
                ob_end_clean();
                $this->text.= $out;

                $this->text.='<br><br><br>';
            }
        ob_start();
        $out = ob_get_contents();
        ob_end_clean();
        $this->text.= $out;
        $this->endWidget();

        print $this->text;

    }
}