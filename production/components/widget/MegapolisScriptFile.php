<?php

class MegapolisScriptFile extends CWidget {
    public $cs;
    public $flag;
    public function run() {
        Yii::app()->bootstrap->register();

        if(get_class($cs = $this->cs) === 'ExtendedClientScript'){
            Yii::app()->clientScript->registerCssFile(
                Yii::app()->clientScript->getCoreScriptUrl().
                '/jui/css/base/jquery-ui.css'
            );
            if(Yii::app()->user->checkAccess('user')){
                switch(Yii::app()->user->auth->db->country){
                    case 276:
                        $language     = 'ger';
                        break;
                    default:
                        $language = '';
                }
            }

            $cs->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('js').DS.'megapolis.main.js'));
            $cs->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('js').DS.'jquery.jgrowl.min.js'));
            $cs->registerCssFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('css').DS.'jquery.jgrowl.min.css'));
            $cs->registerCssFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('css').DS.'megapolis.main.css'));

            switch($this->flag){
                case 'order':
                    $cs->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('js').DS.'/datepicker/jquery.ui.datepicker-ru.js'));
                    $cs->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('js').DS.$language.DS.'megapolis.order.js'));
                    $cs->registerCssFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('css').DS.'megapolis.order.css'));
                break;
                case 'getcur':
                    $cs->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('js').DS.'/megapolis.app.js'));
                    $cs->registerCssFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('css').DS.'/getcur.css'));
                break;
                case 'app':
                    $cs->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('js').DS.'/datepicker/jquery-ui.js'));
                    $cs->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('js').DS.'/datepicker/jquery.ui.datepicker-ru.js'));
                    $cs->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('js').DS.'/megapolis.app.js'));
                    $cs->registerCssFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('css').DS.'/getcur.css'));
                break;
                case 'calc':
                    $cs->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('js').DS.'/total.js'));
                    $cs->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('js').DS.'/megapolis.calc.js'));
                    $cs->registerCssFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('css').DS.'/megapolis.calc.css'));
                break;
                case 'documents':
                    $cs->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('js').DS.'/total.js'));
                    $cs->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('js').DS.'/megapolis.documents.js'));
                    $cs->registerCssFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('css').DS.'/megapolis.calc.css'));
                break;
				case 'sms':
					$cs->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('js').DS.'/sms.js'));
					$cs->registerCssFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('css').DS.'/sms.css'));
					break;
	            case 'nomenclature':
		            $cs->registerCssFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('css').DS.'/megapolis.myw.css'));
		        break;
	            case 'group':

		            Yii::app()->getClientScript()->registerCoreScript( 'jquery.ui' );
		            $cs->registerCssFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('css').DS.'/megapolis.warehouse.css'));
		            $cs->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('js').DS.'/megapolis.warehouse.js'));
		            //$cs->registerScriptFile('http://idea-logic.ru/bitrix/js/idea/typeahead.js');
		            $cs->registerCssFile("http://idea-logic.ru/design/css/idea/jquery.formstyler.css");

		            $cs->registerScriptFile('https://twitter.github.io/typeahead.js/releases/latest/typeahead.bundle.js');
		            $cs->registerScriptFile('http://twitter.github.io/typeahead.js/releases/latest/bloodhound.js');
					$cs->registerScriptFile('https://twitter.github.io/typeahead.js/releases/latest/typeahead.bundle.js');

		        break;

	            case 'delivery':

		            $cs->registerCssFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('css').DS.'/megapolis.delivery.css'));
		            $cs->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('js').DS.'/megapolis.delivery.js'));

		        break;
            };
        }
    }
}