<?php
/*
 * @name :: MegapolisLanguageWidget
 * @comment :: Смена языка
 *
 */
class MegapolisLanguageWidget extends CWidget {
    public static $ENG = 'eng';
    public static $RUS = 'ru';
    public static $GER = 'ger';

    public function run(){
        if($language = Yii::app()->request->cookies['language']){
            $lang = $language->value;
            switch($lang){
                case self::$ENG:
                    $short = self::$ENG;
                break;
                case self::$RUS:
                    $short = self::$RUS;
                break;
                case self::$GER:
                    $short = self::$GER;
                break;
                default:
                    $short = self::$RUS;
                break;
            }

            Yii::app()->language = $short;
        }
    }

}