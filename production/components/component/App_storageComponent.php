<?php

class App_storageComponent extends CComponent{
     
    public $doc_type = 590;
    public $id;
    public $contractor;
    public $provider;
    public $internal;
    public $artical;
    public $nomencl;
    public $unit;
    public $amount;
    public $woVAT;
    public $wiVAT;
    public $rateVAT;
    public $cost;
    public $note;
    public $product;
    
    public function __construct(){
        $behavior = $this->behaviors();
        $this->attachBehavior('extras', $behavior['rend']['class']);
    }
    public function load($components){
        foreach($components as $k=>$component){
            $this->id = $component['id'];
            $product = $this->product[] = new $this;
            $product->contractor = $component['contractor'];
            $product->provider = $component['provider'];
            $product->internal = $component['internal'];
            $product->artical = $component['artical'];
            $product->nomencl = $component['nomencl'];
            $product->unit = $component['unit'];
            $product->amount = $component['amount'];
            $product->woVAT = $component['woVAT'];
            $product->wiVAT = $component['wiVAT'];
            $product->rateVAT = $component['rateVAT'];
            $product->cost = $component['cost'];
            $product->note = $component['note'];
        }
    }
    public function behaviors(){
        return array(
            'rend' => array(
                'class'=> new ConvertToFormatOrders(),
            ),
        );
    }
    public function getdoc_type(){return $this->doc_type;}
    
    public function convertToXML(){
        $data = $this;
        $products = $this->product;
        $xml = new DOMDocument( "1.0", "UTF-8" );
        $object = new stdClass();

        $object->document = $xml->createElement("document");
        $object->doc_type = $xml->createElement("doc_type" , $data->doc_type);
        $object->application = $xml->createElement("application");
        
        $object->id = $xml->createElement("id", $data->id);
        $object->application->appendChild($object->id);
        $object->products = $xml->createElement("products");
        
        while(($product =  array_pop($products)) !== NULL ){
            $object->products->product[$product->artical] = $xml->createElement("product");
            
            $object->products->product[$product->artical]->appendChild($xml->createElement("contractor", $product->contractor));
            $object->products->product[$product->artical]->appendChild($xml->createElement("provider", $product->provider));
            $object->products->product[$product->artical]->appendChild($xml->createElement("internal", $product->internal));
            $object->products->product[$product->artical]->appendChild($xml->createElement("artical", $product->artical));
            $object->products->product[$product->artical]->appendChild($xml->createElement("nomencl", $product->nomencl));
            $object->products->product[$product->artical]->appendChild($xml->createElement("unit", $product->unit));
            $object->products->product[$product->artical]->appendChild($xml->createElement("amount", $product->amount));
            $object->products->product[$product->artical]->appendChild($xml->createElement("woVAT", $product->woVAT));
            $object->products->product[$product->artical]->appendChild($xml->createElement("wiVAT", $product->wiVAT));
            $object->products->product[$product->artical]->appendChild($xml->createElement("rateVAT", $product->rateVAT));
            $object->products->product[$product->artical]->appendChild($xml->createElement("cost", $product->cost));
            $object->products->product[$product->artical]->appendChild($xml->createElement("note", $product->note));

            $object->products->appendChild($object->products->product[$product->artical]);
        }
        $object->document->appendChild($object->doc_type);
        $object->document->appendChild($object->application);
        $object->document->appendChild($object->products);
        $xml->appendChild($object->document);
        $result = $xml->saveXML();
        
        return isset($result) ? $result : 0;
    }
    
    public function parseXML($xml){
         $doc = simplexml_load_string($xml);
         if($doc->error){
             $string = (string) $doc->errors->error;
             if($string == 'Этот номер не уникален') return FuncMegapolis::$API_MEGAPOLIS_NUMBER_REPEATED;
         }
         if($doc->good){
             return FuncMegapolis::$API_MEGAPOLIS_NUMBER_TRUE;
         }
         
         return FuncMegapolis::$API_MEGAPOLIS_NUMBER_FALSE;
    }
}