<?
class ConvertToXml{
    public static $instance;

    public static function get(){ return empty(self::$instance) ? self::$instance = new self() : self::$instance;}
    public static function getFormatMegapolis(FileLoadOrderComponent $component){
            $pile['array'] = array(
                'document' => array(
                    'doc_type' => $component->docType,
                    'doc_date' => $component->dateCreate,
                    'key' => $component->secretkey,
                ),
                'order' => array(
                    'registry' => $component->registry,
                    'megapolis' => $component->megapolis,
                    'agent_id' => $component->agent,
                    'comment' =>  $component->comment,
                    'articles' => array(
                        'article_id' => '',
                        'article_name' => '',
                        'article_price' => '',
                        'article_weight' => '',
                        'article_count' => '',
                    ),
                    'option' => array(
                        'op_inventory' => $component->opInventory,
                        'op_category' => $component->opCategory,
                        'op_notification' => $component->opNotification,
                        'op_fragile' => $component->opFragile,
                        'op_packing' => $component->opPacking,
                        'op_type' => $component->opType, # Категория отправления
                        'op_take' => $component->opTake, # Вид приема
                    ),
                    'cost' => array(
                        'cost_delivery' => $component->costDelivery,
                        'cost_insurance' => $component->costInsurance,
                        'cost_public' => $component->costPublic,
                        'cost_article' => $component->costArticle,
                    ),
                    'dimension' => array(
                        'weight_end' => $component->weightEnd,
                        'weight_article' => $component->weightArticle,
                        'capacity' => $component->capacity,
                    ),
                    'person' => array(
                        'phone' => $component->phone,
                        'email' => $component->email,
                        'fio' => $component->fio,
                    ),
                    'address' => array(
                        'zip' => $component->zip,
                        'region' => $component->region,
                        'area' => $component->area,
                        'city' => $component->city, # Пункт доставки
                        'place' => $component->place,
                        'street' => $component->street,
                        'house' => $component->house,
                        'building' => $component->building,
                        'flat' => $component->flat,
                        'office' => $component->office,
                        'floor' => $component->floor,
                        'full' => $component->full,
                    ),
                    'un' => array(
                        'surrend' => $component->date_dep,
                        'intercom' => $component->intercom,
                        'description' => $component->dai,
                        'addressdep'  => $component->address_dep,
                    ),
                    'client' => array(
                        'client_contract' => $component->clientContract,
                    ),
                   )
            );
            ########################
            # @info - формируем XML

            $pile['data']['xml']['order'] = array( # Пример всех переменных
                'registry', 'megapolis', 'agent_id', 'comment', 'articles', 'article', 'article_id', 'article_name',
                'article_price', 'article_weight', 'article_count', 'option', 'op_inventory', 'op_category', 'op_type',
                'op_notification', 'op_fragile', 'op_packing', 'op_take', 'cost', 'cost_delivery', 'cost_insurance', 'cost_public',
                'cost_article', 'dimension', 'weight_end', 'weight_article', 'capacity', 'person', 'fio', 'phone', 'email',
                'address', 'zip', 'region', 'area', 'city', 'place', 'street', 'house', 'building', 'flat', 'office', 'floor',
                'full', 'client', 'client_contract',
            );

            $xml = new DOMDocument( "1.0", "UTF-8" );
            $pile['xml']['document'] = $xml->createElement( "document" );
            $pile['xml']['doc_type'] = $xml->createElement( "doc_type" , $pile['array']['document']['doc_type']);
            $pile['xml']['key'] = $xml->createElement( "key" , $pile['array']['document']['key']);
            $pile['xml']['doc_date'] = $xml->createElement( "doc_date" , $pile['array']['document']['doc_date']);
            $pile['xml']['order'] = $xml->createElement( "order" );

            foreach($pile['array']['order'] as $k=>$v){
                if(is_array($v)){ # @comment - если переменная массив, то добавляем ветку
                    $pile['xml'][$k] = $xml->createElement($k);
                    foreach($v as $k2=>$v2){
                        $pile['xml'][$k2] = $xml->createElement($k2, $v2);
                        $pile['xml'][$k]->appendChild($pile['xml'][$k2]);
                    }
                    $pile['xml']['order']->appendChild($pile['xml'][$k]);
                }else{
                    $pile['xml'][$k] = $xml->createElement($k, $v);
                    $pile['xml']['order']->appendChild($pile['xml'][$k]);
                }
            }

            $pile['xml']['document']->appendChild($pile['xml']['doc_type']);
            $pile['xml']['document']->appendChild($pile['xml']['doc_date']);
            $pile['xml']['document']->appendChild($pile['xml']['key']);

            $pile['xml']['document']->appendChild($pile['xml']['order']);

            $xml->appendChild($pile['xml']['document']);
            $result = $xml->saveXML();
            #header('Content-Type: text/xml; ');

            return isset($result) ? $result : 0;
    }

    public static function getFormatComplement(ComplementComponent $component){
        $data = $component;

        $xml = new DOMDocument( "1.0", "UTF-8" );
        $object = new stdClass();

        $object->document = $xml->createElement("document");
        $object->application = $xml->createElement("application");

        $object->id = $xml->createElement("id", $data->id);
        $object->application->appendChild($object->id);
        $object->products = $xml->createElement("products");


        foreach($data->product as $k=>$v){

            $data = $v;

            $object->products->product[$k] = $xml->createElement("product");
            $object->products->product[$k]->appendChild($xml->createElement("keeper", $data->keeper));
            $object->products->product[$k]->appendChild($xml->createElement("bailor", $data->bailor));
            $object->products->product[$k]->appendChild($xml->createElement("act", $data->act));
            $object->products->product[$k]->appendChild($xml->createElement("coder", $data->coder));
            $object->products->product[$k]->appendChild($xml->createElement("surname", $data->surname));
            $object->products->product[$k]->appendChild($xml->createElement("name", $data->name));
            $object->products->product[$k]->appendChild($xml->createElement("lastname", $data->lastname));
            $object->products->product[$k]->appendChild($xml->createElement("phone", $data->phone));
            $object->products->product[$k]->appendChild($xml->createElement("country", $data->country));
            $object->products->product[$k]->appendChild($xml->createElement("region", $data->region));
            $object->products->product[$k]->appendChild($xml->createElement("area", $data->area));
            $object->products->product[$k]->appendChild($xml->createElement("delivery", $data->delivery));
            $object->products->product[$k]->appendChild($xml->createElement("zip", $data->zip));
            $object->products->product[$k]->appendChild($xml->createElement("street", $data->street));
            $object->products->product[$k]->appendChild($xml->createElement("house", $data->house));
            $object->products->product[$k]->appendChild($xml->createElement("body", $data->body));
            $object->products->product[$k]->appendChild($xml->createElement("floor", $data->floor));
            $object->products->product[$k]->appendChild($xml->createElement("office", $data->office));
            $object->products->product[$k]->appendChild($xml->createElement("codei", $data->codei));
            $object->products->product[$k]->appendChild($xml->createElement("artical", $data->artical));
            $object->products->product[$k]->appendChild($xml->createElement("attachments", $data->attachments));
            $object->products->product[$k]->appendChild($xml->createElement("nomencl", $data->nomencl));
            $object->products->product[$k]->appendChild($xml->createElement("unit", $data->unit));
            $object->products->product[$k]->appendChild($xml->createElement("amount", $data->amount));
            $object->products->product[$k]->appendChild($xml->createElement("implprice", $data->implprice));
            $object->products->product[$k]->appendChild($xml->createElement("implsum", $data->implsum));
            $object->products->product[$k]->appendChild($xml->createElement("nod", $data->nod));
            $object->products->product[$k]->appendChild($xml->createElement("viewdep", $data->viewdep));
            $object->products->product[$k]->appendChild($xml->createElement("categorydep", $data->categorydep));
            $object->products->product[$k]->appendChild($xml->createElement("aoii", $data->aoii));
            $object->products->product[$k]->appendChild($xml->createElement("flagnotif", $data->flagnotif));
            $object->products->product[$k]->appendChild($xml->createElement("viewnotif", $data->viewnotif));
            $object->products->product[$k]->appendChild($xml->createElement("fragility", $data->fragility));
            $object->products->product[$k]->appendChild($xml->createElement("accpack", $data->accpack));
            $object->products->product[$k]->appendChild($xml->createElement("viewaccpack", $data->viewaccpack));
            $object->products->product[$k]->appendChild($xml->createElement("weightdep", $data->weightdep));
            $object->products->product[$k]->appendChild($xml->createElement("tdv", $data->tdv));
            $object->products->product[$k]->appendChild($xml->createElement("codamount", $data->codamount));
            $object->products->product[$k]->appendChild($xml->createElement("note", $data->note));

            $object->products->appendChild($object->products->product[$k]);
        }

        $object->document->appendChild($object->application);
        $object->document->appendChild($object->products);
        $xml->appendChild($object->document);
        $result = $xml->saveXML();

        return isset($result) ? $result : 0;
    }
}