<?php

class MegapolisMessageRequest{

    private $status = NULL; # Идентификация операции
    private $countError;    # Количество ошибок
    private $array;         # Данные

    public function processing($file, $flag=1){
		if($flag == 3){ # Костыль
			foreach($file['error'] as $message){
				$this->countError = 1;
				$this->setArray($message);
			}
			return TRUE;
		}
        if($flag == 2){ # Костыль
            $messages = ApiMessage::message($file, ApiMessage::$MODEL_ORDERACTIONMODEL);

            if($messages[0] != 'Операция не выполнена!'){
                $json = json_decode($file);
                $this->setArray(1);
                OrderActionModel::get()->refresh=1;

            }else{
                $this->setArray('Операция не выполнена!');
                $this->countError = 1;
                #$this->form_error = $order;
                #return Yii::app()->user->setFlash('error', 'Операция не выполнена!<br> Данные номера грузов или номера реестров не доступны!');
            }
            return TRUE;
        }
        $xml = new SimpleXMLElement($file);

        $this->countError = $xml->error->__toString();

        if($this->countError){
            $messages = $xml->messages->message;
            foreach($messages as $k=>$value){
                $text = $value->__toString();
                $this->setArray($text);
            }
            $this->array = array_unique($this->array);
        }else{
            $messages = $xml->orders->order;
            foreach($messages as $k=>$value){
                $text = $value->megapolis->__toString();
                $this->setArray($text);
            }
            $this->array = array_unique($this->array);

        }


        return TRUE;
    }
    public function status($status){ $this->status = $status;}
    public function getCountError(){return $this->countError;}
    public function getArray(){return $this->array;}
    public function setArray($value){$this->array[] = $value;}

    public function kostil(){
        $this->countError = 0;
    }

}
