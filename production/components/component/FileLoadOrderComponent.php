<?php
abstract class FileLoadOrderAbstract extends CModel{

    public function attributeNames(){}

    public $secretkey;
    public $docType;
    public $registry;
    public $orderN;
    public $nhash;
    public $megapolis;
    public $agent;
    public $ahash;
    public $mhash;
    public $done;
    public $opInventory;
    public $opCategory;
    public $opType;
    public $opNotification;
    public $opFragile;
    public $opPacking;
    public $opTake;
    public $dateUpdate;
    public $dateCreate;
    public $dateSend;
    public $idClient;
    public $clientContract;
    public $chaash;
    public $zip;
    public $region;
    public $area;
    public $city;
    public $place;
    public $street;
    public $house;
    public $building;
    public $flat;
    public $office;
    public $floor;
    public $address;
    public $idPerson;
    public $phone;
    public $email;
    public $fio;
    public $weightEnd;
    public $weightArticle;
    public $countArticle;
    public $capacity;
    public $costDelivery;
    public $costInsurance;
    public $costPublic;
    public $costArticle;
    public $comment;
    public $price;
    public $idAgent;
    public $status;
    public $id1s;
    public $full;
    // unique
    public $date_dep;       // surrend
    public $intercom;       // intercom
    public $dai;            // description
    public $address_dep;    // addressdep
}

class FileLoadOrderComponent extends FileLoadOrderAbstract{
    public function load($data){
	    
        foreach($data as $k=>$dep){
            $this->opType = $dep['op_type'];
            $this->opTake = $dep['op_take'];
            $this->house = $dep['house'];
            #$this->num = 1;
            $this->zip = $dep['zip'];
            $this->region = $dep['region'];
            $this->area = $dep['area'];
            $this->street = $dep['street'];
            $this->phone = $dep['phone'];
            #$this->country;
            $this->city = $dep['city'];
            $this->fio = $dep['fio'];
            $this->agent = $dep['agent_id'];
            $this->opFragile = $dep['op_fragile'];
            $this->costInsurance = $dep['cost_insurance'];
            $this->opInventory = $dep['op_inventory'];
            
            print_r($data);
            exit;
        }
    }
    public function validate($attributes=null, $clearErrors=true){

        if(empty($attributes->registry)){
            $this->addError('registry', Yii::t('orders', 1));
        }
        if($attributes->megapolis == ""){
            $this->addError('megapolis', Yii::t('orders', 2));
        }

        if(empty($attributes->agent)){
            $this->addError('agent_id', Yii::t('orders', 3));
        }

        if($attributes->opInventory === ""){
            $this->addError('op_inventory', Yii::t('orders', 4));
        }else{
            if(!is_numeric($attributes->opInventory)){
                $this->addError('op_inventory', Yii::t('orders', 17));
            }else{
                if($attributes->opInventory > 1){
                    $this->addError('op_inventory', Yii::t('orders', 18));
                }
            }
        }
        if($attributes->opCategory == ""){
            $this->addError('op_category', Yii::t('orders', 5));
        }else{
            if(!is_numeric($attributes->opCategory)){
                $this->addError('op_category', Yii::t('orders', 19));
            }
        }
        if($attributes->opFragile === ""){
            $this->addError('op_fragile', Yii::t('orders', 6));
        }else{
            if(!is_numeric($attributes->opFragile)){
                $this->addError('op_fragile', Yii::t('orders', 20));
            }
        }

        if($attributes->opType == ""){
            $this->addError('op_type', Yii::t('orders', 7));
        }else{
            if(!is_numeric($attributes->opType)){
                $this->addError('op_type', Yii::t('orders', 21));
            }
        }
        if(!is_numeric($attributes->costDelivery)){
            $this->addError('cost_delivery', Yii::t('orders', 22));
        }
        if(!is_numeric($attributes->costPublic)){
            $this->addError('cost_public', Yii::t('orders', 23));
        }

        if($attributes->phone == ""){
            $this->addError('phone', Yii::t('orders', 10));
        }
        if($attributes->fio == ""){
            $this->addError('fio', Yii::t('orders', 11));
        }
        if($attributes->zip == ""){
            $this->addError('zip', Yii::t('orders', 12));
        }
        if($attributes->region == ""){
            $this->addError('region', Yii::t('orders', 13));
        }
        if($attributes->house == ""){
            $this->addError('house', Yii::t('orders', 15));
        }

        if($attributes->opCategory == 3){ # Категория отправления - С НП
            if($attributes->costDelivery <= 0){
                $this->addError('delivery', Yii::t('orders', 22));
            }
            if($attributes->costPublic < $attributes->costDelivery){
                $this->addError('cost_delivery', Yii::t('orders', 22));
                $this->addError('cost_public', Yii::t('orders', 23));
            }
        }
        if($attributes->opCategory == 4){ # Категория отправления - С ОЦ
            if($attributes->costDelivery != 0){
                $this->addError('cost_delivery', Yii::t('orders', 22));
                $this->addError('cost_delivery', Yii::t('orders', 23));
            } # НП
            if($attributes->costPublic <= 0)  {
                $this->addError('cost_public', Yii::t('orders', 22));
            }  # ОЦ
        }
        if($attributes->agent == 1){
            if($attributes->costDelivery > 50000){
                $this->addError('cost_delivery', Yii::t('orders', 26));
            } # НП
            if($attributes->costPublic > 50000)   {
                $this->addError('cost_public', Yii::t('orders', 26));
            } # ОЦ
        }
        if($attributes->agent == 2){
            if($attributes->costDelivery > 50000){
                $this->addError('cost_delivery', Yii::t('orders', 26));
            } # НП
            if($attributes->costPublic > 50000)  {
                $this->addError('cost_public', Yii::t('orders', 26));
            }  # ОЦ
        }
        if($attributes->agent == 4){
            if($attributes->costDelivery > 50000){
                $this->addError('cost_delivery', Yii::t('orders', 26));
            } # НП
            if($attributes->costPublic > 50000)  {
                $this->addError('cost_public', Yii::t('orders', 26));
            }  # ОЦ
        };
        return $this->errors ? false : true;
    }

}
