<?php
class TrackComponent extends CComponent
{
    public $id;
    public $done;
    public $data;
    
    public $code;
    public $weight;
    public $np;
    public $oc;
    public $zip;
    public $address;
    public $step;
    
    
    public function setData($data){
        $data = str_replace('u', '\u', $data);
        $data = str_replace('stat\us', 'status', $data);
        $data = json_decode($data);
        if(!$this->code) return FALSE;
        $this->code = $data->code;
        $this->weight = $data->weight;
        $this->np = $data->np;
        $this->oc = $data->oc;
        $this->zip = $data->zip;
        $this->address = $data->address;
        
        if($data->step){
            foreach($data->step as $k=>$v){
                $step = new StepComponent;
                $step->date = $v->date;
                $step->zip = $v->zip;
                $step->address = $v->address;
                $step->operation = $v->operation;
                $step->status = $v->status;

                $this->step[] = clone $step;
            }
        }
        
        return $this;
    }
}
class StepComponent extends CComponent{
    public $date;
    public $zip;
    public $address;
    public $operation;
    public $status;
}