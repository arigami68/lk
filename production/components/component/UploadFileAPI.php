<?php
class UploadFileAPI extends CComponent{
    public static $COMPONENT_COMPLEMENT = 'complement';
    
    public function render(CUploadedFile $file, $param){
        
        if($param == self::$COMPONENT_COMPLEMENT){
            switch ($file->type){
                case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
                case 'application/vnd.ms-excel':
                    $format = $this->attachBehavior('extras', 
                        array('class' => 'application.modules.complement.behaviors.ReadXlsx',
                               'component'=> new ComplementComponent,
                               'file' => $file->tempName,
                               'inputFileType' => 'Excel5',
                        )
                    )->read();
                    break;
                case 1:
                    break;
                case 2:
                    break;
                default:
                   $format = 0;
                   break;
            }
        }
        
        return isset($format) ? $format : 0;
    }
    public function getFormatMegapolis($render){
        $megapolis = $render->read();
        return $megapolis;
    }
}
