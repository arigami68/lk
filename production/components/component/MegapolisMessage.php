<?php

class MegapolisMessage extends CModel{
    public static $CODE_EXC = array(
        '1' => 'Номер договора не верный',
        '2' => 'Ошибка при проверке данных',
        '3' => 'Заполните пожалуйста обязательные поля',
    );
    public static $instance;
    public static function get(){ return empty(self::$instance) ? self::$instance = new self() : self::$instance;}

    public static $MEGAPOLIS_TEXT;
    public static $MODEL_ORDERACTIONMODEL   = 'OrderActionModel';
    public static $FORMAT_MESSAGE_JSON      = 'json';
    public static $OPERATION_INSERT         = 'insert';
    public static $OPERATION_ERROR          = 'error';
    public static $class                    = NULL;
    public $format                          = 'json';
    public $file;
    public $model;

    public function attributeNames(){}

    public function process(){
        $message = array();
        if($this->model == self::$MODEL_ORDERACTIONMODEL){
            $file = $this->file;

            if($file->operation == 'delete'){
                if(count($file->error)){
                    foreach($file->error as $k=>$v){
                        $message[] = $v;
                    }
                }
            }elseif(!empty($file->data)){
                foreach($file->data as $k2=>$v2){
                    if(isset($v2->insert)){
                        $message['insert'] =  '<b>Успешное добавление '.count($v2->insert).' номеров</b>';
                    }
                    if(isset($v2->error)){
                        foreach($v2 as $k3 =>$v3){
                            $message['error'] =  '<b>Номера не могут быть добавлены.</b><br>'.implode(",<br>", $v3).'.';
                        }
                    }
                }
            }elseif($file->error){
                if(count($file->error)){
                    foreach($file->error as $k=>$v){
                        $message['error'][] = $v;
                    }
                }
            }
        }
        return $message;
    }
    public function file($file){
        if($this->format === self::$FORMAT_MESSAGE_JSON){
            if($this->file = json_decode($file)) return $this;
        }
    }
    public function model($model){ $this->model = $model; return $this;}
    public function format($format){
        if($format === self::$FORMAT_MESSAGE_JSON){
            if($this->format = self::$FORMAT_MESSAGE_JSON) return $this;
        }
        return NULL;
    }
}