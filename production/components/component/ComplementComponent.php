<?php

class ComplementComponent extends CComponent{
    public $id;
    public $keeper;
    public $bailor;
    public $act;
    public $coder;
    public $surname;
    public $name;
    public $lastname;
    public $phone;
    public $country;
    public $region;
    public $area;
    public $delivery;
    public $zip;
    public $street;
    public $house;
    public $body;
    public $floor;
    public $office;
    public $codei;
    public $artical;
    public $attachments;
    public $nomencl;
    public $unit;
    public $amount;
    public $implprice;
    public $implsum;
    public $nod;
    public $viewdep;
    public $categorydep;
    public $aoii;
    public $flagnotif;
    public $viewnotif;
    public $fragility;
    public $accpack;
    public $viewaccpack;
    public $weightdep;
    public $tdv;
    public $codamount;
    public $note;
    public $attributes = array();
    public $product = array();
    public $errors = array();
    public $form = array();
    
    public  function conversion($param){
        $conv = new stdClass();
        $conv->param = $param;
        if(!isset($conv->param['main']) OR !isset($conv->param['add'])) return false;
        
        $conv->comp = new ComplementComponent();
        
        $conv->comp->id = $conv->param['id'];
        $conv->comp->keeper = $conv->param['keeper'];
        $conv->comp->bailor = $conv->param['bailor'];
        $conv->comp->form = $conv->param;
        
        foreach($conv->param['main'] as $k=>$value){
            if(!isset($conv->param['add'][$k])) continue;
                $conv->array = new ComplementComponent();
                
                $conv->array->attributes['id'] = $conv->array->id = $conv->param['id'];
                $conv->array->attributes['keeper'] = $conv->array->keeper = $conv->param['keeper'];
                $conv->array->attributes['bailor'] = $conv->array->bailor = $conv->param['bailor'];
                $conv->array->attributes['act'] = $conv->array->act = $conv->param['main'][$k]['act'];
                $conv->array->attributes['coder'] = $conv->array->coder = $conv->param['main'][$k]['coder'];
                $conv->array->attributes['surname'] = $conv->array->surname = $conv->param['main'][$k]['surname'];
                $conv->array->attributes['name'] = $conv->array->name = $conv->param['main'][$k]['name'];
                $conv->array->attributes['lastname'] = $conv->array->lastname = $conv->param['main'][$k]['lastname'];
                $conv->array->attributes['phone'] = $conv->array->phone = $conv->param['main'][$k]['phone'];
                $conv->array->attributes['country'] = $conv->array->country = $conv->param['main'][$k]['country'];
                $conv->array->attributes['region'] = $conv->array->region = $conv->param['main'][$k]['region'];
                $conv->array->attributes['area'] = $conv->array->area = $conv->param['main'][$k]['area'];
                $conv->array->attributes['delivery'] = $conv->array->delivery = $conv->param['main'][$k]['delivery'];
                $conv->array->attributes['zip'] = $conv->array->zip = $conv->param['main'][$k]['zip'];
                $conv->array->attributes['street'] = $conv->array->street = $conv->param['main'][$k]['street'];
                $conv->array->attributes['house'] = $conv->array->house = $conv->param['main'][$k]['house'];
                $conv->array->attributes['body'] = $conv->array->body = $conv->param['main'][$k]['body'];
                $conv->array->attributes['floor'] = $conv->array->floor = $conv->param['main'][$k]['floor'];
                $conv->array->attributes['office'] = $conv->array->office = $conv->param['main'][$k]['office'];
                $conv->array->attributes['codei'] = $conv->array->codei = $conv->param['main'][$k]['codei'];
                $conv->array->attributes['nod'] = $conv->array->nod = $conv->param['main'][$k]['nod'];
                $conv->array->attributes['viewdep'] = $conv->array->viewdep = $conv->param['main'][$k]['viewdep'];
                $conv->array->attributes['categorydep'] = $conv->array->categorydep = $conv->param['main'][$k]['categorydep'];
                $conv->array->attributes['aoii'] = $conv->array->aoii = $conv->param['main'][$k]['aoii'];
                $conv->array->attributes['flagnotif'] = $conv->array->flagnotif = $conv->param['main'][$k]['flagnotif'];
                $conv->array->attributes['viewnotif'] = $conv->array->viewnotif = $conv->param['main'][$k]['viewnotif'];
                $conv->array->attributes['fragility'] = $conv->array->fragility = $conv->param['main'][$k]['fragility'];
                $conv->array->attributes['note'] = $conv->array->note = $conv->param['main'][$k]['note'];
                foreach($conv->param['add'][$k] as $k2=>$goods){
                    $conv->goods[$k2] = clone $conv->array;
                    
                    $conv->goods[$k2]->attributes['artical'] = $conv->goods[$k2]->artical = $goods['artical'];
                    $conv->goods[$k2]->attributes['attachments'] = $conv->goods[$k2]->attachments = $goods['attachments'];
                    $conv->goods[$k2]->attributes['nomencl'] = $conv->goods[$k2]->nomencl = $goods['nomencl'];
                    $conv->goods[$k2]->attributes['unit'] = $conv->goods[$k2]->unit = $goods['unit'];
                    $conv->goods[$k2]->attributes['amount'] = $conv->goods[$k2]->amount= $goods['amount'];
                    $conv->goods[$k2]->attributes['implprice'] = $conv->goods[$k2]->implprice = $goods['implprice'];
                    $conv->goods[$k2]->attributes['implsum'] = $conv->goods[$k2]->implsum = $goods['implsum'];
                    $conv->goods[$k2]->attributes['accpack'] = $conv->goods[$k2]->accpack = $goods['accpack'];
                    $conv->goods[$k2]->attributes['viewaccpack'] = $conv->goods[$k2]->viewaccpack = $goods['viewaccpack'];
                    $conv->goods[$k2]->attributes['weightdep'] = $conv->goods[$k2]->weightdep = $goods['weightdep'];
                    $conv->goods[$k2]->attributes['tdv'] = $conv->goods[$k2]->tdv = $goods['tdv'];
                    $conv->goods[$k2]->attributes['codamount'] = $conv->goods[$k2]->codamount = $goods['codamount'];
                    
                    $conv->comp->product[] = $conv->goods[$k2];
                }
        }
        return isset($conv->comp) ? $conv->comp : FALSE;
    }
    public function convertToXML($data){
        $products = $data;
        $xml = new DOMDocument( "1.0", "UTF-8" );
        $object = new stdClass();

        $object->document = $xml->createElement("document");
        $object->application = $xml->createElement("application");
        
        $object->id = $xml->createElement("id", $products[0]->id);
        $object->application->appendChild($object->id);
        $object->products = $xml->createElement("products");
        
        while(($product =  array_pop($products)) !== NULL ){
            $object->products->product[$product->artical] = $xml->createElement("product");
            
            $object->products->product[$product->artical]->appendChild($xml->createElement("keeper", $product->keeper));
            $object->products->product[$product->artical]->appendChild($xml->createElement("bailor", $product->bailor));
            $object->products->product[$product->artical]->appendChild($xml->createElement("act", $product->act));
            $object->products->product[$product->artical]->appendChild($xml->createElement("coder", $product->coder));
            $object->products->product[$product->artical]->appendChild($xml->createElement("surname", $product->surname));
            $object->products->product[$product->artical]->appendChild($xml->createElement("name", $product->name));
            $object->products->product[$product->artical]->appendChild($xml->createElement("lastname", $product->lastname));
            $object->products->product[$product->artical]->appendChild($xml->createElement("phone", $product->phone));
            $object->products->product[$product->artical]->appendChild($xml->createElement("country", $product->country));
            $object->products->product[$product->artical]->appendChild($xml->createElement("region", $product->region));
            $object->products->product[$product->artical]->appendChild($xml->createElement("area", $product->area));
            $object->products->product[$product->artical]->appendChild($xml->createElement("delivery", $product->delivery));
            $object->products->product[$product->artical]->appendChild($xml->createElement("zip", $product->zip));
            $object->products->product[$product->artical]->appendChild($xml->createElement("street", $product->street));
            $object->products->product[$product->artical]->appendChild($xml->createElement("house", $product->house));
            $object->products->product[$product->artical]->appendChild($xml->createElement("body", $product->body));
            $object->products->product[$product->artical]->appendChild($xml->createElement("floor", $product->floor));
            $object->products->product[$product->artical]->appendChild($xml->createElement("office", $product->office));
            $object->products->product[$product->artical]->appendChild($xml->createElement("codei", $product->codei));
            $object->products->product[$product->artical]->appendChild($xml->createElement("artical", $product->artical));
            $object->products->product[$product->artical]->appendChild($xml->createElement("attachments", $product->attachments));
            $object->products->product[$product->artical]->appendChild($xml->createElement("nomencl", $product->nomencl));
            $object->products->product[$product->artical]->appendChild($xml->createElement("unit", $product->unit));
            $object->products->product[$product->artical]->appendChild($xml->createElement("amount", $product->amount));
            $object->products->product[$product->artical]->appendChild($xml->createElement("implprice", $product->implprice));
            $object->products->product[$product->artical]->appendChild($xml->createElement("implsum", $product->implsum));
            $object->products->product[$product->artical]->appendChild($xml->createElement("nod", $product->nod));
            $object->products->product[$product->artical]->appendChild($xml->createElement("viewdep", $product->viewdep));
            $object->products->product[$product->artical]->appendChild($xml->createElement("categorydep", $product->categorydep));
            $object->products->product[$product->artical]->appendChild($xml->createElement("aoii", $product->aoii));
            $object->products->product[$product->artical]->appendChild($xml->createElement("flagnotif", $product->flagnotif));
            $object->products->product[$product->artical]->appendChild($xml->createElement("viewnotif", $product->viewnotif));
            $object->products->product[$product->artical]->appendChild($xml->createElement("fragility", $product->fragility));
            $object->products->product[$product->artical]->appendChild($xml->createElement("accpack", $product->accpack));
            $object->products->product[$product->artical]->appendChild($xml->createElement("viewaccpack", $product->viewaccpack));
            $object->products->product[$product->artical]->appendChild($xml->createElement("weightdep", $product->weightdep));
            $object->products->product[$product->artical]->appendChild($xml->createElement("tdv", $product->tdv));
            $object->products->product[$product->artical]->appendChild($xml->createElement("codamount", $product->codamount));
            $object->products->product[$product->artical]->appendChild($xml->createElement("note", $product->note));

            $object->products->appendChild($object->products->product[$product->artical]);
        }
        $object->document->appendChild($object->application);
        $object->document->appendChild($object->products);
        $xml->appendChild($object->document);
        $result = $xml->saveXML();

        return isset($result) ? $result : 0;
    }
}