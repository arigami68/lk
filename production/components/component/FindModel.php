<?php

class FindModel extends CModel{
    public static $FORMAT_CSV = 'csv';
    public static $FORMAT_JSON = 'json';

    public static $SEPARATION_COMMA = ',';
    public static $SEARCH_FULL = 'full';
    public static $SEARCH_REGION = 'region';
    public static $SEARCH_AREA = 'area';
    public static $SEARCH_CITY = 'city';
    public static $SEARCH_KLADR = 'kladr';
    public static $SEARCH_REGION_CITY = 'region_city';

    public $format  = 'csv';     # Формат текста
    public $item    = '';        # Данные которые пришли от USER
    public $limit   = 5 ;         # Сколько записей выводить

    public function setLimit($limit){ return $limit>11 ? $limit = 10 : $limit;}
    public function attributeNames(){}
    public function search($flag){
        $result = NULL;
        if($flag == self::$SEARCH_REGION){
            $result = $this->region();
        }elseif($flag == self::$SEARCH_REGION_CITY){
            $result = $this->region_city();
        }elseif($flag == self::$SEARCH_AREA){
            $result = $this->area();
        }elseif($flag == self::$SEARCH_CITY){
            $result = $this->city();
        }elseif($flag == self::$SEARCH_KLADR){
            //$result = $this->kladr();
        }elseif($flag == self::$SEARCH_FULL){
            $result = $this->next();
        }else{
            $result = $this->next();
        }

        return $result;
    }
    public function city(){
        $region     = $this->item[0];
        $area       = $this->item[1];
        $city       = $this->item[2];
        $criteria   = new CDbCriteria();

        $criteria->condition = 'region ="'.$region.'" AND area= "'.$area.'" AND ((city LIKE "'.$city.'%") OR (locality LIKE "'.$city.'%"))';
        $criteria->select = 'idKladr, city, locality';
        $criteria->limit = $this->limit;
        $criteria->group = 'city';
        $result = MEGE::model()->findAll($criteria);

        return $result;
    }
    public function kladr($kladr){
        $result = NULL;
        $criteria = new CDbCriteria();
        $criteria->condition = 'kladr='.$kladr;
        $criteria->select = 'zip';
        $result = CLKL::model()->find($criteria);

        return $result;
    }
    public function region(){
        $result = NULL;
        $region = $this->item[0];
        $criteria = new CDbCriteria();
        $criteria->addSearchCondition('region', $region);
        $criteria->select = 'idKladr, region';
        $criteria->limit = $this->limit;
        $criteria->group = 'region';
        $result = MEGE::model()->findAll($criteria);
        return $result;
    }
    public function region_city(){
        
        $region     = $this->item[0];
        $locality = $city       = $this->item[1];
        if($city){
            $criteria   = new CDbCriteria();
            $criteria->addSearchCondition('region', $region, false);
            $criteria->addSearchCondition('area', '', 'AND');
            $criteria->addSearchCondition('city', $city.'%', false);

            if(!$result = MEGE::model()->findAll($criteria)){
                $criteria   = new CDbCriteria();
                $criteria->addSearchCondition('region', $region, false);
                $criteria->addSearchCondition('area', '', 'AND');
                $criteria->addSearchCondition('locality', $locality.'%', false);

            }
        }
        $criteria->limit = $this->limit;
        $result = MEGE::model()->findAll($criteria);

        return $result;
    }
    public function area(){
        $region     = $this->item[0];
        $area       = $this->item[1];

        $criteria   = new CDbCriteria();

        $criteria->condition = 'region ="'.$region.'"';
        $criteria->addSearchCondition('area', $area);
        $criteria->select = 'idKladr, area';
        $criteria->limit = $this->limit;
        $criteria->group = 'area';

        $result = MEGE::model()->findAll($criteria);

        return $result;
    }

    public function next(){
        $step[1] = isset($this->item[0]) ? trim($this->item[0]) : '';
        $step[2] = isset($this->item[1]) ? trim($this->item[1]) : '';
        $step[3] = isset($this->item[2]) ? trim($this->item[2]) : '';
        $result = array();

        if($step[1]){
            $criteria = new CDbCriteria();
            $criteria->addSearchCondition('city', $step[1].'%', false);
            $criteria->limit = $this->limit;
            $result1 = MEGE::model()->findAll($criteria);

            if(true){

                $criteria = new CDbCriteria();
                $criteria->addSearchCondition('locality', $step[1].'%', false);
                $criteria->limit = $this->limit;
                if(!count($result = MEGE::model()->findAll($criteria))){}
                $result = array_merge($result1, $result);
            }

        }
        if($step[2]){
            $criteria->addSearchCondition('area', $step[2].'%', false,'AND');
            $criteria->addSearchCondition('region', $step[2].'%', false,'OR');
            $criteria->addSearchCondition('city', $step[1].'%', false, 'OR');
            $criteria->addSearchCondition('locality', $step[1].'%', false,'AND');

            $result = MEGE::model()->findAll($criteria);
        }

        if(true){
            $criteria = new CDbCriteria();
            $criteria->addSearchCondition('region', $step[1].'%', false);
            $criteria->addCondition('data0 IN (188571, 188572, 188573, 188574)');

            if($MEGE = MEGE::model()->find($criteria)){
                array_unshift($result, $MEGE);
            }
        }

        /*
        $criteria = new CDbCriteria();
        $criteria->addCondition('data0 IN (188571, 188572, 188573, 188574)');

        if($step[1]){
            $criteria->addSearchCondition('city', $step[1].'%', false);
            $criteria->addSearchCondition('locality', $step[1].'%', false, 'OR');
            $criteria->limit = $this->limit;
        }
        $text2 = !empty($step[2]) ?  $step[2] : $step[1];
        $criteria->addSearchCondition('region', $text2.'%', false);
        if($step[2]){
            $criteria->addSearchCondition('area', $step[2].'%', false,'OR');
            //$criteria->addSearchCondition('region', $step[2].'%', false,'OR');
        }
        print_r($criteria);
        exit;
        $result = MEGE::model()->findAll($criteria);
        */
        return isset($result) ? $result : FALSE;
    }

    public function toProcessCsv(MEGE $find, $format){

        $result = '';
        if($format == self::$SEARCH_FULL){
            if(true){
                $city = explode(' ', $find->city);
                array_pop($city);
                $city = implode(' ', $city);
            }
            if(true){
                $result.= $find->idKladr.';';
                $result.= $city;
                $result.= empty($find->locality) ? '' : self::$SEPARATION_COMMA;
                $result.= $find->locality ? $find->locality.self::$SEPARATION_COMMA : self::$SEPARATION_COMMA;
                $result.= $find->area ? $find->area.self::$SEPARATION_COMMA : '';
                $result.= $find->region;
                $result.= "\t\n";
            }
        }elseif( $format== self::$SEARCH_REGION){
            $result.= $find->idKladr.';';
            $result.= $find->region;
            $result.= "\t\n";
        }elseif( $format== self::$SEARCH_AREA){
            $result.= $find->idKladr.';';
            $result.= $find->area;
            $result.= "\t\n";
        }elseif( $format== self::$SEARCH_CITY){

            $result.= $find->idKladr.';';
            if(!empty($find->locality)){
                $result.= $find->locality.', ';
            }
            if(!empty($find->city)){
                $result.= ' '.$find->city;
            }
            $result.= "\t\n";
        }
        return $result = mb_convert_case(substr($result, 0, -1), MB_CASE_LOWER, "UTF-8");
    }
    public function toProcessJson(MEGE $find, $format){
        $result = '';
        if($format == self::$SEARCH_FULL){
            if(true){
                $city = explode(' ', $find->city);
                array_pop($city);
                $city = implode(' ', $city);
            }
            if($zip = $this->kladr($find->idKladr)){ # @OPTIM оставить только на исплючениях
                $result.= $zip->zip.',';
            }
            if(true){
                if(!empty($city) AND !empty($find->locality)){
                    $result.= $find->idKladr.',';

                    #$result.= empty($find->locality) ? '' : self::$SEPARATION_COMMA.' ';
                    $result.= $find->locality ? $find->locality.self::$SEPARATION_COMMA : self::$SEPARATION_COMMA;

                    $result.= $city.self::$SEPARATION_COMMA;
                    $result.= $find->area ? $find->area.self::$SEPARATION_COMMA : '';
                    $result.= $find->region;
                }else{
                    $result.= $find->idKladr.',';
                    $result.= empty($city) ? '' : $city.self::$SEPARATION_COMMA;
                    $result.= empty($find->locality) ? '' : $find->locality.self::$SEPARATION_COMMA;
                    $result.= $find->area ? $find->area.self::$SEPARATION_COMMA : '';
                    $result.= $find->region;

                }
            }
        }elseif( $format== self::$SEARCH_REGION){
            if($zip = $this->kladr($find->idKladr)){ # @OPTIM оставить только на исплючениях
                $result.= $zip->zip.';';
            }
            $result.= $find->region;
        }elseif( $format== self::$SEARCH_AREA){
            $result = $find->area;
        }elseif($format== self::$SEARCH_CITY){
            if($zip = $this->kladr($find->idKladr)){
                $result.= $zip->zip.';';
            }
            if(!empty($find->locality)){
                $result.= $find->locality;
            }
            if(!empty($find->city)){
                $result.= $find->city ? ''.$find->city : ',';

            }
        }elseif($format == self::$SEARCH_REGION_CITY){
            if(true){
                $city = explode(' ', $find->city);
                array_pop($city);
                $city = implode(' ', $city);
            }
            if(!empty($city) OR !empty($find->locality)){

                $text= $find->locality ? $find->locality : $city;
                $text = mb_strtolower($text, 'UTF-8');
                $item = mb_strtolower($this->item[1], 'UTF-8');

                if(strstr($text, $item) !== FALSE){
                    if($zip = $this->kladr($find->idKladr)){
                        $result.= $zip->zip.';';
                    }
                    $text = mb_strtoupper($text, 'UTF-8');
                    $result.=$text;
                }
            }
        }


        return $result;
    }
    public function explodeItem(){
        $this->item = explode(self::$SEPARATION_COMMA, $this->item);
        foreach($this->item as &$value){
            $value = trim($value);
        }
        return $this->item;
    }
    # name='Потапов Геннадий'
    # description='Выбираем формат данных на выход'
    public function format($input, $format){
        if($this->format == self::$FORMAT_CSV){
            $output = '';
            foreach($input as $k=>$v){
                $output.= $this->toProcessCsv($v, $format);
            }
        }elseif($this->format == self::$FORMAT_JSON){
            $output = array();

            foreach($input as $k=>$v){
                if(empty($v)) conitnue;
                if($json = $this->toProcessJson($v, $format)){
                    array_push($output, $json);
                }
            }
            $output = json_encode($output);
        }
        
        return $output;
    }
}
