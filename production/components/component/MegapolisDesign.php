<?php
/*
 * Усорвные обозначения
 *
 *Пример применения
 * $this->widget('application.components.component.MegapolisDesign',array('array' => array(
        array("icon-eye-open", 'просмотр'),
        array('icon-edit', 'редактирование'),
        array('icon-trash', 'удаление'),
        array('icon-barcode', 'печать штрихкодов'),
        array('icon-th-large', 'просмотр отправлений'),
        array('icon-print', 'печать'),
    )));
 */

class MegapolisDesign extends CWidget{
    public $array; #
    public $style;
    public function run() {
        $array = $this->array;
        $style = $this->style;

        $text = '<table style="'.$style.'">';

        $text.= '<tr><td colspan="2">'.Yii::t("total", "условные обозначения").'</td></tr>';
        foreach($array as $k=>$v){
            $class = $v[0];
            $write  = $v[1];
            $text.= '<tr><td><span class='.$class.'></td><td>'.$write.'</td></tr>';
        }

        $text.= '</table>';
        print $text;

    }
}