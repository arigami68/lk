<?php

# Сообщения в форму
class ApiMessage extends CModel{
    public static $MODEL_ORDERACTIONMODEL = 'OrderActionModel';
    public static $TYPE_MESSAGE = 'json';
    public function attributeNames(){}
    
    public static function message($echo, $model){
        $message = array();

        if($model == self::$MODEL_ORDERACTIONMODEL){

            $decode = json_decode($echo);

            if($decode->operation == 'delete'){
                if(count($decode->error)){
                    foreach($decode->error as $k=>$v){
                        $message[] = $v;
                    }
                }
            }elseif(!empty($decode->data)){

                foreach($decode->data as $k2=>$v2){

                    foreach($v2 as $k3 =>$v3){

                        if($k3 == 'error'){

                            foreach($v3 as $k4=>$v4){

                                $message[] = $v4;
                            }
                        }
                    }
                }
            }elseif($decode->error){
                if(count($decode->error)){
                    foreach($decode->error as $k=>$v){
                        $message[] = $v;
                    }
                }
            }

            if($message){
                array_unshift($message, 'Операция не выполнена!');
                $message = array('Операция не выполнена!');
            }else{
                $message[] = 'Успешное выполнение операции';
            }
        }

        return $message;
    }

}