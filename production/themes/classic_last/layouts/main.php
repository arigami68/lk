<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="language" content="en" />
            <title><?php echo CHtml::encode($this->pageTitle); ?></title>
             <?php
             /*
              * Подключение скриптов css, js;
              * input  : ExtendedClientScript,
              * output : подключение css, js.
              */
                 $this->widget('application.components.widget.MegapolisScriptFile',array(
                     'cs'   => Yii::app()->clientScript,
                     'flag' => $this->action->id,
                 ));
             ?>
    </head>
<body>
<?php
    $this->widget('application.components.widget.QuestionWidget',array(
        'page'  => $this->id,
        'title' => $this->title,
    ));
?>
<?php
$name = Yii::t("linkmenu", "name:login");
if(Yii::app()->user->checkAccess('user')){
    switch(Yii::app()->user->auth->db->country){
        case 276:
            $array['dep']['data']['dep'] = array(
                'label'=>Yii::t("linkmenu", "departure"), 'url'=>array(Yii::t("linkmenu", "departure:link")),  'items'=>
                    array(
                        array('label'=>Yii::t("linkmenu", "yoursOrders"), 'url'=>array(Yii::t("linkmenu", "yoursOrders:link"))),
                        array('label'=>Yii::t("linkmenu", "createOrders"), 'url'=>array(Yii::t("linkmenu", "createOrders:link"))),
                        array('label'=>Yii::t("linkmenu", "depTracking"), 'url'=>array(Yii::t("linkmenu", "depTracking:link"))),
                        array('label'=>Yii::t("linkmenu", "calcDelivery"), 'url'=>array(Yii::t("linkmenu", "calcDelivery:link"))),
                        array('label'=>Yii::t("linkmenu", "reportDep"), 'url'=>array(Yii::t("linkmenu", "reportDep:link"))),
                    )
            );
            break;
        default:
            $array['dep']['data']['dep'] = array(
                'label'=>Yii::t("linkmenu", "departure"), 'url'=>array(Yii::t("linkmenu", "departure:link")),  'items'=>
                    array(
                        array('label'=>Yii::t("linkmenu", "yoursOrders"), 'url'=>array(Yii::t("linkmenu", "yoursOrders:link"))),
                        array('label'=>Yii::t("linkmenu", "createOrders"), 'url'=>array(Yii::t("linkmenu", "createOrders:link"))),
                        array('label'=>Yii::t("linkmenu", "depTracking"), 'url'=>array(Yii::t("linkmenu", "depTracking:link"))),
                        array('label'=>Yii::t("linkmenu", "calcDelivery"), 'url'=>array(Yii::t("linkmenu", "calcDelivery:link"))),
                        array('label'=>Yii::t("linkmenu", "callCourier"), 'url'=>array(Yii::t("linkmenu", "callCourier:link"))),
                        array('label'=>Yii::t("linkmenu", "reportDep"), 'url'=>array(Yii::t("linkmenu", "reportDep:link"))),
                    )
            );
    }
}

$array['dep']['data']['war'] = array('label'=>Yii::t("linkmenu", "warehouse"), 'url'=>array(Yii::t("linkmenu", "warehouse:link")),  'items'=>
            array(
                array('label'=>Yii::t("linkmenu", "myWarehouse"), 'url'=>array(Yii::t("linkmenu", "myWarehouse:link"))),
                array('label'=>Yii::t("linkmenu", "inflowGoods"), 'url'=>array(Yii::t("linkmenu", "inflowGoods:link"))),
                array('label'=>Yii::t("linkmenu", "complement"), 'url'=>array(Yii::t("linkmenu", "complement:link"))),
                array('label'=>Yii::t("linkmenu", "makeup"), 'url'=>array(Yii::t("linkmenu", "makeup:link"))),
                array('label'=>Yii::t("linkmenu", "reportmyw"), 'url'=>array(Yii::t("linkmenu", "reportmyw:link"))),
            )
);

$array['dep']['data']['company'] = array('label'=>Yii::t("linkmenu", "company"), 'url'=>array(Yii::t("linkmenu", "company:link")),  'items'=>
            array(
                array('label'=>Yii::t("linkmenu", "details"), 'url'=>array(Yii::t("linkmenu", "details:link"))),
                #Yii::app()->user->checkAccess('user') ? array('label'=>Yii::t("linkmenu", "contract"), 'url'=>array(Yii::t("linkmenu", "contract:link"))) : FALSE, # @info Удалил 23.10.2014 перенесли в один раздел
            )
);
$array['dep']['data']['mutual'] = array('label'=>Yii::t("linkmenu", "mutual"), 'url'=>array(Yii::t("linkmenu", "mutual:link")),  'items'=>
            array(
              #  array('label'=>Yii::t("linkmenu", "deposits"), 'url'=>array(Yii::t("linkmenu", "deposits:link"))),
                array('label'=>Yii::t("linkmenu", "np"), 'url'=>array(Yii::t("linkmenu", "np:link"))),
                array('label'=>Yii::t("linkmenu", "documents"), 'url'=>array(Yii::t("linkmenu", "documents:link"))),
            )
);
if(true){ # @info - Ссылка - Регистрация - Если пользователь авторизован, то ему не нужно отображать
    if(true){ # @info -  пользователь не авторизован
        Yii::app()->user->getIsGuest() ? $array['reg']['none'] = array('label'=>Yii::t("linkmenu", "reg"), 'url'=>array(Yii::t("linkmenu", "reg:link"))) : $array['reg']['none'] = '';
    }
    if(true){ # @info -  пользователь авторизован
        !Yii::app()->user->getIsGuest() ? $array['dep']['link'] = $array['dep']['data']['dep'] : $array['dep']['link'] = '';
        !Yii::app()->user->getIsGuest() ? $array['war']['link'] = $array['dep']['data']['war'] : $array['war']['link'] = '';
    #    !Yii::app()->user->getIsGuest() ? $array['company']['link'] = $array['dep']['data']['company'] : $array['company']['link'] = '';
        !Yii::app()->user->getIsGuest() ? $array['mutual']['link'] = $array['dep']['data']['mutual'] : $array['mutual']['link'] = '';

        !Yii::app()->user->getIsGuest() ? $name = Yii::t("linkmenu", "name:exit") : TRUE;
        !Yii::app()->user->getIsGuest() ? $array['api']['link'] = array('label'=>Yii::t("linkmenu", "api"), 'url'=>array(Yii::t("linkmenu", "api:link"))) : $array['api']['link'] = '';
    }
}
if(true){
    if(!Yii::app()->user->getIsGuest()){
        $array['reg']['menu'] = array('label'=>$name, 'url'=>array(Yii::t("linkmenu", "name:link")),  'items'=>
                array(
                    array('label'=>Yii::t("linkmenu", "details"), 'url'=>array(Yii::t("linkmenu", "details:link"))),
                )
        );
    }else{
        $array['reg']['menu'] = array('label'=> $name, 'url'=>array(Yii::t("linkmenu", "name:link")));
    }
}
?>
<div class="container" id="megapolis-page">
    <?php
    if(Yii::app()->user->checkAccess('user')){
        $this->widget('bootstrap.widgets.TbNavbar',array(
            'items'=> array(
                array(
                    'class'=>'bootstrap.widgets.TbMenu',
                    'items'=>array(
                        $array['reg']['menu'],
                        $array['dep']['link'],
                      #  $array['war']['link'],
                       # $array['company']['link'],
                        $array['mutual']['link'],
                        $array['reg']['none'],
                       # $array['api']['link'],
                    ),

                ),
              # '<form class="navbar-search pull-left" action=""><input type="text" class="search-query span2" placeholder="Search"></form>',
            ),
            'htmlOptions' => array('id' => 'megapolis_header')
        ));
    }elseif(Yii::app()->user->checkAccess('tmp')){
        $this->widget('bootstrap.widgets.TbNavbar',array(
            'items'=> array(
                array(
                    'class'=>'bootstrap.widgets.TbMenu',
                    'items'=>array(
                        $array['reg']['menu'],
                        $array['company']['link'],
                    ),
                ),
            ),
            'htmlOptions' => array('id' => 'megapolis_header')
        ));
    }

?>
<div id='main-block-company'>

</div>

<div id="megapolis_block" style="background: url(<?php print Yii::app()->assetManager->publish(Yii::getPathOfAlias('images').DS.'megapolis-header.png');?>);width: 870px; height: 189px; top: 48px; color: white;">
    <div id="top-logo">
        <div class="language">
            <span class="idea-language lang_panel_ru" lang="ru"></span>
            <span class="idea-language lang_panel_de" lang="ger"></span>
            <span class="idea-language lang_panel_eng" lang="eng"></span>
        </div>
        <?php
            # Добавить картинку
        ?>
        <?php
        if(Yii::app()->user->checkAccess('user')){

            switch(Yii::app()->user->auth->db->country){
                case 276:
                    ?>
                        <a href="/"><img src="http://idea-logic.de/design/img/idealogic_logo_big_de.png"></a>
                    <?
                    break;
                default:
                    ?>
                        <a href="/"><img src="<?php print Yii::app()->assetManager->publish(Yii::getPathOfAlias('images').DS.'megapolis-log-blue.jpg'); ?>"></a>
                    <?
            }
        }
        ?>


    </div>
    <div id="top-tel">

            <?php
            if(Yii::app()->user->checkAccess('user')){

                switch(Yii::app()->user->auth->db->country){
                    case 276:
                        ?>
                    <div id="top-tel-s">
                        <div style="padding-bottom: 10px; "><?=Yii::t("total", "Москва")?></div>
                    </div>
                    <div id="top-tel-n">
                        <div style="padding-bottom: 10px; font-weight: bold; text-align: left">+ 49 30 7623 971 971</div>
                    </div>
                        <?
                        break;
                    default:
                        ?>
                        <div id="top-tel-s">
                            <div style="padding-bottom: 10px; "><?=Yii::t("total", "Москва")?></div>
                            <div><?=Yii::t("total", "По России")?></div>
                        </div>
                        <div id="top-tel-n">
                            <div style="padding-bottom: 10px; font-weight: bold; text-align: left">+7 (495) 228-14-95</div>
                            <div style="float: left; font-weight: bold;">8 800 200-80-18 (<?=Yii::t("total", "бесплатно по России")?>)</div>
                        </div>
                        <?
                }
            }
            ?>


    </div>
    <?php if(!Yii::app()->user->getIsGuest()){ ?>
    <div id="top-client">
        <div id="top-client-s">
            <div  style="padding-bottom: 5px; font-weight: bold"><?=Yii::t("total", "договор")?></div>
            <div style="font-weight: bold; padding-bottom: 5px;"><?=Yii::t("total", "компания")?></div>
            <div style="font-weight: bold"><?=Yii::t("total", "дата заключение договора")?></div>
        </div>
        <div id="top-client-n">
            <div style="padding-bottom: 5px;"><?php print Yii::app()->user->auth->contract; ?></div>
            <div style="padding-bottom: 5px;"><?php print Yii::app()->user->auth->name; ?></div>
            <div><?php print date('d-m-Y', strtotime(Yii::app()->user->auth->db->contractDate)); ?></div>
            <div id="top-client-n-exit" style="padding-top: 25px;"><a  style="font-size: 16px; font-weight: bold" href="/logout"><?=Yii::t("total", "Выход")?></a></div>
        </div>
            <p onload="startTime()" id="js-time"> </p>
    </div>
    <?php } ?>

</div>
    <div id="megapolis-title">
        <?php print $this->title;?>

    </div>
    <?php echo $content; ?>

    <div class="clear"></div>

<!--
<div align="center" style="margin: auto; height: 10px; position: relative;">
    <pre>
        <b>
            <div style="float: right;"><a href="http://www.megapolis-exp.ru/" target="_blank">ООО «ИдеаЛоджик»</a> г. Москва, ул. Ткацкая, д. 5, стр. 5</div>
            <div style="width: 100%;">тел. 8 800 200 80 18 (по России звонок бесплатный)<div>
                    <div style="clear: both">

                    </div>
        </b>
    </pre>
</div>
!-->
<!--
    <div id="megapolis_block" style="margin-top: 52px;background: url(<?php print Yii::app()->assetManager->publish(Yii::getPathOfAlias('images').DS.'megapolis-footer.png');?>);width: 870px; height: 189px; color: white;">
!-->
<div class="info">
    <?php
        $text='';
        foreach(Yii::app()->user->getFlashes() as $key => $message) { $text.= "$.jGrowl('".$message."', { sticky: true });";}
        Yii::app()->clientScript->registerScript("success", $text);
    ?>
</div>

</body>
</html>