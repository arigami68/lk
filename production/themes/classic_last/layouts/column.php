<?php $this->beginContent('application.themes.classic_last.layouts.main'); ?>
<div class="megapolis-row">
        <div id="content">
            <?php echo $content; ?>
        </div><!-- content -->
    <div class="span3">
        <div id="sidebar"> 
        <?php
            $this->beginWidget('zii.widgets.CPortlet', array(
                'title'=>'Operations',
            ));
            $this->widget('bootstrap.widgets.TbMenu', array(
                'items'=>$this->menu,
                'htmlOptions'=>array('class'=>'operations'),
            ));
            $this->endWidget();
        ?>
        </div>
    </div>
   
</div>
<?php $this->endContent(); ?>