function getApp($option, $element){
    for($a=0; $a<19; $a++){
        if($a<$option.val()-9) $element.eq($a).attr('disabled','disabled');
        else $element.eq($a).attr('disabled',false);
    }
    $('#AppForm_timeEnd').val(18);
}

$(function(){

    if($.datepicker !== undefined){
        $.datepicker.setDefaults({
            minDate: +1
        });
    $("#AppForm_phone").mask("+7 (999) 999-9999");
    $("#AppForm_date").datepicker();
    $("#AppForm_timeStart").mouseup(function(){
       getApp($('#AppForm_timeStart'), $('#AppForm_timeEnd option'));
    });
    }
    $("body").delegate( '.s_delete', 'click', function(e){
        var name = $(this).parent().parent().find('td:eq(0)').html();
        if(!confirm('Удалить заявку?: '+ name)) return false;
    });
    $("body").delegate( '.s_read', 'click', function(e){
        var name = $(this).parent().parent().find('td:eq(0)').html();
        if(!confirm('Закрыть заявку?: '+ name)) return false;
    });


});