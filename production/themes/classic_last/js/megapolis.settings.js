function check_get_num(){
    var flag = $("#SettingsForm_getnum");
    var flag_registry = $("#SettingsForm_number_registry");
    var range = $("#range");
    var range_registry = $("#range_registry");
    
    range.keypress(function(e){
        var symbol = (e.which) ? e.which : e.keyCode;
            if (symbol < 48 || symbol > 57)  return false;
    });
    if(true){
        var code = $("#SettingsForm_range").val();
            var string = '';
            string = ((code*10000)+1)+'-'+((code*10000)+10000);
            $("#span_range").text(string);
    }
    range.keyup(function(e){
            var code = $("#SettingsForm_range").val();
            var string = '';
            string = ((code*10000)+1)+'-'+((code*10000)+10000);
            $("#span_range").text(string);
    });

    range_registry.keyup(function(e){
            var code = $("#SettingsForm_range_registry").val();
            var string = '';
            string = ((code*1000)+1)+'-'+((code*1000)+1000);
            $("#span_range_registry").text(string);
    });
    range_registry.keypress(function(e){
        var symbol = (e.which) ? e.which : e.keyCode;
            if (symbol < 48 || symbol > 57)  return false;
    });
    if(true){
            var code = $("#SettingsForm_range_registry").val();
            var string = '';
            string = ((code*1000)+1)+'-'+((code*1000)+1000);
            $("#span_range_registry").text(string);        
    }
    
    if(flag.val() == 1){ range.show();}
    flag.click(function(e){
        if(flag.val() == 0){
            range.hide();
        }else if(flag.val() == 1){
            range.show();
        }
        
    });
    if(flag_registry.val() == 1){ range_registry.show();}
    flag_registry.click(function(e){
        if(flag_registry.val() == 0){
            range_registry.hide();
        }else if(flag_registry.val() == 1){
            range_registry.show();
        }
        
    });
}
function logout(){
    $.ajax({
        data: {'logout': 1},
        type: "POST",
        url: document.location.href,
        success: function(e){
            location.reload();
        },
        beforeSend: function( ) {},
        complete: function(){}
    });
}
function save(){
    var form = $('#createOrder').serialize();
    $.ajax({
        data: form,
        type: "POST",
        url: document.location.href,
        success: function(e){
            location.reload();
        },
        beforeSend: function() {},
        complete: function(){}
    });
}
$(document).ready(function(){
    $("#logout").click(function(){
       logout();      
    });
    $("#save").click(function(){
       save();
    });
    check_get_num();
});