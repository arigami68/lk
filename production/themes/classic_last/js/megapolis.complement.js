var compl = {};
compl.orders = {};
compl.goods = {};
compl.orders.name = '#addOrders';
compl.orders.name_delete = '.deleteOrders';
compl.orders.link = '/complement/complement/ordersAdd';
compl.goods.name = '#addGoods';
compl.goods.name_delete = '.deleteGoods';
compl.goods.link = '/complement/complement/goodsAdd';
function getName(str){
    if (str.lastIndexOf('\\')){
        var i = str.lastIndexOf('\\')+1;
    }
    else{
        var i = str.lastIndexOf('/')+1;
    }						
    var filename = str.slice(i);			
    var uploaded = document.getElementById("fileformlabel");
    uploaded.innerHTML = filename;
}
$(function(){
    if($("#EditForm_date_shipping").length){
        $.datepicker.setDefaults(
               $.extend($.datepicker.regional["ru"])
        );
       $("#EditForm_date_shipping").datepicker();
    }
        
    $('input[type=file]').on('change', function(){
        getName($(this).val());
    });
    $('body').delegate( compl.goods.name_delete, 'click', function(){
        var el = $(this);
        var el_a = el.find('a').attr('class');
        console.log(el_a);
        $('#'+el.parent().attr('id')).remove();
        $('#'+el_a).remove();
        return false;
    });
    
    $('body').delegate( compl.orders.name_delete, 'click', function(){
        var el = $(this).find('a');
        $('#'+el.attr('class')).remove();
        $('#'+el.attr('name')).remove();
        return false;
    });
    
    $('body').delegate( compl.goods.name, 'click', function(){
        compl.goods.id = $(this).parent().parent().parent().attr('id');
        compl.goods.ul = $("#"+compl.goods.id);
        compl.goods.len = compl.goods.ul.find('li').length +1;
        
        $.ajax({
            data: {'goods': compl.goods.len, 'orders' : compl.goods.id},
            type: 'POST',
            url: compl.goods.link,
            success: function(e){
                
                if( $('#'+compl.goods.id).find('.tab-content2').length)
                    $('#'+compl.goods.id).find('.tab-content2').append(e);
                else
                    $('#'+compl.goods.id).find('.tab-content').append(e);
            },
            beforeSend: function(){},
            complete: function(){},
            error: function(){},
          });
        var str = compl.goods.id.split('_');
        str = str[2];
        
        $('#'+compl.goods.id).find('ul')
            .append(
                '<li ><a id=goods'+(compl.goods.len)+' data-toggle=tab name=yw'+str+'_tab_'+compl.goods.len+' href=#yw'+str+'_tab_'+compl.goods.len+'>Товар №'+(compl.goods.len-1)+'</a></li>'
            );
        return false;
    });
    
    $('body').delegate( compl.orders.name, 'click', function(){
        compl.orders.id = $(compl.orders.name).parent().parent().attr('id');
        compl.orders.ul = $("#"+compl.orders.id);
        var yw3 = $('#'+compl.orders.id).parent().attr('id');
        
        compl.orders.len = compl.orders.ul.find('li').length +1;
        var add_success = $('#'+yw3).find('.tab-content:first');
        $.ajax({
            data: {'goods': compl.orders.len, 'yw3' : yw3},
            type: 'POST',
            url: compl.orders.link,
            success: function(e){
                add_success.append(e);
            },
            beforeSend: function(){},
            complete: function(){},
            error: function(){},
          });
        
        
        $('#'+compl.orders.id)
            .append(
                '<li ><a id=order'+(compl.orders.len+1)+' data-toggle=tab  href=#'+yw3+'_tab_'+(compl.orders.len+16)+'>Заказ №'+(compl.orders.len-1)+'</a></li>'
            );
        return false;
    });
    
});