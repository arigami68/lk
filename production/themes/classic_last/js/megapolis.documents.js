var request = function (name, path){
    var json = JSON.stringify({
        name : name,
        path : path
    });
    var document = $('.document');
    $.ajax({
        data: {'json': json},
        type: 'POST',
        url: '',
        success: function(data){
            document.html(data);
            console.log(data);
        },
        beforeSend: function(){},
        complete: function(){},
        error: function(){}
    });
};
$(function(){
    request('', '');
    $(".document" ).delegate( ".dir", "click", function(){
        var name = $(this).text();
        var path = $(this).attr('path');
        request(name, path);
    });
});