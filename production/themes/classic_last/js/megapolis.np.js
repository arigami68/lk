function getName(str){
    if (str.lastIndexOf('\\')){
        var i = str.lastIndexOf('\\')+1;
    }
    else{
        var i = str.lastIndexOf('/')+1;
    }
    var filename = str.slice(i);
    var uploaded = document.getElementById("fileformlabel");
    uploaded.innerHTML = filename;
}

$(document).ready(function(){
    $.datepicker.setDefaults({
        minDate: null
    });

    $('#Permy').click(function(){
        var date2 = $('#NpForm_date_f').val();
        var list   = $('#NpForm_list').val();
        if(date2.length && list.length){
            alert('Загрузка данных доступна либо по дате, либо по ШПИ/ №грузу');
            return false;
        }

    });

    $("#NpForm_date_s").datepicker();
    $("#NpForm_date_f").datepicker();
    $('input[type=file]').on('change', function(){
        console.log($(this).val());
        getName($(this).val());
    });

});