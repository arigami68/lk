MEG.calc = {
    settings : {
        req: 0
    },
    tab : {
        from : 'hidden_from'
    }
}
obj = new Object();
obj.len = 1;
obj.limit = 10;
obj.url = '/calc/calc/find';
obj.output = {};
obj.output.text = new Array();
obj.output.id = new Array();
obj.payment={}
obj.payment.url='calc/calc/ideaPayment';
obj.payment.w = 'CalcForm_weight';
obj.payment.oc = 'CalcForm_oc';
obj.payment.np = 'CalcForm_np';
obj.total = new Object();
obj.total.num = 0;
obj.total.sum = new Array();
obj.total.sum['non-nds'] = 0;
obj.total.sum['nds'] = 0;
function Number(){
    $('#'+obj.payment.np).trigger('keypress');
    $('#'+obj.payment.oc).keypress(function(e) {

        if(!putNumber(e)) return false;
        return true;
    });
    $('#'+obj.payment.np).keypress(function(e) {
        if(!putNumber(e)) return false;
        return true;
    });
    $('#'+obj.payment.np).keyup(function(e) {
       var np = parseInt($('#'+obj.payment.np).val());
       var oc = parseInt($('#'+obj.payment.oc).val());
       if(oc <= np){
            $.jGrowl('Объявленная ценность должна быть больше или равна Наложенному платежу');
            $('#'+obj.payment.np).val(oc);
       }
    });
}
$(function(){
    Number();
    $('[name=test5]').attr('disabled', 'disabled')
    $('#get_payment').click(function(){
       if($('#'+obj.payment.w).val() == ''){
           $.jGrowl('Не указан вес');
           return false
       }
       var from = $('#hidden_from').val();
       var to   = $('#hidden_to').val();
       var weight = $('#'+obj.payment.w).val();
       var oc = $('#'+obj.payment.oc).val();
       var np = $('#'+obj.payment.np).val();
       $.ajax({
            data: {'from': from, 'to' : to, 'w' : weight, 'oc': oc, 'np': np, 'npR': npR},
            type: 'POST',
            url: obj.payment.url,
            success: function(e){
                $('#put_vladivostok').html(e);
            },
            beforeSend: function(){},
            complete: function(){},
            error: function(){}
       });
    });
    $('body').delegate( '#get_total', 'click', function(){
       var AR = new Array();
       var CH = $('#check:checked').parent().parent().parent();
       if(CH.length == 0) return false;
       var Xabarovsk = $("#Xabarovsk");
       var non_nds = $("#non-nds");

       var nds = $("#nds");

       AR['AI'] = ++obj.total.num;
       AR['CT'] = $('#typeahead').val();
       AR['WE'] = $('#CalcForm_weight').val();
       AR['CF'] = $('#CalcForm_typeahead2').val();
       AR['OD'] = CH.find("td:eq(2)").text();
       AR['TP'] = CH.find("td:eq(3)").text();
       AR['TA'] = CH.find("td:eq(4)").text();
       AR['TE'] = CH.find("td:eq(5)").text();
       
       var text = "<tr>";
       text+= "<td class='sold'>"+AR['AI']+"</td>";
       text+= "<td class='sold'>"+AR['CF']+"</td>";
       text+= "<td class='sold'>"+AR['CT']+"</td>";
       text+= "<td class='sold'>"+AR['OD']+"</td>";
       text+= "<td class='sold'>"+AR['TP']+"</td>";
       text+= "<td class='sold'>"+AR['WE']+"</td>";
       text+= "<td class='sold'>"+AR['TA']+"</td>";
       text+= "<td class='sold'>"+AR['TE']+"</td>";
       text+= "<td class='sold'><p style='cursor:pointer;' class='IL-close'>x</p></td>";
       text+= "</tr>";
       var sum = parseFloat(AR['TA']);

       Xabarovsk.find('tbody').append(text);

       obj.total.sum['non-nds'] = sum+parseFloat(obj.total.sum['non-nds']);
       obj.total.sum['nds']     = sum+parseFloat(obj.total.sum['nds']);
       
       non_nds.find(".itog:first").text(obj.total.sum['non-nds'].toFixed(2) );
       nds.find(".itog:last").text(obj.total.sum['nds'].toFixed(2) );
    });
    $('body').delegate('.IL-close', 'click', function(){

        var del = $(this).parent().parent()
        var min = del.find("td:eq(6)").text();
        var non_nds = $("#non-nds");
        var nds = $("#nds");
        console.log(obj.total.sum['non-nds']);
        obj.total.sum['non-nds']-= min;
        obj.total.sum['nds']-=min;

        non_nds.find(".itog:first").text(obj.total.sum['non-nds'].toFixed(2) );
        nds.find(".itog:last").text(obj.total.sum['nds'].toFixed(2) );
        
        $(this).parent().parent().remove();
    });
    $('#excel').click(function(){
        var t = new Array;
        if($('#Xabarovsk').find('tbody tr').length > 1){
            $('#Xabarovsk').find('tbody tr').each(function(i,elem) {
                if (i == 0){
                }else{
                    var id = $(elem).find('td:eq(0)').text()
                    t[id] = new Array;
                    t[id][1] = $(elem).find('td:eq(1)').text()
                    t[id][2] = $(elem).find('td:eq(2)').text()
                    t[id][3] = $(elem).find('td:eq(3)').text()
                    t[id][4] = $(elem).find('td:eq(4)').text()
                    t[id][5] = $(elem).find('td:eq(5)').text()
                    t[id][6] = $(elem).find('td:eq(6)').text()
                    t[id][7] = $(elem).find('td:eq(7)').text()
                }
            });
        }
        $('#excel').val(JSON.stringify(t));

        return true;
    });
    $('#get_clean').click(function(){
        var Vla = $('#Vladivostok').remove();
        var Button = $('#get_total').remove();
        
       $('#hidden_from').val(' ');
       $('#hidden_to').val(' ');
    });
});