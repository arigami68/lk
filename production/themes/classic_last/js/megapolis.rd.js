rd = {};
rd.setting = {
    cl  : 'Ekaterinburg',
    link: 'rec',
    s : 'ReportForm_date_s',
    f : 'ReportForm_date_f',
    time : 31, // дней
}
rd.Ekaterinburg = function(data){
    var s = $('#'+this.setting.s).val();
    var f = $('#'+this.setting.f).val();
    var time = this.setting.time*3600*24*1000;
    var sD = +new Date(s.replace(/(\d+)-(\d+)-(\d+)/, '$2/$1/$3'));
    var fD = +new Date(f.replace(/(\d+)-(\d+)-(\d+)/, '$2/$1/$3'));
    if(sD+time - fD < 0){
        $('#summary').animate({
            'color': 'white',
            'background-color': "#7832A9"
        }, 1500 );
        $('#summary').animate({
            'color': 'red',
            'background-color': "#FFFFFF"
        }, 1500 );
        return false;
    }
};
$(document).ready(function() {
    var megapolis = rd;

    $.datepicker.setDefaults(
        $.extend($.datepicker.regional['ru'].minDate = null),
        $.extend($.datepicker.regional["ru"])
    );


    if($('.table').text() == ''){
        $('[name=excel]').hide();
    }else if($('.table').find('td:eq(0)').text() == "Нет результатов."){
        $('[name=excel]').hide();
    }

    $("#ReportForm_date_s").datepicker();
    $("#ReportForm_date_f").datepicker();

    $('#Ekaterinburg').click(function(data){return megapolis.Ekaterinburg(data)});


    $('body').delegate( '#'+megapolis.setting.cl, 'click', function(){
        var s = $('#ReportForm_date_s').val() || 0;
        var f = $('#ReportForm_date_f').val() || 0;
        var l = parseInt($('#ReportForm_list').val())+1;
        var sep = '&';
        var link = '?'+'s='+s+sep+'f='+f+sep+'list='+l;
        var myWindow = window.open(megapolis.setting.link+link,
            "Отчет",
            "width=1000,height=600,resizable=yes,scrollbars=yes,status=yes"
        );
    });
});