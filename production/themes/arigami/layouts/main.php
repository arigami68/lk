<?php
    require_once __DIR__.DS.'TbNavbar.php';
    require_once __DIR__.DS.'head.php';
?>

</div>
<header class="header">
    <? require_once __DIR__.DS.'header.php';?>
</header>
<div class="container" id="megapolis-page">
    <div id="megapolis-title">
        <?php print $this->title;?>

    </div>
    <?php echo $content; ?>

    <div class="clear"></div>

    <div class="info">
        <?php
            $text='';
            foreach(Yii::app()->user->getFlashes() as $key => $message) { $text.= "$.jGrowl('".$message."', { sticky: true });";}
            Yii::app()->clientScript->registerScript("success", $text);
        ?>
    </div>
</div>
<div class="footer">
    <?
    if(Yii::app()->user->checkAccess('user')){
	    $autoOpen = (Yii::app()->request->requestUri == '/') ? true : false;
        if(MegapolisRatingWidget::getsee(DBrating::getSeeCriteria(), Yii::app()->user->auth->db->dateCreate, Yii::app()->user->auth->contract)){
		    $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
			    'id' => 'megapolis-rating',
			    'options' => array(
				    'title'     => 'Опрос',
				    'autoOpen'  => $autoOpen,
				    'modal'     => false,
				    'width'     =>'950px',
				    'height'    =>'auto',
				    'resizable' =>'auto',
			    ),
		    ));
		    $this->widget('widget.MegapolisRatingWidget',array(
			    'date'      => Yii::app()->user->auth->db->dateCreate,
			    'contract'  => Yii::app()->user->auth->contract,
			    ));
		    $this->endWidget('zii.widgets.jui.CJuiDialog');
        }
    }

        $this->widget('application.components.widget.QuestionWidget',array(
            'page'  => $this->id,
            'title' => $this->title,
    ));


    ?>
</div>

</body>
</html>