<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="en" />
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <?php
    /*
     * Подключение скриптов css, js;
     * input  : ExtendedClientScript,
     * output : подключение css, js.
     */
    $this->widget('application.components.widget.MegapolisScriptFile',array(
        'cs'   => Yii::app()->clientScript,
        'flag' => $this->action->id,
    ));
    ?>
</head>
<body>