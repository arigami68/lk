<?php
$name = Yii::t("linkmenu", "name:login");
if(Yii::app()->user->checkAccess('user')){
    switch(Yii::app()->user->auth->db->country){
        case 276:
            $array['dep']['data']['dep'] = array(
                'label'=>Yii::t("linkmenu", "departure"), 'url'=>array(Yii::t("linkmenu", "departure:link")),  'items'=>
                    array(
                        array('label'=>Yii::t("linkmenu", "yoursOrders"), 	'url'=>array(Yii::t("linkmenu", "yoursOrders:link"))),
                        array('label'=>Yii::t("linkmenu", "createOrders"), 	'url'=>array(Yii::t("linkmenu", "createOrders:link"))),
                        array('label'=>Yii::t("linkmenu", "depTracking"), 	'url'=>array(Yii::t("linkmenu", "depTracking:link"))),
                        array('label'=>Yii::t("linkmenu", "calcDelivery"), 	'url'=>array(Yii::t("linkmenu", "calcDelivery:link"))),
                        array('label'=>Yii::t("linkmenu", "reportDep"), 	'url'=>array(Yii::t("linkmenu", "reportDep:link"))),
                    )
            );
            break;
        default:
            $array['dep']['data']['dep'] = array(
                'label'=>Yii::t("linkmenu", "departure"), 'url'=>array(Yii::t("linkmenu", "departure:link")),  'items'=>
                    array(
                        array('label'=>Yii::t("linkmenu", "yoursOrders"), 	'url'=>array(Yii::t("linkmenu", "yoursOrders:link"))),
                        array('label'=>Yii::t("linkmenu", "createOrders"), 	'url'=>array(Yii::t("linkmenu", "createOrders:link"))),
                        array('label'=>Yii::t("linkmenu", "depTracking"), 	'url'=>array(Yii::t("linkmenu", "depTracking:link"))),
                        array('label'=>Yii::t("linkmenu", "calcDelivery"), 	'url'=>array(Yii::t("linkmenu", "calcDelivery:link"))),
                        array('label'=>Yii::t("linkmenu", "callCourier"), 	'url'=>array(Yii::t("linkmenu", "callCourier:link"))),
                        array('label'=>Yii::t("linkmenu", "reportDep"), 	'url'=>array(Yii::t("linkmenu", "reportDep:link"))),
                    )
            );
    }
}

$array['dep']['data']['war'] = array('label'=>Yii::t("linkmenu", "warehouse"), 'url'=>array(Yii::t("linkmenu", "warehouse:link")),  'items'=>
    array(
      #  array('label'=>Yii::t("linkmenu", "myWarehouse"), 	'url'=>array(Yii::t("linkmenu", "myWarehouse:link"))),
	    array('label'=>Yii::t("linkmenu", "w1"),  'url'=>array(Yii::t("linkmenu", "warehouseGroup:link"))),
	    array('label'=>Yii::t("linkmenu", "w2"),  'url'=>array(Yii::t("linkmenu", "warehouseGroup:link"))),
	    array('label'=>Yii::t("linkmenu", "warehouseGroupDelivery"), 	'url'=>array(Yii::t("linkmenu", "warehouseGroupDelivery:link"))),
	    array('label'=>Yii::t("linkmenu", "w3"), 	'url'=>array(Yii::t("linkmenu", "warehouseOptions:link"))),

	    array('label'=>Yii::t("linkmenu", "myWarehouseN"),  'url'=>array(Yii::t("linkmenu", "warehouseGroup:link"))),


        array('label'=>Yii::t("linkmenu", "w4"), 		'url'=>array(Yii::t("linkmenu", "warehouseReporting:link"))),
	    array('label'=>Yii::t("linkmenu", "settings"), 	    'url'=>array(Yii::t("linkmenu", "warehouseSettings:link"))),
   #     array('label'=>Yii::t("linkmenu", "reportmyw"), 	'url'=>array(Yii::t("linkmenu", "reportmyw:link"))),
    )
);

$array['dep']['data']['company'] = array('label'=>Yii::t("linkmenu", "company"), 'url'=>array(Yii::t("linkmenu", "company:link")),  'items'=>
    array(
        array('label'=>Yii::t("linkmenu", "details"), 'url'=>array(Yii::t("linkmenu", "details:link"))),
		array('label'=>Yii::t("linkmenu", "sms"), 'url'=>array(Yii::t("linkmenu", "sms:link"))),
        #Yii::app()->user->checkAccess('user') ? array('label'=>Yii::t("linkmenu", "contract"), 'url'=>array(Yii::t("linkmenu", "contract:link"))) : FALSE, # @info Удалил 23.10.2014 перенесли в один раздел
    )
);
$array['dep']['data']['mutual'] = array('label'=>Yii::t("linkmenu", "mutual"), 'url'=>array(Yii::t("linkmenu", "mutual:link")),  'items'=>
    array(
        #  array('label'=>Yii::t("linkmenu", "deposits"), 'url'=>array(Yii::t("linkmenu", "deposits:link"))),
        array('label'=>Yii::t("linkmenu", "np"), 'url'=>array(Yii::t("linkmenu", "np:link"))),
        array('label'=>Yii::t("linkmenu", "documents"), 'url'=>array(Yii::t("linkmenu", "documents:link"))),
    )
);
if(true){ # @info - Ссылка - Регистрация - Если пользователь авторизован, то ему не нужно отображать
    if(true){ # @info -  пользователь не авторизован
        Yii::app()->user->getIsGuest() ? $array['reg']['none'] = array('label'=>Yii::t("linkmenu", "reg"), 'url'=>array(Yii::t("linkmenu", "reg:link"))) : $array['reg']['none'] = '';
    }
    if(true){ # @info -  пользователь авторизован
        !Yii::app()->user->getIsGuest() ? $array['dep']['link'] = $array['dep']['data']['dep'] : $array['dep']['link'] = '';
        !Yii::app()->user->getIsGuest() ? $array['war']['link'] = $array['dep']['data']['war'] : $array['war']['link'] = '';
        #    !Yii::app()->user->getIsGuest() ? $array['company']['link'] = $array['dep']['data']['company'] : $array['company']['link'] = '';
        !Yii::app()->user->getIsGuest() ? $array['mutual']['link'] = $array['dep']['data']['mutual'] : $array['mutual']['link'] = '';

        !Yii::app()->user->getIsGuest() ? $name = Yii::t("linkmenu", "name:exit") : TRUE;
        !Yii::app()->user->getIsGuest() ? $array['api']['link'] = array('label'=>Yii::t("linkmenu", "api"), 'url'=>array(Yii::t("linkmenu", "api:link"))) : $array['api']['link'] = '';
	    if(Yii::app()->user->checkAccess('user')){
			if(MegapolisRatingWidget::getsee(DBrating::getSeeCriteria(), Yii::app()->user->auth->db->dateCreate, Yii::app()->user->auth->contract)){
				$array['rating']['link'] = array('label'=> "Опрос", 'url'=>array('#'), 'linkOptions'=>array('class'=>'megapolis-interview'), 'onclick' => "$(#id).dialog('open'); return false;");
			}else{
				$array['rating']['link'] = '';
			}
	    }
    }
}
if(true){
    if(!Yii::app()->user->getIsGuest()){
        $array['reg']['menu'] = array('label'=>$name, 'url'=>array(Yii::t("linkmenu", "name:link")),  'items'=>
            array(
                array('label'=>Yii::t("linkmenu", "details"), 'url'=>array(Yii::t("linkmenu", "details:link"))),
				array('label'=>Yii::t("linkmenu", "sms"), 'url'=>array(Yii::t("linkmenu", "sms:link"))),
            )
        );
    }else{
        $array['reg']['menu'] = array('label'=> $name, 'url'=>array(Yii::t("linkmenu", "name:link")));
    }
}
ob_start();
if(Yii::app()->user->checkAccess('user')){
    $this->widget('bootstrap.widgets.TbNavbar',array(
        'items'=> array(
            array(
                'class'=>'bootstrap.widgets.TbMenu',
                'items'=>array(
                    $array['reg']['menu'],
                    $array['dep']['link'],
                    $array['war']['link'],
                    # $array['company']['link'],
                    $array['mutual']['link'],
                    $array['reg']['none'],
                    # $array['api']['link'],
					$array['rating']['link']
                ),
            ),
        ),
    ));
}elseif(Yii::app()->user->checkAccess('tmp')){
    $this->widget('bootstrap.widgets.TbNavbar',array(
        'items'=> array(
            array(
                'class'=>'bootstrap.widgets.TbMenu',
                'items'=>array(
                    $array['reg']['menu'],
                    $array['company']['link'],
                ),
            ),
        ),
    ));
}

$TbNavbar = ob_get_contents();
ob_end_clean();