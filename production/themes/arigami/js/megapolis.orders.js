orders = new Object();
orders.id = '.button';
orders.link = '/orders/orders/addDep';
orders.print = new Object();
orders.print.idr = '.printR';
orders.print.link = '/orders/orders/print';

$(document).ready(function() {
    var message = '<div style="font-weight: bold; text-align: center">Попробуйте повторить запрос позже!</div>';
    $('.ui-state-highlight').css("color","#FFFFFF");
    var count = getUrlVars()['count'];
    if(count !== undefined){
        $('#orders-count').val(((count/10)-1));
    }

    $('body').delegate( orders.id, 'click', function(){

        var idr= $(this).parent().parent().find("td:eq(0)").html();
            $.ajax({
                data: {'idr' : idr},
                type: 'GET',
                url: orders.link,
                success: function(e){
                   if(!e){e = message}
                   $('#megapolis').html(e);
                   $("#megapolis").dialog("open")
                   $('#megapolis-page').css('opacity', '0.6');
                },
                beforeSend: function( ) {
                  $("#loadGif").show();
                },
                complete: function(agent) {
                  $("#loadGif").hide();
                }
            });
        return false;
    });
    $(".s_delete").click(function(e){
        var name = $(this).parent().parent().find('td:eq(0)').html();
        if(!confirm(Message.delete_registry+' : '+name)) return false;
    });
    $("body").delegate( '#orders-count', 'change', function(){
        var count = (parseInt($('#orders-count').val())+1)*10;
        location.href='?count='+count;
    });
    $("body").delegate( '.s2_delete', 'click', function(){
        var name = $(this).parent().parent().find('td:eq(2)').text();
        if(!confirm(Message.delete_order+' : '+name)) return false;
    });
    $("body").delegate( '.s_read', 'click', function(){
        var name = $(this).parent().parent().find('td:eq(0)').html();
        if(!confirm(Message.close_registry+' : '+name)) return false;
    });
    $("body").delegate( '.ui-dialog-titlebar-close', 'click', function(){
        $('#megapolis-page').css('opacity', '1');
    });

    $('body').delegate( orders.print.idr, 'click', function(){
        var idre= $(this).parent().parent().find("td:eq(0)").html();
        var myWindow = window.open(orders.print.link+'?registry='+idre,
           "Штрих код",
           "width=420,height=230,resizable=yes,scrollbars=yes,status=yes"
        );
    });

});
