var tracking = new Object();
agent_length = 0;

tracking.addgrid = function(){

    if(typeof agent !== 'undefined') var shift =  Array.isArray(agent) ?  agent.shift() : 0;
    if (shift){
       if(tracking.parentview.find('tbody tr td').text() == 'Нет результатов.') tracking.parentview.find('tbody tr td').remove();
    $.ajax({
        data: {'agent': shift},
        type: "POST",
        url: tracking.url.auto,
        success: function(e){

            if(!e){
                $('#megapolis-loadGif').css('display', 'none');
                agent_length++;
                console.log(shift);
                var html = '<tr ><td >'+agent_length+'/'+(agent.length+agent_length)+'</td><td>'+shift+'</td><td  colspan="8" style="font-weight: bold; text-align: center">Время ожидание превышено. Попробуйте позже!</tr>';
                tracking.parentview.find('tbody').append(html);
            }else{
                $('#megapolis-loadGif').css('display', 'none');
                agent_length++;
                var html = '<tr><td>'+agent_length+'/'+(agent.length+agent_length)+'</td>'+e+'</tr>';
                tracking.parentview.find('tbody').append(html);
            }
        },
        beforeSend: function()      {$('#megapolis-loadGif').css('display', 'block')},
        complete: function(agent)   {
            if(!tracking.stoptrack){
                tracking.addgrid();
            }
        }
    });
   }
   return true;
}
function getName(str){
    if (str.lastIndexOf('\\')){
        var i = str.lastIndexOf('\\')+1;
    }
    else{
        var i = str.lastIndexOf('/')+1;
    }						
    var filename = str.slice(i);			
    var uploaded = document.getElementById("fileformlabel");
    uploaded.innerHTML = filename;
}

function getAgent(agent){
    $.ajax({
        data: {'agent': agent},
        type: "GET",
        url: 'http://megapolis.idea-logic.ru/calc?agent='+agent+'&view=html',
        success: function(e){
            $('#tableSourceShow').html(e);
        },
        beforeSend: function( ) {
            $("#loadGif").show();
        },
        complete: function(agent) {
            $("#loadGif").hide();
        }
    });
}

function pushAgentError(agent){
    $('#tableOperationShow').html('');
    $('#tableAddressShow').html('');
    $('#tableSourceShow').html('');
    $("#tableErrorShow").html('По запросу:: <b>"'+agent+'"</b> , в базе ничего не найденно');
}
function pushAgentSuccess(data, text){
    $("#tableErrorShow").html('');
    var table = tableShow(data);
    
    $("#tableErrorShow").html('По запросу::<b>"'+text+'</b>"');
    $('#tableOperationShow').html(table['operation']);
    $('#tableAddressShow').html(table['address']);
    $('#tableSourceShow').html(table['source']);
}

function tableShow(data){
    var pile = new Array();
    var address = new Array();
    var source = new Array();

    parce = JSON.parse(data);
     if(parce){
        address['zip'] = parce.zip;
        address['address'] = parce.address;         
        
        source['code'] = parce.code;
        source['weight'] = parce.weight;
        source['np'] = parce.np;
        source['oc'] = parce.oc;
     }else{
         return false;
     }
    pile['source'] = tableSourceShow(source);
    pile['address'] = tableAddressShow(address);
    pile['operation'] = tableOperationShow(parce.step);
    return pile;
}
function tableSourceShow(data){
    var pile = new Array();
    
    pile['content']='';
    pile['content']+='<div style="color: #60AFDF"><H3>Данные почтового отправления</H3></div>';
    pile['content']+='<table>';
    pile['content']+='<tr><td>Код оператора доставки: </td><td>'+data.code+'</td></tr>';
    pile['content']+='<tr><td>Вес, кг :</td><td>'+data.weight+'</td></tr>';
    pile['content']+='<tr><td>Сумма наложенного платежа, руб. :</td><td>'+data.np+'</td></tr>';
    pile['content']+='<tr><td>Сумма объявленной ценности, руб. :</td><td>'+data.oc+'</td></tr>';
    pile['content']+='</table>';
    return pile['content'];
    
}
function tableAddressShow(data){
    var pile = new Array();
    pile['content']='';
    pile['content']+='<div style="color: #60AFDF"><H3>Адрес получения</H3></div>';
    pile['content']+='<table>';
    pile['content']+='<tr><td>Индекс :</td><td>'+data.zip+'</td></tr>';
    pile['content']+='<tr><td>Адрес доставки :</td><td>'+data.address+'</td></tr>';
    pile['content']+='</table>';
    
    return pile['content'];
    
}
function tableOperationShow(data){   
    var pile = new Array();
    
    pile['content'] = '';
    pile['content']+='<div style="color: #60AFDF"><H3>Операции над почтовым отправлением</H3></div>';
    pile['content']+= '<table border=1 style="font-family: arial; font-size: 14px">';
    pile['content']+= '<tr style="background-color: rgb(153, 153, 153);color: rgb(255, 255, 255); text-align: center"><td>Дата и время операции</td><td>Индекс</td><td>Место операции</td><td>Операция</td><td>Статус</td></tr>';
    for(var a=0;a<data.length;a++){
        var fulldate = formatDate(new Date(data[a].date*1000));
        pile['content']+='<tr class=\'odd\'><td>'+fulldate+'</td><td>'+data[a].zip+'</td><td>'+data[a].address+'</td><td>'+data[a].operation+'</td><td>'+data[a].status+'</td></tr>';
    }
    pile['content']+= '</table>';
    return pile['content'];
}
function showRadioOrder(){
    $('#check_manual').click(function(e){
        document.cookie="trL=0;";
        $('#manual').hide();
        $('#automatic').show();
    });
    $('#check_automatic').click(function(e){
        document.cookie="trL=1;";
        $('#manual').show();
        $('#automatic').hide();
    });
    if(get_cookie('trL') == 1){
        $('#manual').show();
        $('#automatic').hide();
    }
}
function showManual(){
    $('#check_manual').click(function(e){
        $("#loadGifManual").show();
    });
    
}

function get_cookie(cookie_name)
{
  var results = document.cookie.match ( '(^|;) ?' + cookie_name + '=([^;]*)(;|$)' );
  if ( results )
    return ( unescape ( results[2] ) );
  else
    return null;
}
function formatDate(date) {
  
  var yy = date.getFullYear();
  var dd = date.getDate();
  if ( dd < 10 ) dd = '0' + dd;
  var mm = date.getMonth()+1;
  if ( mm < 10 ) mm = '0' + mm;
  var hh = date.getHours();
  if ( hh < 10 ) hh = '0' + hh;
  var mimi = date.getMinutes();
  if ( mimi < 10 ) mimi = '0' + mimi;
  var ss = date.getSeconds();
  if ( ss < 10 ) ss = '0' + ss;
  return dd+'.'+mm+'.'+yy+' '+hh+':'+mimi+':'+ss;
}

$(document).ready(function(){
    $('#excel').click(function(){
        var t = new Array;
        var item =  $('.items tbody tr');
        if(item.length > 1){
            item.each(function(i,elem) {
                var id = i;
                t[id] = new Array;
                t[id][1] = $(elem).find('td:eq(0)').text()
                t[id][2] = $(elem).find('td:eq(1)').text()
                t[id][3] = $(elem).find('td:eq(2)').text()
                t[id][4] = $(elem).find('td:eq(3)').text()
                t[id][5] = $(elem).find('td:eq(4)').text()
                t[id][6] = $(elem).find('td:eq(5)').text()
                t[id][7] = $(elem).find('td:eq(6)').text()
                t[id][8] = $(elem).find('td:eq(7)').text()
                t[id][9] = $(elem).find('td:eq(8)').text()
                t[id][10] = $(elem).find('td:eq(9)').text()
            });
        }
        $('#excel').val(JSON.stringify(t));
        return true;
    });

    $('input[type=file]').on('change', function(){
        getName($(this).val());
    });
    
    tracking.parentview = $("#parentView");
    tracking.url = { auto : 'tracking/tracking/auto'};
    tracking.stoptrack = 0;
    tracking.addgrid();
});
