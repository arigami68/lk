
$(document).ready(function(){
    $(".save").click(function(){
       console.log('save');
    });
    $(".addNomencl").click(function(){
        var text = $('.tt-input').typeahead('val');
        if(text){
            var choose = $(".choose").val();
            $(".nomencl").append("<li group="+choose+">"+text+"</li>");
        }
    });
    $("#group").click(function(){
        $( "#dialog" ).dialog({
            closeText: "",
            minWidth: 250,
            minHeight: 200
        });
    });

    var callback = function(){
        return $.ajax({
            data: {
                json: JSON.stringify({
                    operation   : 'read',
                    article     : $('.tt-input').typeahead('val'),
                })
            },
            dataType:"json",
            type: "POST",
            crossDomain: true,
            url: 'request?flag=warehouse',

            success: function($json){
                console.log($json)
                   return $json;
            }
        });

        return ['fse', 'Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California',
            'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii',
            'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana',
            'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota',
            'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire',
            'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota',
            'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island',
            'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont',
            'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'
        ];
    }
    var substringMatcher = function(strs) {
        return function findMatches(q, cb) {

            var matches, substringRegex;
            matches = [];
            substrRegex = new RegExp(q, 'i');
            $.each(strs, function(i, str) {
                if (substrRegex.test(str)) {
                    matches.push(str);
                }
            });

            cb(matches);
        };
    };
    var bestPictures = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,

        remote: {
            url: 'request?flag=warehouse&operation=read&article=%QUERY',
        }
    });

    $('#the-basics .typeahead').typeahead(null, {
        name: 'best-pictures',
        display: 'value',
        source: bestPictures
    });
    /*
    $('#the-basics .typeahead').typeahead({
            hint: true,
            highlight: true,
            minLength: 1,
        },
        {
            name: 'states',
            source: substringMatcher(callback())
        }
    );
    */
    $('.treeview-group li').click(function(){
        var id = $(this).attr('id');

        $.ajax({
            data: {
                json: JSON.stringify({
                    operation   : 'read',
                    id     : id,
                })
            },
            dataType:"json",
            type: "POST",
            crossDomain: true,
            url: 'request?flag=warehousegroup',
            success: function($json){
                console.log($json);
                if($json){
                    $(".items > tbody").empty();

                    for(key in $json ){
                        var article = $json[key].article;

                        $(".items tbody").append(
                            '<tr>'       +
                            '<td>-</td>' +
                            '<td><a href=/mywN?operation=read&art='+article+'>'+article+'</a></td>' +
                            '<td>-</td>' +
                            '<td>-</td>' +
                            '<td>-</td>' +
                            '</tr>'
                        );

                    }
                }
            }
        });
        return false;
    });
});