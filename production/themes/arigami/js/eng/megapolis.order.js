MEG.address = {
    len : 1,
    limit: 10,
    region : new Array,
    area : new Array,
    city : new Array()
};
MEG.def = {
    error : new Array(),
    flagOD : {}
};
MEG.settings = {
    wd       : [], // Ограничение по весу
    op_type  : [],  // Вид отправления
    tab      : {
        'OD'          : 'agent_id',
        'TYPE'        : 'op_type',
        'CATEGORY'    : 'op_category',
        'TAKE'        : 'op_take',
        'FIO'         : 'fio',
        'PHONE'       : 'phone',
        'CITY'        : 'city',
        'REGION'      : 'region',
        'STREET'      : 'street',
        'HOUSE'       : 'house',
        'PUBLIC'      : 'cost_public',
        'DELIVERY'    : 'cost_delivery',
        'WD'          : 'wd',
        'ZIP'         : 'zip',
        'AddRegistry' : 'addRegister'
    },

    wd : {
        'EMS'            : [31.5],
        'PR'             : [2, 2.5, 10, 20, 20], // Бандероль  Бандероль 1-го Посылка Стандартная Нестандартная Тяжеловесная
        'DPD'            : [30, 31.5],
        'IDEA'           : [32],
        'PR_LOWER'       : [0.1,0,0,0,10], // Бандероль  Бандероль 1-го Посылка Стандартная Нестандартная Тяжеловесная
        'MAXIPOST'       : [20, 30],
        'MAXIPOST_LOWER' : [0, 20]
    },
    type : {
        'EMS'                : 1,
        'Otpravlenie'        : 2,
        'Pismo'              : 3,
        'Karta'              : 4,
        'Pismo_1_class'      : 5,
        'Banderol'           : 6,
        'Banderol_1_class'   : 7,
        'Posilka'            : 8,
        'Posilka_standart'   : 9,
        'Posilka_nestandart' : 10,
        'Posilka_tya'        : 11,
        'Posilka_tya_krypn'  : 12,
        'IDEA'               : 14,
        'Dpd_consumer'       : 15,
        'Dpd_classic_parcel' : 16,
        'Dpd_express'        : 17,
        'Dpd_classic'        : 18,
        'Maxipost_gab'       : 19,
        'Maxipost_kryp'      : 20,
        'IDEA_pvz'           : 21,

    },
    url1     : '/calc/calc/find'
};


// При добавлении/обновлении нового отправления выполняет дефолтные функции
MEG.def.flag1 = 300; // номера которые присваиваются отправлениям
MEG.def.proccess = function(data){

    var obj = $(data);
    var OD = obj.find('.'+MEG.settings.tab.OD);
    var CATEGORY = obj.find('.'+MEG.settings.tab.CATEGORY);
    var TYPE = obj.find('.'+MEG.settings.tab.TYPE);
    var PUBLIC = obj.find('.'+MEG.settings.tab.PUBLIC);
    var DELIVERY = obj.find('.'+MEG.settings.tab.DELIVERY);
    // Убираем DPD

    //$('.agent_id').find('option').append( $('<option value="4">four</option>'));

    if(typeof MEG.def.flagOD[OD.attr('id')] == 'undefined'){
        if(!$(OD).val() === 0){
            $(OD).find('option').remove();
            $.each(MEG.settings.od, function(key, value) {
                $(OD).append( $('<option value='+key+'>'+value+'</option>'));
            });
            MEG.def.flagOD[OD.attr('id')] = 1;
        }
        ($(OD).find('option:eq(3)').html() === "DPD") ? $(OD).find('option:eq(3)').remove() : null ;

    }
    // Оператор доставки, влияние на выбор Вида отправления и Категории отправления
    if(OD.val() == 0){

        CATEGORY.parent().hide();
        TYPE.parent().hide();
    }else{
        CATEGORY.parent().show();
        TYPE.parent().show();
    }
    if(CATEGORY.val() == 2){ // PROSTOE
        DELIVERY
            .removeClass('error')
            .attr('readonly', 'readonly')
            .val(0)
            .parent().find('.help-inline').first().remove();
        PUBLIC
            .removeClass('error')
            .attr('readonly', 'readonly')
            .val(0)
            .parent().find('.help-inline').first().remove();
    }else if(CATEGORY.val() == 4){ // OC
        DELIVERY
            .removeClass('error')
            .attr('readonly', 'readonly')
            .val(0)
            .parent().find('.help-inline').first().remove();
    }

};
MEG.def.idea = function(){
    $.ajax({
        type: 'POST',
        dataType:'json',
        url: this.link1,
        success: function(data){
            return data;
        },
        beforeSend: function(){},
        complete: function(){},
        error: function(){}
    });
};
MEG.def.check = function(data, flag){
    var obj = $(data);
    var mandatory = [
        'OD',
        'FIO',
        'PHONE',
        'CITY',
        'STREET',
        'HOUSE',
        'PUBLIC',
        'DELIVERY',
        'WD',
        'ZIP'
    ];
    var err = {
        'OD'       : 'Не указан оператор доставки',
        'FIO'      : 'Не указано Ф.И.О',
        'PHONE'    : 'Не указан Телефонный номер',
        'ZIP'      : 'Не указан Индекс. Пожалуйста выберите город из списка ниже',
        'HOUSE'    : 'Не указана улица, либо дом ',
        'DELIVERY' : 'Не заполнен наложенный платеж',
        'PUBLIC'   : 'Не заполнена объявленная ценность',
        'WD'       : 'Не заполнен вес отправления'
    };

    var array =  new Array, error = new Array;
    for(var a in mandatory){
        element = mandatory[a];
        var flr = false, flr_cost = false, flr_other = false;
        switch(MEG.settings.tab[element]){
            case MEG.settings.tab.PUBLIC:
            case MEG.settings.tab.DELIVERY:
                if($(obj).find('.'+MEG.settings.tab[element]).parent().find('.help-inline').length){
                    flr_cost = true;
                }
                break;
            case MEG.settings.tab.WD:
                if(obj.find('.'+MEG.settings.tab[element]).val() === "") flr = true;
                break;
            default:
                if(obj.find('.'+MEG.settings.tab[element]).val() == 0) flr = true;
                break;
        }
        if(flr_cost){
            array.push(MEG.settings.tab[element]);
            error.push(MEG.settings.name[element]+' '+MEG.settings.messages[7]);
        }
        if(flr){
            array.push(MEG.settings.tab[element]);
            error.push(MEG.settings.name[element]+' '+MEG.settings.messages[7]);
            if(typeof err[element] != "undefined"){
                if(!$(obj).find('.'+MEG.settings.tab[element]).hasClass('error')){
                    $(obj).find('.'+MEG.settings.tab[element]).parent().append('<span class="help-inline error">'+err[element]+'</span>')
                }
            }
            obj.find('.'+MEG.settings.tab[element]).addClass('error');
        }else{
            if(typeof err[element] != "undefined"){
                if($(obj).find('.'+MEG.settings.tab[element]).hasClass('error')){
                    $(obj).find('.'+MEG.settings.tab[element]).parent().find('.help-inline').first().remove();
                }
            }
            obj.find('.'+MEG.settings.tab[element]).removeClass('error');
        }
    };
    if(flag){
        if(error.length){
            if((mark = $('#'+megapolis.yw).find('.active')).length){
                mark.addClass('error');
                mark.find('a').css('background', '#FF8A65');
            }
        }else{
            var id = $(data).parent().find('.active').attr('id');
            if((mark = $('#'+megapolis.yw).find('.active')).length){
                mark.removeClass('error');
                mark.find('a').css('background', '#AED581');
            }
        }
    }
    return error;
};

function form(event){
    var e = event.target;
    var param = new Object();
    for(var prop in MEG.settings.tab) { param[prop] = $(this).find("."+MEG.settings.tab[prop]);}
    MEG.def.check(this, 1); // Вызываем дефолтные значения
    if(true){
        switch(event.type){
            case "keyup":
                if($(e).hasClass(MEG.settings.tab.DELIVERY)){cost_delivery();
                }else if($(e).hasClass( MEG.settings.tab.PUBLIC)){ cost_public();}
                break;
            case "change":
                if($(e).hasClass( MEG.settings.tab.CATEGORY)){ op_category();}
                break;
        }
    }
    function cost_delivery(){ // НП
        if(param.DELIVERY.val() > 50000){
            $.jGrowl(MEG.settings.messages[5]);
            param.DELIVERY.val(50000);
        };

        if(param.DELIVERY.val() <= 0){
            //param.DELIVERY.val(1);
            $.jGrowl(MEG.settings.messages[13]);
            if(param.DELIVERY.parent().find('.help-inline').length){
                param.DELIVERY.parent().find('.help-inline').first().text(MEG.settings.messages[13]);
            }else{
                param.DELIVERY.parent().append('<span class="help-inline error">'+MEG.settings.messages[13]+'</span>');
            }
        }else{
            param.DELIVERY.parent().find('.help-inline').first().remove();
        };

        if(param.CATEGORY.val() == 3){
            if(parseInt(param.PUBLIC.val()) < parseInt(param.DELIVERY.val())){
                $.jGrowl(MEG.settings.messages[6]);param.DELIVERY.val(parseInt(param.PUBLIC.val()));
                //if(!param.PUBLIC.parent().find('.help-inline').length){ param.PUBLIC.parent().append('<span class="help-inline error">'+MEG.settings.messages[6]+'</span>')};
            }
        }
        if(param.DELIVERY.val() >= 50000){
            if(!param.DELIVERY.parent().find('.help-inline').length){
                //param.DELIVERY.parent().append('<span class="help-inline error">'+MEG.settings.messages[5]+'</span>');
            };
        }
    }
    function cost_public(){ // ОЦ
        if(param.PUBLIC.val() > 0)    { param.PUBLIC.removeClass('error'); };
        if(param.PUBLIC.val() > 50000){ $.jGrowl(MEG.settings.messages[5]);param.PUBLIC.val(50000); };
        if(param.PUBLIC.val() <= 0){
            //param.PUBLIC.val(1);
            $.jGrowl(MEG.settings.messages[14]);

            if(param.PUBLIC.parent().find('.help-inline').length){
                param.PUBLIC.parent().find('.help-inline').first().text(MEG.settings.messages[14]);
            }else{
                param.PUBLIC.parent().append('<span class="help-inline error">'+MEG.settings.messages[14]+'</span>');
            }

        }else{
            param.PUBLIC.parent().find('.help-inline').first().remove();
        };
        if(param.PUBLIC.val() >= 50000){
            if(!param.PUBLIC.parent().find('.help-inline').length){
                //param.PUBLIC.parent().append('<span class="help-inline error">'+MEG.settings.messages[5]+'</span>');
            };
        }
        if(param.CATEGORY.val() == 3) cost_delivery();
    }

    function op_category(){// Категория отправления
        if(param.CATEGORY.val() == 2){ // Простое
            param.DELIVERY
                .attr('readonly', 'readonly')
                .val(0)
                .parent().find('.help-inline').first().remove();
            param.PUBLIC
                .attr('readonly', 'readonly')
                .val(0)
                .parent().find('.help-inline').first().remove();
        }else if(param.CATEGORY.val() == 4){ // С ОЦ
            param.DELIVERY
                .attr('readonly', 'readonly')
                .val(0)
                .parent().find('.help-inline').first().remove();
            param.PUBLIC
                .removeAttr('readonly', 'readonly')
                .val('');

            if(!param.PUBLIC.parent().find('span').hasClass('help-inline')){
                param.PUBLIC.parent().append('<span class="help-inline error">'+MEG.settings.messages[11]+'</span>');
            }


        }else if(param.CATEGORY.val() == 3){ // С НП

            if(!param.DELIVERY.parent().find('span').hasClass('help-inline')){
                param.DELIVERY.parent().append('<span class="help-inline error">'+MEG.settings.messages[10]+'</span>');
            }
            if(!param.PUBLIC.parent().find('span').hasClass('help-inline')){
                param.PUBLIC.parent().append('<span class="help-inline error">'+MEG.settings.messages[11]+'</span>');
            }

            param.DELIVERY
                .removeAttr('readonly', 'readonly')
                .val('')
            param.PUBLIC
                .removeAttr('readonly', 'readonly')
                .val('')
        }
    };

    var array = new Array(
        'agent_id',
        'op_category',   // Категория отправления
        'cost_delivery', // НП
        'cost_public',   // ОЦ
        'zip',           // Индекс
        'region',        // Регион
        'area',          //
        'city',
        'op_type',        // Вид отправления
        'wd'              // Вес отправления
    ); // Убрать, использовать MEG

    var hide = new Array();
    p = this;
    var o = $(e);

    if(o.hasClass(MEG.settings.tab.OD)){
        if(event.type == 'click'){
            agent_id(o,p);
        }
        var op_category = $(p).find("."+array[1]).trigger('click');
        wd(o,p);
    }else if(o.hasClass(array[9])){
        wd(o,p);
    }else if(o.hasClass(MEG.settings.tab.TYPE)){
        wd(o,p);
    }


    (function init(){
        var o = $(p).find("."+array[0]);
        agent_id(o,p, 1);
    }());

    function wd(o, p){
        if(event.type){

            var op_type = $(p).find("."+array[8]);
            var agent_id = $(p).find("."+array[0]);
            var wd = $(p).find("."+MEG.settings.tab.WD);
            if(true){
                if(op_type.val() == MEG.settings.type['EMS']){                      wd_help(wd, MEG.settings.wd['EMS'][0], MEG.settings.messages[3]+MEG.settings.wd['EMS'][0]+'кг')}
                else if(op_type.val() == MEG.settings.type['Banderol']){            wd_help(wd, MEG.settings.wd['PR'][0], MEG.settings.messages[3]+MEG.settings.wd['PR'][0]+'кг')}
                else if(op_type.val() == MEG.settings.type['Banderol_1_class']){    wd_help(wd, MEG.settings.wd['PR'][1], MEG.settings.messages[3]+MEG.settings.wd['PR'][1]+'кг')}
                else if(op_type.val() == MEG.settings.type['Posilka']){             wd_help(wd, MEG.settings.wd['PR'][2], MEG.settings.messages[3]+MEG.settings.wd['PR'][2]+'кг')}
                else if(op_type.val() == MEG.settings.type['Posilka_standart']){    wd_help(wd, MEG.settings.wd['PR'][2], MEG.settings.messages[3]+MEG.settings.wd['PR'][2]+'кг')}
                else if(op_type.val() == MEG.settings.type['Posilka_nestandart']){  wd_help(wd, MEG.settings.wd['PR'][3], MEG.settings.messages[3]+MEG.settings.wd['PR'][3]+'кг')}
                else if(op_type.val() == MEG.settings.type['Posilka_tya']){         wd_help(wd, MEG.settings.wd['PR'][4], MEG.settings.messages[3]+MEG.settings.wd['PR'][4]+'кг')}
                else if(op_type.val() == MEG.settings.type['Dpd_consumer']){        wd_help(wd, MEG.settings.wd['DPD'][0], MEG.settings.messages[3]+MEG.settings.wd['DPD'][0]+'кг')}
                else if(op_type.val() == MEG.settings.type['Dpd_classic_parcel']){  wd_help(wd, MEG.settings.wd['DPD'][1], MEG.settings.messages[3]+MEG.settings.wd['DPD'][1]+'кг')}
                else if(op_type.val() == MEG.settings.type['Dpd_express']){         wd_help(wd, MEG.settings.wd['DPD'][1], MEG.settings.messages[3]+MEG.settings.wd['DPD'][1]+'кг')}
                else if(op_type.val() == MEG.settings.type['Dpd_classic']){         wd_help(wd, MEG.settings.wd['DPD'][1], MEG.settings.messages[3]+MEG.settings.wd['DPD'][1]+'кг')}
                else if(op_type.val() == MEG.settings.type['IDEA']){                wd_help(wd, MEG.settings.wd['IDEA'][0], MEG.settings.messages[3]+MEG.settings.wd['IDEA'][0]+'кг')}
                else if(op_type.val() == MEG.settings.type['Maxipost_gab']){        wd_help(wd, MEG.settings.wd['MAXIPOST'][0], MEG.settings.messages[3]+MEG.settings.wd['MAXIPOST'][0]+'кг')}
                else if(op_type.val() == MEG.settings.type['Maxipost_kryp']){       wd_help(wd, MEG.settings.wd['MAXIPOST'][1], MEG.settings.messages[3]+MEG.settings.wd['MAXIPOST'][0]+'кг')}
                else if(op_type.val() == MEG.settings.type['IDEA_pvz']){            wd_help(wd, MEG.settings.wd['IDEA'][0], MEG.settings.messages[3]+MEG.settings.wd['IDEA'][0]+'кг')}

                if(op_type.val() == MEG.settings.type['Banderol']){ wd_help_lower(wd, MEG.settings.wd['PR_LOWER'][0], MEG.settings.messages[4]+MEG.settings.wd['PR_LOWER'][0]+'кг')}
                else if(op_type.val() == MEG.settings.type['Posilka_tya']){ wd_help_lower(wd, MEG.settings.wd['PR_LOWER'][4], MEG.settings.messages[4]+MEG.settings.wd['PR_LOWER'][4]+'кг')}
                else if(op_type.val() == MEG.settings.type['Maxipost_kryp']){ wd_help_lower(wd, MEG.settings.wd['MAXIPOST_LOWER'][1], MEG.settings.messages[4]+MEG.settings.wd['MAXIPOST_LOWER'][1]+'кг')}
            }
        }
    }
    function wd_help(wd, val2, message){

        if(wd.val() > val2){ wd.val(val2); $.jGrowl(message);}
    };
    function wd_help_lower(wd, val2, message){ if(wd.val() < val2){ wd.val(val2); $.jGrowl(message);} };
    function agent_id(o, p, f1){

        var op_type = $(p).find("."+array[8]);
        var op_category = $(p).find("."+array[1]);
        var wd = $(p).find("."+array[9]);

        var checkType = $(op_type).find('option:selected').val();
        var checkCategory = $(op_category).find('option:selected').val();
        var agent_id = o.val();

        if(agent_id == 0){
            wd.attr('readonly', 'readonly');
            hide[2] = $(p).find('.controls:eq(2)').parent().css('display', 'none');
            hide[3] = $(p).find('.controls:eq(3)').parent().css('display', 'none');
        }else{
            wd.removeAttr('readonly', 'readonly');
            hide[2] = $(p).find('.controls:eq(2)').parent().css('display', 'block');
            hide[3] = $(p).find('.controls:eq(3)').parent().css('display', 'block');
        }


        if(agent_id == 1){ // EMS
            $(op_type).find('option').remove();
            $(op_type).append( $('<option value=1>'+MEG.settings.type_name[1]+'</option>'));


            $(op_category).find('option').each(function(el, k){
                var val = $(k).val();
                if((val != 2) && (val != 3) && (val != 4)){
                    if(val == checkCategory) $(op_category).find('option:eq(1)').attr('selected', true);
                    $(k).hide();
                }else{
                    $(k).show();
                }
            });
        }else if(agent_id == 2){ // Почта
            if(checkType < 6 || checkType > 11){
                $(op_type).find('option').remove();
                $(op_type).append( $('<option value=7>'+MEG.settings.type_name[7]+'</option>'));
                $(op_type).append( $('<option value=8>'+MEG.settings.type_name[8]+'</option>'));
                $(op_type).append( $('<option value=9>'+MEG.settings.type_name[9]+'</option>'));
                $(op_type).append( $('<option value=10>'+MEG.settings.type_name[10]+'</option>'));
                $(op_type).append( $('<option value=11>'+MEG.settings.type_name[11]+'</option>'));
            }
            $(op_type).find('option').each(function(el, k){
                var val = $(k).val();
                if((val != 6) && (val != 7) && (val != 8) && (val != 9) && (val != 10) && (val != 11)){
                    if(val == checkType){
                        $(op_type).find('option:eq(5)').attr('selected', true);
                    }
                    $(k).hide();
                }else{
                    $(k).show();
                }
            });

            $(op_category).find('option').each(function(el, k){
                var val = $(k).val();
                if((val != 2) && (val != 3) && (val != 4)){
                    if(val == checkCategory) $(op_category).find('option:eq(1)').attr('selected', true);
                    $(k).hide();
                }else{
                    $(k).show();
                }
            });

        }else if(agent_id == 3){
            $(op_type).find('option').each(function(el, k){
                var val = $(k).val();
                if((val != 15) && (val != 16) && (val != 17) && (val != 18)){
                    if(val == checkType){
                        $(op_type).find('option:eq(13)').attr('selected', true);
                    }
                    $(k).hide();
                }else{
                    $(k).show();
                }
            });

            $(op_category).find('option').each(function(el, k){
                var val = $(k).val();
                if((val != 3) && (val != 4)){
                    if(val == checkCategory) $(op_category).find('option:eq(2)').attr('selected', true);
                    $(k).hide();
                }else{
                    $(k).show();
                }
            });
        }else if(agent_id == 4){ // ИдеаЛоджик
            if(checkType != 14 && checkType != 21){
                $(op_type).find('option').remove();
                $(op_type).append( $('<option value=14>'+MEG.settings.type_name[14]+'</option>'));
                $(op_type).append( $('<option value=21>'+MEG.settings.type_name[21]+'</option>'));
            }
            $(op_category).find('option').each(function(el, k){
                var val = $(k).val();
                if((val != 2) && (val != 3) && (val != 4)){
                    if(val == checkCategory) $(op_category).find('option:eq(2)').attr('selected', true);
                    $(k).hide();
                }else{
                    $(k).show();
                }
            });
        }else if(agent_id == 5){ // Максипост
            console.log(checkType);
            if(checkType < 19 || checkType > 20){
                $(op_type).find('option').remove();
                $(op_type).append( $('<option value=19>'+MEG.settings.type_name[19]+'</option>'));
                $(op_type).append( $('<option value=20>'+MEG.settings.type_name[20]+'</option>'));
            }
            $(op_category).find('option').each(function(el, k){
                var val = $(k).val();
                if((val != 2) && (val != 3) && (val != 4)){
                    if(val == checkCategory) $(op_category).find('option:eq(2)').attr('selected', true);
                    $(k).hide();
                }else{
                    $(k).show();
                }
            });
        }
        MEG.def.proccess(p);
    };
}(MEG);


function getName(str){
    if (str.lastIndexOf('\\')){
        var i = str.lastIndexOf('\\')+1;
    }
    else{
        var i = str.lastIndexOf('/')+1;
    }
    var filename = str.slice(i);
    var uploaded = document.getElementById("fileformlabel");
    uploaded.innerHTML = filename;
}

function Idea(agents){
    if(this.agents == 'undefined') return false;
    this.agents = agents;
    this.element = new Array();
    this.name = 'OrderLoad_num_';
    this.zakaz = 'zakaz';
    this.num_count = 'num_count';
    this.count = 0;
    this.yw = 'yw1';

    this.createElement = function(text){
        if(this.agents == 'undefined') return false;
        this.element.push(text);
    },
        this.deleteElement = function(num){
            if(this.agents == 'undefined') return false;
            var element = this.element;
            this.element.forEach(function(el, k){
                if(num == el){
                    element.splice(k, 1);
                }
            });
        },
        this.placement = function(){
            if(this.agents == 'undefined') return false;
            var agents = this.agents;
            var name = this.name;
            var zakaz = this.zakaz;
            var arr = new Array;
            this.element.forEach(function(el, k){
                var auto = k;
                var flag2 = false;
                var num  = agents[auto];
                document.getElementById(name+el).value = num;
                document.getElementById(zakaz+el).text = num;
            });
            var val = this.element.length+this.count;
            document.getElementById(this.num_count).value = val;
        },
        this.castling = function(){
            if(this.agents == 'undefined') return false;
            var create = this;

            $('#'+this.yw+' li').each(function(i){
                if(i !== 0){
                    create.createElement(i);
                }
            });
            create.placement();
        }
    this.castlingExc = function(){
        if(this.agents == 'undefined') return false;
        var create = this;
        var zakaz = this.zakaz;
        var name = this.name;
        $('#'+this.yw+' li').each(function(i){
            if(i !== 0){
                var val = document.getElementById(name+i).value;
                document.getElementById(zakaz+i).text = val;
            }
        });
        this.count = parseInt(document.getElementById(this.num_count).value);
    }
};

var order={};
order.register={};
order.register.id='#addRegister';
order.register.vara = new Array();
order.link = 'order/order/addRegister';
order.deletea = { name : '.delete'};

$(document).ready(function(){
    if(typeof idea !=="undefined"){
        megapolis = new Idea(window.idea);
        if(typeof scenario == undefined){
            megapolis.castling();
        }else{
            if(scenario == 'update'){
                megapolis.castlingExc();
            }
        }
    }else{
        megapolis = new Idea(undefined);
    }

    var vars_idea = getUrlVars()['idea'];

    if((typeof vars_idea) != "undefined"){
        $('#'+megapolis.yw).find('li').each(function(k, el){
            if($(el).children().text() == vars_idea){
                $(el).addClass('active');
                $($(el).children().attr('href')).removeClass('fade')
                $($(el).children().attr('href')).addClass('active');

            }
        });
    }

    $('body').delegate('.tab-content', 'keyup', function(){ MEG.def.check($(this).find('.active'), 1);});
    $('body').delegate('.tab-content', 'click', function(){ MEG.def.check($(this).find('.active'), 1);});

    $( "body" ).delegate( ".tab-pane", "click", form);
    $( "body" ).delegate( ".tab-pane", "change", form);
    $( "body" ).delegate( ".tab-pane", "keyup", form);
    $( "body" ).delegate( ".tab-pane", "keypress", form);
    //$( "body" ).delegate( ".tab-pane", "mouseover", form);

    $("#OrderLoad_date_dep").datepicker();

    $('input[type=file]').on('change', function(){
        getName($(this).val());
    });

    if(true){
        $.jGrowl.defaults.closerTemplate = '<div>Закрыть все уведомления</div>';

        $('body').delegate(order.deletea.name, 'click', function(){
            var el = $(this).find('a');
            var num = $('#'+el.attr('class')).attr('num');
            megapolis.deleteElement(num);
            megapolis.placement();
            $('#'+el.attr('class')).parent().remove();
            $(this).parent().parent().parent().parent().parent().remove();
            return false;
        });
    }

    $('body').delegate( order.register.id, 'click', function(){
        order.register.vara['id'] = $(this).parent().parent().parent().attr('id');
        order.register.vara['ul'] = $('#'+order.register.vara['id']).find('ul');
        order.register.vara['li'] = order.register.vara['ul'].find('li');
        order.register.vara['len'] = order.register.vara['li'].length;
        order.register.vara['len'] = ++MEG.def.flag1;
        order.register.vara['tab'] = $('#'+order.register.vara['id']).find('.tab-content:first');
        order.register.vara['len1'] = (order.register.vara['len']+1);
        order.register.vara['len16'] = (order.register.vara['len']+16);

        $.ajax({
            data: {'id': order.register.vara['len'], 'len16' : order.register.vara['len16']},
            type: 'POST',
            url: order.link,
            success: function(data){
                if(data){
                    order.register.vara['tab'].append(data);
                    var t= order.register.vara['tab'].find("[num ="+$(data).attr('num')+"]")
                    t.addClass('active');
                    MEG.def.proccess(t);
                }else{
                    $.jGrowl(MEG.settings.messages[9]);
                }
            },
            beforeSend: function(){},
            complete: function(){},
            error: function(){}
        });


        var ln = megapolis.createElement(order.register.vara['len16']) == false ? ('Отправление №'+(order.register.vara['len']-MEG.def.flag1+1)) : '';

        $('.tab-content').find('.active').removeClass('active');
        $('.nav-tabs').find('.active').removeClass('active');

        $(order.register.vara['ul']).append('<li class="active"><a num='+order.register.vara['len16']+' id=zakaz'+order.register.vara['len16']+' data-toggle=tab  href=#'+order.register.vara['id']+'_tab_'+order.register.vara['len16']+'>'+ln+'</a></li>');

        var ln = (typeof megapolis != "undefined") ? setTimeout(function(){megapolis.placement()}, 500) : false;

        return false;
    });

    // Сброс полей
    $( "body" ).delegate( ".reset", "click", function(f){
        var array = [
            'region',
            'area',
            'city',
            'zip'
        ];
        var num = $(this).attr('name');
        var pre = '#OrderLoad_';
        for(var a=0; a< array.length; a++){
            $(pre+array[a]+'_'+num).val('');
        }

        return false;
    });

    $("#save").click(function(){
        var yw = megapolis.yw;
        if($('#'+yw+' li').length <= 1){
            $.jGrowl(MEG.settings.messages[2]);
            return false;}
    });
    $("#OrderLoad_volume").keypress(function(f){ return putNumber(f);});

    $( "body" ).delegate( ".cost_public", "keypress", function(f){ return putNumber(f);});
    $( "body" ).delegate( ".cost_delivery", "keypress", function(f){ return putNumber(f);});
    $( "body" ).delegate( ".wd", "keypress", function(f){ return putNumber(f);});
    $( "body" ).delegate( ".num", "keyup", function(){
        if($(this).val().length){
            var val = $(this).val();
            var num = $(this).parent().parent().parent().attr('num');
            $('#zakaz'+num).text(val);
        };
    });

    $('#'+MEG.settings.tab.AddRegistry).click(function(){
        var tab, mark, flag = 0;
        var err = false;
        var tab = $(this).parent().parent().find('li');
        pane = $(this).parent().last();

        tab.each(function(k, value){
            if($(value).hasClass('error') && !err) err = true;
        });
        if(err){
            $.jGrowl(MEG.settings.messages[12]);
            return false;
        }else{

            if((tab = $('.tab-content').find('.tab-pane').last()).length){
                if((err = MEG.def.check(tab, flag)).length){
                    if((mark = pane.parent().find('li').last()).length){
                        mark.addClass('error');
                        mark.find('a').css('background', '#FF8A65');
                        $.jGrowl(err.join(',<br>'), { life: 10000 });
                        return false;
                    }
                }else{
                    if((mark = pane.parent().find('.active')).length){
                        mark.find('a').css('background', '#AED581');
                    }
                }
            }
        }
    });
    $('#save').click(function(){
        var mark, err, flag = 1, pane = $('#addRegister').parent().last(), tab = $('#addRegister').parent().parent().find('li');
        tab.each(function(k, value){
            if($(value).hasClass('error') && !err) err = true;
        });
        if(err){
            $.jGrowl(MEG.settings.messages[12]);
            return false;
        }else{
            if((tab = $('.tab-content').find('.tab-pane').last()).length){ MEG.def.check(tab, flag);}
        }
    });
    $("body").delegate( "."+MEG.settings.tab.TAKE, "click", function(data){ ($(this).val() == 2) ? $('.'+MEG.settings.address).replaceWith(MEG.settings.text1) : $('.'+MEG.settings.address).replaceWith(MEG.settings.text2);});
    if(getUrlVars()['operation'] == 'create'){
        $('#check_automatic').click(function(){document.cookie="trL=0;";$('#manual').hide();$('#automatic').show();});
        $('#check_manual').click(function(){document.cookie="trL=1;";$('#manual').show();$('#automatic').hide();});
        if(get_cookie('trL') == 0){$('#check_manual').removeClass('active');$('#check_automatic').addClass('active');$('#automatic').show();$('#manual').hide();
        }else{ $('#automatic').hide();$('#manual').show();}
    }
    $('.tab-pane').each(function(el, k){ MEG.def.proccess(k);});
    $.datepicker.setDefaults( $.extend($.datepicker.regional[MEG.main.lang]));
    $("body").delegate( "."+MEG.settings.tab.PHONE, "click", function(f){ $("."+MEG.settings.tab.PHONE).mask("+7 (999) 999-9999");});
    $("body").delegate( '#OrderLoad_address_dep', "keyup", function(f){ $(this).val() ? $(this).removeClass('error') : $(this).addClass('error')});
});