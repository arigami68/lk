var NoSmoke = "Нет результатов.";
var filter = function(){
    if($(".items tbody tr").first().text() == NoSmoke) $(".items tbody").empty();
    return true;
};
var template = function(obj){
    var name    = obj.name;
    var article = obj.article;
    var unit    = obj.unit;
    var rate    = obj.rate;
    var len     = $(".items tbody tr").length + 1;

    var result =
    '<tr>'       +
        '<td>'+len+'</td>' +
        '<td>'+name+'</td>' +
        '<td><a href=/mywN?operation=read&art='+article+'>'+article+'</a></td>' +
        '<td>'+unit+'</td>' +
        '<td><input class="span1" type="text"/></td>' +
        '<td><input class="span1" type="text"/></td>' +
        '<td>-</td>' +
        '<td>'+rate+'</td>' +
        '<td>-</td>' +
    '</tr>';

    return result;
};
$(document).ready(function(){
    $('.add').click(function(){
        var article = $(".choose").find('option:selected').text();
        $.ajax({
            data: {
                json: JSON.stringify({
                    operation   : 'list',
                    article     : article,
                })
            },
            dataType:"json",
            type: "POST",
            crossDomain: true,
            url: 'request?flag=delivery',
            success: function($json){
                if($json){
                    filter();
                    for(var k in $json ){

                        var obj = {
                            article : $json[k].article,
                            name    : $json[k].name,
                            unit    : $json[k].unit,
                            rate    : $json[k].rate,

                        }
                        $(".items tbody").append(template(obj));
                    }
                }


            }
        });

        return false;
    })
});