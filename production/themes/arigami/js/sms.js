function tr(s){
    s = s.replace(/а/g, 'a');
    s = s.replace(/б/g, 'b');
    s = s.replace(/в/g, 'v');
    s = s.replace(/г/g, 'g');
    s = s.replace(/д/g, 'd');
    s = s.replace(/е/g, 'e');
    s = s.replace(/ё/g, 'e');
    s = s.replace(/ж/g, 'zh');
    s = s.replace(/з/g, 'z');
    s = s.replace(/и/g, 'i');
    s = s.replace(/й/g, 'y');
    s = s.replace(/к/g, 'k');
    s = s.replace(/л/g, 'l');
    s = s.replace(/м/g, 'm');
    s = s.replace(/н/g, 'n');
    s = s.replace(/о/g, 'o');
    s = s.replace(/п/g, 'p');
    s = s.replace(/р/g, 'r');
    s = s.replace(/с/g, 's');
    s = s.replace(/т/g, 't');
    s = s.replace(/у/g, 'u');
    s = s.replace(/ф/g, 'f');
    s = s.replace(/х/g, 'kh');
    s = s.replace(/ц/g, 'ts');
    s = s.replace(/ч/g, 'ch');
    s = s.replace(/ш/g, 'sh');
    s = s.replace(/щ/g, 'sch');
    s = s.replace(/ы/g, 'y');
    s = s.replace(/ь/g, "'");
    s = s.replace(/ъ/g, "'");
    s = s.replace(/э/g, 'e');
    s = s.replace(/ю/g, 'yu');
    s = s.replace(/я/g, 'ya');
    s = s.replace(/А/g, 'A');
    s = s.replace(/Б/g, 'B');
    s = s.replace(/В/g, 'V');
    s = s.replace(/Г/g, 'G');
    s = s.replace(/Д/g, 'D');
    s = s.replace(/Е/g, 'E');
    s = s.replace(/Ё/g, 'E');
    s = s.replace(/Ж/g, 'Zh');
    s = s.replace(/З/g, 'Z');
    s = s.replace(/И/g, 'I');
    s = s.replace(/Й/g, 'Y');
    s = s.replace(/К/g, 'K');
    s = s.replace(/Л/g, 'L');
    s = s.replace(/М/g, 'M');
    s = s.replace(/Н/g, 'N');
    s = s.replace(/О/g, 'O');
    s = s.replace(/П/g, 'P');
    s = s.replace(/Р/g, 'R');
    s = s.replace(/С/g, 'S');
    s = s.replace(/Т/g, 'T');
    s = s.replace(/У/g, 'U');
    s = s.replace(/Ф/g, 'F');
    s = s.replace(/Х/g, 'Kh');
    s = s.replace(/Ц/g, 'Ts');
    s = s.replace(/Ч/g, 'Ch');
    s = s.replace(/Ш/g, 'Sh');
    s = s.replace(/Щ/g, 'Sch');
    s = s.replace(/Ы/g, 'Y');
    s = s.replace(/Ь/g, "'");
    s = s.replace(/Ъ/g, "'");
    s = s.replace(/Э/g, 'E');
    s = s.replace(/Ю/g, 'Yu');
    s = s.replace(/Я/g, 'Ya');
    return s;
}
// GSM
var GSM = new Array();
GSM[0x20] = 1;
GSM[0x21] = 1;
GSM[0x22] = 1;
GSM[0x23] = 1;
GSM[0x24] = 1;
GSM[0x25] = 1;
GSM[0x26] = 1;
GSM[0x27] = 1;
GSM[0x28] = 1;
GSM[0x29] = 1;
GSM[0x2A] = 1;
GSM[0x2B] = 1;
GSM[0x2C] = 1;
GSM[0x2D] = 1;
GSM[0x2E] = 1;
GSM[0x2F] = 1;
GSM[0x30] = 1;
GSM[0x31] = 1;
GSM[0x32] = 1;
GSM[0x33] = 1;
GSM[0x34] = 1;
GSM[0x35] = 1;
GSM[0x36] = 1;
GSM[0x37] = 1;
GSM[0x38] = 1;
GSM[0x39] = 1;
GSM[0x3A] = 1;
GSM[0x3B] = 1;
GSM[0x3C] = 1;
GSM[0x3D] = 1;
GSM[0x3E] = 1;
GSM[0x3F] = 1;
GSM[0x40] = 1;
GSM[0x41] = 1;
GSM[0x42] = 1;
GSM[0x43] = 1;
GSM[0x44] = 1;
GSM[0x45] = 1;
GSM[0x46] = 1;
GSM[0x47] = 1;
GSM[0x48] = 1;
GSM[0x49] = 1;
GSM[0x4A] = 1;
GSM[0x4B] = 1;
GSM[0x4C] = 1;
GSM[0x4D] = 1;
GSM[0x4E] = 1;
GSM[0x4F] = 1;
GSM[0x50] = 1;
GSM[0x51] = 1;
GSM[0x52] = 1;
GSM[0x53] = 1;
GSM[0x54] = 1;
GSM[0x55] = 1;
GSM[0x56] = 1;
GSM[0x57] = 1;
GSM[0x58] = 1;
GSM[0x59] = 1;
GSM[0x5A] = 1;
GSM[0x5B] = 2;
GSM[0x5C] = 2;
GSM[0x5D] = 2;
GSM[0x5E] = 2;
GSM[0x5F] = 1;
GSM[0x61] = 1;
GSM[0x62] = 1;
GSM[0x63] = 1;
GSM[0x64] = 1;
GSM[0x65] = 1;
GSM[0x66] = 1;
GSM[0x67] = 1;
GSM[0x68] = 1;
GSM[0x69] = 1;
GSM[0x6A] = 1;
GSM[0x6B] = 1;
GSM[0x6C] = 1;
GSM[0x6D] = 1;
GSM[0x6E] = 1;
GSM[0x6F] = 1;
GSM[0x70] = 1;
GSM[0x71] = 1;
GSM[0x72] = 1;
GSM[0x73] = 1;
GSM[0x74] = 1;
GSM[0x75] = 1;
GSM[0x76] = 1;
GSM[0x77] = 1;
GSM[0x78] = 1;
GSM[0x79] = 1;
GSM[0x7A] = 1;
GSM[0x7B] = 2;
GSM[0x7C] = 2;
GSM[0x7D] = 2;
GSM[0x7E] = 2;
GSM[0xA] = 1;
GSM[0xC] = 2;
GSM[0xC2A1] = 1;
GSM[0xC2A3] = 1;
GSM[0xC2A4] = 1;
GSM[0xC2A5] = 1;
GSM[0xC2A7] = 1;
GSM[0xC2BF] = 1;
GSM[0xC384] = 1;
GSM[0xC385] = 1;
GSM[0xC386] = 1;
GSM[0xC387] = 1;
GSM[0xC389] = 1;
GSM[0xC391] = 1;
GSM[0xC396] = 1;
GSM[0xC398] = 1;
GSM[0xC39C] = 1;
GSM[0xC39F] = 1;
GSM[0xC3A0] = 1;
GSM[0xC3A4] = 1;
GSM[0xC3A5] = 1;
GSM[0xC3A6] = 1;
GSM[0xC3A8] = 1;
GSM[0xC3A9] = 1;
GSM[0xC3AC] = 1;
GSM[0xC3B1] = 1;
GSM[0xC3B2] = 1;
GSM[0xC3B6] = 1;
GSM[0xC3B8] = 1;
GSM[0xC3B9] = 1;
GSM[0xC3BC] = 1;
GSM[0xCE93] = 1;
GSM[0xCE94] = 1;
GSM[0xCE98] = 1;
GSM[0xCE9B] = 1;
GSM[0xCE9E] = 1;
GSM[0xCEA0] = 1;
GSM[0xCEA3] = 1;
GSM[0xCEA6] = 1;
GSM[0xCEA8] = 1;
GSM[0xCEA9] = 1;
GSM[0xD] = 1;
GSM[0xE282AC] = 2;
GSM[916] = 1;
GSM[934] = 1;
GSM[915] = 1;
GSM[161] = 1; // ?
GSM[191] = 1; // ?
GSM[163] = 1; // ?
GSM[165] = 1; // ?
GSM[232] = 1; // e
GSM[923] = 1; // ?
GSM[164] = 1; // В¤
GSM[233] = 1; // e
GSM[937] = 1; // ?
GSM[249] = 1; // u
GSM[928] = 1; // ?
GSM[236] = 1; // i
GSM[936] = 1; // ?
GSM[242] = 1; // o
GSM[931] = 1; // ?
GSM[199] = 1; // C
GSM[920] = 1; // ?
GSM[926] = 1; // ?
GSM[216] = 1; // O
GSM[196] = 1; // A
GSM[228] = 1; // a
GSM[248] = 1; // o
GSM[198] = 1; // ?
GSM[214] = 1; // O
GSM[246] = 1; // o
GSM[230] = 1; // ?
GSM[209] = 1; // N
GSM[241] = 1; // n
GSM[197] = 1; // A
GSM[223] = 1; // ?
GSM[220] = 1; // U
GSM[252] = 1; // u
GSM[229] = 1; // a
GSM[201] = 1; // E
GSM[167] = 1; // В§
GSM[224] = 1; // a
GSM[8364] = 2; // в‚¬

////// ISO
var ISO = new Array();
ISO[0x20] = 1;
ISO[0x21] = 1;
ISO[0x22] = 1;
ISO[0x23] = 1;
ISO[0x24] = 1;
ISO[0x25] = 1;
ISO[0x26] = 1;
ISO[0x27] = 1;
ISO[0x28] = 1;
ISO[0x29] = 1;
ISO[0x2A] = 1;
ISO[0x2B] = 1;
ISO[0x2C] = 1;
ISO[0x2D] = 1;
ISO[0x2E] = 1;
ISO[0x2F] = 1;
ISO[0x30] = 1;
ISO[0x31] = 1;
ISO[0x32] = 1;
ISO[0x33] = 1;
ISO[0x34] = 1;
ISO[0x35] = 1;
ISO[0x36] = 1;
ISO[0x37] = 1;
ISO[0x38] = 1;
ISO[0x39] = 1;
ISO[0x3A] = 1;
ISO[0x3B] = 1;
ISO[0x3C] = 1;
ISO[0x3D] = 1;
ISO[0x3E] = 1;
ISO[0x3F] = 1;
ISO[0x40] = 1;
ISO[0x41] = 1;
ISO[0x42] = 1;
ISO[0x43] = 1;
ISO[0x44] = 1;
ISO[0x45] = 1;
ISO[0x46] = 1;
ISO[0x47] = 1;
ISO[0x48] = 1;
ISO[0x49] = 1;
ISO[0x4A] = 1;
ISO[0x4B] = 1;
ISO[0x4C] = 1;
ISO[0x4D] = 1;
ISO[0x4E] = 1;
ISO[0x4F] = 1;
ISO[0x50] = 1;
ISO[0x51] = 1;
ISO[0x52] = 1;
ISO[0x53] = 1;
ISO[0x54] = 1;
ISO[0x55] = 1;
ISO[0x56] = 1;
ISO[0x57] = 1;
ISO[0x58] = 1;
ISO[0x59] = 1;
ISO[0x5A] = 1;
ISO[0x5B] = 1;
ISO[0x5C] = 1;
ISO[0x5D] = 1;
ISO[0x5E] = 1;
ISO[0x5F] = 1;
ISO[0x60] = 1;
ISO[0x61] = 1;
ISO[0x62] = 1;
ISO[0x63] = 1;
ISO[0x64] = 1;
ISO[0x65] = 1;
ISO[0x66] = 1;
ISO[0x67] = 1;
ISO[0x68] = 1;
ISO[0x69] = 1;
ISO[0x6A] = 1;
ISO[0x6B] = 1;
ISO[0x6C] = 1;
ISO[0x6D] = 1;
ISO[0x6E] = 1;
ISO[0x6F] = 1;
ISO[0x70] = 1;
ISO[0x71] = 1;
ISO[0x72] = 1;
ISO[0x73] = 1;
ISO[0x74] = 1;
ISO[0x75] = 1;
ISO[0x76] = 1;
ISO[0x77] = 1;
ISO[0x78] = 1;
ISO[0x79] = 1;
ISO[0x7A] = 1;
ISO[0x7B] = 1;
ISO[0x7C] = 1;
ISO[0x7D] = 1;
ISO[0x7E] = 1;
ISO[0xA0] = 1;
ISO[0xA1] = 1;
ISO[0xA2] = 1;
ISO[0xA3] = 1;
ISO[0xA4] = 1;
ISO[0xA5] = 1;
ISO[0xA6] = 1;
ISO[0xA7] = 1;
ISO[0xA8] = 1;
ISO[0xA9] = 1;
ISO[0xAA] = 1;
ISO[0xAB] = 1;
ISO[0xAC] = 1;
ISO[0xAD] = 1;
ISO[0xAE] = 1;
ISO[0xAF] = 1;
ISO[0xB0] = 1;
ISO[0xB1] = 1;
ISO[0xB2] = 1;
ISO[0xB3] = 1;
ISO[0xB4] = 1;
ISO[0xB5] = 1;
ISO[0xB6] = 1;
ISO[0xB7] = 1;
ISO[0xB8] = 1;
ISO[0xB9] = 1;
ISO[0xBA] = 1;
ISO[0xBB] = 1;
ISO[0xBC] = 1;
ISO[0xBD] = 1;
ISO[0xBE] = 1;
ISO[0xBF] = 1;
ISO[0xC0] = 1;
ISO[0xC1] = 1;
ISO[0xC2] = 1;
ISO[0xC3] = 1;
ISO[0xC4] = 1;
ISO[0xC5] = 1;
ISO[0xC6] = 1;
ISO[0xC7] = 1;
ISO[0xC8] = 1;
ISO[0xC9] = 1;
ISO[0xCA] = 1;
ISO[0xCB] = 1;
ISO[0xCC] = 1;
ISO[0xCD] = 1;
ISO[0xCE] = 1;
ISO[0xCF] = 1;
ISO[0xD0] = 1;
ISO[0xD1] = 1;
ISO[0xD2] = 1;
ISO[0xD3] = 1;
ISO[0xD4] = 1;
ISO[0xD5] = 1;
ISO[0xD6] = 1;
ISO[0xD7] = 1;
ISO[0xD8] = 1;
ISO[0xD9] = 1;
ISO[0xDA] = 1;
ISO[0xDB] = 1;
ISO[0xDC] = 1;
ISO[0xDD] = 1;
ISO[0xDE] = 1;
ISO[0xDF] = 1;
ISO[0xE0] = 1;
ISO[0xE1] = 1;
ISO[0xE2] = 1;
ISO[0xE3] = 1;
ISO[0xE4] = 1;
ISO[0xE5] = 1;
ISO[0xE6] = 1;
ISO[0xE7] = 1;
ISO[0xE8] = 1;
ISO[0xE9] = 1;
ISO[0xEA] = 1;
ISO[0xEB] = 1;
ISO[0xEC] = 1;
ISO[0xED] = 1;
ISO[0xEE] = 1;
ISO[0xEF] = 1;
ISO[0xF0] = 1;
ISO[0xF1] = 1;
ISO[0xF2] = 1;
ISO[0xF3] = 1;
ISO[0xF4] = 1;
ISO[0xF5] = 1;
ISO[0xF6] = 1;
ISO[0xF7] = 1;
ISO[0xF8] = 1;
ISO[0xF9] = 1;
ISO[0xFA] = 1;
ISO[0xFB] = 1;
ISO[0xFC] = 1;
ISO[0xFD] = 1;
ISO[0xFE] = 1;
ISO[0xFF] = 1;

language = {};
language["TRANSLITERATION_COMPLETED"] = 'Успешный перевод';
language["BEFORE_TRANSLITERATION_NEED_WRITE"] = 'Введите пожалуйста текст';
language["ENTER_SMS"] = 'Введите текст Вашей СМС';
language["TEMPLATE_SAVE"]= 'Успешное сохранение шаблона';
language["TEMPLATE_LOAD"]= 'Шаблон загружен';

function set_val(doc, val, class_text, type) {
    if (!doc.value) {
        doc.value = val;
        doc.className = class_text + ' prompt';
    }
}

function reset_val(doc, val, class_text, type) {
    doc.className = class_text;
    if (doc.value == val) {
        doc.value = '';
    }
}

function set_len_sms(set_all_sms) {
    var letter = document.getElementById('letters_sms');
    var text = document.getElementById('text_sms');
    var num = document.getElementById('num_sms');
    if (!num) num = new Object;

    var len = 0;
    if(!text) return false;

    if (text.value == language["ENTER_SMS"]) {
        num.innerHTML = 0;
    } else {
        if (check_latin(text.value)) {
            len = len_char_sms(text.value);
            if (len > 160)
                num.innerHTML = Math.ceil(len / 153);
            else
                num.innerHTML = 1;
        } else {
            len = text.value.length;
            if (len != 0) {
                if (len > 70)
                    num.innerHTML = Math.ceil(len / 67);
                else
                    num.innerHTML = 1;
            } else
                num.innerHTML = 0;
        }
    }

    letter.innerHTML = len;
}

function check_latin(s) {
    var len_text = s.length;
    var len = 0;
    var k;

    for (i = 0; i < len_text; i++) {
        k = s.charCodeAt(i);
        if (GSM[k] === undefined && ISO[k] === undefined)
            return false;
    }
    return true;
}

function len_char_sms(s) {
    var len_text = s.length;
    var len_GSM = 0,
        len_ISO = 0;

    for (i = 0; i < len_text; i++) {
        k = s.charCodeAt(i);
        if (GSM[k] !== undefined)
            len_GSM += GSM[k];
        if (ISO[k] !== undefined)
            len_ISO += ISO[k];
    }
    if (len_GSM > len_ISO)
        return len_GSM;
    else
        return len_ISO;
}
function translit_sms() {
    var text = document.getElementById('text_sms');
    if (text.value && text.value != language["ENTER_SMS"]) {
        text.value = tr(text.value);
        set_len_sms(1);
        alert(language["TRANSLITERATION_COMPLETED"]);
    } else {
        alert(language["BEFORE_TRANSLITERATION_NEED_WRITE"]);
    }
}
function set_value(text, val) {
    text = document.getElementById(text);
    text.focus();
    text.value += val;
}
function limitation(obj){
    var status = [2, 12, 34],
        statusLock = 0;

    status.forEach(function(name, value){
        if(obj.status == name){
            $('#input_num_text').val(0);
            statusLock = 1;
            return true;
        }
    });

    $('#input_num_text').prop('readonly', statusLock);


}
function save_template(id, flag){
    var obj = getParam().val;

    obj.id  = id;
    obj.flag= flag;

    if (obj.text_sms.value == language["ENTER_SMS"] && flag==2) { alert(language["ENTER_SMS"]);return false;}
    var json = JSON.stringify(obj);

    var message = $('#message');
    limitation(obj);
    $.ajax({
        data: {json: json},
        type: 'POST',
        dataType: 'json',
        url: 'request/request/request?flag=sms',
        success: function(e){
            if(!e) return false;

            if(flag == 2){
                var cl = "alert alert-success";
                var text = language["TEMPLATE_SAVE"];
            }else if(flag == 3){
                if(!e.text){
                    var e = {
                        text : language["ENTER_SMS"],
                        time : {
                            fromHH : 0,
                            fromMM : 0,
                            toHH   : 0,
                            toMM   : 0,
                        }
                    };
                    setParam(e);
                    set_len_sms(0);
                    return false;
                }
                setParam(e);
                var cl = "alert alert-info";
                var text = language["TEMPLATE_LOAD"];
                set_len_sms(e.text.length);

            }

            animate_template(message, cl, text);
        }
    });
    return false;
}
function animate_template(message, cl, text){
    message.removeClass();
    message.text(text);
    message.fadeIn(1000);
    message.addClass(cl);
    setTimeout(function(){message.fadeOut(3000);},5000);
}
function getParam(){
    var obj = {};
    var key = obj.key = {
        od      : $('#listname_od'),
        status  : $('#listname_status'),
        sms_num : $('#input_num_text'),
        check_time  : $('#check_time'),
        text_sms    : $('#text_sms'),
        fromHH      : $('#fromHH'),
        fromMM      : $('#fromMM'),
        toHH        : $('#toHH'),
        toMM        : $('#toMM')
    };
    obj.val = {
        od        : key.od.val(),
        status    : key.status.val(),
        sms_num   : key.sms_num.val(),
        check_time: key.check_time.checked ? 1 : 0,
        text_sms  : key.text_sms.val(),
        from      : parseInt(key.fromHH.val())*3600+parseInt(key.fromMM.val()*60),
        to        : parseInt(key.toHH.val())*3600+parseInt(key.toMM.val()*60),
    };
    return obj;
}

function setParam(data){
    var obj = getParam().key;

    obj.text_sms.val(data.text);

    obj.fromHH.val(data.time.fromHH);
    obj.fromMM.val(data.time.fromMM);
    obj.toHH.val(data.time.toHH);
    obj.toMM.val(data.time.toMM);

}
window.onload = function(){
    set_len_sms();
};