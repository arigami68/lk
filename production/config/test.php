<?php

return CMap::mergeArray(
	require(dirname(__FILE__).'/megapolis.php'),
	array(
                'preload' => array(
                    'log',
                ),
		'components'=>array(
                    'log'=>array(
                        'class'=>'CLogRouter',
                        'routes'=>array(
                            array(
                                
                                'class'=>'ext.yii-debug-toolbar.YiiDebugToolbarRoute',
                                #'ipFilters'=>array('127.0.0.1'),
                                #'ipFilters'=>array('127.0.0.1', '192.168.10.103'),
                            ),
                        ),
                    ),
                    'db'=>array( # Optimus
                        'class'=>'CDbConnection',
                        'connectionString' => 'mysql:host=192.168.10.31;dbname=megapolis',
                        'emulatePrepare' => true,
                        'username' => 'root',
                        'password' => 'root',
                        'charset' => 'utf8',
                        'enableProfiling'=>true,
                        'enableParamLogging'=>true,
                    ),
                    
		),
	)
);
