<?php

return CMap::mergeArray(
	require(dirname(__FILE__).'/megapolis.php'),
	array(
                'preload' => array(
                    #'log',
                ),
		'components'=>array(
                    'log'=>array(
                        'class'=>'CLogRouter',
                        'routes'=>array(
                            array(
                                'class'=>'extensions.yii-debug-toolbar.YiiDebugToolbarRoute',
                                #'ipFilters'=>array('87.118.208.248'),
                            ),
                            array(
                                'class'=>'CFileLogRoute',
                                'levels'=>'info', // эти ещё и в файл
                            ),
                        ),
                    ),
                    
                    'db'=>array( # Optimus
                        'class'=>'CDbConnection',
                        'connectionString' => 'mysql:host=192.168.10.3;dbname=megapolis',
                        'emulatePrepare' => true,
                        'username' => 'root',
                        'password' => '9PdHDDvNPRufBhqhhmEX',
                        'charset' => 'utf8',
                        'enableProfiling'=>true,
                        'enableParamLogging'=>true,
                    ),
		),
	)
);