<?php

require_once CONFIG_PATH.DS.'default.php';
Yii::setPathOfAlias('app', DATA);

return array(
    'basePath'          => APP_PATH,
    'name'              => 'ИдеаЛоджик',
    'defaultController' => 'login/login/index',
    'theme'             => 'arigami',#'classic_last',
    'import'            => array(
		'app.db.*',
	    'app.components.abstract.*',
	    'app.components.*',
        'application.components.*',
        'application.components.db.*',
        'application.components.widget.*',
        'application.components.activeRecordBehaviors.*',
        'application.components.abstract.*',
        'application.components.component.*',
        'application.components.helpers.*',
        'application.controllers.*',
        'application.components.behaviors.*',
        'application.components.interface.*',
    ),
    'language'          =>'ru',
    'modules'           => $modules,
    'components'        => array(
        'request'=>array(
            'csrfCookie' =>array(
                'httpOnly' =>true,
            ),
        ),
        'email'         =>array(
            'class'   =>'extensions.email.Email',
            'delivery'=>'php',
        ),
        'CjsTree'         =>array(
	        'class'   => 'extensions.jsTree.CjsTree',
        ),

        'clientScript'  =>array(
            'class'=>'extensions.ExtendedClientScript.ExtendedClientScript',
            /*
            'scriptMap' => array(
                'jquery.js' => '//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js',
                'jquery.min.js' => '//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js',
                'jquery-ui.min.js' => '//ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js',
            ),
            */

            'combineCss'=>false,
            'compressCss'=>false,
            'combineJs'=>false,
            'compressJs'=>false,
        ),

        'session' => array (
            //'savePath' => SESSION_PATH,
            'timeout'=> 3600*24*10,
        ),
        'authManager' => array(
            'class' => 'PhpAuthManager',
        ),
        'bootstrap'=>array(
            'class'=>'application.components.bootstrap.components.Bootstrap',
        ),
        'user'=>array(
            'class' => 'WebUser',
            'loginUrl'=> 'login',
            'authTimeout' => 3600*24*10,
            'guestName' => 'guest',
        ),
        'errorHandler'=>array(
            'errorAction'=>'error/error/error',
        ),
        'themeManager' => array(
            'basePath' => APP_PATH,
        ),
        'urlManager'=>array(
            'urlFormat'=>'path',
            'rules'=> $rules,
            'showScriptName'=>false,
        ),

    ),
);
