<?php

ini_set('display_errors', 1);
error_reporting(E_ALL);

#define('YII_ENABLE_ERROR_HANDLER', false);
/*
define('YII_ENABLE_EXCEPTION_HANDLER', false);
*/
require __DIR__ . '/production/config/constants.php';
$yii=dirname(__FILE__).'/framework/yii.php';

$config=dirname(__FILE__).'/production/config/work.php';
require_once($yii);
defined('YII_DEBUG') or define('YII_DEBUG',false);

Yii::createWebApplication($config)->run();